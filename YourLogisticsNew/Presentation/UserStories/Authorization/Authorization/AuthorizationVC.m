//
//  AuthorizationVC.m
//  YourLogistics
//
//  Created by Aleksandr on 30.05.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import "AuthorizationVC.h"
#import "APIService.h"
#import <SDVersion/SDiOSVersion.h>
#import "OrderMessage.h"
#import "FinanceMessage.h"
#import "TextMessage.h"
#import "NewsRead.h"
#import <MagicalRecord/MagicalRecord.h>
#import "ColorPallet.h"
#import "NSUserDefaults+DSExtension.h"
//#import <FirebaseMessaging.h>

@import Firebase;


@interface AuthorizationVC ()

@property (weak, nonatomic) IBOutlet ShadowButton *loginButton;

@property (weak, nonatomic) IBOutlet ClientCodeInputView *clientCodeInputView;
@property (weak, nonatomic) IBOutlet UIButton *clientCodeButton;

@property (weak, nonatomic) IBOutlet PinCodeInputView *pinCodeInputView;
@property (weak, nonatomic) IBOutlet UIButton *pinCodeButton;

@property (weak, nonatomic) IBOutlet UITextField *countryCodeTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumberTextField;

@property (weak, nonatomic) IBOutlet UIButton *servicezAndCompanies;

@property (strong,nonatomic) UITapGestureRecognizer *panRecognizer;

@property (strong,nonatomic) UIVisualEffectView *visualEffectView;

@property (strong,nonatomic) UIView *dimingView;

@end


@implementation AuthorizationVC
    
- (void)viewDidLoad {
    [super viewDidLoad];
    self.loginButton.backgroundColor = [ColorPallet new].baseBlue;
    self.servicezAndCompanies.backgroundColor = [ColorPallet new].baseBlue;
    self.servicezAndCompanies.titleLabel.textColor = UIColor.whiteColor;
    [self registerForKeyboardNotifications];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard:)];
    self.panRecognizer = tap;
    self.panRecognizer.delegate = self;
    [self.view addGestureRecognizer:tap];
    
#if DEBUG
    BOOL isDriver = NO;
    
    if (isDriver) {
        self.clientCodeInputView.code = @"02768";
        self.pinCodeInputView.code = @"7825";
        self.phoneNumberTextField.text = @"0954929815";
        self.countryCodeTextField.text = @"38";
    } else {
        self.clientCodeInputView.code = @"01235";
        self.pinCodeInputView.code = @"2910";
        self.phoneNumberTextField.text = @"0930944570";
        self.countryCodeTextField.text = @"38";
    }
#endif
    
    @weakify(self);
    [self.loginButton didPressWithCompletion:^{
        @strongify(self);
        [self loginButtonTapped];
    }];
}
    
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}
    
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    //    [self.navigationController setNavigationBarHidden:NO animated:YES];
}
    
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (![self isNotificationEnabled]) {
        //        [self showOpenSettingsRequest];
    }
    if (![self isBackgroundAppRefreshEnabled]) {
        [self showOpenSettingsForBackgroundRefresh];
    }
}
    
- (void)loginButtonTapped {
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"YourLogisticsGCMPushToken"] == nil) {
        NSString *fcmToken = [FIRMessaging messaging].FCMToken;
        NSLog(@"FCM registration token: %@", fcmToken);
        [[NSUserDefaults standardUserDefaults] setObject:fcmToken forKey:kPushTokenKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    [self addLoadingActivityView];

    @weakify(self);
    [[APIService sharedAPIClient] loginWithClientCode:self.clientCodeInputView.code
                                          phoneNumber:self.phoneNumber
                                              pinCode:self.pinCodeInputView.code
                                           completion:^(NSString *clientType) {
                                               @strongify(self);
                                               [self loginHandler:clientType];
                                           } failure:^(NSError *error) {
                                               @strongify(self);
                                               [self loginFailureHandler:error];
                                           }];
}

- (NSString *)phoneNumber {
    NSString *phoneNumber = [self.phoneNumberTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (phoneNumber.length == 9 && self.countryCodeTextField.text.length == 3) {
        phoneNumber = [@"0" stringByAppendingString: phoneNumber];
    }
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud saveEntryPhoneNumber:phoneNumber];
    return phoneNumber;
}


- (void)loginFailureHandler:(NSError *)error {
    [self removeLoadingActivityView];
    [self hideKeyboard:nil];

    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[error isKindOfClass:[NSString class]] ? [NSString stringWithFormat:@"%@", error] : [error localizedDescription]
                                                                             message:nil
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alertController addAction:defaultAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
    //                                                      [[[[UIApplication sharedApplication].delegate window] rootViewController] presentViewController:alertController animated:YES completion:nil];
}

- (void)loginHandler:(NSString *)clientType {
    [self removeLoadingActivityView];
    
    NSString *phoneNumber = self.phoneNumber;
    
    NSUserDefaults *UD = [NSUserDefaults standardUserDefaults];
    NSString *lastPhoneNumber = [UD objectForKey:kLastClientPhoneNumberKey];
    NSString *lastClientCodeKey = [UD objectForKey:kLastClientCodeKey];
    
    if (![lastPhoneNumber isEqualToString:phoneNumber] || ![self.clientCodeInputView.code isEqualToString:lastClientCodeKey]) {
        [OrderMessage MR_deleteAllMatchingPredicate:nil];
        [FinanceMessage MR_deleteAllMatchingPredicate:nil];
        [TextMessage MR_deleteAllMatchingPredicate:nil];
        [NewsRead MR_deleteAllMatchingPredicate:nil];
        
        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
    }
    
    [UD setObject:clientType forKey:kClientTypeKey];
    [UD setObject:self.clientCodeInputView.code forKey:kClientCodeKey];
    [UD setObject:phoneNumber forKey:kClientPhoneNumberKey];
    [UD setObject:self.clientCodeInputView.code forKey:kLastClientCodeKey];
    [UD setObject:phoneNumber forKey:kLastClientPhoneNumberKey];
    [UD synchronize];
    
    [self.delegate dismissModal:nil];
    //                                                   [self.delegate performSegueWithIdentifier:@"ShowMessageCategoriesList" sender:nil];
    
    NSUserDefaults *UDGroupe = [[NSUserDefaults alloc] initWithSuiteName:@"group.photoshare.extension"];
    [UDGroupe removeObjectForKey:kClientCodeKey];
    [UDGroupe synchronize];
    
    UIStoryboard *storyboard;
    if ([clientType isEqualToString: @"client"]) {
        storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    } else if ([clientType isEqualToString: @"driver"]) {
        [UDGroupe setObject:self.clientCodeInputView.code forKey:kClientCodeKey];
        [UDGroupe synchronize];
        
        storyboard = [UIStoryboard storyboardWithName:@"MainDriver" bundle:nil];
    } else {
        storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    }
    
    UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"MainNav"];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[[UIApplication sharedApplication].delegate window] setRootWithViewController:vc animated:YES completion:nil];
    });
}

- (void)removeLoadingActivityView {
    [UIView animateWithDuration:0.2 animations:^{
        self.dimingView.alpha = 0.0;
    } completion:^(BOOL finished) {
        [self.dimingView removeFromSuperview];
        self.dimingView = nil;
    }];
}

- (void)addLoadingActivityView {
    if (self.dimingView) return;
    
    self.dimingView = [[UIView alloc] initWithFrame:self.view.frame];
    self.dimingView.backgroundColor = [UIColor blackColor];
    CGRect indicatorFrame = CGRectMake(self.view.center.x - 10.f, self.view.center.y - 10 - 64, 20, 20);
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithFrame:indicatorFrame];
    [indicator startAnimating];

    self.dimingView.alpha = 0.0;
    
    [self.dimingView addSubview:indicator];
    [self.view addSubview:self.dimingView];
    
    [UIView animateWithDuration:0.2 animations:^{
        self.dimingView.alpha = 0.5;
    }];
}
    
- (BOOL)isNotificationEnabled{
    UIUserNotificationType types = [[[UIApplication sharedApplication] currentUserNotificationSettings] types];
    return types != UIUserNotificationTypeNone;
}
    
-(BOOL)isBackgroundAppRefreshEnabled {
    
    if ([[UIApplication sharedApplication] backgroundRefreshStatus] == UIBackgroundRefreshStatusAvailable) {
        return YES;
    }else{
        return NO;
    }
}
    
- (void)showOpenSettingsRequest {
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"ВНИМАНИЕ!"
                                                                        message:@"Для работы с данным разделом нужно включить Push уведомления. Открыть настройки?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *openSettingsAction = [UIAlertAction actionWithTitle:@"Открыть" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        [[UIApplication sharedApplication] openURL: url options: nil completionHandler:nil];
    }];
    [controller addAction:openSettingsAction];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Отмена" style:UIAlertActionStyleCancel handler:nil];
    [controller addAction:cancelAction];
    [self presentViewController:controller animated:YES completion:nil];
}
    
- (void)showOpenSettingsForBackgroundRefresh {
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"ВНИМАНИЕ!" message:@"Для корректной работы приложения нужно включить фоновое обновление. Открыть настройки?" preferredStyle: UIAlertControllerStyleAlert];
    UIAlertAction *openSettingsAction = [UIAlertAction actionWithTitle:@"Открыть" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        [[UIApplication sharedApplication] openURL:url options:nil completionHandler:nil];
    }];
    [controller addAction:openSettingsAction];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Отмена" style:UIAlertActionStyleCancel handler:nil];
    [controller addAction:cancelAction];
    [self presentViewController:controller animated:YES completion:nil];
}
    
- (void)keyboardWasShow:(NSNotification*)aNotification {
//    if (self.heightScreen < 667) {
//        [UIView beginAnimations:nil context:NULL];
//        [UIView setAnimationDuration:0.25];
//        self.view.frame = CGRectMake(0, -104,self.view.frame.size.width,self.view.frame.size.height);
//        [UIView commitAnimations];
//    }
}
    
- (void)registerForKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShow:)
                                                 name:UIKeyboardDidShowNotification object:nil];
}
    
- (void)hideKeyboard:(UIPanGestureRecognizer *)sender {
    [self.view endEditing:YES];
    
//    [UIView beginAnimations:nil context:NULL];
//    [UIView setAnimationDuration:0.25];
//    self.view.frame = CGRectMake(0,self.navigationController.view.frame.size.height - self.view.frame.size.height,self.view.frame.size.width,self.view.frame.size.height);
//    [UIView commitAnimations];
}

- (IBAction)textEditingChanged:(UITextField *)sender {
    NSMutableAttributedString* attributedString = [[NSMutableAttributedString alloc] initWithString:sender.text];
    [attributedString addAttribute:NSFontAttributeName
                             value:[UIFont fontWithName:@"Museo Sans Cyrl" size:24.f]
                             range:NSMakeRange(0, [sender.text length])];
    [attributedString addAttribute:NSKernAttributeName value:@10 range:NSMakeRange(0, [sender.text length])];
    [attributedString addAttribute:NSForegroundColorAttributeName value: [UIColor colorWithRed:74/255.f green:74/255.f blue:74/255.f alpha:1.f] range:NSMakeRange(0, [sender.text length])];
}
    
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField == self.countryCodeTextField) {
        if (textField.text.length + string.length <= 3 || string.length == 0) {
            return YES;
        }else{
            return NO;
        }
    }
    
    if (textField == self.phoneNumberTextField) {
        NSCharacterSet* validationSet = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
        NSArray* components = [string componentsSeparatedByCharactersInSet:validationSet];
        
        if ([components count] > 1) {
            return NO;
        }
        
        NSString* newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        //+XX (XXX) XXX-XXXX
        NSArray* validComponents = [newString componentsSeparatedByCharactersInSet:validationSet];
        
        newString = [validComponents componentsJoinedByString:@""];
        
        // XXXXXXXXXXXX
        static const int localNumberMaxLength = 7;
        static const int areaCodeMaxLength = 3;
        static const int countryCodeMaxLength = 1;
        
        if ([newString length] == localNumberMaxLength + areaCodeMaxLength + countryCodeMaxLength) {
            return NO;
        }
        
        NSMutableString* resultString = [NSMutableString string];
        
        /*
         XXXXXXXXXXXX
         +XX (XXX) XXX-XXXX
         */
        
        NSInteger localNumberLength = MIN([newString length], localNumberMaxLength);
        
        if (localNumberLength > 0) {
            
            NSString* number = [newString substringFromIndex:(int)[newString length] - localNumberLength];
            
            [resultString appendString:number];
            
            if ([resultString length] > 3) {
                [resultString insertString:@" " atIndex:3];
            }
            if ([resultString length] > 6) {
                [resultString insertString:@" " atIndex:6];
            }
            
        }
        
        if ([newString length] > localNumberMaxLength) {
            
            NSInteger areaCodeLength = MIN((int)[newString length] - localNumberMaxLength, areaCodeMaxLength);
            
            NSRange areaRange = NSMakeRange((int)[newString length] - localNumberMaxLength - areaCodeLength, areaCodeLength);
            
            NSString* area = [newString substringWithRange:areaRange];
            
            area = [NSString stringWithFormat:@"%@ ", area];
            
            [resultString insertString:area atIndex:0];
        }
        
        if ([newString length] > localNumberMaxLength + areaCodeMaxLength) {
            
            NSInteger countryCodeLength = MIN((int)[newString length] - localNumberMaxLength - areaCodeMaxLength, countryCodeMaxLength);
            
            NSRange countryCodeRange = NSMakeRange(0, countryCodeLength);
            
            NSString* countryCode = [newString substringWithRange:countryCodeRange];
            
            countryCode = [NSString stringWithFormat:@"+%@ ", countryCode];
            
            [resultString insertString:countryCode atIndex:0];
        }
        
        textField.text = resultString;
        return NO;
    }
    
    return YES;
}
- (IBAction)showClientCodeInfoAction:(UIButton *)sender {
    [self hideKeyboard:nil];
    
    UIVisualEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *visualEffectView = [[UIVisualEffectView alloc]initWithEffect:blurEffect];
    visualEffectView.frame = self.view.bounds;
    UITapGestureRecognizer* tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeBlurEffect)];
    [visualEffectView addGestureRecognizer:tapGestureRecognizer];
    self.visualEffectView = visualEffectView;
    
    UIImage *image = [UIImage imageNamed:@"img_client_cod"];
    UIImageView *imgView = [[UIImageView alloc] initWithImage:image];
    [imgView setContentMode:UIViewContentModeRight];
    
    CGRect buttonFrame = [self.clientCodeButton convertRect:self.clientCodeButton.imageView.frame toView:self.view];
    CGRect imgFrame = CGRectMake(0,
                                 buttonFrame.origin.y,
                                 buttonFrame.origin.x + buttonFrame.size.width,
                                 image.size.height);
    
    [imgView setFrame:imgFrame];
    [imgView setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    
    self.visualEffectView.alpha = 0.0;
    
    [self.view addSubview:self.visualEffectView];
    [self.visualEffectView.contentView addSubview:imgView];
    
    [UIView animateWithDuration:0.2 animations:^{
        self.visualEffectView.alpha = 1.0;
    }];
}

- (IBAction)showPinCodeInfoAction:(UIButton *)sender {
    [self hideKeyboard:nil];
    
    UIVisualEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *visualEffectView = [[UIVisualEffectView alloc]initWithEffect:blurEffect];
    visualEffectView.frame = self.view.bounds;
    UITapGestureRecognizer* tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeBlurEffect)];
    [visualEffectView addGestureRecognizer:tapGestureRecognizer];
    self.visualEffectView = visualEffectView;
    
    UIImage *image = [UIImage imageNamed:@"img_pin_cod"];
    UIImageView *imgView = [[UIImageView alloc] initWithImage:image];
    [imgView setContentMode:UIViewContentModeRight];
    
    CGRect buttonFrame = [self.pinCodeButton convertRect:self.pinCodeButton.imageView.frame toView:self.view];
    CGRect imgFrame = CGRectMake(0,
                                 buttonFrame.origin.y,
                                 buttonFrame.origin.x + buttonFrame.size.width,
                                 image.size.height);
    
    [imgView setFrame:imgFrame];
    [imgView setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    
    [self.visualEffectView.contentView addSubview:imgView];
    [self.view addSubview:self.visualEffectView];
    
    self.visualEffectView.alpha = 0.0;
    [UIView animateWithDuration:0.2 animations:^{
        self.visualEffectView.alpha = 1.0;
    }];
}
    
#pragma mark - removeBlurEffect
-(void)removeBlurEffect {
    [UIView animateWithDuration:0.2 animations:^{
        self.visualEffectView.alpha = 0.0;
    } completion:^(BOOL finished) {
        [self.visualEffectView removeFromSuperview];
    }];
}
    
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (IBAction)textFieldDidChange:(UITextField *)textField
{
    [UIView animateWithDuration:0.1 animations:^{
        [self.view layoutIfNeeded];
        [textField invalidateIntrinsicContentSize];
        [self.view layoutIfNeeded];
    }];
}

@end
