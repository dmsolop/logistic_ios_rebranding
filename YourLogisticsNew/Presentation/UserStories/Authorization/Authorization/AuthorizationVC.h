//
//  AuthorizationVC.h
//  YourLogistics
//
//  Created by Aleksandr on 30.05.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import "BasicVC.h"

@interface AuthorizationVC : BasicVC <UIGestureRecognizerDelegate, UITextFieldDelegate>

@property (nonatomic, weak) id delegate;

@end
