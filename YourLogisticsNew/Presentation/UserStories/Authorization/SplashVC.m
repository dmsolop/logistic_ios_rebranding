//
//  SplashVC.m
//  YourLogistics
//
//  Created by Bohdan on 22.11.2017.
//  Copyright © 2017 iCenter. All rights reserved.
//

#import "SplashVC.h"

@interface SplashVC ()

@property (weak, nonatomic) IBOutlet SolidButton *enterButton;

@end

@implementation SplashVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    @weakify(self);
    [self.enterButton didPressWithCompletion:^{
        @strongify(self);
        [self performSegueWithIdentifier:@"showAuth" sender:self];
    }];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [UIView animateWithDuration:0.3f delay:0.f options:UIViewAnimationOptionAllowUserInteraction animations:^{
        self.enterButton.transform = CGAffineTransformMakeScale(1.25f, 1.25f);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3f delay:0.f options:UIViewAnimationOptionAllowUserInteraction animations:^{
            self.enterButton.transform = CGAffineTransformMakeScale(1.f, 1.f);
        } completion:nil];
    }];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
