//
//  ServiceDetailsVC.m
//  YourLogistics
//
//  Created by Aleksandr on 04.05.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import "ArticleVC.h"
#import "UIImageView+AFNetworking.h"
#import "Utilities.h"


@interface ArticleVC ()

@property (weak, nonatomic) IBOutlet UIScrollView *contentScrollView;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingActivity;

@property (weak, nonatomic) IBOutlet UIImageView *articleImageView;

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@property (weak, nonatomic) IBOutlet UILabel *articleTitleLabel;

@property (weak, nonatomic) IBOutlet UIView *imageOverlayView;

@property (weak, nonatomic) IBOutlet UITextView *contentTextView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentTextViewHeight;

@end

@implementation ArticleVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.contentScrollView.hidden = YES;
    self.navigationItem.title = [self titleForType:self.contentType];
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    self.contentTextViewHeight.constant = [self.contentTextView sizeThatFits:CGSizeMake(self.contentTextView.frame.size.width, FLT_MAX)].height;
}

-(void)updateUI{
    if (self.service)
        [self.loadingActivity stopAnimating];
    [self.articleImageView setImageWithURL:[NSURL URLWithString:self.service.largeImageLink] placeholderImage:[UIImage imageNamed:@"NewsNoImageLarge"]];
    
    //    NSLog(@"%@",self.service.content);
    NSAttributedString *content = [[NSAttributedString alloc] initWithData:[self.service.content dataUsingEncoding:NSUTF8StringEncoding]
                                                                   options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                                             NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
                                                        documentAttributes:nil
                                                                     error:nil];
    //    NSLog(@"%@", content);
    self.contentTextView.attributedText = content;
    self.contentTextView.font = [UIFont fontWithName:@"Museo Sans Cyrl" size:13];
    [self.contentTextView sizeToFit];
    self.dateLabel.attributedText = [Utilities greenMarkDateFromDate:self.service.date];
    self.articleTitleLabel.text = self.service.title;
    self.contentScrollView.hidden = NO;
}

-(NSString *)titleForType:(ArticleType)type{
    NSString *title = @"";
    switch (type) {
        case ArticleTypeCompany:
            title = @"Новости компании";
            break;
        case ArticleTypeLogistics:
            title = @"Новости логистики";
            break;
        case ArticleTypeCompanyService:
            title = @"Услуги компании";
            break;
        default:
            break;
    }
    return title;
}

-(void)showDownloadingError{
    UIAlertController *alert = [Utilities downloadingErrorAlert];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)setService:(NewsArticleDetails *)service{
    _service = service;
    if (service)
        [self updateUI];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
}

@end
