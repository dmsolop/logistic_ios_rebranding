//
//  ServiceDetailsVC.h
//  YourLogistics
//
//  Created by Aleksandr on 04.05.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import "BasicVC.h"
#import "ArticleDetails.h"
#import "NewsArticleDetails.h"

@interface ArticleVC : BasicVC

@property (nonatomic, strong) NewsArticleDetails *service;

@property (nonatomic) ArticleType contentType;

-(void)showDownloadingError;

@end
