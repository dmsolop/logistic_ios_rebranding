//
//  NewsVC.m
//  YourLogistics
//
//  Created by Aleksandr on 04.05.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import "NewsVC.h"
#import <MagicalRecord/MagicalRecord.h>
#import <Realm/Realm.h>
#import "UIImageView+AFNetworking.h"
#import "APIService.h"

#import "NewsCell.h"
#import "ReportType1Cell.h"
#import "ReportType2Cell.h"
#import "ReportType3Cell.h"
#import "ReportType4Cell.h"
#import "ReportType4LCLCell.h"
#import "ReportType5Cell.h"
#import "ReportType6Cell.h"
#import "Article.h"
#import "LoadingActivityCell.h"
#import "ArticleVC.h"
#import "Utilities.h"
#import "NewsRead.h"
#import "ChatMessageListVC.h"
#import "MessageSaver.h"
#import "SharedManager.h"
#import "Constants.h"

#import "ClientDebitClass.h"
#import "OrderClass.h"

#import "ReportRLM.h"
#import "ManagerRLM.h"
#import "OrderRLM.h"
#import "DayRLM.h"
#import "ClientDebitRLM.h"

@interface NewsVC () <TopTabBarViewDelegate, UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSArray *logisticsNews;

@property (nonatomic, strong) NSArray *companyNews;

@property (nonatomic, strong) NSMutableArray *reports;

@property (nonatomic) ArticleType currentNewsType;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topTableViewPosition;

@property (nonatomic, strong) UIRefreshControl *refreshControll;

//@property (nonatomic, weak) SharedManager *myManager;

@property (strong,nonatomic) UIView *dimingView;
@property (assign,nonatomic) NSInteger dimingViewCount;

@end

@implementation NewsVC

Constants *consta = nil;
SharedManager *singletone = nil;

- (void)viewDidLoad {
    [super viewDidLoad];
    consta = [[Constants alloc] init];
    singletone = [SharedManager sharedManager];
    UIScreenEdgePanGestureRecognizer *lgr = [[UIScreenEdgePanGestureRecognizer alloc] init];
    [lgr addTarget:self action:@selector(dismissVC)];
    lgr.edges = UIRectEdgeLeft;
    [self.view addGestureRecognizer:lgr];
    if (!self.reports) {
        self.reports = [NSMutableArray array];
    }
    self.title = [self titleForReports:singletone.isBossPhoneUsing];
    [self prepareContent];

//    self.currentNewsType = ArticleTypeCompany;
    self.tableView.allowsSelection = !singletone.isBossPhoneUsing;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 134.f;
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(prepareContent) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
    self.refreshControll = refreshControl;
    self.currentNewsType = ArticleTypeCompany;
}


-(NSString *)titleForReports:(BOOL)type{
    NSString *title = @"";
    switch (type) {
        case NO:
            title = consta.titleNews;
            break;
        case YES:
            title = consta.titleReports;
            break;
        default:
            break;
    }
    return title;
}


- (void)updateBarButtonBadges {
    
    for (UITabBarItem* item in self.tabBarController.tabBar.items) {
        if (item.tag == 0) {
            continue;
        }
        
#warning Contracts badge disabled
        NSInteger messCount = 0;
        
        if (item.tag != 3) {
            messCount = [MessageSaver UnreadMessageCountWithType:item.tag];
        }
        
        if (messCount == 0) {
            [item setBadgeValue:nil];
        }else{
            [item setBadgeValue:[NSString stringWithFormat:@"%d", messCount]];
        }
    }
}

#pragma mark - Prepare content
-(void)prepareContent {
    
    if (singletone.isBossPhoneUsing) {
        [self sendReportRequest];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"reportId >= %ld", consta.initialTime];
        
        RLMResults<ReportRLM *> *reports = [ReportRLM objectsWithPredicate:predicate];
        
        ///Cleaning code for displaing in reports
        NSMutableSet<ReportRLM *> *unicReports = [NSMutableSet set];
        
        for (ReportRLM * report in reports) {
            BOOL isDublicate = NO;
            if (unicReports.count > 0) {
                NSSet *tempSet = [NSSet setWithSet:unicReports];
                for (ReportRLM * item in tempSet) {
                    if (item.reportId == report.reportId) {
                        isDublicate = YES;
                        break;
                    }
                }
                if (!isDublicate) {
                    [unicReports addObject:report];
                }
            } else {
                [unicReports addObject:report];
            }
        }
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"reportId"
        ascending:NO];
        NSArray<ReportRLM *> *sortedArray = [unicReports sortedArrayUsingDescriptors:@[sortDescriptor]];
        ///
        
        [self getReports:sortedArray];
        [self updateBarButtonBadges];

    } else {
        //    self.currentNewsType == ArticleTypeLogistics ? [self getLogisticsNews] : [self getCompanyNews];
        NSArray<NewsRead *> *news = [NewsRead MR_findAll];
        [news enumerateObjectsUsingBlock:^(NewsRead * _Nonnull newsRead, NSUInteger idx, BOOL * _Nonnull stop) {
            newsRead.isNew = NO;
        }];
        [[NSManagedObjectContext MR_rootSavingContext] MR_saveToPersistentStoreAndWait];
        
        [self getCompanyNews];
        [self updateBarButtonBadges];
    }

}

- (void)setSeenStatusToReport:(ReportClass *)report {
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"reportId == %ld", report.reportId];
    RLMResults<ReportRLM *> *reports = [ReportRLM objectsWithPredicate:pred];
    [[RLMRealm defaultRealm] transactionWithBlock:^{
        for (ReportRLM * rep in reports) {
            rep.isNew = NO;
        }
    }];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:[MessageSaver allUnreadMessagesCount]];
}

#pragma mark - UITableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *currentDataSource = singletone.isBossPhoneUsing ? self.reports : [self currentDataSource];
    return currentDataSource.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (singletone.isBossPhoneUsing) {
        ReportClass * report = self.reports[indexPath.row];
        [self setSeenStatusToReport:report];
        
        switch (report.type) {
            case ReportTypeSalesDayLogistic:
            case ReportTypeSalesMonthLogistic: {
                ReportType1Cell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass ([ReportType1Cell class])];
                [cell configWithReport:report];
                return  cell;
                break;
            }
            case ReportTypeSalesTO:
            case ReportTypeSalesOPT_NI:
            case ReportTypeDaylyTransit: {
                ReportType2Cell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass ([ReportType2Cell class])];
                [cell configWithReport:report];
                return  cell;
                break;
            }
            case ReportTypeDepartment:
            case ReportTypeAccumTransit: {
                ReportType3Cell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass ([ReportType3Cell class])];
                [cell configWithReport:report];
                return  cell;
                break;
            }
            case ReportTypeMeeting: {
                if (report.managers.count <= 4) {
                    ReportType4LCLCell *cell = nil;
                    cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass ([ReportType4LCLCell class])];
                    [cell configWithReport:report];
                    return  cell;
                } else {
                    ReportType4Cell *cell = nil;
                    cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass ([ReportType4Cell class])];
                    [cell configWithReport:report];
                    return  cell;
                }
                break;
            }
            case ReportTypeDebit: {
                ReportType5Cell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass ([ReportType5Cell class])];
                [cell configWithReport:report];
                return  cell;
                break;
            }
            case ReportTypeCalculated: {
                ReportType6Cell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass ([ReportType6Cell class])];
                [cell configWithReport:report];
                return  cell;
                break;
            }
            default:{
                ReportType3Cell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass ([ReportType3Cell class])];
//                [cell configWithReport:report];
                return cell;
                break;
            }

        }
        
    } else {
        //    if ([self currentDataSource] == nil) {
        //        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        //        LoadingActivityCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LoadingActivityCell"];
        //        [cell startAnimating];
        //        return cell;
        //    } else {
        NewsArticle *articleToShow = [self currentDataSource][indexPath.row];
        //    NewsRead *newsRead = [NewsRead MR_findFirstByAttribute:@"newsId" withValue:@([articleToShow ID])];
        
        NewsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NewsCell"];
        UIImage *image = [UIImage imageNamed:@"NewsNoImage"];
        [cell.articleImageView setImageWithURL:[NSURL URLWithString:articleToShow.imageLink]
                              placeholderImage:image];
        
        //    cell.contentView.alpha = newsRead.isRead ? 0.5 : 1.0;
        
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineSpacing = 3;
        paragraphStyle.lineBreakMode = NSLineBreakByTruncatingTail;
        cell.articleTitleLabel.attributedText = [[NSAttributedString alloc] initWithString:articleToShow.title
                                                                                attributes:@{NSParagraphStyleAttributeName : paragraphStyle,
                                                                                             NSFontAttributeName : [UIFont fontWithName:@"Museo Sans Cyrl" size:15]}];
        cell.dateLabel.attributedText = [Utilities greenMarkDateFromDate:articleToShow.date];
        return cell;
        //    }
    }
//    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (singletone.isBossPhoneUsing){ return; }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self performSegueWithIdentifier:@"ShowNewsDetails" sender:[self currentDataSource][indexPath.row]];
}

///Не используется
//-(void)selectedSectionChanged:(NSInteger)sectionNumber{
//    self.currentNewsType = (ArticleType)sectionNumber;
//    [self prepareContent];
//}

-(NSArray *)currentDataSource{
    return self.currentNewsType == ArticleTypeLogistics ? self.logisticsNews : self.companyNews;
}

-(void)getLogisticsNews{
    if (!self.logisticsNews) {
        [self.refreshControll beginRefreshing];
        [self.tableView setContentOffset:CGPointMake(0, -self.refreshControll.frame.size.height) animated:NO];
        [[APIService sharedAPIClient] getLogisticsNewsWithCompletion:^(NSArray *news) {
            
            [news enumerateObjectsUsingBlock: ^(NewsArticle *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                [NewsRead MR_findFirstOrCreateByAttribute: @"newsId" withValue: @(obj.ID) inContext: [NSManagedObjectContext MR_rootSavingContext]];
            }];
            
            [[NSManagedObjectContext MR_rootSavingContext] MR_saveToPersistentStoreAndWait];
            
            self.topTableViewPosition.constant = kDefaultTopTabBarHeight;
//            [self addTabBarWithTitles:@[@"Новости логистики", @"Новости компании"]
//                                   colors:nil
//                              forDelegate:self];
            self.logisticsNews = news;
            [self.tableView reloadData];
            self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
            [self.refreshControll endRefreshing];
        } failure:^(NSError *error) {
            [self.refreshControll endRefreshing];
            UIAlertController *alert = [Utilities downloadingErrorAlert];
            [self presentViewController:alert animated:YES completion:nil];
            self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        }];
    }
}

-(void)getCompanyNews{
//    if (!self.companyNews) {
        [self.refreshControll beginRefreshing];
        [self.tableView setContentOffset:CGPointMake(0, -self.refreshControll.frame.size.height) animated:NO];
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [[APIService sharedAPIClient] getCompanyNewsWithCompletion:^(NSArray *articles) {
            
            [articles enumerateObjectsUsingBlock: ^(NewsArticle *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                [NewsRead MR_findFirstOrCreateByAttribute: @"newsId" withValue: @(obj.ID) inContext: [NSManagedObjectContext MR_rootSavingContext]];
            }];
            
            [[NSManagedObjectContext MR_rootSavingContext] MR_saveToPersistentStoreAndWait];
            
            self.companyNews = articles;
            [self.tableView reloadData];
            self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
            [self.refreshControll endRefreshing];
        } failure:^(NSError *error) {
            [self.refreshControll endRefreshing];
            UIAlertController *alert = [Utilities downloadingErrorAlert];
            [self presentViewController:alert animated:YES completion:nil];
            self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
            NSLog(@"Error while downloading company news");
        }];
//    }
}

-(void)sendReportRequest {
    [self addLoadingActivityView];
    NSString* messageId5 = @"";
    ReportRLM *lastReport = [[[ReportRLM allObjects] sortedResultsUsingKeyPath:@"reportId" ascending:NO] firstObject];
    NSInteger reportID = lastReport.reportId;
    messageId5 = [NSString stringWithFormat:@"%ld", reportID];
    if ([messageId5 isEqualToString:@"(null)"]){ messageId5 = @"";}
    
    [[APIService sharedAPIClient] getMessageWithType:5 messageId:messageId5 completion:^(NSArray *response) {
        [self removeLoadingActivityView];
        NSLog(getJSON(response));
        
        for (NSDictionary *report in response) {
            if (lastReport.reportId < [report[@"id"] integerValue] || lastReport == nil) {
                [MessageSaver saveReportRLM:report];
            }
        }
    } failure:^(NSError *error) {
        [self removeLoadingActivityView];
    }];
}


#pragma mark - Get reports
-(void)getReports:(NSArray*) reports {
    
    if (reports.count == 0) return;
    
    [self.refreshControll beginRefreshing];
    [self.tableView setContentOffset:CGPointMake(0, -self.refreshControll.frame.size.height) animated:NO];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
   
    for (ReportRLM *repo in reports) {
        ReportClass *report = [[ReportClass alloc] init];
        NSMutableArray *managers = [NSMutableArray array];
        report.isNew        = repo.isNew;
        report.reportName   = repo.name;
        report.reportId     = repo.reportId;
        report.type         = repo.typeReport;
        report.date = [NSDate dateWithTimeIntervalSince1970:repo.reportId/1000.0];
        
        if (report.type == 9) {
            
            for (OrderRLM *ord in repo.orders) {
                OrderClass *order = [[OrderClass alloc] init];
                order.name      = ord.name;
                order.uid       = ord.uid;
                order.product   = ord.product;
                order.price     = ord.price;

                [managers addObject:order];
            }
            report.orders = managers;
        } else if (report.type == 10) {
            
            for (ClientDebitRLM *clnt in repo.clients) {
                NSMutableArray *orders = [NSMutableArray array];
                ClientDebitClass *client = [[ClientDebitClass alloc] init];
                
                for (OrderRLM *ord in clnt.orders) {
                    OrderClass *order = [[OrderClass alloc] init];
                    order.orderNumber = ord.orderNumber;
                    order.invoiceDate = ord.invoiceDate;
                    order.usd = ord.usd;
                    order.uah = ord.uah;
                    order.eur = ord.eur;
                    order.rub = ord.rub;
                    [orders addObject:order];
                }
                
                client.orders = orders;
                client.clientCode = clnt.clientCode;
                client.creditOfTrust = clnt.creditOfTrust;
                client.dateToWork = clnt.dateToWork;
                client.cause = clnt.cause;
                client.action = clnt.action;
                client.debtStatus = clnt.debtStatus;
                [managers addObject:client];
            }
            report.clients = managers;
        } else {
            
            for (ManagerRLM *man in repo.managers) {
                ManagerClass *manager = [[ManagerClass alloc] init];
                manager.name    = man.name;
                manager.uid     = man.uid;
                
                switch (report.type) {
                    case ReportTypeSalesDayLogistic:
                    case ReportTypeSalesMonthLogistic: {
                        manager.to      = man.to;
                        manager.lcl     = man.lcl;
                        manager.ltl     = man.ltl;
                        manager.ftl     = man.ftl;
                        manager.a       = man.a;
                        manager.ni      = man.ni;
                        manager.fcl     = man.fcl;
                        manager.k       = man.k;
                        manager.fraht   = man.fraht;
                        break;
                    }
                    case ReportTypeSalesTO: {
                        manager.to                  = man.to;
                        manager.toFormalize         = man.toFormalize;
                        manager.logisticFormalize   = man.logisticFormalize;
                        manager.marginLogistic      = man.marginLogistic;
                        manager.amountToClear       = man.amountToClear;
                        break;
                    }
                    case ReportTypeSalesOPT_NI: {
                        manager.amountNiFormalize   = man.amountNiFormalize;
                        manager.marginNi            = man.marginNi;
                        manager.amountLclFormalize  = man.amountLclFormalize;
                        manager.mzLclFormalize      = man.mzLclFormalize;
                        break;
                    }
                    case ReportTypeDepartment: {
                        manager.amountLclFormalize  = man.amountLclFormalize;
                        manager.mzLclFormalize      = man.mzLclFormalize;
                        break;
                    }
                    case ReportTypeDaylyTransit: {
                        manager.pogran          = man.pogran;
                        manager.weekNumber      = man.weekNumber;
                        manager.dayWeekNumber   = man.dayWeekNumber;
                        manager.ttAmount        = man.ttAmount;
                        manager.guarantyAmount  = man.guarantyAmount;
                        break;
                    }
                    case ReportTypeAccumTransit: {
                        manager.pogran          = man.pogran;
                        manager.ttAmount        = man.ttAmount;
                        manager.guarantyAmount  = man.guarantyAmount;
                        break;
                    }
                    case ReportTypeMeeting: {
                        NSMutableArray *days = [NSMutableArray array];
                        for (DayRLM *date in man.days) {
                            DayClass *day = [[DayClass alloc] init];
                            day.date            = date.date;
                            day.client_code     = date.client_code;
                            day.effectiveness   = date.effectiveness;
                            
                            [days addObject:day];
                        }
                        manager.days = days;
                    }
                    default:
                        break;
                }
                [managers addObject:manager];
            }
            report.managers = managers;
        }

        [self.reports addObject:report];
    }
    
    [self.tableView reloadData];
    [self.refreshControll endRefreshing];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ShowNewsDetails"])
    {
        ArticleVC *avc = segue.destinationViewController;
        avc.contentType = self.currentNewsType;
        
        NewsRead *newsRead = [NewsRead MR_findFirstByAttribute:@"newsId" withValue:@([sender ID]) inContext:[NSManagedObjectContext MR_rootSavingContext]];
        newsRead.isRead = YES;
        [[NSManagedObjectContext MR_rootSavingContext] MR_saveToPersistentStoreAndWait];
        
        [self updateBarButtonBadges];
        [self.tableView reloadData];
        
        
        [[APIService sharedAPIClient] getNewsArticleDetailsForArticle:sender
                                                           completion:^(NewsArticleDetails *article)
         {
             avc.service = article;
         }
                                                              failure:^(NSError *error)
         {
             NSLog(@"Error while downloading article details");
             [avc showDownloadingError];
         }];
    }
}
     
     -(void)setCurrentNewsType:(ArticleType)currentNewsType{
         _currentNewsType = currentNewsType;
         [self.tableView reloadData];
     }
     
     -(void)dismissVC {
         [self dismissViewControllerAnimated:YES completion:nil];
     }
     
     - (void)removeLoadingActivityView {
         self.dimingViewCount--;
         
         if (self.dimingViewCount <= 0) {
             self.dimingViewCount = 0;
             
             [UIView animateWithDuration:0.2 animations:^{
                 self.dimingView.alpha = 0.0;
             } completion:^(BOOL finished) {
                 [self.dimingView removeFromSuperview];
                 self.dimingView = nil;
             }];
         }
     }
     
     - (void)addLoadingActivityView {
         self.dimingViewCount++;
         
         if (self.dimingView) {
             return;
         }
         
         self.dimingView = [[UIView alloc] initWithFrame:self.view.frame];
         self.dimingView.backgroundColor = [UIColor blackColor];
         CGRect indicatorFrame = CGRectMake(self.view.center.x - 10.f, self.view.center.y - 10 - 64, 20, 20);
         UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithFrame:indicatorFrame];
         [indicator startAnimating];
         
         self.dimingView.alpha = 0.0;
         
         [self.dimingView addSubview:indicator];
         [self.view addSubview:self.dimingView];
         
         [UIView animateWithDuration:0.2 animations:^{
             self.dimingView.alpha = 0.5;
         }];
     }

@end
