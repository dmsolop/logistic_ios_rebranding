//
//  MessageListVC.m
//  YourLogistics
//
//  Created by Aleksandr on 20.05.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import "MessageListVC.h"
#import "MessageListCell.h"
#import "MessageListHeaderCell.h"
#import "OrderMessage.h"
#import <MagicalRecord/MagicalRecord.h>
#import "FiltringCondition.h"
#import "ChatMessageListVC.h"
#import "MessageSaver.h"
#import "MessageCategoryCell.h"
#import "ChatMessage.h"
#import "ListOfCategories.h"
#import "TextMessage.h"
#import "APIService.h"
#import "ColorPallet.h"

@interface MessageListVC () <UITableViewDataSource, UITableViewDelegate, OrderCellDelegate, SortingOrderDelegate, UISearchResultsUpdating>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSArray *orders;

@property (nonatomic, strong) NSPredicate *filterCondition;

@property (weak, nonatomic) IBOutlet UILabel *noMessagesLabel;

@property (nonatomic, strong) ListOfCategories *messageCategories;

@property (nonatomic, assign) BOOL isDetails;

@property (nonatomic, strong) OrderMessage* orderMessage;

@property (nonatomic, strong) UIRefreshControl *refreshControll;

@property (nonatomic, strong) NSTimer* timer;

@property (nonatomic, strong) UISearchController *resultSearchController;

@end

@implementation MessageListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(invalidateTimer) name:@"InvalidateTimerNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshDataAndStartTimer) name:@"RefreshDataNotification" object:nil];

    
    [self.tableView setBackgroundColor:[UIColor clearColor]];
    
    if (self.isDetails) {
        
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BackButton"] style:UIBarButtonItemStylePlain target:self action:@selector(popVC:)];
        self.navigationItem.leftBarButtonItem.tintColor = [UIColor blackColor];
        [self.view setBackgroundColor:[UIColor whiteColor]];
        
    }else{
        
        /// gesture recognizer
        UIScreenEdgePanGestureRecognizer *lgr = [[UIScreenEdgePanGestureRecognizer alloc] init];
        [lgr addTarget:self action:@selector(dismissVC)];
        lgr.edges = UIRectEdgeLeft;
        [self.view addGestureRecognizer:lgr];
        
    }
    
    [self observeNewMessageNotifications];
    self.tableView.estimatedRowHeight = 150.f;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 20, 0);
    CGRect frame = self.tableView.tableHeaderView.frame;
    frame.size.height = 1;
    UIView *headerView = [[UIView alloc] initWithFrame:frame];
    [self.tableView setTableHeaderView:headerView];
    
    CGRect frame2 = self.tableView.tableFooterView.frame;
    frame2.size.height = 1;
    UIView *footerView = [[UIView alloc] initWithFrame:frame2];
    [self.tableView setTableFooterView:footerView];
    self.navigationItem.title = @"Контракты";
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refreshData:) forControlEvents:UIControlEventValueChanged];
    self.refreshControll = refreshControl;

    if (@available(iOS 10.0, *)) {
        self.tableView.refreshControl = self.refreshControll;
    } else {
        [self.tableView addSubview:self.refreshControll];
    }
    
    if (!self.isDetails) {
        self.timer =  [NSTimer scheduledTimerWithTimeInterval:60.0
                                                       target:self
                                                     selector:@selector(refreshData:)
                                                     userInfo:nil
                                                      repeats:YES];
    }

    if (!self.isDetails) {
        self.resultSearchController = [[UISearchController alloc] initWithSearchResultsController:nil];
        self.resultSearchController.searchResultsUpdater = self;
        self.resultSearchController.hidesNavigationBarDuringPresentation = false;
        self.resultSearchController.dimsBackgroundDuringPresentation = false;
        self.resultSearchController.searchBar.searchBarStyle = UISearchBarStyleProminent;
        [self.resultSearchController.searchBar sizeToFit];
        self.tableView.tableHeaderView = self.resultSearchController.searchBar;
    }
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.refreshControll beginRefreshing];
    [self.tableView setContentOffset:CGPointMake(0, self.tableView.contentOffset.y -self.refreshControll.frame.size.height) animated:YES];
    [self refreshData:@"viewWillAppear"];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self invalidateTimer];
}

-(void)invalidateTimer{
    [self.timer invalidate];
}

-(void)refreshDataAndStartTimer {
    [self refreshData:nil];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:60.0
                                                  target:self
                                                selector:@selector(refreshData:)
                                                userInfo:nil
                                                 repeats:YES];
}

-(void)refreshData:(id)type {
    
    if (![type isKindOfClass:[NSTimer class]]) {
        if ([type isKindOfClass:[UIRefreshControl class]] || [type isEqualToString:@"viewWillAppear"]) {
            self.orders = [NSArray array];
        }
    }

    if (!self.isDetails) {
        
        NSString* messageId = @"";
    
        OrderMessage *lastOrderMessage = [OrderMessage MR_findFirstOrderedByAttribute:@"messageId" ascending:NO];
        
        messageId = [NSString stringWithFormat:@"%@", lastOrderMessage.messageId];

        [[APIService sharedAPIClient] getMessageWithType:3 messageId:@"0"  completion:^(NSArray *response) {
//            NSLog(getJSON(response));
            
            if (response.count > 0) {
                //New delete type
//                NSFetchRequest  *request = [OrderMessage MR_requestAll];
//                [request setReturnsObjectsAsFaults:YES];
//                [request setIncludesPropertyValues:NO];
//
//                NSArray *objectsToTruncate = [OrderMessage MR_executeFetchRequest:request];
//
//                for (id objectToTruncate in objectsToTruncate)
//                {
//                    [objectToTruncate MR_deleteEntity];
//                }
                
//                [OrderMessage MR_deleteAllMatchingPredicate:nil];
//                [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
                
                dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
                dispatch_async(queue, ^{

                    for (NSDictionary* message in response) {
                        NSArray* messageEntity = [OrderMessage MR_findByAttribute:@"messageId" withValue:@([message[@"id"] integerValue])];
                        if (messageEntity.count == 0) {
                            [MessageSaver saveOrderMessage:message];
                        }
                    }
                    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{

                    });
                });
            }
                
            [[APIService sharedAPIClient] getMessageWithType:4 messageId:@""  completion:^(NSArray *response2) {
//                NSLog(getJSON(response2));
                
                if (response2.count > 0) {
                    
                    NSPredicate * predicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:self.filterCondition,nil]];
                    
                    NSArray *orders = [OrderMessage MR_findAllSortedBy:@"orderNumber,messageId"
                                                             ascending:NO
                                                         withPredicate:predicate];
                    
                    for (OrderMessage *mess in orders) {
                        NSInteger isTrue = NO;
                        for (NSDictionary*  mess2 in response2) {
                            if ([mess.orderNumber isEqualToString:[mess2 objectForKey:@"order_number"]]) {
                                isTrue = YES;
                            }
                        }
                        if (isTrue == NO) {
                            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"orderNumber = %@", mess.orderNumber];
                            [OrderMessage MR_deleteAllMatchingPredicate:predicate];
                            [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
                        }
                    }
                    
                }

                
                [self prepareContent];
                [self.refreshControll endRefreshing];
                
            } failure:^(NSError *error) {
//                NSLog(@"%@", error.localizedDescription);
                
            }];
                
            
        } failure:^(NSError *error) {
//            NSLog(@"%@", error.localizedDescription);
            
            [self.refreshControll endRefreshing];
        }];
    }
    
    if (self.isDetails) {
        [self.refreshControll endRefreshing];
    }
    
    if (self.orders.count == 0) {
        [self prepareContent];
    }
}

- (void)prepareContent {
    if (self.isDetails) {
        NSArray *separatedOrderNumbers = [self.orderMessage.orderNumber componentsSeparatedByString:@" "];
        NSString *testString = separatedOrderNumbers.lastObject;
        NSString *separatedOrderNumber;
        if (testString != nil && ![testString  isEqual: @""]) {
            separatedOrderNumber = testString;
        }
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"orderNumber CONTAINS[cd] %@", separatedOrderNumber];

        NSArray *orders = [OrderMessage MR_findAllWithPredicate:predicate];
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"messageId"
                                                                       ascending:NO];
        self.orders = [orders sortedArrayUsingDescriptors:@[sortDescriptor]];

        self.noMessagesLabel.hidden = self.orders.count > 0 ? YES : NO;
    } else {
        NSPredicate *filterPredicate;
        NSString *searchText = self.resultSearchController.searchBar.text;
        if (searchText != nil && searchText.length > 0) {
            filterPredicate = [NSPredicate predicateWithFormat:@"orderNumber CONTAINS[cd] %@", searchText];
        }
        
        NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:filterPredicate, nil]];

        NSArray *orders = [OrderMessage MR_findAllSortedBy:@"orderNumber,messageId"
                                                 ascending:NO
                                             withPredicate:predicate];
        
        self.orders = [self groupByOrderNumber:orders];
        
        self.noMessagesLabel.hidden = self.orders.count > 0 ? YES : NO;
        
        //set tabbar badge
        for (UITabBarItem *item in self.tabBarController.tabBar.items) {
            if (item.tag == 0) {
                continue;
            }
            
#warning Contracts badge disabled
            NSInteger messCount = 0;
            if (item.tag != 3) {
                messCount = [MessageSaver UnreadMessageCountWithType:item.tag];
            }

            [item setBadgeValue:(messCount == 0 ? nil : @(messCount).stringValue)];
        }
    }
    
    [self.tableView reloadData];
}

-(void)setSeenStatusToOrders:(NSArray *)orders{
    for (OrderMessage *message in orders) {
        [MagicalRecord saveWithBlock:^(NSManagedObjectContext * _Nonnull localContext) {
            OrderMessage *localMessage = [message MR_inContext:localContext];
            localMessage.isNew = @(NO);
        } completion:^(BOOL contextDidSave, NSError * _Nullable error) {
            [[UIApplication sharedApplication] setApplicationIconBadgeNumber:[MessageSaver allUnreadMessagesCount]];
        }];
    }
}

-(void)setSeenStatusToOrder:(OrderMessage *)message{
    
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext * _Nonnull localContext) {
        OrderMessage *localMessage = [message MR_inContext:localContext];
        localMessage.isNew = @(NO);
    } completion:^(BOOL contextDidSave, NSError * _Nullable error) {
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:[MessageSaver allUnreadMessagesCount]];
    }];
}

-(ServiceType)serviceTypeForCategory{
    MessageCategoryType type = self.category.type;
    
    if (type == MessageCategoryTypeAuto)
        return ServiceTypeAuto;
    
    else if (type == MessageCategoryTypeSea)
        return ServiceTypeSea;
    
    else if (type == MessageCategoryTypeAvia)
        return ServiceTypeAvia;
    
    else if (type == MessageCategoryTypeTO)
        return ServiceTypeTO;
    
    else if (type == MessageCategoryTypeKuski)
        return ServiceTypeKuski;
    
    else if (type == MessageCategoryTypeCourier)
        return ServiceTypeCourier;
    
    else if (type == MessageCategoryTypeDistribucia)
        return ServiceTypeDistribucia;
    
    else
        return ServiceTypeUndefined;
}

-(NSArray *)groupByOrderNumber:(NSArray *)toGroup{

    NSMutableArray *grouped = [NSMutableArray array];
    NSMutableSet *setNumber = [NSMutableSet set];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"messageId"
                                                                   ascending:NO];
    toGroup = [toGroup sortedArrayUsingDescriptors:@[sortDescriptor]];
    
    for (OrderMessage *message in toGroup) {
        NSString *separatedOrderNumber = [message.orderNumber componentsSeparatedByString:@" "].lastObject;
        if (![setNumber containsObject:separatedOrderNumber]) {
            [setNumber addObject:separatedOrderNumber];
            [grouped addObject:message];
        }
    }
    return [grouped copy];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.orders.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return UITableViewAutomaticDimension;//93;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    OrderMessage *message = self.orders[indexPath.section];

    MessageCategoryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MessageCategoryCell"];
    cell.categoryTitleLabel.text = message.orderNumber;
    cell.statusLabel.text = message.status;
    cell.statusLabel.textAlignment = NSTextAlignmentCenter;
    cell.descriptionLabel.text = message.category;
    
    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
    formater.locale = [NSLocale localeWithLocaleIdentifier:@"ru_RU"];
    formater.dateFormat = @"dd.MM.YYYY";
    formater.timeZone = [NSTimeZone systemTimeZone]; //[NSTimeZone timeZoneWithAbbreviation:@"GMT"];

    if (message.port != nil) {
        cell.dateLabel.text = message.port;
    }else{
        if ([message.clearanceDate integerValue] != 0) {
            cell.dateLabel.text = [formater stringFromDate:[NSDate dateWithTimeIntervalSince1970:[message.clearanceDate integerValue]/1000]];
        }else{
            cell.dateLabel.text = @"";//[formater stringFromDate:[NSDate dateWithTimeIntervalSince1970:[message.messageId integerValue]/1000]];
        }
    }
    
    
    UIImage* img = [UIImage imageNamed:[self getImageFromService:[message.serviceId integerValue] andOperatorId:message.operatorId]];
    if (img) {
        cell.categoryImageView.image = img;
    }else{
        cell.categoryImageView.image = [UIImage imageNamed:@"img_type_courier"];
    }
    
    if (self.isDetails) {
        [self setSeenStatusToOrder:message];
        [cell setNumberOfNewMessages:0];
        
        NSDateFormatter *formaterLastMessage = [[NSDateFormatter alloc] init];
        formaterLastMessage.locale = [NSLocale localeWithLocaleIdentifier:@"ru_RU"];
        formaterLastMessage.dateFormat = @"dd.MM HH:mm";
        formaterLastMessage.timeZone = [NSTimeZone systemTimeZone];

        cell.lastMessageLabel.text = [formaterLastMessage stringFromDate:[NSDate dateWithTimeIntervalSince1970:[[message messageId] integerValue]/1000]];
        
        cell.categoryTitleLabel.textColor = [UIColor colorWithRed:74/255.f green:74/255.f blue:74/255.f alpha:1.f];
        cell.dateLabel.textColor = [UIColor colorWithRed:74/255.f green:74/255.f blue:74/255.f alpha:1.f];

        cell.bgView.layer.cornerRadius = 16;
        cell.bgView.layer.borderColor = [ColorPallet new].baseLightBlue.CGColor;
        cell.bgView.layer.borderWidth = 1;
    }else{
        self.messageCategories = [MessageSaver messageOrderWithType:MessageTypeOrder andOrderNumber:message.orderNumber];
        [cell setNumberOfNewMessages:0];
        
        NSDateFormatter *formaterLastMessage = [[NSDateFormatter alloc] init];
        formaterLastMessage.locale = [NSLocale localeWithLocaleIdentifier:@"ru_RU"];
        formaterLastMessage.dateFormat = @"dd.MM HH:mm";
        formaterLastMessage.timeZone = [NSTimeZone systemTimeZone];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"orderNumber = %@", message.orderNumber];

        NSArray *messages = [OrderMessage MR_findAllSortedBy:@"messageId"
                                                 ascending:YES
                                             withPredicate:predicate];
        
        cell.lastMessageLabel.text = [formaterLastMessage stringFromDate:[NSDate dateWithTimeIntervalSince1970:[[(OrderMessage* )messages.lastObject messageId] integerValue]/1000]];
        
        cell.bgView.layer.shadowColor = [[UIColor colorWithRed:188/255.f green:188/255.f blue:188/255.f alpha:.5f] CGColor];
        cell.bgView.layer.shadowOffset = CGSizeMake(0,2);
        cell.bgView.layer.shadowRadius = 2;
        cell.bgView.layer.shadowOpacity = 1.f;
        cell.bgView.layer.masksToBounds = NO;
        cell.layer.rasterizationScale = [[UIScreen mainScreen] scale];
        cell.layer.shouldRasterize = YES;
    }
    

    return cell;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{

    UIView* aView = [[UIView alloc] initWithFrame:CGRectZero];
    [aView setBackgroundColor:[UIColor clearColor]];
    
    return aView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    if (self.isDetails) {
        return 22;
    }
    return 5;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 0;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (!self.isDetails) {
        
        MessageListVC* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"MessageListVC"];
        OrderMessage *message = self.orders[indexPath.section];
        vc.isDetails = YES;
        vc.orderMessage = message;
        [self.navigationController pushViewController:vc animated:YES];
        [self.resultSearchController setActive:NO];
    }
    NSLog(@"%@",indexPath);
}

-(NSString *)getImageFromService:(NSInteger)serviceId andOperatorId:(NSString *)operatorId {
    NSString *imageName;
    if (serviceId == ServiceTypeSea) {
        return imageName = @"img_type_sea";
    } else if (serviceId == ServiceTypeAuto){
        return imageName = @"img_type_auto";
    } else if (serviceId == ServiceTypeAvia){
        return imageName = @"img_type_avia";
    }else if (serviceId == ServiceTypeTO){
        return imageName = @"img_type_to";
    }else if (serviceId == ServiceTypeKuski){
        return imageName = @"img_type_kuski";
    }else if (serviceId == ServiceTypeCourier){
        if ([operatorId isEqualToString:@""] || operatorId == nil) {
            return imageName = @"img_type_courier";
        }else{
            return imageName = [NSString stringWithFormat:@"ic_%@",operatorId];
        }
    }else if (serviceId == ServiceTypeDistribucia){
        return imageName = @"img_type_courier";
    }else if (serviceId == ServiceTypeKonsKonteiner){
        return imageName = [NSString stringWithFormat:@"ic_%@",operatorId];
    }else if (serviceId == ServiceTypeKonsAvto){
        return imageName = [NSString stringWithFormat:@"ic_%@",operatorId];
    }
    
    return imageName = @"img_type_auto";
}

-(void)updateUIOnMessageRecieving{
    NSLog(@"update message list vc");
    
}

-(void)dismissVC {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)popVC:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)setMessageCategories:(ListOfCategories *)messageCategories{
    _messageCategories = messageCategories;
}

#pragma mark - Navigation
 
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"ShowFilteredMessagesList"]) {
        ChatMessageListVC *cmlvc = segue.destinationViewController;
        cmlvc.filterCondition = sender;
    }
}

- (void)updateSearchResultsForSearchController:(nonnull UISearchController *)searchController {
    [self prepareContent];
}


@end
