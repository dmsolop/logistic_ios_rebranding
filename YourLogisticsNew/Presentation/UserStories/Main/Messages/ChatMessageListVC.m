//
//  ChatMessageListVC.m
//  YourLogistics
//
//  Created by Aleksandr Ponomarenko on 26.05.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import "ChatMessageListVC.h"
#import "MessageSaver.h"
#import "ChatMessage.h"
#import "MessageTypeDesign9Cell.h"
#import "MessageTypeDesign12Cell.h"
#import "MessageTypeDesign13Cell.h"
#import "MessageTypeDesign15Cell.h"
#import "MessageTypeDesign16Cell.h"
#import "MessageTypeDesign17Cell.h"
#import "MessageTypeDesign18Cell.h"
#import "MessageTypeDesign19Cell.h"
#import "MessageTypeDesign20Cell.h"
#import "TextAndFinanceTableCell.h"
#import <MagicalRecord/MagicalRecord.h>
#import "TextMessage.h"
#import "OrderMessage.h"
#import "APIService.h"

@interface ChatMessageListVC () <UITableViewDelegate, UITableViewDataSource, TopTabBarViewDelegate, UISearchResultsUpdating>

@property (nonatomic, strong) NSArray *messages;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) UIImage *bubleImage;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewTopPosition;

//@property (weak, nonatomic) IBOutlet UILabel *noMessagesLabel;

@property (nonatomic, strong) TopTabBarView *tabBarView;

@property (nonatomic, assign) NSUInteger countNotReadMessage;

@property (nonatomic, strong) UIRefreshControl *refreshControll;

@property (nonatomic, assign) NSInteger messageType;

@property (nonatomic, assign) NSInteger newMessCount;

@property (nonatomic, strong) NSTimer* timer;

@property (nonatomic, strong) UISearchController *resultSearchController;

@end

@implementation ChatMessageListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(invalidateTimer) name:@"InvalidateTimerNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshDataAndStartTimer) name:@"RefreshDataNotification" object:nil];

    self.countNotReadMessage = 0;
    
    self.messageType = 1;
    
    if (self.tabBarController.selectedIndex == 0 || self.tabBarController.selectedIndex == 1) {
        // gesture recognizer
        UIScreenEdgePanGestureRecognizer *lgr = [[UIScreenEdgePanGestureRecognizer alloc] init];
        [lgr addTarget:self action:@selector(dismissVC)];
        lgr.edges = UIRectEdgeLeft;
        [self.view addGestureRecognizer:lgr];
    } else {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BackButton"] style:UIBarButtonItemStylePlain target:self action:@selector(popVC)];
        self.navigationItem.leftBarButtonItem.tintColor = [UIColor blackColor];
    }
    
    [self observeNewMessageNotifications];
    
    [self configureUI];

    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refreshData:) forControlEvents:UIControlEventValueChanged];
    self.refreshControll = refreshControl;

    if (@available(iOS 10.0, *)) {
        self.tableView.refreshControl = self.refreshControll;
    } else {
        [self.tableView addSubview:self.refreshControll];
    }
    
    self.timer = [NSTimer scheduledTimerWithTimeInterval:60.0
                                                  target:self
                                                selector:@selector(refreshData:)
                                                userInfo:nil
                                                 repeats:YES];
    
    self.resultSearchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    
    self.resultSearchController.searchResultsUpdater = self;
    self.resultSearchController.hidesNavigationBarDuringPresentation = false;
    self.resultSearchController.dimsBackgroundDuringPresentation = false;
    self.resultSearchController.searchBar.searchBarStyle = UISearchBarStyleProminent;
    [self.resultSearchController.searchBar sizeToFit];
    self.tableView.tableHeaderView = self.resultSearchController.searchBar;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.refreshControll beginRefreshing];
    [self.tableView setContentOffset:CGPointMake(0, self.tableView.contentOffset.y -self.refreshControll.frame.size.height) animated:YES];
    
    [self refreshData:@"viewWillAppear"];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.timer invalidate];
    
    //set tabbar badge
    for (UITabBarItem* item in self.tabBarController.tabBar.items) {
        if (item.tag == 0) {
            continue;
        }
        
#warning Contracts badge disabled
        NSInteger messCount = 0;
        if (item.tag != 3) {
            messCount = [MessageSaver UnreadMessageCountWithType:item.tag];
        }
        if (messCount == 0) {
            [item setBadgeValue:nil];
        } else {
            [item setBadgeValue:[NSString stringWithFormat:@"%d", messCount]];
        }
    }
}

-(void)invalidateTimer{
    [self.timer invalidate];
}

-(void)refreshDataAndStartTimer {
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self otherRequests];
    });
    [self refreshData:nil];
    
    self.timer = [NSTimer scheduledTimerWithTimeInterval:60.0
                                                  target:self
                                                selector:@selector(refreshData:)
                                                userInfo:nil
                                                 repeats:YES];
}

-(void)otherRequests{
    
    NSString* messageId = @"";
    NSInteger messType = 1;
    
    
    OrderMessage *lastOrderMessage = [OrderMessage MR_findFirstOrderedByAttribute:@"messageId" ascending:NO];
    TextMessage *lastTextMessage = [TextMessage MR_findFirstOrderedByAttribute:@"messageId" ascending:NO];
    FinanceMessage *lastFinanceMessage = [FinanceMessage MR_findFirstOrderedByAttribute:@"messageId" ascending:NO];
    
    if (![self.navigationItem.title isEqualToString:@"Сообщения"]) {
        messType = 1;
        
        messageId = [NSString stringWithFormat:@"%@", lastTextMessage.messageId];
        
        if ([messageId isEqualToString:@"(null)"]){
            messageId = @"";
        }
        
        [self getRequestWithId:messType andMessageId:messageId];
        
    }
    if (![self.navigationItem.title isEqualToString:@"Финансы"]){
        messType = 2;
        
        messageId = [NSString stringWithFormat:@"%@", lastFinanceMessage.messageId];
        
        if ([messageId isEqualToString:@"(null)"]){
            messageId = @"";
        }
        
        [self getRequestWithId:messType andMessageId:messageId];

    }
    
}

-(void)getRequestWithId:(NSInteger)messType andMessageId:(NSString* )messageId {
    
    TextMessage *lastTextMessage = [TextMessage MR_findFirstOrderedByAttribute:@"messageId" ascending:NO];
    FinanceMessage *lastFinanceMessage = [FinanceMessage MR_findFirstOrderedByAttribute:@"messageId" ascending:NO];
    
    [[APIService sharedAPIClient] getMessageWithType:messType messageId:messageId completion:^(NSArray *response) {
//        NSLog(getJSON(response));
        
        for (NSDictionary* message in response) {
            if (messType == 1) {
                if ([lastTextMessage.messageId integerValue] < [message[@"id"] integerValue] || lastTextMessage == nil) {
                    NSArray* messageEntity = [TextMessage MR_findByAttribute:@"messageId" withValue:@([message[@"id"] integerValue])];
                    if (messageEntity.count == 0) {
                        [MessageSaver saveTextMessage:message];
                    }
                }
            } else if (messType == 2){
                if ([lastFinanceMessage.messageId integerValue] < [message[@"id"] integerValue] || lastFinanceMessage == nil) {
                    NSArray* messageEntity = [FinanceMessage MR_findByAttribute:@"messageId" withValue:@([message[@"id"] integerValue])];
                    if (messageEntity.count == 0) {
                        [MessageSaver saveFinanceMessage:message];
                    }
                }
            }
        }
        
    } failure:^(NSError *error) {
        NSLog(@"%@", error.localizedDescription);
    }];
    
}

- (void)refreshData:(id)type {
    if (![type isKindOfClass:[NSTimer class]]) {
        if ([type isKindOfClass:[UIRefreshControl class]] || [type isEqualToString:@"viewWillAppear"]) {
            self.messages = [NSArray array];
        }
    }
    
    NSString *messageId = @"";
    
    TextMessage *lastTextMessage = [TextMessage MR_findFirstOrderedByAttribute:@"messageId" ascending:NO];
    FinanceMessage *lastFinanceMessage = [FinanceMessage MR_findFirstOrderedByAttribute:@"messageId" ascending:NO];

    if ([self.navigationItem.title isEqualToString:@"Сообщения"]) {
        self.messageType = 1;
        messageId = [NSString stringWithFormat:@"%@", lastTextMessage.messageId];

    } else if ([self.navigationItem.title isEqualToString:@"Финансы"]){
        self.messageType = 2;
        messageId = [NSString stringWithFormat:@"%@", lastFinanceMessage.messageId];
    }
    
    if ([messageId isEqualToString:@"(null)"]) {
        messageId = @"";
    }
    
    [[APIService sharedAPIClient] getMessageWithType:self.messageType messageId:messageId completion:^(NSArray *response) {
//        NSLog(getJSON(response));
        
        self.newMessCount = 0;
        for (NSDictionary *message in response) {
            if (self.messageType == 1) {
                if ([lastTextMessage.messageId integerValue] < [message[@"id"] integerValue] || lastTextMessage == nil) {
                    NSArray *messageEntity = [FinanceMessage MR_findByAttribute:@"messageId" withValue:@([message[@"id"] integerValue])];
                    if (messageEntity.count == 0) {
                        [MessageSaver saveTextMessage:message];
                        self.newMessCount++;
                    }
                }
            } else {
                if ([lastFinanceMessage.messageId integerValue] < [message[@"id"] integerValue] || lastFinanceMessage == nil) {
                    NSArray *messageEntity = [FinanceMessage MR_findByAttribute:@"messageId" withValue:@([message[@"id"] integerValue])];
                    if (messageEntity.count == 0) {
                        [MessageSaver saveFinanceMessage:message];
                        self.newMessCount++;
                    }
                }
            }
        }

        if (self.newMessCount > 0) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self prepareContent];
                
                [self.refreshControll endRefreshing];
            });
        } else {
            [self.refreshControll endRefreshing];
        }
        
    } failure:^(NSError *error) {
        [self.refreshControll endRefreshing];
    }];

    if (self.messages.count == 0) {
        [self prepareContent];
    }
}

- (void)prepareContent {
    //set tabbar badge
    for (UITabBarItem *item in self.tabBarController.tabBar.items) {
        if (item.tag == 0) {
            continue;
        }
        
#warning Contacts badge disabled
        NSInteger messCount = 0;
        if (item.tag != 3) {
            messCount = [MessageSaver UnreadMessageCountWithType:item.tag];
        }
        
        if (messCount == 0) {
            [item setBadgeValue:nil];
        }else{
            [item setBadgeValue:[NSString stringWithFormat:@"%d", messCount]];
        }
    }

    if (self.messageType == 1) {
        self.messages = [MessageSaver textChatMessagesIsNew:YES];
        self.isAllMessages = YES;
    } else {
        self.messages = [MessageSaver financeChatMessagesWithType:FinanceMessageTypeUndefined isNew:YES];
        self.isAllMessages = YES;
    }
    
    NSPredicate *filterPredicate;
    NSString *searchText = self.resultSearchController.searchBar.text;
    if (searchText != nil && searchText.length > 0) {
        filterPredicate = [NSPredicate predicateWithFormat:@"text CONTAINS[cd] %@ OR orderNumber CONTAINS[cd] %@", self.resultSearchController.searchBar.text, self.resultSearchController.searchBar.text];
        self.messages = [self.messages filteredArrayUsingPredicate:filterPredicate];
    }
    
    [self.tableView reloadData];
}


- (void)configureUI {
    self.tableView.estimatedRowHeight = 110;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    UIImage *defaultBubleImage = [UIImage imageNamed:@"MessageBackgroundImage"];
    self.bubleImage = [self stretchableImageFromImage:defaultBubleImage withCapInsets:UIEdgeInsetsMake(13, 15, 27, 35)];
    
    if (self.isAllMessages || self.tabBarController.selectedIndex == 1) {
        self.navigationItem.title = @"Сообщения";
    } else if (self.filterCondition) {
        self.navigationItem.title = @"Сообщения";
        self.navigationItem.rightBarButtonItem = nil;
    } else {
        self.navigationItem.title = @"Финансы";
        self.navigationItem.rightBarButtonItem = nil;
    }
}

- (void)setSeenStatusToOrder:(ChatMessage *)chatMessage {
    if (self.tabBarController.selectedIndex == 1) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"messageId = %ld", chatMessage.messageId];
        NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"orderNumber = %@", chatMessage.orderNumber];
        NSPredicate* condition = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:predicate, predicate2,nil]];
        TextMessage* txtMessage = [TextMessage MR_findFirstWithPredicate:condition];
        [MagicalRecord saveWithBlock:^(NSManagedObjectContext * _Nonnull localContext) {
            TextMessage *localMessage = [txtMessage MR_inContext:localContext];
            localMessage.isNew = @(NO);
            [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
        } completion:^(BOOL contextDidSave, NSError * _Nullable error) {
            [[UIApplication sharedApplication] setApplicationIconBadgeNumber:[MessageSaver allUnreadMessagesCount]];
        }];
    } else if (self.tabBarController.selectedIndex == 0) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"messageId = %ld", chatMessage.messageId];
        FinanceMessage* financeMessage = [FinanceMessage MR_findFirstWithPredicate:predicate];
        
        [MagicalRecord saveWithBlock:^(NSManagedObjectContext * _Nonnull localContext) {
            FinanceMessage *localMessage = [financeMessage MR_inContext:localContext];
            localMessage.isNew = @(NO);
            [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
        } completion:^(BOOL contextDidSave, NSError * _Nullable error) {
            [[UIApplication sharedApplication] setApplicationIconBadgeNumber:[MessageSaver allUnreadMessagesCount]];
        }];
    }
}

- (void)selectedSectionChanged:(NSInteger)sectionNumber {
    id headerContent = self.messages[0];
    NSMutableArray *arr = [NSMutableArray arrayWithObject:headerContent];
    [arr addObjectsFromArray:[MessageSaver financeChatMessagesWithType:(FinanceMessageType)sectionNumber isNew:YES]];
    self.messages = [arr copy];
}

- (NSString *)headerCellTextUsingContent:(id)content {
    if ([content isKindOfClass:[FiltringCondition class]]) {
        return [self filteringHeaderCellContent:content];
    } else {
        return [self financeHeaderCellContent:content];
    }
}

-(NSString *)financeHeaderCellContent:(id)content{
    NSString *text = @"";
    NSNumber *currentBalance = [content objectForKey:@"balance"];
    NSDate *date = [content objectForKey:@"date"];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"d.MM.yyyy";
    if (currentBalance.floatValue < 0) {
        text = [NSString stringWithFormat:@"Уважаемый клиент, на %@ сумма задолжности составляет %@ грн", [formatter stringFromDate:date], currentBalance];
    } else {
        text = [NSString stringWithFormat:@"Текущий баланс по счету составляет %@ грн", currentBalance];
    }
    return text;
}

-(NSString *)filteringHeaderCellContent:(id)content{
    FiltringCondition *condition = (FiltringCondition *)content;
    NSString *conditionString = [self nameOfFilteringConditionForType:condition.type];
    return [NSString stringWithFormat:@"Виборка %@: %@", conditionString, condition.filterBy];
}

-(NSString *)nameOfFilteringConditionForType:(FilteringType)type{
    if (type == FilteringTypeStatus) {
        return @"по статусу";
    } else if (type == FilteringTypeOrderNumber){
        return @"по номеру заказа";
    } else {
        return @"по категории груза";
    }
}

#pragma mark - TableView

-(BOOL)tableView:(UITableView *)tableView shouldShowMenuForRowAtIndexPath:(NSIndexPath *)indexPath {
    ChatMessage *message = self.messages[indexPath.row];
    if (message.type == 1) {
        return YES;
    } else {
        return NO;
    }
}

- (BOOL)tableView:(UITableView *)tableView canPerformAction:(SEL)action forRowAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
    if (action == @selector(copy:)){
        ChatMessage *message = self.messages[indexPath.row];
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        if (message.typeDesign == 9) {
            MessageTypeDesign9Cell *cell = [tableView cellForRowAtIndexPath:indexPath];
            [pasteboard setString:cell.textForCopy.text];
        } else if (message.typeDesign == 12) {
            MessageTypeDesign12Cell *cell = [tableView cellForRowAtIndexPath:indexPath];
            [pasteboard setString:cell.textForCopy.text];
        }  else if (message.typeDesign == 13) {
            MessageTypeDesign13Cell *cell = [tableView cellForRowAtIndexPath:indexPath];
            [pasteboard setString:cell.textForCopyLabel.text];
        } else if (message.typeDesign == 15) {
            MessageTypeDesign15Cell *cell = [tableView cellForRowAtIndexPath:indexPath];
            [pasteboard setString:cell.labelForCopy.text];
        } else if (message.typeDesign == 16) {
            MessageTypeDesign16Cell *cell = [tableView cellForRowAtIndexPath:indexPath];
            [pasteboard setString:cell.labelForCopy.text];
        } else if (message.typeDesign == 17) {
            MessageTypeDesign17Cell *cell = [tableView cellForRowAtIndexPath:indexPath];
            [pasteboard setString:cell.textForCopyLabel.text];
        } else if (message.typeDesign == 18) {
            MessageTypeDesign18Cell *cell = [tableView cellForRowAtIndexPath:indexPath];
            [pasteboard setString:cell.textForCopy.text];
        } else if (message.typeDesign == 19) {
            MessageTypeDesign19Cell *cell = [tableView cellForRowAtIndexPath:indexPath];
            [pasteboard setString:cell.textForCopy.text];
        } else if (message.typeDesign == 20) {
            MessageTypeDesign20Cell *cell = [tableView cellForRowAtIndexPath:indexPath];
            [pasteboard setString:cell.textForCopy.text];
        } else {
            TextAndFinanceTableCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            [pasteboard setString:cell.textForCopy.text];
        }
        [self sentAllert];
    }
    return  nil; //(action == @selector(copy:));
}

- (void)tableView:(UITableView *)tableView performAction:(SEL)action forRowAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
    if (action == @selector(copy:)){
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (self.messages.count == 0) return nil;
    
    ChatMessage *message = self.messages[indexPath.row];
    [self setSeenStatusToOrder:message];
    
   if (message.typeDesign == 9) {
        MessageTypeDesign9Cell *cell = [tableView dequeueReusableCellWithIdentifier:@"MessageTypeDesign9Cell"];
        [cell configWithMessage:message];
        return cell;
    } else if (message.typeDesign == 12) {
        MessageTypeDesign12Cell *cell = [tableView dequeueReusableCellWithIdentifier:@"MessageTypeDesign12Cell"];
        [cell configWithMessage:message];
        return cell;
    } else if (message.typeDesign == 15) {
        MessageTypeDesign15Cell *cell = [tableView dequeueReusableCellWithIdentifier:@"MessageTypeDesign15Cell"];
        [cell configWithMessage:message];
        return cell;
    } else if (message.typeDesign == 16) {
        MessageTypeDesign16Cell *cell = [tableView dequeueReusableCellWithIdentifier:@"MessageTypeDesign16Cell"];
        [cell configWithMessage:message];
        return cell;
    } else if (message.typeDesign == 13) {
        MessageTypeDesign13Cell *cell = [tableView dequeueReusableCellWithIdentifier:@"MessageTypeDesign13Cell"];
        [cell configWithMessage:message];
        return cell;
    } else if (message.typeDesign == 17) {
        MessageTypeDesign17Cell *cell = [tableView dequeueReusableCellWithIdentifier:@"MessageTypeDesign17Cell"];
        [cell configWithMessage:message];
        return cell;
    } else if (message.typeDesign == 18) {
        MessageTypeDesign18Cell *cell = [tableView dequeueReusableCellWithIdentifier:@"MessageTypeDesign18Cell"];
        [cell configWithMessage:message];
        return cell;
    } else if (message.typeDesign == 19) {
        MessageTypeDesign19Cell *cell = [tableView dequeueReusableCellWithIdentifier:@"MessageTypeDesign19Cell"];
        [cell configWithMessage:message];
        return cell;
    } else if (message.typeDesign == 20) {
        MessageTypeDesign20Cell *cell = [tableView dequeueReusableCellWithIdentifier:@"MessageTypeDesign20Cell"];
        [cell configWithMessage:message];
        return cell;
    } else {
        TextAndFinanceTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TextAndFinanceTableCell"];
        [cell configWithMessage:message];
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    ChatMessage *message = self.messages[indexPath.row];

    if (message.typeDesign == 9 || message.typeDesign == 18) {
        return 490.0f;
    } else if (message.typeDesign == 12) {
        return 355.0f;
    } else if (message.typeDesign == 15) {
        if ([message.text isEqualToString:@""]) {
            return 500.0f - 360.0f;
        } else {
            return 500.0f;
        }
    } else if (message.typeDesign == 16) {
        return 176.0f;
    } else if (message.typeDesign == 17) {
        return 232.0f;
    } else if (message.typeDesign == 19) {
        return 380.0f;
    } else if (message.typeDesign == 20) {
        return 470.0f;
    }
    return UITableViewAutomaticDimension;
}

    - (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
        return UITableViewAutomaticDimension;
    }
    
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.messages.count;
}

- (void)sentAllert {
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Копирование"
                                                                   message:@"Сообщение успешно скопированно."
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              [self.tableView reloadData];
                                                          }];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}



- (UIImage *)stretchableImageFromImage:(UIImage *)image withCapInsets:(UIEdgeInsets)capInsets {
    UIImage *strachableTemplateImage = [image resizableImageWithCapInsets:capInsets resizingMode:UIImageResizingModeStretch];
    
    return [strachableTemplateImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
}

-(void)setMessages:(NSArray *)messages{
    _messages = messages;
    [self.tableView reloadData];
}
- (IBAction)backButtonTaped:(UIBarButtonItem *)sender {
    [self popVC];
}

-(void)dismissVC {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)popVC {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"NotificationMessageRecieved" object:nil];
}

- (void)updateSearchResultsForSearchController:(nonnull UISearchController *)searchController {
    [self prepareContent];
}

@end
