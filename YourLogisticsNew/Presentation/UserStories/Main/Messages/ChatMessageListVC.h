//
//  ChatMessageListVC.h
//  YourLogistics
//
//  Created by Aleksandr Ponomarenko on 26.05.16.
//  Copyright © 2016 iCenter. All rights reserved.
//
//#import <JSQMessagesViewController/JSQMessages.h>
#import "JSQMessages.h"
#import "FiltringCondition.h"
#import "BasicVC.h"

@interface ChatMessageListVC : BasicVC

@property (nonatomic) BOOL isAllMessages;

@property (nonatomic, strong) FiltringCondition *filterCondition;
 
@end
