////
////  MessageCategoriesListVC.m
////  YourLogistics
////
////  Created by Aleksandr on 16.05.16.
////  Copyright © 2016 iCenter. All rights reserved.
////
//
//#import "MessageCategoriesListVC.h"
//#import "ListOfCategories.h"
//#import "MessageCategoryCell.h"
//#import "MessageCategory.h"
//#import "MessageListVC.h"
//#import "MessageSaver.h"
//#import "ChatMessageListVC.h"
//#import "OrderMessage.h"
//#import <MagicalRecord/MagicalRecord.h>
//
//@interface MessageCategoriesListVC () <UITableViewDelegate, UITableViewDataSource>
//
//@property (weak, nonatomic) IBOutlet UITableView *tableView;
//
//@property (weak, nonatomic) IBOutlet UIView *unreadMessagesContainerView;
//
//@property (weak, nonatomic) IBOutlet UILabel *unreadMessagesCountLabel;
//
//@property (nonatomic, strong) ListOfCategories *messageCategories;
//
//@property (weak, nonatomic) IBOutlet UIButton *allMessagesButton;
//
//@property (weak, nonatomic) IBOutlet UILabel *noMessagesLabel;
//
//@property (nonatomic, strong) NSArray *orders;
//
//@end
//
//@implementation MessageCategoriesListVC
//
//- (void)viewDidLoad {
//    [super viewDidLoad];
//    [self observeNewMessageNotifications];
//    
////    self.navigationController.tabBarItem.badgeValue = [NSString stringWithFormat:@"1%d",[MessageSaver UnreadMessageCountWithType:MessageTypeFinance]];
//}
//
//-(void)viewWillAppear:(BOOL)animated{
//    [super viewWillAppear:animated];
//    [self configureUI];
//    
//}
//
//-(void)viewDidAppear:(BOOL)animated {
//    [super viewDidAppear:animated];
//    
//    [self prerareContent];
//    
//    [self.tableView reloadData];
//}
//
//-(void)configureUI{
//    self.tableView.contentInset = UIEdgeInsetsMake(10, 0, 10, 0);
//    BOOL messagesExist = [MessageSaver isMessagesExist];
//    self.allMessagesButton.hidden = !messagesExist;
//    self.noMessagesLabel.hidden = messagesExist;
//}
//
//-(void)prerareContent {
//    if (self.tabBarController.selectedIndex == 0){
//        self.messageCategories = [MessageSaver messageCategoriesWithType:MessageTypeFinance];
//    }else if (self.tabBarController.selectedIndex == 2){
//        self.messageCategories = [MessageSaver messageCategoriesWithType:MessageTypeOrder];
//    }else{
//        self.messageCategories = [MessageSaver messageCategoriesWithType:MessageTypeText];
//    }
//    
//    
//    NSArray *orders = [OrderMessage MR_findAllSortedBy:@"orderNumber,messageId"
//                                             ascending:NO
//                                         withPredicate:nil];
////    [self setSeenStatusToOrders:orders];
//    self.orders = orders;
//    
//    //set tabbar badge
//    for (UITabBarItem* item in self.tabBarController.tabBar.items) {
//        if (item.tag == 0) {
//            continue;
//        }
//        
//#warning Contracts badge disabled
//        NSInteger messCount = 0;
//        if (item.tag != 3) {
//            messCount = [MessageSaver UnreadMessageCountWithType:item.tag];
//        }
//        
//        if (messCount == 0) {
//            [item setBadgeValue:nil];
//        }else{
//            [item setBadgeValue:[NSString stringWithFormat:@"%d", messCount]];
//        }
//    }
//    
////    self.orders = [self groupByOrder:orders];
//}
//
//-(void)setSeenStatusToOrders:(NSArray *)orders{
//    for (OrderMessage *message in orders) {
//        [MagicalRecord saveWithBlock:^(NSManagedObjectContext * _Nonnull localContext) {
//            OrderMessage *localMessage = [message MR_inContext:localContext];
//            localMessage.isNew = @(NO);
//        } completion:^(BOOL contextDidSave, NSError * _Nullable error) {
//            [[UIApplication sharedApplication] setApplicationIconBadgeNumber:[MessageSaver allUnreadMessagesCount]];
//        }];
//    }
//}
//
//-(NSArray *)groupByOrder:(NSArray *)toGroup{
//    OrderMessage *previousMessage;
//    NSMutableArray *grouped = [NSMutableArray array];
//    for (OrderMessage *message in toGroup) {
////        NSLog(@"%@ %@", message.messageId, message.status);
//        if (![previousMessage.orderNumber isEqualToString:message.orderNumber]) {
//            [grouped addObject:message];
//        }
//        previousMessage = message;
//    }
//    return [grouped copy];
//}
//
//- (IBAction)showAllMessagesTapped:(UIButton *)sender {
//    [self performSegueWithIdentifier:@"ShowChatMessagesList" sender:nil];
//}
//
//-(void)setNumberOfAllNewMessages:(NSUInteger)newMessagesCount{
//    self.unreadMessagesContainerView.hidden = newMessagesCount == 0;
//    self.unreadMessagesCountLabel.text = [NSString stringWithFormat:@"%d", newMessagesCount];
//    
//}
//
//-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
//    return self.messageCategories.categories.count;
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
//    return 1;
//}
//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
//    MessageCategory *category = self.messageCategories.categories[indexPath.section];
//    MessageCategoryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MessageCategoryCell"];
//    cell.categoryTitleLabel.text = category.name;
//    cell.categoryImageView.image = [UIImage imageNamed:[category categoryImageName]];
//    [cell setNumberOfNewMessages:category.numberOfNewMessages];
//    
//    cell.bgView.layer.shadowColor = [[UIColor colorWithRed:188/255.f green:188/255.f blue:188/255.f alpha:.5f] CGColor];
//    cell.bgView.layer.shadowOffset = CGSizeMake(0,2);
//    cell.bgView.layer.shadowRadius = 2;
//    cell.bgView.layer.shadowOpacity = 1.f;
//    cell.bgView.layer.masksToBounds = NO;
//    
//    cell.layer.rasterizationScale = [[UIScreen mainScreen] scale];
//    cell.layer.shouldRasterize = YES;
//    
//    return cell;
//}
//
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
//    
//    return 5.f;
//}
//
//-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
//    
//    UIView* view = [[UIView alloc] initWithFrame:CGRectZero];
//    [view setBackgroundColor:[UIColor clearColor]];
//    
//    return view;
//}
//
//-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//    MessageCategory *category = self.messageCategories.categories[indexPath.section];
//    if (category.type == MessageCategoryTypeFinance) {
//        [self performSegueWithIdentifier:@"ShowChatMessagesList" sender:category];
//    } else {
//        [self performSegueWithIdentifier:@"ShowOrderList" sender:category];
//    }
//}
//
//#pragma mark - Navigation
//
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    if ([segue.identifier isEqualToString:@"ShowOrderList"]) {
//        MessageListVC *mlvc = segue.destinationViewController;
//        mlvc.category = sender;
//    } else if ([segue.identifier isEqualToString:@"ShowChatMessagesList"]){
//        ChatMessageListVC *cmlvc = segue.destinationViewController;
//        cmlvc.isAllMessages = sender == nil;
//    }
//}
//
//-(void)setMessageCategories:(ListOfCategories *)messageCategories{
//    _messageCategories = messageCategories;
//    [self.tableView reloadData];
//    [self setNumberOfAllNewMessages:messageCategories.numberOfAllNewMessages];
//    [self configureUI];
//}
//
//-(void)updateUIOnMessageRecieving{
//    NSLog(@"update categories");
//    [self prerareContent];
//}
//
//@end
