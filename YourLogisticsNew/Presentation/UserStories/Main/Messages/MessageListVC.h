//
//  MessageListVC.h
//  YourLogistics
//
//  Created by Aleksandr on 20.05.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import "BasicVC.h"
#import "MessageCategory.h"

@interface MessageListVC : BasicVC

@property (nonatomic, strong) MessageCategory *category;

@end
