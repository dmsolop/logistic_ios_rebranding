//
//  SendPhotoViewController.m
//  YourLogisticsNew
//
//  Created by Roma Dudka on 20.05.2018.
//  Copyright © 2018 iCenter. All rights reserved.
//

#import "SendPhotoViewController.h"
#import "Transit.h"
#import "APIService.h"

@interface SendPhotoViewController () <UIPickerViewDelegate, UIPickerViewDataSource, UIGestureRecognizerDelegate, UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UIPickerView *inputPicker;
@property (weak, nonatomic) IBOutlet UIPickerView *outputPicker;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;

@property (weak, nonatomic) IBOutlet UISearchBar *inputSearchBar;
@property (weak, nonatomic) IBOutlet UISearchBar *outputSearchBar;

@property (strong,nonatomic) UITapGestureRecognizer* panRecognizer;

@property (strong, nonatomic) NSMutableArray *transitList;
@property (strong, nonatomic) UIImage *image;
@property (strong, nonatomic) Transit *inputSelectedTransit;
@property (strong, nonatomic) Transit *outputSelectedTransit;

@end

@implementation SendPhotoViewController

- (void)configureImage:(UIImage *)image {
    self.image = image;
}

- (void)configureTransitList:(NSArray *)transitList {
    self.transitList = transitList.mutableCopy;
    [self.transitList insertObject:[[Transit alloc] init] atIndex:0];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(hideKeyboard:)];
    self.panRecognizer = tap;
    self.panRecognizer.delegate = self;
    [self.view addGestureRecognizer:tap];
    
    self.imageView.image = self.image;
    
    self.inputPicker.dataSource = self;
    self.outputPicker.dataSource = self;
    
    self.inputPicker.delegate = self;
    self.outputPicker.delegate = self;
    
    self.inputSearchBar.delegate = self;
    self.outputSearchBar.delegate = self;
    
    self.inputSelectedTransit = self.transitList.firstObject;
    self.outputSelectedTransit = self.transitList.firstObject;
    [self validateButton];
}

-(void)hideKeyboard:(UIPanGestureRecognizer *)sender{
    [self.inputSearchBar resignFirstResponder];
    [self.outputSearchBar resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)numberOfComponentsInPickerView:(nonnull UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(nonnull UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return self.transitList != nil ? self.transitList.count : 0;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    Transit *transit = self.transitList[row];
    return transit != nil ? transit.name : @"";
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if (pickerView.tag == 1) {
        self.inputSelectedTransit = self.transitList[row];
    } else {
        self.outputSelectedTransit = self.transitList[row];
    }
    [self validateButton];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"SELF.name contains[cd] %@", searchText];
    NSArray *filteredArray = [self.transitList filteredArrayUsingPredicate:bPredicate];
    if (filteredArray.count) {
        NSUInteger index = [self.transitList indexOfObject:filteredArray.firstObject];
        if (searchBar.tag == 1) {
            [self.inputPicker selectRow:index inComponent:0 animated:YES];
            [self pickerView:self.inputPicker didSelectRow:index inComponent:0];
        } else if (searchBar.tag == 2) {
            [self.outputPicker selectRow:index inComponent:0 animated:YES];
            [self pickerView:self.outputPicker didSelectRow:index inComponent:0];
        }
    }
}

- (IBAction)cancelButtonAction:(UIButton *)sender {
    [self dismissViewControllerAnimated:true completion:nil];
}

- (IBAction)sendButtonTapped:(UIButton *)sender {
    if (self.inputSelectedTransit.guid == nil || self.outputSelectedTransit.guid == nil)
    {
        UIAlertController *errorAlert = [UIAlertController alertControllerWithTitle:@"Уведомление"
                                                                            message:@"Внесите данные Вход/Выход"
                                                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        }];
        [errorAlert addAction:okAction];
        [self presentViewController:errorAlert animated:YES completion:nil];
        
        return;
    }
    
    UIView* dimingView = [self addLoadingActivityView];
    
    [[APIService sharedAPIClient] postImage:self.image
                              enterTranstit:self.inputSelectedTransit
                                exitTransit:self.outputSelectedTransit
                                 completion:^(NSString *message) {
                                     [dimingView removeFromSuperview];
                                     UIAlertController *errorAlert = [UIAlertController alertControllerWithTitle:@"Уведомление"
                                                                                                         message:@"Изображение загружено успешно"
                                                                                                  preferredStyle:UIAlertControllerStyleAlert];
                                     UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
                                         [self dismissViewControllerAnimated:YES completion:nil];
                                     }];
                                     [errorAlert addAction:okAction];
                                     [self presentViewController:errorAlert animated:YES completion:nil];
                                 } failure:^(NSError *error) {
                                     [dimingView removeFromSuperview];
                                     UIAlertController *errorAlert = [UIAlertController alertControllerWithTitle:@"Ошибка" message:@"Не удается загрузить изображение!" preferredStyle:UIAlertControllerStyleAlert];
                                     UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
                                         [self dismissViewControllerAnimated:YES completion:nil];
                                     }];
                                     [errorAlert addAction:okAction];
                                     
                                     [self presentViewController:errorAlert animated:YES completion:nil];
                                 }];
}

- (void)validateButton {
    [self.sendButton setEnabled: self.inputSelectedTransit != nil && self.outputSelectedTransit != nil];
}

- (UIView *)addLoadingActivityView {
    UIView *dimingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    dimingView.backgroundColor = [UIColor blackColor];
    dimingView.alpha = 0.5;
    [self.view addSubview:dimingView];
    CGRect indicatorFrame = CGRectMake(self.view.center.x - 10.f, self.view.center.y - 10 - 64, 20, 20);
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithFrame:indicatorFrame];
    [indicator startAnimating];
    [dimingView addSubview:indicator];
    return dimingView;
}

@end
