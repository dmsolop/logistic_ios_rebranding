//
//  SendPhotoViewController.h
//  YourLogisticsNew
//
//  Created by Roma Dudka on 20.05.2018.
//  Copyright © 2018 iCenter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SendPhotoViewController : UIViewController

- (void)configureImage:(UIImage *)image;
- (void)configureTransitList:(NSArray *)transitList;

@end
