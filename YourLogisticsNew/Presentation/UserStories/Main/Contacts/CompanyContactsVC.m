//
//  CompanyContactsVC.m
//  YourLogistics
//
//  Created by Aleksandr on 04.05.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import "CompanyContactsVC.h"
#import "APIService.h"

@interface CompanyContactsVC ()

@property (weak, nonatomic) IBOutlet UILabel *firstAddressLabel;

@property (weak, nonatomic) IBOutlet UILabel *secondAddressLabel;

@property (weak, nonatomic) IBOutlet UIButton *firstPhoneButton;

@property (weak, nonatomic) IBOutlet UIButton *secondPhoneButton;

@property (weak, nonatomic) IBOutlet UITextView *emailTextView;
@end

@implementation CompanyContactsVC

- (void)viewDidLoad {
    [super viewDidLoad];
//    [[APIService sharedAPIClient] getCompanyContactsWithCompletion:^(NSDictionary *contacts) {
//        
//        [self updateUIIfneededWithContacts:contacts];
//        
//    } failure:^(NSError *error) {
//        NSLog(@"Error while getting contacts");
//    }];
    self.emailTextView.linkTextAttributes = @{NSForegroundColorAttributeName : [UIColor blackColor]};
    
}

-(void)updateUIIfneededWithContacts:(NSDictionary *)contacts{
    if (contacts[@"address1"])
        self.firstAddressLabel.text = contacts[@"address1"];
    if (contacts[@"address2"])
        self.secondAddressLabel.text = contacts[@"address2"];
    if (contacts[@"phone1"])
        [self.firstPhoneButton setTitle:contacts[@"phone1"] forState:UIControlStateNormal];
    if (contacts[@"phone2"])
        [self.secondPhoneButton setTitle:contacts[@"phone2"] forState:UIControlStateNormal];
//    if (contacts[@"email"])
//        [self.emailButton setTitle:contacts[@"email"] forState:UIControlStateNormal];
}
//
//-(void)updateAddressForLabel:(UILabel *)label withAddress:(NSString *)newAddress{
//    NSString *address = [label.text stringByReplacingOccurrencesOfString:@"\n" withString:@""];
//    if (![address isEqualToString:newAddress] && newAddress) {
//        label.text = newAddress;
//    }
//}

- (IBAction)callPhoneNumber:(UIButton *)sender {
    NSString *phoneNumber = [sender.titleLabel.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    phoneNumber = [@"tel://" stringByAppendingString:phoneNumber];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
}

@end
