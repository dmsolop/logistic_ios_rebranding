//
//  MainMenuVC.m
//  YourLogistics
//
//  Created by Aleksandr on 04.05.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import "MainMenuVC.h"
#import <SDVersion/SDiOSVersion.h>
//#import <Realm/Realm.h>
#import <RLMRealm.h>
#import "Utilities.h"
#import "MessageSaver.h"
#import "AuthorizationVC.h"
#import "APIService.h"
#import "ChatMessage.h"
#import "UIButton+Badge.h"
#import "ChatMessageListVC.h"
#import <MagicalRecord/MagicalRecord.h>

#include "TargetConditionals.h"
#import <UserNotifications/UserNotifications.h>

#import "OrderMessage.h"
#import "FinanceMessage.h"
#import "TextMessage.h"
#import "NewsRead.h"
//#import "Report+CoreDataClass.h"
#import "NewsArticle.h"
#import "SendPhotoViewController.h"
#import "UIImage+vImageScaling.h"
#import "NSUserDefaults+DSExtension.h"
#import "Constants.h"
#import "ColorPallet.h"
#import "SharedManager.h"

#import "ReportRLM.h"
#import "ManagerRLM.h"
#import "OrderRLM.h"
#import "DayRLM.h"
#import "ClientDebitRLM.h"

@interface MainMenuVC () <UINavigationControllerDelegate, UIImagePickerControllerDelegate>

//@property (weak, nonatomic) IBOutlet UIButton *myLogisticsButton;

//@property (weak, nonatomic) IBOutlet UIView *tileButtonsView;

//@property (weak, nonatomic) IBOutlet UIButton *servicesButton;
//@property (weak, nonatomic) IBOutlet UIButton *calculationButton;
//@property (weak, nonatomic) IBOutlet UIButton *contactsButton;

//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *myLogisticsButtonHeight;

//@property (weak, nonatomic) IBOutlet UIView *unreadMessagesContainerView;

//@property (weak, nonatomic) IBOutlet UILabel *unreadMessagesCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *clientCodeLabel;
@property (weak, nonatomic) IBOutlet UILabel *newsLabel;

@property (weak, nonatomic) IBOutlet ShadowButton *financeButtons;
@property (weak, nonatomic) IBOutlet ShadowButton *logisticButton;
@property (weak, nonatomic) IBOutlet ShadowButton *allMessageButton;
@property (weak, nonatomic) IBOutlet ShadowButton *newsButton;

@property (strong,nonatomic) UIView *dimingView;
@property (assign,nonatomic) NSInteger dimingViewCount;


@end

@implementation MainMenuVC

Constants *cons = nil;
RLMRealm *realm = nil;
SharedManager *myManager = nil;

- (void)viewDidLoad {
    [super viewDidLoad];
    cons = [[Constants alloc] init];
//    [self configureUI];
    [self observeNewMessageNotifications];
    NSUserDefaults *UD = [NSUserDefaults standardUserDefaults];
    myManager = [SharedManager sharedManager];
    NSString *currentPhoneNumber = [UD takeEntryPhoneNumber];
    NSString *bossNumber1 = cons.bossNumber1;
    NSString *bossNumber2 = cons.bossNumber2;
    NSString *bossNumber3 = cons.bossNumber3;
    NSString *bossNumber4 = cons.bossNumber4;

    if ([currentPhoneNumber isEqualToString:bossNumber1] ||
        [currentPhoneNumber isEqualToString:bossNumber2] ||
        [currentPhoneNumber isEqualToString:bossNumber3] ||
        [currentPhoneNumber isEqualToString:bossNumber4]) {
        myManager.isBossPhoneUsing = YES;
    } else {
        myManager.isBossPhoneUsing = NO;
    }
    
    [self setNewsOrReportButton];

    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateBadges) name:@"UpdateBadges" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(actionButtonsWithNotification:) name:@"EnterScreenNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(incrementBadge:) name:@"IncrementBadgeNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sendRequests) name:@"RefreshDataNotification" object:nil];
    
    self.clientCodeLabel.text = [NSString stringWithFormat:@"#%@", [UD objectForKey:kClientCodeKey]];
    if ([[UD objectForKey:kClientCodeKey] isEqual:@"00416"]) {
        [self getFakeCompanyNews];
    }
    

}

-(void)viewWillAppear:(BOOL)animated{
    realm = [RLMRealm defaultRealm];
    [self cleenRealmDB];
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
//    [self updateMessagesCounter];
    [self sendRequests];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    [self updateBadges];
}

-(void)setNewsOrReportButton {
    
     if (myManager.isBossPhoneUsing){
        self.newsLabel.text = @"Отчеты";
//        [self.newsButton setTintColor: UIColor.redColor];
        self.newsButton.image.image = [UIImage imageNamed:@"Reports"];
    }
}



-(void)incrementBadge:(NSNotification* )notification {
    
    NSInteger tag = [(NSNumber*)[notification object] integerValue];

    switch (tag) {
        case 1:
            [self incrementBadgeFor:self.allMessageButton];
            break;
        case 2:
            [self incrementBadgeFor:self.financeButtons];
            break;
        case 3:
#warning logisticButton badge disable
//            [self incrementBadgeFor:self.logisticButton];
            break;
        default:
            break;
    }
    
}

- (void)setupPositionOfBadgeForButton:(UIButton *)button {
    if ([button isKindOfClass:[ShadowButton class]]) {
        ShadowButton *btn = (ShadowButton *)button;
        
        button.badgeOriginX = CGRectGetMaxX(btn.image.frame) - button.badge.bounds.size.width/3.0;
        button.badgeOriginY = CGRectGetMinY(btn.image.frame) - button.badge.bounds.size.height/2.0;
        
        if ([button isEqual:self.financeButtons]) {
            button.badgeOriginY += 11;
        }
        
        if ([button isEqual:self.logisticButton]) {
            button.badgeOriginY += 36;
        }
    } else {
        button.badgeOriginX = CGRectGetMaxX(button.imageView.frame) - button.badge.bounds.size.width;
        button.badgeOriginY = CGRectGetMinY(button.imageView.frame);// + button.badge.bounds.size.height;
    }
}

-(void)updateBadges {
    [self setBadgeFor:self.financeButtons withType:MessageTypeFinance];
    [self setBadgeFor:self.allMessageButton withType:MessageTypeText];
    [self setBadgeFor:self.newsButton withType:MessageTypeNews];
    
#warning logisticButton badge disable
    //    [self setBadgeFor:self.logisticButton withType:MessageTypeOrder];
}

- (void)setBadgeFor:(UIButton *)btn withType:(MessageType)type {
    [self setBadgeFor:btn withNumber:[MessageSaver UnreadMessageCountWithType:type]];
}

- (void)setBadgeFor:(UIButton *)btn withNumber:(NSInteger)number {
    btn.badgeValue = @(number).stringValue;
    [self setupPositionOfBadgeForButton:btn];
}

- (void)incrementBadgeFor:(UIButton *)btn {
    [self setBadgeFor:btn withNumber:btn.badgeValue.integerValue + 1];
}

//-(void)updateMessagesCounter{
//    NSUInteger numberOfAllUnreadMessage = [MessageSaver allUnreadMessagesCount];
//    self.unreadMessagesContainerView.hidden = numberOfAllUnreadMessage == 0;
//    self.unreadMessagesCountLabel.text = [NSString stringWithFormat:@"%d", numberOfAllUnreadMessage];
//}

//-(void)configureUI{
//    for (UIView *view in self.tileButtonsView.subviews)
//        [Utilities addShadowToView:view];
//    [Utilities addShadowToView:self.myLogisticsButton];
//    if ([SDiOSVersion deviceSize] == Screen3Dot5inch) {
//        [self.servicesButton setImage:[UIImage imageNamed:@"ServiceButton_3_5"] forState:UIControlStateNormal];
//        [self.calculationButton setImage:[UIImage imageNamed:@"CalculationButton_3_5"] forState:UIControlStateNormal];
//        [self.newsButton setImage:[UIImage imageNamed:@"NewsButton_3_5"] forState:UIControlStateNormal];
//        [self.contactsButton setImage:[UIImage imageNamed:@"ContactButton_3_5"] forState:UIControlStateNormal];
//        self.myLogisticsButtonHeight.constant = 90;
//    }
//}

-(void)configureNavBar{
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],
                                                                      NSFontAttributeName:[UIFont fontWithName:@"PFDinTextCompPro-Thin" size:22]}];
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
}
//- (IBAction)myLogisticsButtonTapped:(UIButton *)sender {
//    if ([APIService sharedAPIClient].isUserAuthorized) {
//        [self performSegueWithIdentifier:@"ShowMessageCategoriesList" sender:nil];
//    } else {
//        [self performSegueWithIdentifier:@"ShowAuthorizationVC" sender:nil];
//    }
//}


//-(void)updateUIOnMessageRecieving{
//    [self updateMessagesCounter];
//}

- (IBAction)logoutAction:(UIButton *)sender {
    
    [APIService cabinetLogout];
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
//    [OrderMessage MR_deleteAllMatchingPredicate:nil];
//    [FinanceMessage MR_deleteAllMatchingPredicate:nil];
//    [TextMessage MR_deleteAllMatchingPredicate:nil];
//    [NewsRead MR_deleteAllMatchingPredicate:nil];

//    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];

    UIViewController *vc = [[UIStoryboard storyboardWithName:@"Authorization" bundle:nil] instantiateViewControllerWithIdentifier:@"SplashNav"];
    [[[UIApplication sharedApplication].delegate window] setRootViewController:vc];
}

- (IBAction)actionButtons:(UIButton *)sender {
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UITabBarController* tabBar = [sb instantiateViewControllerWithIdentifier:@"TabBarVC"];
    
    if (tabBar){
            switch (sender.tag) {
                case 0:
                    tabBar.selectedIndex = 3;
                    break;
                case 1:
                    tabBar.selectedIndex = 0;
                    break;
                case 2:
                    tabBar.selectedIndex = 1;
                    break;
                case 3:
                    tabBar.selectedIndex = 2;
                    break;
                default:
                    tabBar.selectedIndex = 0;
                    break;
            }
            
            tabBar.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;

            [self presentViewController:tabBar animated:YES completion:nil];
        
    } else {
        
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Error Tabbar"
                                       message:@"TabBar not created yet."
                                       preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
           handler:^(UIAlertAction * action) {}];

        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
    

}

- (void)actionButtonsWithNotification:(NSNotification *)notification {
    
    NSInteger tag = [(NSNumber*)[notification object] integerValue];
    
    UITabBarController* tabBar = [[self storyboard] instantiateViewControllerWithIdentifier:@"TabBarVC"];
    
    switch (tag) {
        case 1:
            tabBar.selectedIndex = 1;
            break;
        case 2:
            tabBar.selectedIndex = 0;
            break;
        case 3:
            tabBar.selectedIndex = 2;
            break;
        default:
            tabBar.selectedIndex = 0;
            break;
    }
    
    tabBar.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    [self presentViewController:tabBar animated:YES completion:NULL];
}

-(void)getFakeCompanyNews{
    
    [[APIService sharedAPIClient] getCompanyNewsWithCompletion:^(NSArray *articles) {
        NSLog(@"Success");
        
        NSMutableArray* array = [NSMutableArray array];
        NSMutableDictionary* dict1 = [NSMutableDictionary dictionary];
        NSMutableDictionary* dict2 = [NSMutableDictionary dictionary];
        NSMutableDictionary* dict3 = [NSMutableDictionary dictionary];
        NSMutableDictionary* mess1 = [NSMutableDictionary dictionary];
        NSMutableDictionary* mess2 = [NSMutableDictionary dictionary];

        [mess1 setValue:@"2" forKey:@"balance"];
        [mess1 setValue:@"dt" forKey:@"operation"];
        [mess1 setValue:@"100500" forKey:@"sum"];
        [mess1 setValue:@"Баланс" forKey:@"text"];

        [dict1 setValue:[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970]*1000] forKey:@"id"];
        [dict1 setValue:@"2" forKey:@"type"];
        [dict1 setValue:mess1 forKey:@"message"];
        
        
        [mess2 setValue:@"розы181" forKey:@"cargo"];
        [mess2 setValue:@"000000008" forKey:@"operatorID"];
        [mess2 setValue:@"23654-474" forKey:@"order_number"];
        [mess2 setValue:@"00229" forKey:@"service"];
        [mess2 setValue:@"оформляем" forKey:@"status"];
        [mess2 setValue:@"000000141" forKey:@"statusId"];
        [mess2 setValue:@"Статус по вашему заказу изменился на оформляем" forKey:@"text"];
        
        [dict2 setValue:[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970]*1000] forKey:@"id"];
        [dict2 setValue:@"3" forKey:@"type"];
        [dict2 setValue:mess2 forKey:@"message"];
    
        
        [dict3 setValue:[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970]*1000] forKey:@"id"];
        [dict3 setValue:@"1" forKey:@"type"];
        [dict3 setValue:@"Заказ оформлен" forKey:@"message"];
        
        [array addObject:dict3];
        [array addObject:dict1];
        [array addObject:dict2];


//        for (NSDictionary* obj in array) {
//            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                if ([SDiOSVersion versionGreaterThanOrEqualTo:@"10.0"]) {
//                    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
//                    UNMutableNotificationContent *content = [UNMutableNotificationContent new];
//                    content.title = @"Твоя логистика";
//                    content.body = @"Новое уведоление";
//                    content.sound = [UNNotificationSound defaultSound];
//                    content.userInfo = obj;
//
//                    UNTimeIntervalNotificationTrigger *trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:1 repeats:NO];
//
//                    NSString *identifier = @"UYLLocalNotification";
//                    UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:identifier content:content trigger:trigger];
//
//                    [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
//                        if (error != nil) {
//                            NSLog(@"Something went wrong: %@",error);
//                        }
//                    }];
//
//                }else{
//                    // Schedule the notification
//                    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
//                    localNotification.alertBody = @"Новое уведоление";
//                    localNotification.alertTitle = @"Твоя логистика";
//                    localNotification.userInfo = obj;
//                    localNotification.soundName = UILocalNotificationDefaultSoundName;
//                    localNotification.timeZone = [NSTimeZone defaultTimeZone];
//
//                    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
//                }
//            });
//        }
        
        NSDictionary *messageDic = dict2[@"message"];
        NSArray* messageEntity = [OrderMessage MR_findByAttribute:@"messageId" withValue:@([dict2[@"id"] integerValue])];
        if (messageEntity.count == 0) {
            [MagicalRecord saveWithBlock:^(NSManagedObjectContext * _Nonnull localContext) {
                OrderMessage *messageEntity = [OrderMessage MR_createEntityInContext:localContext];
                messageEntity.messageId = @([dict2[@"id"] integerValue]);
                messageEntity.serviceId = @([OrderMessage serviceTypeFromCode:messageDic[@"service"]]);
                messageEntity.status = messageDic[@"status"];
                if ([messageEntity.serviceId isEqual:@(ServiceTypeCourier)]) {
                    messageEntity.operatorId = messageDic[@"operatorID"];
                }
                messageEntity.messageContent = messageDic[@"text"];
                messageEntity.category = messageDic[@"cargo"];
                messageEntity.orderNumber = messageDic[@"order_number"];
                messageEntity.isNew = @(YES);
            } completion:^(BOOL contextDidSave, NSError * _Nullable error) {
                UIAlertController *con = [UIAlertController alertControllerWithTitle:@"Новое сообщение" message:nil preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *ac = [UIAlertAction actionWithTitle:@"ОК" style:UIAlertActionStyleDefault handler:nil];
                [con addAction:ac];
                [self presentViewController:con animated:YES completion:nil];
                
                [[UIApplication sharedApplication] setApplicationIconBadgeNumber:[MessageSaver allUnreadMessagesCount]];
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationMessageRecieved object:nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateBadges" object:nil];
            }];
        }
        
        
    } failure:^(NSError *error) {
        UIAlertController *errorAlert = [UIAlertController alertControllerWithTitle:@"Ошибка" message:@"Отсутсвует интернет соединение!" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK"
                                                           style:UIAlertActionStyleDefault
                                                         handler:nil];
        [errorAlert addAction:okAction];
        
        [self presentViewController:errorAlert animated:YES completion:nil];
    }];
    
}

-(void)sendRequests {
    
    NSString* messageId = @"";
    NSString* messageId2 = @"";
    NSString* messageId3 = @"";
    NSString* messageId5 = @"";
    
    OrderMessage *lastOrderMessage = [OrderMessage MR_findFirstOrderedByAttribute:@"messageId" ascending:NO];
    TextMessage *lastTextMessage = [TextMessage MR_findFirstOrderedByAttribute:@"messageId" ascending:NO];
    FinanceMessage *lastFinanceMessage = [FinanceMessage MR_findFirstOrderedByAttribute:@"messageId" ascending:NO];
    ReportRLM *lastReport = [[[ReportRLM allObjects] sortedResultsUsingKeyPath:@"reportId" ascending:NO] firstObject];
    
    messageId = [NSString stringWithFormat:@"%@", lastTextMessage.messageId];
    
    if ([messageId isEqualToString:@"(null)"]){
        messageId = @"";
    }
    
    [self getRequestWithId:1 andMessageId:messageId];
    
    
    messageId2 = [NSString stringWithFormat:@"%@", lastFinanceMessage.messageId];
    
    if ([messageId2 isEqualToString:@"(null)"]){
        messageId2 = @"";
    }
    
    [self getRequestWithId:2 andMessageId:messageId2];
    
    
    messageId3 = [NSString stringWithFormat:@"%@", lastOrderMessage.messageId];
    
    if ([messageId3 isEqualToString:@"(null)"]){
        messageId3 = @"0";
    }
    
    messageId5 = [NSString stringWithFormat:@"%ld", lastReport.reportId];
    
    if ([messageId5 isEqualToString:@"(null)"]){
        messageId5 = @"";
    }
    
    [self getRequestWithId:5 andMessageId:messageId5];

    
    [self addLoadingActivityView];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[APIService sharedAPIClient] getMessageWithType:3 messageId:messageId3 completion:^(NSArray *response) {
            [self removeLoadingActivityView];
            
            [MagicalRecord saveWithBlock:^(NSManagedObjectContext * _Nonnull localContext) {
                if (response.count > 0) {
                    for (NSDictionary *infoToParse in response) {
                        NSNumber *messageId = @([infoToParse[@"id"] integerValue]);
                        
                        OrderMessage *messageEntity = [OrderMessage MR_findFirstByAttribute:@"messageId" withValue:messageId inContext:localContext];
                        
                        if (messageEntity == nil) {
                            NSDictionary *messageDic = infoToParse[@"message"];

                                messageEntity = [OrderMessage MR_createEntityInContext: localContext];
                                
                                messageEntity.messageId = messageId;
                                messageEntity.serviceId = @([OrderMessage serviceTypeFromCode:messageDic[@"service"]]);
                                messageEntity.status = messageDic[@"status"];
                                messageEntity.operatorId = messageDic[@"operatorID"];
                                messageEntity.messageContent = messageDic[@"text"];
                                messageEntity.category = messageDic[@"cargo"];
                                messageEntity.orderNumber = messageDic[@"order_number"];
                                messageEntity.isNew = @(YES);
                                messageEntity.clearanceDate = @([infoToParse[@"clearance_date"] integerValue]);
                        }
                    }
                }
            } completion:^(BOOL contextDidSave, NSError * _Nullable error) {
                [self addLoadingActivityView];
                [[APIService sharedAPIClient] getMessageWithType:4 messageId:@"" completion:^(NSArray *response2) {
                    [self removeLoadingActivityView];

                    if (response2.count > 0) {
                        NSArray *orders = [OrderMessage MR_findAllSortedBy:@"orderNumber,messageId"
                                                                 ascending:NO
                                                             withPredicate:nil];

                        for (OrderMessage *mess in orders) {
                            NSInteger isTrue = NO;
                            for (id mess2 in response2) {
                                if ([mess.orderNumber isEqualToString: mess2[@"order_number"]]) {
                                    isTrue = YES;
                                    break;
                                }
                            }
                            if (isTrue == NO) {
                                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"orderNumber = %@", mess.orderNumber];
                                [OrderMessage MR_deleteAllMatchingPredicate:predicate];
                            }
                        }
                    }
                    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
                    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:[MessageSaver allUnreadMessagesCount]];
                
                    [self addLoadingActivityView];
                    [[APIService sharedAPIClient] getCompanyNewsWithCompletion:^(NSArray *articles) {
                         [self removeLoadingActivityView];
                         
                         [MagicalRecord saveWithBlock:^(NSManagedObjectContext * _Nonnull localContext) {
                             [articles enumerateObjectsUsingBlock: ^(NewsArticle *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                                 [NewsRead MR_findFirstOrCreateByAttribute: @"newsId" withValue: @(obj.ID) inContext: localContext];
                             }];
                         } completion:^(BOOL contextDidSave, NSError * _Nullable error) {
                         }];
                     } failure:^(NSError *error) {
                         [self removeLoadingActivityView];
                     }];
                    
                } failure:^(NSError *error) {
                    [self removeLoadingActivityView];
//                    NSLog(@"%@", error.localizedDescription);
                }];
            }];
        } failure:^(NSError *error) {
//            NSLog(@"%@", error.localizedDescription);
            
            [self removeLoadingActivityView];
        }];
    });
    [self updateBadges];
}

- (void)getRequestWithId:(NSInteger)messType andMessageId:(NSString *)messageId {
    [self addLoadingActivityView];
    
    TextMessage *lastTextMessage = [TextMessage MR_findFirstOrderedByAttribute:@"messageId" ascending:NO];
    FinanceMessage *lastFinanceMessage = [FinanceMessage MR_findFirstOrderedByAttribute:@"messageId" ascending:NO];
    ReportRLM *lastReport = [[[ReportRLM allObjects] sortedResultsUsingKeyPath:@"reportId" ascending:NO] firstObject];
    
    [[APIService sharedAPIClient] getMessageWithType:messType messageId:messageId completion:^(NSArray *response) {
        [self removeLoadingActivityView];
//        NSLog(getJSON(response));
        
        for (NSDictionary *message in response) {
            if (messType == 1) {
                if ([lastTextMessage.messageId integerValue] < [message[@"id"] integerValue] || lastTextMessage == nil) {
                    [MessageSaver saveTextMessage:message];
                }
            } else if (messType == 2) {
                if ([lastFinanceMessage.messageId integerValue] < [message[@"id"] integerValue] || lastFinanceMessage == nil) {
                    [MessageSaver saveFinanceMessage:message];
                }
            } else if (messType == 5) {
                if (lastReport.reportId < [message[@"id"] integerValue] || lastReport == nil) {
                    [MessageSaver saveReportRLM:message];

                }
            }
        }
        
    } failure:^(NSError *error) {
        [self removeLoadingActivityView];
        NSLog(@"%@", error.localizedDescription);
    }];
    
//    RLMResults<ReportRLM *> *allReportsAnd = [ReportRLM allObjects];
//    NSLog(@"");
}

- (void)removeLoadingActivityView {
    self.dimingViewCount--;
    
    if (self.dimingViewCount <= 0) {
        self.dimingViewCount = 0;
        
        [UIView animateWithDuration:0.2 animations:^{
            self.dimingView.alpha = 0.0;
        } completion:^(BOOL finished) {
            [self.dimingView removeFromSuperview];
            self.dimingView = nil;
        }];
    }
}

- (void)addLoadingActivityView {
    self.dimingViewCount++;

    if (self.dimingView) {
        return;
    }
    
    self.dimingView = [[UIView alloc] initWithFrame:self.view.frame];
    self.dimingView.backgroundColor = [UIColor blackColor];
    CGRect indicatorFrame = CGRectMake(self.view.center.x - 10.f, self.view.center.y - 10 - 64, 20, 20);
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithFrame:indicatorFrame];
    [indicator startAnimating];
    
    self.dimingView.alpha = 0.0;
    
    [self.dimingView addSubview:indicator];
    [self.view addSubview:self.dimingView];
    
    [UIView animateWithDuration:0.2 animations:^{
        self.dimingView.alpha = 0.5;
    }];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:true completion:^{
        UIImage *image = info[UIImagePickerControllerOriginalImage];
        
        if (image) {            
            UIImage *scaledImage = [image vImageScaledImageWithSize:CGSizeMake(1920, 1920) contentMode:UIViewContentModeScaleAspectFit];
            
            SendPhotoViewController *sendVC = [[self storyboard] instantiateViewControllerWithIdentifier:@"SendPhotoViewController"];
            
            [[APIService sharedAPIClient] getTransitListWithCompletion:^(NSArray *transitList) {
                [sendVC configureImage:scaledImage];
                [sendVC configureTransitList:transitList];

                [self presentViewController:sendVC
                                   animated:YES
                                 completion:nil];
            } failure:^(NSError *error) {
                UIAlertController *errorAlert = [UIAlertController alertControllerWithTitle:@"Ошибка" message:@"Отсутсвует интернет соединение!" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK"
                                                                   style:UIAlertActionStyleDefault
                                                                 handler:nil];
                [errorAlert addAction:okAction];
                
                [self presentViewController:errorAlert animated:YES completion:nil];
            }];
        }
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:true completion:nil];
}

-(void)cleenRealmDB {
    
//    [realm transactionWithBlock:^{
//    [[RLMRealm defaultRealm]deleteAllObjects];
//        }];

    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"reportId < %ld", cons.initialTime];
    RLMResults<ReportRLM *> *reports = [[ReportRLM objectsWithPredicate:predicate] sortedResultsUsingKeyPath:@"reportId" ascending:NO];
    
    NSMutableArray *reportsNew = [NSMutableArray new];
    
    for (ReportRLM *repo in reports) {
        [reportsNew addObject:repo];
    }
    
    for (ReportRLM *repo in reportsNew) {
        switch (repo.typeReport) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:{
                [realm transactionWithBlock:^{
                    [realm deleteObjects:repo.managers];
                }];
            }
            case 8:
                for (ManagerRLM *manager in repo.managers){
                    [realm transactionWithBlock:^{
                        [realm deleteObjects:manager.days];
                    }];
                }
                break;
            case 9:{
                [realm transactionWithBlock:^{
                    [realm deleteObjects:repo.orders];
                }];
            }
                break;
            case 10:
                for (ClientDebitRLM *client in repo.clients){
                    [realm transactionWithBlock:^{
                        [realm deleteObjects:client.orders];
                    }];
                }
                break;
            default:
                break;
        }
        [realm transactionWithBlock:^{
            [realm deleteObject:repo];
        }];
    }
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"ShowAuthorizationVC"]) {
        
        AuthorizationVC *avc = [[(UINavigationController *)segue.destinationViewController viewControllers] firstObject];
        avc.delegate = self;
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:@"UpdateBadges"];
}

@end
