//
//  TabBarVC.m
//  YourLogistics
//
//  Created by Bohdan on 12.02.2018.
//  Copyright © 2018 iCenter. All rights reserved.
//

#import "TabBarVC.h"
#import "SharedManager.h"

@interface TabBarVC ()
@end

@implementation TabBarVC


- (void)viewDidLoad {
    [super viewDidLoad];

    SharedManager *myManager = [SharedManager sharedManager];

    if (myManager.isBossPhoneUsing) {
        [[self.tabBar.items objectAtIndex:3] setTitle:@"Отчеты"];
        [[self.tabBar.items objectAtIndex:3] setImage:[UIImage imageNamed:@"tabbar_reports"]];
        [[self.tabBar.items objectAtIndex:3] setSelectedImage:[UIImage imageNamed:@"tabbar_reports"]];
//        [[self.tabBar.items objectAtIndex:3] setBadgeColor:[UIColor greenColor]];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(selectViewController:) name:@"SelectTabIndexNotification" object:nil];
}


- (void)selectViewController:(NSNotification* )notification {
    NSInteger index = [(NSNumber* )[notification object] integerValue];
    
    switch (index) {
        case 1:
            [self setSelectedIndex:1];
            break;
        case 2:
            [self setSelectedIndex:0];
            break;
        case 3:
            [self setSelectedIndex:2];
            break;
        default:
            [self setSelectedIndex:0];
            break;
    }
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


@end
