//
//  BasicVC.m
//  YourLogistics
//
//  Created by Aleksandr on 04.05.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import "BasicVC.h"
#import "APIService.h"
#import "MessageCategoriesListVC.h"
#import "Utilities.h"

#define FirstColor [UIColor colorWithRed:252.f/255.f green:215.f/255.f blue:172.f/255.f alpha:1.0]
#define SecondColor [UIColor colorWithRed:34.f/255.f green:192.f/255.f blue:100.f/255.f alpha:1.0]
#define ThirdColor [UIColor redColor]

@interface BasicVC ()

@end

@implementation BasicVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    self.navigationController.interactivePopGestureRecognizer.delegate = nil;
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:YES];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateBadges" object:nil];
}

-(IBAction)backButtonTapped:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)dismissModal:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

//-(IBAction)logout:(id)sender{
//    [APIService cabinetLogout];
//    if ([self isKindOfClass:[MessageCategoriesListVC class]]) {
//        [self.navigationController popViewControllerAnimated:YES];
//    }
//}

-(TopTabBarView *)addTabBarWithTitles:(NSArray *)titles colors:(NSArray *)colors forDelegate:(id<TopTabBarViewDelegate>)delegate {
    CGRect tabBarFrame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, kDefaultTopTabBarHeight);
    TopTabBarView *tabBar = [[TopTabBarView alloc] initWithFrame:tabBarFrame
                                                   sectionTitles:titles];
    tabBar.delegate = delegate;
    [self.view addSubview:tabBar];
    return tabBar;
}

-(void)observeNewMessageNotifications{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateUIOnMessageRecieving) name:kNotificationMessageRecieved object:nil];
}

-(void)updateUIOnMessageRecieving{
//    NSLog(@"Super method called");
}

- (void)dealloc {
    NSLog(@"dealoc %@", [self class]);
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationMessageRecieved object:nil];
}

@end
