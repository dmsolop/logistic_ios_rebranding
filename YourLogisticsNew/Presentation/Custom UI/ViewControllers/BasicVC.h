//
//  BasicVC.h
//  YourLogistics
//
//  Created by Aleksandr on 04.05.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TopTabBarView.h"

@interface BasicVC : UIViewController

-(TopTabBarView *)addTabBarWithTitles:(NSArray *)titles colors:(NSArray *)colors forDelegate:(id<TopTabBarViewDelegate>)delegate;

-(IBAction)dismissModal:(id)sender;

-(void)updateUIOnMessageRecieving;

-(void)observeNewMessageNotifications;

@end
