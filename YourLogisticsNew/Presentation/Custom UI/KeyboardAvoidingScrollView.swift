//
//  ScrollViewExcludeDelayTouch.swift
//  OdlarYurdu
//
//  Created by VitaliyK on 24.05.17.
//  Copyright © 2017 VitaliyK. All rights reserved.
//

//import TPKeyboardAvoidingScrollView
import UIKit

@objc class KeyboardAvoidingScrollView: TPKeyboardAvoidingScrollView {
    var disableButtonsDelays: Bool = false {
        didSet {
            if disableButtonsDelays {
                delaysContentTouches = false
            }
        }
    }

    override func touchesShouldCancel(in view: UIView) -> Bool {
        if !(view is UIButton) && disableButtonsDelays {
            return true
        }
        return super.touchesShouldCancel(in: view)
    }
}
