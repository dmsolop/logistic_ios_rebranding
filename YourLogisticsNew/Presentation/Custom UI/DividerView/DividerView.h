//
//  DividerView.h
//  YourLogisticsNew
//
//  Created by Dima on 4/1/19.
//  Copyright © 2019 iCenter. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DividerView : UIView

@property (nonatomic, assign)  CGFloat type;

- (instancetype)initWithType:(CGFloat)type;

@end

NS_ASSUME_NONNULL_END
