//
//  DividerView.m
//  YourLogisticsNew
//
//  Created by Dima on 4/1/19.
//  Copyright © 2019 iCenter. All rights reserved.
//

#import "DividerView.h"

@interface DividerView()

@property (nonatomic, assign) CGFloat sizeHeight;

@end

@implementation DividerView

- (instancetype)initWithType:(CGFloat)type
{
    self = [super init];
    if (self) {
        _type = type;
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    
    self.sizeHeight = (self.type == 1 || self.type == 2) ? (self.frame.size.height/15) : 0;
    
    UIBezierPath* curvLinePath = [UIBezierPath bezierPath];
    [curvLinePath moveToPoint:CGPointMake(self.frame.size.width - self.frame.size.width/4, (self.frame.size.height/2) - self.sizeHeight)];
    [curvLinePath addLineToPoint:CGPointMake(self.frame.size.width - self.frame.size.width/4, (self.frame.size.height/2) - self.sizeHeight + 1)];
    [curvLinePath addLineToPoint:CGPointMake(self.frame.size.width/4, (self.frame.size.height/2) - self.sizeHeight + 1)];
    [curvLinePath addLineToPoint:CGPointMake(self.frame.size.width/4, (self.frame.size.height/2) - self.sizeHeight)];
    [curvLinePath closePath];
    
    CAShapeLayer *curvLineMaskLayer = [CAShapeLayer layer];
    [curvLineMaskLayer setPath:curvLinePath.CGPath];
    self.layer.mask = curvLineMaskLayer;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.backgroundColor = UIColor.blackColor;
}

@end
