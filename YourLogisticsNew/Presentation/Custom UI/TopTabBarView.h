//
//  TopTabBarView.h
//  Pioneer UA
//
//  Created by Aleksandr Ponomarenko on 08.02.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import <UIKit/UIKit.h>

static const CGFloat kDefaultTopTabBarHeight = 58.f;

@protocol TopTabBarViewDelegate <NSObject>

-(void)selectedSectionChanged:(NSInteger)sectionNumber;

@end

@interface TopTabBarView : UIView

@property (nonatomic, weak) id<TopTabBarViewDelegate> delegate;

@property (nonatomic, strong) UIFont *labelsFont;

@property (nonatomic) NSInteger selectedSection;

@property (nonatomic, strong) NSArray *sectionsTitles;

@property (nonatomic, strong) NSArray *sectionsColors; //temp

@property (nonatomic, strong) UIColor *sectionColor;

@property (nonatomic, strong) UIColor *selectedSectionColor;

@property (nonatomic, strong) UIColor *sectionTitleColor;

@property (nonatomic, strong) UIColor *selectedSectionTitleColor;

@property (nonatomic, strong) UIColor *sectionUnderlineColor;

@property (nonatomic, strong) UIColor *selectedSectionUnderlineColor;

@property (nonatomic, strong) UIFont *sectionTitleFont;

@property (nonatomic, strong) UIFont *selectedSectionTitleFont;

@property (nonatomic, strong) UIColor *dividerColor;

- (instancetype)initWithFrame:(CGRect)aRect
                sectionTitles:(NSArray *)sectionsTitles
               sectionsColors:(NSArray *)sectionsColors;

- (instancetype)initWithFrame:(CGRect)aRect
                sectionTitles:(NSArray *)sectionsTitles;

@end
