//
//  MessageTypeDesign17Cell.h
//  YourLogisticsNew
//
//  Created by Dima on 6/18/19.
//  Copyright © 2019 iCenter. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MessageTypeDesign17Cell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *textForCopyLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *exportDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *supplyToCustomsLabel;

@property (weak, nonatomic) IBOutlet UILabel *nameValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *exportDateValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *supplyToCustomsValueLabel;

@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *borderViews;

- (void)configWithMessage:(ChatMessage *)message;

@end

NS_ASSUME_NONNULL_END
