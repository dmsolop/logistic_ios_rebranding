//
//  MessageTypeDesign17Cell.m
//  YourLogisticsNew
//
//  Created by Dima on 6/18/19.
//  Copyright © 2019 iCenter. All rights reserved.
//

#import "MessageTypeDesign17Cell.h"
#import "ColorPallet.h"

@interface MessageTypeDesign17Cell()

@property (nonatomic, copy) ColorPallet *colorSet;

@end

@implementation MessageTypeDesign17Cell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)prepareForReuse {
    [super prepareForReuse];
    self.textForCopyLabel.text = @"";
}

- (void)configWithMessage:(ChatMessage *)message {
    NSInteger designCode = message.typeDesign;
    self.colorSet = [ColorPallet new];
    self.colorSet.isNew = message.isNew.boolValue;
    
    
    
    UIFont *myFont = [UIFont fontWithName:@"MuseoSansCyrl-300" size:13];
    
    UIColor *headerColor = [self.colorSet headerSetColorWithCodeDesign:designCode];
    
    NSArray *strings = [message.text componentsSeparatedByString:@"ß"];
    
    //Date
    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
    formater.locale = [NSLocale localeWithLocaleIdentifier:@"ru_RU"];
    formater.dateFormat = @"d MMMM yyyy HH:mm";
    formater.timeZone = [NSTimeZone systemTimeZone];
    
    NSDateFormatter *formaterWithoutTime = [[NSDateFormatter alloc] init];
    formaterWithoutTime.locale = [NSLocale localeWithLocaleIdentifier:@"ru_RU"];
    formaterWithoutTime.dateFormat = @"d.MM.yyyy";
    formaterWithoutTime.timeZone = [NSTimeZone systemTimeZone];
    
    ///TextTableviewCell
    ///Date
    self.dateLabel.text = [formater stringFromDate:message.date];
    
    ///textForCopy date
    //        self.textForCopy.text = [NSString stringWithFormat:@"%@\n", [formater stringFromDate:message.date]];
    
    ///Header rows
    self.orderNumberLabel.text = message.orderNumber;
    self.nameValueLabel.text = strings[0];
    self.phoneValueLabel.text = strings[1];
    self.exportDateValueLabel.text = strings[2];
    self.supplyToCustomsValueLabel.text = strings[3];
    
    self.nameLabel.font = myFont;
    self.phoneLabel.font = myFont;
    self.exportDateLabel.font = myFont;
    self.supplyToCustomsLabel.font = myFont;
    self.nameValueLabel. font = myFont;
    self.phoneValueLabel.font = myFont;
    self.exportDateValueLabel.font = myFont;
    self.supplyToCustomsValueLabel.font = myFont;
    
    self.orderNumberLabel.backgroundColor = headerColor;
    self.orderNumberLabel.borderColor = headerColor;
    for (UIView *item in self.borderViews) {
        item.backgroundColor = headerColor;
    }
    
    ///textForCopy header
    self.textForCopyLabel.text = [NSString stringWithFormat:@"%@%@\nФИО - %@\nТелефон - %@\nДата вывоза - %@\nПодача на таможню - %@",
                                  self.textForCopyLabel.text,
                                  message.orderNumber,
                                  strings[0],
                                  strings[1],
                                  strings[2],
                                  strings[3]];
}


@end
