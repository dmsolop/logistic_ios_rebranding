//
//  MessageTypeDesign12Cell.m
//  YourLogisticsNew
//
//  Created by Mac on 27.11.2018.
//  Copyright © 2018 iCenter. All rights reserved.
//

#import "MessageTypeDesign12Cell.h"
#import "ColorPallet.h"

@implementation MessageTypeDesign12Cell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)configWithMessage:(ChatMessage *)message {
    NSInteger designCode = message.typeDesign;
    ColorPallet *colorPallet = [[ColorPallet alloc] init];
    colorPallet.isNew = message.isNew.boolValue;
    
    UIColor *headerColor = [colorPallet headerSetColorWithCodeDesign:designCode];
    UIColor *bodyLineColor = [colorPallet decorViewSetColorWithCodeDesign:designCode];
    
    NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    
    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
    formater.locale = [NSLocale localeWithLocaleIdentifier:@"ru_RU"];
    formater.dateFormat = @"d MMMM yyyy HH:mm";
    formater.timeZone = [NSTimeZone systemTimeZone];
    
    if (![message.clearanceDate isEqualToDate:[NSDate dateWithTimeIntervalSince1970:0]]) {
        self.dateLabel.text = [formater stringFromDate:message.clearanceDate];
    } else {
        self.dateLabel.text = [formater stringFromDate:message.date];
    }
    
    self.dateLabel.textColor = [UIColor blackColor];
    
    if ([message.isNew isEqualToNumber:@(0)]) {
        
    } else {
        
    }
    
    NSArray *strings = [message.text componentsSeparatedByString:@"@"];
    
    for (UILabel *currentLabel in self.valueLabels) {
        NSInteger index = currentLabel.tag;
        switch (index) {
            case 0:
                currentLabel.text = [NSString stringWithFormat:@"%@ m²", strings[index]];
                break;
            case 1:
                    currentLabel.text = [NSString stringWithFormat:@"Доставка %@ - Киев (Жуляны)", strings[index]];
                break;
            case 5:
                currentLabel.text = strings[index];
                break;
            case 8:
                currentLabel.text = [NSString stringWithFormat:@"%@ kg",
                                     ![message.weight isEqualToString:@""] ? message.weight : @"--"];
                break;
            case 9:
                currentLabel.text = message.orderNumber;
                break;
            default:
                if ([strings[index] rangeOfCharacterFromSet:notDigits].location == NSNotFound) {
                    currentLabel.text = [NSString stringWithFormat:@"%@ €",
                                         strings[index]];
                } else {
                    currentLabel.text = strings[index];
                }
                break;
        }
    }

    [self createTextForCopyFrom:strings withDate:self.dateLabel.text];
    
    for (UILabel *currentLabel in self.setColorLabels) {
        NSInteger index = currentLabel.tag;
        switch (index) {
            case 12:
                currentLabel.backgroundColor = headerColor;
                break;
            case 9:
            case 6:
                currentLabel.backgroundColor = bodyLineColor;
                break;
            default:
                break;
        }
    }
    
    for (UILabel * currentLabel in self.setColorBordersViews) {
        currentLabel.backgroundColor = colorPallet.pearlDarkGray;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (NSString*)getTextByTag:(int)tag {
    NSString* text = [NSString new];
    for (UILabel *lab in self.prepareForCopyLabels) {
        if (tag == lab.tag) { text = lab.text; }
    }
    return text;
}

- (void)createTextForCopyFrom:(NSArray*)strings withDate:(NSString*) date {
    self.textForCopy.text = [NSString stringWithFormat:@"%@\n%@\n%@ %@\n%@\n",
                             date,
                             [self getTextByTag:12],
                             [self getTextByTag:8],
                             [self getTextByTag:0],
                             [self getTextByTag:9]
                             ];
    
    self.textForCopy.text = [NSString stringWithFormat:@"%@\n%@ %@\n%@ %@\n%@ %@\n%@\n%@\n",
                             self.textForCopy.text,
                             [self getTextByTag:1],
                             [self getTextByTag:2],
                             [self getTextByTag:3],
                             [self getTextByTag:4],
                             [self getTextByTag:11],
                             [self getTextByTag:5],
                             [self getTextByTag:6],
                             [self getTextByTag:7]
                             ];
}


@end


