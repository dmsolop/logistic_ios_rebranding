//
//  MessageTypeDesign12Cell.h
//  YourLogisticsNew
//
//  Created by Mac on 27.11.2018.
//  Copyright © 2018 iCenter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageTypeDesign12Cell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *textForCopy;

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *setColorBordersViews;

@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *setColorLabels;

@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *valueLabels;

@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *prepareForCopyLabels;


- (void)configWithMessage:(ChatMessage *)message;

@end
