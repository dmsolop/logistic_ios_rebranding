//
//  MessageTypeDesign18Cell.m
//  YourLogisticsNew
//
//  Created by Димон on 12.02.2020.
//  Copyright © 2020 iCenter. All rights reserved.
//

#import "MessageTypeDesign18Cell.h"
#import "ColorPallet.h"

@interface MessageTypeDesign18Cell()

@end

@implementation MessageTypeDesign18Cell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)configWithMessage:(ChatMessage *)message {
    NSInteger designCode = message.typeDesign;
    ColorPallet *colorPallet = [[ColorPallet alloc] init];
    colorPallet.isNew = message.isNew.boolValue;

    UIColor *rowGreenColor = [colorPallet headerSetColorWithCodeDesign:designCode];
    UIColor *rowOrangeColor = [colorPallet decorViewSetColorWithCodeDesign:designCode];
    
    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
    formater.locale = [NSLocale localeWithLocaleIdentifier:@"ru_RU"];
    formater.dateFormat = @"d MMMM yyyy HH:mm";
    formater.timeZone = [NSTimeZone systemTimeZone];
    
    NSArray *strings = [message.text componentsSeparatedByString:@"@"];

    ///textForCopy date
    [self createTextForCopyFrom:strings withDate:[formater stringFromDate:message.date]];
    
    ///Create views components
    if (![message.clearanceDate isEqualToDate:[NSDate dateWithTimeIntervalSince1970:0]]) {
        self.dateLabel.text = [formater stringFromDate:message.clearanceDate];
    }else{
        self.dateLabel.text = [formater stringFromDate:message.date];
    }
    
    self.dateLabel.textColor = [UIColor blackColor];
    
    
    for (UILabel *currentLabel in self.valueLabels) {
        NSInteger index = currentLabel.tag;
        switch (index) {
            case 0:
                currentLabel.text = [NSString stringWithFormat:@"%@ kg", strings[index]];
                break;
            case 1:
                currentLabel.text = [NSString stringWithFormat:@"%@ m³", strings[index]];
                break;
            case 2:
                currentLabel.text = [strings[index] isEqual: @"NO"]
                ? [NSString stringWithFormat:@"Доставка %@ - наш склад", strings[13]]
                : @"Доставка Стамбул - наш склад";
                break;

            case 6:
                currentLabel.text = [NSString stringWithFormat:@"АВТОДОСТАВКА В ГОРОД %@", strings[index]];
                break;
            case 14:
                currentLabel.text = message.orderNumber;
                break;
            default: {
                NSString *string = ![strings[index] isEqual: @""] ? [NSString stringWithFormat:@"%@ €", strings[index]] : @"-";
                string = [string stringByReplacingOccurrencesOfString:@"," withString:@"."];
                currentLabel.text = string;
                break;
            }
        }
    }
    
    for (UILabel *currentLabel in self.setColorLabels) {
        NSInteger index = currentLabel.tag;
        switch (index) {
            case 14:
                currentLabel.backgroundColor = rowOrangeColor;
                break;
            default:
                currentLabel.backgroundColor = rowGreenColor;
                break;
        }
    }
    
    for (UILabel * currentLabel in self.setColorBorderViews) {
        currentLabel.backgroundColor = colorPallet.pearlDarkGray;
    }
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (NSString*)getTextByTag:(int)tag {
    NSString* text = [NSString new];
    for (UILabel *lab in self.prepareForCopyLabels) {
        if (tag == lab.tag) { text = lab.text; }
    }
    return text;
}

- (void)createTextForCopyFrom:(NSArray*)strings withDate:(NSString*) date {
    self.textForCopy.text = [NSString stringWithFormat:@"%@\n%@\n%@\n%@ %@\n",
                                    date,
                                    [self getTextByTag:13],
                                    [self getTextByTag:14],
                                    [self getTextByTag:0],
                                    [self getTextByTag:1]
                                    ];
    
    self.textForCopy.text = [NSString stringWithFormat:@"%@\n%@\n%@ %@\n%@ %@\n %@ %@\n",
                             self.textForCopy.text,
                             [self getTextByTag:15],
                             [self getTextByTag:2],
                             [self getTextByTag:3],
                             [self getTextByTag:17],
                             [self getTextByTag:4],
                             [self getTextByTag:18],
                             [self getTextByTag:5]
                             ];
    
    self.textForCopy.text = [NSString stringWithFormat:@"%@\n%@\n%@ %@\n%@ %@\n %@ %@\n",
                             self.textForCopy.text,
                             [self getTextByTag:6],
                             [self getTextByTag:19],
                             [self getTextByTag:7],
                             [self getTextByTag:20],
                             [self getTextByTag:8],
                             [self getTextByTag:21],
                             [self getTextByTag:9]
                             ];
    
    self.textForCopy.text = [NSString stringWithFormat:@"%@\n%@\n%@ %@\n%@ %@\n %@ %@\n",
                             self.textForCopy.text,
                             [self getTextByTag:16],
                             [self getTextByTag:22],
                             [self getTextByTag:10],
                             [self getTextByTag:23],
                             [self getTextByTag:11],
                             [self getTextByTag:24],
                             [self getTextByTag:12]
                             ];
    
}

@end

