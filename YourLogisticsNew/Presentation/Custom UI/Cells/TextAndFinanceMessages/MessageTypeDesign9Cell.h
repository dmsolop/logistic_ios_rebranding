//
//  MessageTypeDesign9Cell.h
//  YourLogistics
//
//  Created by Aleksandr on 03.06.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageTypeDesign9Cell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *valueLabels;

@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *setColorLabels;

@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *setColorBorderViews;

@property (weak, nonatomic) IBOutlet UILabel *textForCopy;

@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *prepareForCopyLabels;


- (void)configWithMessage:(ChatMessage *)message;

@end
