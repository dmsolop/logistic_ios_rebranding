//
//  MessageTypeDesign15CellTableViewCell.h
//  YourLogisticsNew
//
//  Created by Dima on 4/4/19.
//  Copyright © 2019 iCenter. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MessageTypeDesign15Cell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labelForCopy;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *setColorLabels;
@property (weak, nonatomic) IBOutlet UIView *borderViewColor;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *setColorBorderViews;

@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *valueLabels;

@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *prepareForCopyLabels;

- (void)configWithMessage:(ChatMessage *)message;

@end

NS_ASSUME_NONNULL_END
