//
//  MessageTypeDasign13Cell.m
//  YourLogisticsNew
//
//  Created by Dima on 2/6/19.
//  Copyright © 2019 iCenter. All rights reserved.


#import "MessageTypeDesign13Cell.h"
#import <PureLayout/PureLayout.h>
#import "ColorPallet.h"
#import "Constants.h"


@interface MessageTypeDesign13Cell ()

@property (nonatomic, strong) UIStackView *stackView;
@property (nonatomic, copy) ColorPallet *colorSet;

@end

@implementation MessageTypeDesign13Cell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)prepareForReuse {
    [super prepareForReuse];
    [self.stackView removeAllArrangedSubviews];
    self.textForCopyLabel.text = @"";
}

- (void)configWithMessage:(ChatMessage *)message {
    NSInteger designCode = message.typeDesign;
    self.colorSet = [ColorPallet new];
    self.colorSet.isNew = message.isNew.boolValue;
    
    UIFont *myFont = [UIFont fontWithName:@"MuseoSansCyrl-300" size:15];
    UIFont *myFontBold = [UIFont fontWithName:@"MuseoSansCyrl-500" size:15];
    
    UIColor *headerColor = [self.colorSet headerSetColorWithCodeDesign:designCode];
    UIColor *backgroundColor = [self.colorSet whiteColor];
    
    NSArray *strings = [message.text componentsSeparatedByString:@"@"];
    NSMutableArray *cars = [NSMutableArray array];
    
    if (strings.count > 1) {
        NSUInteger index = 0;
        for (NSString *str in strings) {
            if (index > 0) {
                NSArray *temp = [str componentsSeparatedByString:@"ß"];
                NSMutableArray *car = [NSMutableArray arrayWithArray:temp];
                id tmp = [car objectAtIndex:0];
                [car removeObjectAtIndex:0];
                [car insertObject:tmp atIndex:1];
                [cars addObject:car];
            }
            ++index;
        }
    }
    
    //Date
    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
    formater.locale = [NSLocale localeWithLocaleIdentifier:@"ru_RU"];
    formater.dateFormat = @"d MMMM yyyy HH:mm";
    formater.timeZone = [NSTimeZone systemTimeZone];
    
    NSDateFormatter *formaterWithoutTime = [[NSDateFormatter alloc] init];
    formaterWithoutTime.locale = [NSLocale localeWithLocaleIdentifier:@"ru_RU"];
    formaterWithoutTime.dateFormat = @"d.MM.yyyy";
    formaterWithoutTime.timeZone = [NSTimeZone systemTimeZone];
    
    [self.contentTableView.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj removeFromSuperview];
    }];
    
    self.stackView = nil;
    
    self.stackView = [[UIStackView alloc] init];
    [self.contentTableView addSubview:self.stackView];
    
    [self.stackView autoPinEdgesToSuperviewEdges];
    
    self.stackView.axis = UILayoutConstraintAxisVertical;
    self.stackView.distribution = UIStackViewDistributionFillProportionally;
    self.stackView.alignment = UIStackViewAlignmentFill;
    self.stackView.spacing = 0;
    
    ///TextTableviewCell
    ///Date
    self.dateLabel.text = [formater stringFromDate:message.date];
    
    ///textForCopy date
    //        self.textForCopy.text = [NSString stringWithFormat:@"%@\n", [formater stringFromDate:message.date]];
    
    ///Header rows
    self.orderNumberLabel.text = message.orderNumber;
    self.cargoTypeLabel.text = message.cargoType;
    self.conteinerLabel.text = strings.firstObject;
    
    self.cargoTypeNameLabel.font = myFont;
    self.conteinerNameLabel.font = myFont;
    self.cargoTypeLabel. font = myFont;
    self.conteinerLabel.font = myFont;
    
    self.orderNumberLabel.backgroundColor = headerColor;
    self.autoHasArriveAtBootLabel.backgroundColor = headerColor;
    self.orderNumberLabel.borderColor = headerColor;
    self.autoHasArriveAtBootLabel.borderColor = headerColor;
    for (UIView *item in self.borderViews) {
        item.backgroundColor = headerColor;
    }
    
    ///textForCopy header
    self.textForCopyLabel.text = [NSString stringWithFormat:@"%@%@\nвид груза - %@\nконтейнер - %@\nАВТО ПРИБЫЛО НА ЗАГРУЗКУ\n",
                                  self.textForCopyLabel.text,
                                  message.orderNumber, message.cargoType, strings.firstObject];
    
    ///Body rows
    int ind = 0;
    for (NSArray *car in cars) {
        int index = 0;
        if (ind == 0 || 1 != ind % 2) {
            backgroundColor = [self.colorSet greenTea];
        } else {
            backgroundColor = [self.colorSet whiteColor];
        }
        ++ind;
        for (NSString *item in car) {
            
            UIStackView *rowView = [[UIStackView alloc] init];
            [self.stackView addArrangedSubview:rowView];
            
            rowView.axis = UILayoutConstraintAxisHorizontal;
            rowView.distribution = UIStackViewDistributionFillEqually;
            rowView.alignment = UIStackViewAlignmentFill;
            rowView.spacing = 0;
            
            UIView *leftLabelView = [[UIView alloc] init];
            UIView *rightLabelView = [[UIView alloc] init];
            
            UILabel *leftLabel = [[UILabel alloc] init];
            [leftLabelView addSubview:leftLabel];
            
            leftLabel.textAlignment = NSTextAlignmentCenter;
            leftLabel.textColor = UIColor.blackColor;
            
            [leftLabel autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:0];
            [leftLabel autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:0];
            [leftLabel autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:0];
            [leftLabel autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:0];
            
            [leftLabelView autoSetDimension:ALDimensionHeight toSize:40];
            
            UILabel *rightLabel = [[UILabel alloc] init];
            [rightLabelView addSubview:rightLabel];
            
            rightLabel.textAlignment = NSTextAlignmentCenter;
            rightLabel.textColor = UIColor.blackColor;
            
            [rightLabel autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:0];
            [rightLabel autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:0];
            [rightLabel autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:0];
            [rightLabel autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:0];
            
            [rowView addArrangedSubview:leftLabelView];
            [rowView addArrangedSubview:rightLabelView];
            
            leftLabelView.borderColor = headerColor;
            leftLabelView.borderWidth = 0.8;
            rightLabelView.borderColor = headerColor;
            rightLabelView.borderWidth = 0.8;
            leftLabelView.backgroundColor = backgroundColor;
            rightLabelView.backgroundColor = backgroundColor;
            
            rightLabel.text = item;

            switch (index) {
                case 0:
                    leftLabel.text = @"№ авто";
                    leftLabel.font = myFontBold;
                    rightLabel.font = myFontBold;
                    break;
                case 1:
                    leftLabel.text = @"водитель";
                    leftLabel.font = myFont;
                    rightLabel.font = myFont;
                    break;
                case 2:
                    leftLabel.text = @"тел";
                    leftLabel.font = myFont;
                    rightLabel.font = myFont;
                    break;
                default:
                    break;
            }
            
            //textForCopy optional rows
            self.textForCopyLabel.text = [NSString stringWithFormat:@"%@%@  %@\n",
                                          self.textForCopyLabel.text, leftLabel.text, rightLabel.text];

            ++index;
        }
    }
}

- (CGSize)settingFlexibleSizeForLabel:(UILabel*)label
                               byFont:(UIFont*)font
                             withText:(NSString*)text {
    label.font = font;
    label.lineBreakMode = NSLineBreakByWordWrapping;
    Constants *constants = [[Constants alloc] init];
    CGSize maximumSize = CGSizeMake(300, 9999);
    
    CGSize myStringSize = [text sizeWithFont:font
                           constrainedToSize:maximumSize
                               lineBreakMode:label.lineBreakMode];
    
    CGRect newFrame = label.frame;
    newFrame.size.height = myStringSize.height;
    label.frame = newFrame;
    label.numberOfLines = 0;
    [label sizeToFit];
    
    return CGSizeMake(myStringSize.width, myStringSize.height + 20 /*constants.spacingInConteiner * 2*/);
}


@end
