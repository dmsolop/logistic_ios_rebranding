//
//  MessageTypeDesign15CellTableViewCell.m
//  YourLogisticsNew
//
//  Created by Dima on 4/4/19.
//  Copyright © 2019 iCenter. All rights reserved.
//

#import "MessageTypeDesign15Cell.h"
#import "ColorPallet.h"


@implementation MessageTypeDesign15Cell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)configWithMessage:(ChatMessage *)message {
    
    NSInteger designCode = message.typeDesign;
    
    ColorPallet *colorPallet = [[ColorPallet alloc] init];
    colorPallet.isNew = message.isNew.boolValue;
    
    UIColor *headerColor = [colorPallet headerSetColorWithCodeDesign:designCode];
    UIColor *designViewColor = [colorPallet decorViewSetColorWithCodeDesign:designCode];

    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
    formater.locale = [NSLocale localeWithLocaleIdentifier:@"ru_RU"];
    formater.dateFormat = @"d MMMM yyyy HH:mm";
    formater.timeZone = [NSTimeZone systemTimeZone];
    
    if (![message.clearanceDate isEqualToDate:[NSDate dateWithTimeIntervalSince1970:0]]) {
        self.dateLabel.text = [formater stringFromDate:message.clearanceDate];
    }else{
        self.dateLabel.text = [formater stringFromDate:message.date];
    }
    
    
    for (UILabel *currentLabel in self.setColorLabels) {
        NSInteger index = currentLabel.tag;
        switch (index) {
            case 19:
                currentLabel.backgroundColor = designViewColor;
                break;
            case 10:
            case 16:
            case 18:
                currentLabel.backgroundColor = headerColor;
                break;
            default:
                break;
        }
    }
    self.borderViewColor.backgroundColor = designViewColor;
    
    for (UILabel * currentLabel in self.setColorBorderViews) {
        currentLabel.backgroundColor = colorPallet.pearlDarkGray;
    }
    
    if ([message.text isEqualToString:@""]) {
        self.borderViewColor.hidden = YES;
        for (UILabel *label in self.setColorBorderViews) {
            label.hidden = YES;
        }
        for (UILabel *label in self.prepareForCopyLabels) {
            NSInteger tag = label.tag;
            if (tag != 10 && tag != 19) {
                label.hidden = YES;
            } else if (label.tag == 19) {
                label.text = @"Нет данных по выбранным параметрам";
            }
        }
        return;
    } else {
        self.borderViewColor.hidden = NO;
        for (UILabel *label in self.setColorBorderViews) {
            label.hidden = NO;
        }
        for (UILabel *label in self.prepareForCopyLabels) {
            label.hidden = NO;
        }
    }
    
    ///textForCopy date
    //    self.textForCopy.text = [NSString stringWithFormat:@"%@\n", [formater stringFromDate:message.date]];
    
    self.dateLabel.textColor = [UIColor blackColor];
    
    NSArray *strings = [message.text componentsSeparatedByString:@"@"];
    
    for (UILabel *currentLabel in self.valueLabels) {
        NSInteger index = currentLabel.tag;
        switch (index) {
            case 19:{
                currentLabel.text = strings[0];
                break;
            }
            case 1:
            case 2:
            case 7: {
                currentLabel.text = strings[index];
                break;
            }
            case 3: {
                currentLabel.text = [NSString stringWithFormat:@"%@ кг",
                                     strings[index]];
                break;
            }
            case 4:
            case 5:
            case 6:
            case 8:
            case 9: {
                currentLabel.text = [NSString stringWithFormat:@"$%@",
                                     strings[index]];
            }
                break;
            default:
                break;
        }
    }
    

    
}


@end
