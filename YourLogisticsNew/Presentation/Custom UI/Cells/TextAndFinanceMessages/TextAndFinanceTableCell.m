//
//  FinanceTableCell.m
//  YourLogisticsNew
//
//  Created by VitaliyK on 09.09.2018.
//  Copyright © 2018 iCenter. All rights reserved.
//

#import "TextAndFinanceTableCell.h"
#import <PureLayout/PureLayout.h>
#import "ColorPallet.h"
#import "Constants.h"


@interface TextAndFinanceTableCell ()

@property (nonatomic, strong) UIStackView *stackView;
@property (nonatomic, copy) ColorPallet *colorSet;

@end

@implementation TextAndFinanceTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
}
    
- (void)prepareForReuse {
    [super prepareForReuse];
    [self.stackView removeAllArrangedSubviews];
    self.textForCopy.text = @"";
}

- (void)configWithMessage:(ChatMessage *)message {
    NSInteger designCode = message.typeDesign;
    self.colorSet = [ColorPallet new];
    self.colorSet.isNew = message.isNew.boolValue;
    
    UIFont *myFont = [UIFont fontWithName:@"MuseoSansCyrl-300" size:14];
    UIFont *myFontBold = [UIFont fontWithName:@"MuseoSansCyrl-500" size:17];

    UIColor *headerColor = [self.colorSet headerSetColorWithCodeDesign:designCode];
    UIColor *borderHeaderColor = [self.colorSet borderSetColorWithCodeDesign:designCode];
    UIColor *separatorColor = [self.colorSet separatorSetColorWithCodeDesign:designCode];
    UIColor *designViewColor = [self.colorSet decorViewSetColorWithCodeDesign:designCode];
    UIColor *textColor = [self.colorSet textSetColorWithCodeDesign:designCode];
    if (message.typeDesign == 14) {
        self.separatorHeaderView.isHidden == YES;
    }

    //Date
    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
    formater.locale = [NSLocale localeWithLocaleIdentifier:@"ru_RU"];
    formater.dateFormat = @"d MMMM yyyy HH:mm";
    formater.timeZone = [NSTimeZone systemTimeZone];
    
    NSDateFormatter *formaterWithoutTime = [[NSDateFormatter alloc] init];
    formaterWithoutTime.locale = [NSLocale localeWithLocaleIdentifier:@"ru_RU"];
    formaterWithoutTime.dateFormat = @"d.MM.yyyy";
    formaterWithoutTime.timeZone = [NSTimeZone systemTimeZone];
    
    [self.contentTableView.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj removeFromSuperview];
    }];
    
    self.stackView = nil;
    
    self.stackView = [[UIStackView alloc] init];
    [self.contentTableView addSubview:self.stackView];
    
    [self.stackView autoPinEdgesToSuperviewEdges];
    
    self.stackView.axis = UILayoutConstraintAxisVertical;
    self.stackView.distribution = UIStackViewDistributionFillProportionally;
    self.stackView.alignment = UIStackViewAlignmentFill;
    self.stackView.spacing = 0;
    
    //TextTableviewCell
    //Other optional rows
    if (message.type == 1) {
        
        self.dateLabel.text = [formater stringFromDate:message.date];
        
        //textForCopy date
//        self.textForCopy.text = [NSString stringWithFormat:@"%@\n", [formater stringFromDate:message.date]];
        //textForCopy header
        if (designCode == 11 || designCode == 10) {
            NSString *headerText = designCode == 11 ? @"ДОЛГ" : message.orderNumber;
            self.textForCopy.text = [NSString stringWithFormat:@"%@%@\n", self.textForCopy.text, headerText];
        } else if (designCode == 14) {
            self.textForCopy.text = [NSString stringWithFormat:@"%@\n", message.orderNumber];
        } else {
            self.textForCopy.text = [NSString stringWithFormat:@"%@%@  %@\n",
                                     self.textForCopy.text,
                                     message.orderNumber, message.cargoType];
        }

        //Additional header row
        if (designCode == 11) {
            UIStackView *rowView = [[UIStackView alloc] init];
            [self.stackView addArrangedSubview:rowView];
            
            rowView.axis = UILayoutConstraintAxisHorizontal;
            rowView.distribution = UIStackViewDistributionFillProportionally;
            rowView.alignment = UIStackViewAlignmentFill;
            rowView.spacing = 10;
            
            
            UIView *centerLabelView = [[UIView alloc] init];
            UILabel *centerLabel = [[UILabel alloc] init];
            [centerLabelView addSubview:centerLabel];
            
            centerLabelView.borderColor = borderHeaderColor;
            centerLabelView.borderWidth = 0.8;
            centerLabelView.backgroundColor = headerColor;
            [centerLabelView autoSetDimension:ALDimensionHeight toSize:40];
            
            [centerLabel autoAlignAxis:ALAxisVertical toSameAxisOfView:centerLabelView];
            [centerLabel autoAlignAxis:ALAxisHorizontal toSameAxisOfView:centerLabelView];
            
            centerLabel.text = message.orderNumber;
            self.textForCopy.text = [NSString stringWithFormat:@"%@ %@\n",
                                     self.textForCopy.text, message.orderNumber];
            centerLabel.font = myFont;
            centerLabel.textAlignment = NSTextAlignmentCenter;
            
            CGSize expectedConteinerSize = CGSizeMake(centerLabelView.frame.size.width, 60.f);
            [rowView autoSetDimensionsToSize:expectedConteinerSize];
            [rowView addArrangedSubview:centerLabelView];
        } else if (designCode == 14) {
            
        }
        
        NSMutableDictionary<NSString *, id> * rows = [[NSMutableDictionary alloc] init];
        NSMutableArray<NSString *> *keys = [[NSMutableArray alloc] init];
        NSArray *strings = [message.text componentsSeparatedByString:@"@"];

        if (message.typeDesign == 14 && ![message.cargoType isEqual: @""]) {
            rows[@"cargoType"] = message.cargoType;
            [keys addObject:@"cargoType"];
        }
        if (message.weight.length > 3) {
            rows[@"weight"] = message.weight;
            [keys addObject:@"weight"];
        }
        if ([message.price integerValue] > 0) {
            rows[@"price"] = message.price;
            [keys addObject:@"price"];
        }
        if (![message.clearanceDate isEqualToDate:[NSDate dateWithTimeIntervalSince1970:0]]) {
            rows[@"clearanceDate"] = message.clearanceDate;
            [keys addObject:@"clearanceDate"];
        }
        if (message.typeDesign == 10) {
            rows[@"borderCrossing"] = strings[0];
            [keys addObject:@"borderCrossing"];
            rows[@"terminal"] = strings[1];
            [keys addObject:@"terminal"];
            rows[@"time"] = strings[2];
            [keys addObject:@"time"];
        }
        if (message.typeDesign == 11) {
            rows[@"clientCode"] = strings[0];
            [keys addObject:@"clientCode"];
            rows[@"debt"] = strings[1];
            [keys addObject:@"debt"];
            rows[@"dateOfDebt"] = strings[2];
            [keys addObject:@"dateOfDebt"];
            rows[@"invoiceSum"] = strings[3];
            [keys addObject:@"invoiceSum"];
        }
        
        for (NSString *key in keys) {
            UIStackView *rowView = [[UIStackView alloc] init];
            [self.stackView addArrangedSubview:rowView];
            
            rowView.axis = UILayoutConstraintAxisHorizontal;
            rowView.distribution = UIStackViewDistributionFillEqually;
            rowView.alignment = UIStackViewAlignmentFill;
            rowView.spacing = 0;
            
            UIView *leftLabelView = [[UIView alloc] init];
            UIView *rightLabelView = [[UIView alloc] init];
            
            UILabel *leftLabel = [[UILabel alloc] init];
            [leftLabelView addSubview:leftLabel];
            
            leftLabel.font = myFont;
            leftLabel.textAlignment = NSTextAlignmentCenter;
            leftLabel.textColor = UIColor.blackColor;
            
            [leftLabel autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:0];
            [leftLabel autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:0];
            [leftLabel autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:0];
            [leftLabel autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:0];
            
            [leftLabelView autoSetDimension:ALDimensionHeight toSize:40];
            
            UILabel *rightLabel = [[UILabel alloc] init];
            [rightLabelView addSubview:rightLabel];
            
            rightLabel.font = myFont;
            rightLabel.textAlignment = NSTextAlignmentCenter;
            rightLabel.textColor = UIColor.blackColor;
            
            [rightLabel autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:0];
            [rightLabel autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:0];
            [rightLabel autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:0];
            [rightLabel autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:0];
            
            if ([rows[key] isKindOfClass:[NSString class]]) {
                if ([key isEqualToString:@"weight"]) {
                    leftLabel.text = @"вес";
                    rightLabel.text = [NSString stringWithFormat:@"%@", rows[key]];
                } else if ([key isEqualToString:@"time"]) {
                    leftLabel.text = @"время";
                    rightLabel.text = [NSString stringWithFormat:@"%@", rows[key]];
                } else if ([key isEqualToString:@"cargoType"]) {
                    leftLabel.text = @"вид груза";
                    rightLabel.text = [NSString stringWithFormat:@"%@", rows[key]];
                } else if ([key isEqualToString:@"terminal"]) {
                    leftLabel.text = @"терминал";
                    rightLabel.text = [NSString stringWithFormat:@"%@", rows[key]];
                } else if ([key isEqualToString:@"borderCrossing"]) {
                    leftLabel.text = @"погранпереход";
                    rightLabel.text = [NSString stringWithFormat:@"%@", rows[key]];
                } else if ([key isEqualToString:@"clientCode"]) {
                    leftLabel.text = @"код клиента";
                    rightLabel.text = [NSString stringWithFormat:@"%@", rows[key]];
                } else if ([key isEqualToString:@"debt"]) {
                    leftLabel.text = @"долг";
                    rightLabel.text = [NSString stringWithFormat:@"%@ %@", rows[key], message.currency];
                } else if ([key isEqualToString:@"dateOfDebt"]) {
                    leftLabel.text = @"дата долга";
                    NSString *date = [rows[key] componentsSeparatedByString:@" "][0];
                    NSString *correctDate = [date isEqualToString:@"01.01.1970"] ? @"" : date;
                    rightLabel.text = [NSString stringWithFormat:@"%@", correctDate];
                } else if ([key isEqualToString:@"invoiceSum"]) {
                    leftLabel.text = @"инвойс сумма";
                    rightLabel.text = [NSString stringWithFormat:@"%@ USD", rows[key]];
                }
            } else if ([rows[key] isKindOfClass:[NSNumber class]]) {
                if ([key isEqualToString:@"price"]) {
                    leftLabel.text = message.typeDesign != 14 ? @"сумма" : @"платежи";
                    rightLabel.text = [NSString stringWithFormat:@"%@ %@", message.price, message.currency];
                }
            } else if ([rows[key] isKindOfClass:[NSDate class]]) {
                leftLabel.text = @"дата растаможки";
                rightLabel.text = [formaterWithoutTime stringFromDate:message.clearanceDate];
            }
            
            //textForCopy optional rows
            self.textForCopy.text = [NSString stringWithFormat:@"%@%@  %@\n",
                                     self.textForCopy.text, leftLabel.text, rightLabel.text];
            
            CGSize expectedConteinerSize = [self settingFlexibleSizeForLabel:leftLabel byFont:myFont withText:leftLabel.text];
            
            [rowView addArrangedSubview:leftLabelView];
            [rowView addArrangedSubview:rightLabelView];
            
            leftLabelView.borderColor = borderHeaderColor;
            leftLabelView.borderWidth = 0.8;
            rightLabelView.borderColor = borderHeaderColor;
            rightLabelView.borderWidth = 0.8;
        }
        
        //Message text
        if (designCode != 10 && designCode != 11 && designCode != 14) {
            UIStackView *rowView = [[UIStackView alloc] init];
            [self.stackView addArrangedSubview:rowView];
            
            rowView.axis = UILayoutConstraintAxisHorizontal;
            rowView.distribution = UIStackViewDistributionFillProportionally;
            rowView.alignment = UIStackViewAlignmentFill;
            
            UIView *centerLabelView = [[UIView alloc] init];
            UIView *decorView = [[UIView alloc] init];
            UILabel *centerLabel = [[UILabel alloc] init];
            [centerLabelView addSubview:centerLabel];
            [centerLabelView addSubview:decorView];
            
            centerLabelView.borderColor = borderHeaderColor;
            centerLabelView.borderWidth = 0.8;
            
            decorView.backgroundColor = designViewColor;
            [decorView addConstraint: [NSLayoutConstraint constraintWithItem:decorView
                                                                   attribute:NSLayoutAttributeWidth
                                                                   relatedBy:NSLayoutRelationEqual
                                                                      toItem:nil
                                                                   attribute: NSLayoutAttributeNotAnAttribute
                                                                  multiplier:1
                                                                    constant:2]];
            
            [decorView autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:10];
            [decorView autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:10];
            [decorView autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:10];
            
            [centerLabel autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:10];
            [centerLabel autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:10];
            [centerLabel autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:22];
            
            centerLabel.text = message.text;
            centerLabel.textAlignment = NSTextAlignmentLeft;
            
            CGSize expectedConteinerSize = [self settingFlexibleSizeForLabel:centerLabel byFont:myFont withText:message.text];
            [rowView autoSetDimensionsToSize:expectedConteinerSize];
            [rowView addArrangedSubview:centerLabelView];
         
            //textForCopy message text
            self.textForCopy.text = [NSString stringWithFormat:@"%@%@\n",
                                     self.textForCopy.text, message.text];
            
            //Header
            self.leftHeaderLabel.textColor = textColor;
            self.rightHeaderLabel.textColor = textColor;
            self.leftHeaderLabel.text = message.orderNumber;
            self.rightHeaderLabel.text = message.cargoType;
            self.rightHeaderLabel.textAlignment = NSTextAlignmentCenter;
            self.leftHeaderLabel.textAlignment = NSTextAlignmentCenter;
            
            self.leftHeaderLabel.hidden = NO;
            self.rightHeaderLabel.hidden = NO;
            self.centerHeaderLabel.hidden = YES;

        } else if (designCode == 10 || designCode == 11) {
            self.centerHeaderLabel.textColor = textColor;
            self.centerHeaderLabel.text = designCode == 10 ? message.orderNumber : @"ДОЛГ";
            self.centerHeaderLabel.font = designCode == 10 ? myFont : myFontBold;
            self.centerHeaderLabel.textAlignment = NSTextAlignmentCenter;
            
            self.leftHeaderLabel.hidden = YES;
            self.rightHeaderLabel.hidden = YES;
            self.centerHeaderLabel.hidden = NO;
            
        }  else if (designCode == 14) {
            self.centerHeaderLabel.textColor = textColor;
            self.centerHeaderLabel.text = message.orderNumber;
            self.centerHeaderLabel.font = myFont;
            self.centerHeaderLabel.textAlignment = NSTextAlignmentCenter;
            
            self.leftHeaderLabel.hidden = YES;
            self.rightHeaderLabel.hidden = YES;
            self.centerHeaderLabel.hidden = NO;
        }
        
        self.headerView.backgroundColor = headerColor;
        self.headerView.borderWidth = 0.8;
        self.headerView.borderColor = borderHeaderColor;
        self.separatorHeaderView.backgroundColor = separatorColor;
        
        
//===================================================================================================//
   //FinanceTableviewCell
    } else {
        
        if (![message.clearanceDate isEqualToDate:[NSDate dateWithTimeIntervalSince1970:0]]) {
            self.dateLabel.text = [formater stringFromDate:message.clearanceDate];
        }else{
            self.dateLabel.text = [formater stringFromDate:message.date];
        }

        //textForCopy date
//        self.textForCopy.text = [NSString stringWithFormat:@"%@\n", [formater stringFromDate:message.date]];
//        //textForCopy header
//        self.textForCopy.text = @"";
//        if (message.orderNumber == nil || [message.orderNumber isEqualToString:@""]) {
//            self.textForCopy.text = [NSString stringWithFormat:@"%@%@  %@\n", self.textForCopy.text, message.clientCode, @"Оплачено"];
//        } else {
//            self.textForCopy.text = [NSString stringWithFormat:@"%@%@  %@\n",
//                                     self.textForCopy.text,
//                                     message.orderNumber, @"Начисленно"];
//        }
        
        for (FinanceTableItem *row in message.rowsOfFinanceTable) {
//            if (row.isPaid) {
//                UIStackView *rowView = [[UIStackView alloc] init];
//                [self.stackView addArrangedSubview:rowView];
//
//                rowView.axis = UILayoutConstraintAxisHorizontal;
//                rowView.distribution = UIStackViewDistributionFillEqually;
//                rowView.alignment = UIStackViewAlignmentFill;
//                rowView.spacing = 0;
//
//                UIView *leftLabelView = [[UIView alloc] init];
//                UIView *rightLabelView = [[UIView alloc] init];
//
//                UILabel *leftLabel = [[UILabel alloc] init];
//                UILabel *rightLabel = [[UILabel alloc] init];
//                rightLabel.font = myFont;
//                leftLabel.font = myFont;
//
//                [leftLabelView addSubview:leftLabel];
//                [rightLabelView addSubview:rightLabel];
//
//                [leftLabel autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:0];
//                [leftLabel autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:0];
//                [leftLabel autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:14];
//                [leftLabel autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:0];
//                [leftLabelView autoSetDimension:ALDimensionHeight toSize:40];
//
//                [rightLabel autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:0];
//                [rightLabel autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:0];
//                [rightLabel autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:14];
//                [rightLabel autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:0];
//
//                leftLabel.text = @"Код клиента";
//                rightLabel.text = message.clientCode;
//
//                //textForCopy row
//                self.textForCopy.text = [NSString stringWithFormat:@"%@%@  %@\n",
//                                         self.textForCopy.text,
//                                         @"Код клиента", message.clientCode];
//
//                [rowView addArrangedSubview:leftLabelView];
//                [rowView addArrangedSubview:rightLabelView];
//
//                leftLabelView.borderColor = [UIColor colorWithRed:0x9B/255.0f green:0x9C/255.0f blue:0x9D/255.0f alpha:1.0f];
//                leftLabelView.borderWidth = 0.5;
//                rightLabelView.borderColor = [UIColor colorWithRed:0x9B/255.0f green:0x9C/255.0f blue:0x9D/255.0f alpha:1.0f];
//                rightLabelView.borderWidth = 0.5;
//            }
            
            UIStackView *rowView = [[UIStackView alloc] init];
            [self.stackView addArrangedSubview:rowView];
            
            rowView.axis = UILayoutConstraintAxisHorizontal;
            rowView.distribution = UIStackViewDistributionFillEqually;
            rowView.alignment = UIStackViewAlignmentFill;
            rowView.spacing = 0;
            
            UIView *leftLabelView = [[UIView alloc] init];
            UIView *rightLabelView = [[UIView alloc] init];
            
            UILabel *leftLabel = [[UILabel alloc] init];
            UILabel *rightLabel = [[UILabel alloc] init];
            rightLabel.font = myFont;
            leftLabel.font = myFont;

            [leftLabelView addSubview:leftLabel];
            [rightLabelView addSubview:rightLabel];
            
            [leftLabel autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:0];
            [leftLabel autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:0];
            [leftLabel autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:14];
            [leftLabel autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:0];
            [leftLabelView autoSetDimension:ALDimensionHeight toSize:40];
            
            [rightLabel autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:0];
            [rightLabel autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:0];
            [rightLabel autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:14];
            [rightLabel autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:0];
            
            if (row.isPaid) {
                UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"paid_icon"]];
                imageView.tintColor = [ColorPallet new].lime;
                [leftLabelView addSubview:imageView];
                [imageView autoCenterInSuperview];
                
                [leftLabelView autoSetDimension:ALDimensionHeight toSize:55];
            } else {
                UIFont *intrinsicFont = ![row.service isEqualToString:@"Долг"] ? myFont : myFontBold;
                UILabel *leftLabel = [[UILabel alloc] init];
                [leftLabelView addSubview:leftLabel];
                
                [leftLabel autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:0];
                [leftLabel autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:0];
                [leftLabel autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:14];
                [leftLabel autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:0];
//                [leftLabelView autoSetDimension:ALDimensionHeight toSize:40];
                
                leftLabel.text = row.service;
                
                [self settingFlexibleSizeForLabel:leftLabel byFont:intrinsicFont withText:leftLabel.text];
            }
            
            [rightLabelView addSubview:rightLabel];
            
            
            [rightLabel autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:0];
            [rightLabel autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:0];
            [rightLabel autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:14];
            [rightLabel autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:0];
            
            NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
            [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
            formatter.currencySymbol = @"";
            formatter.currencyGroupingSeparator = @"";
            NSString *sum = [formatter stringFromNumber:@(row.sum)];
            
            rightLabel.text = [[NSString alloc] initWithFormat:@"%@ %@", sum, row.currency];
            
//            //textForCopy optional rows
//            self.textForCopy.text = [NSString stringWithFormat:@"%@%@  %@ %@\n",
//                                     self.textForCopy.text,
//                                     row.service, sum, row.currency];
            
            [rowView addArrangedSubview:leftLabelView];
            [rowView addArrangedSubview:rightLabelView];
            
            UIColor *color = [[UIColor alloc] init];
            CGFloat alpha = message.isNew.boolValue == NO ? 0.3f : 1.0f;
            if (message.orderNumber == nil || [message.orderNumber isEqualToString:@""]) {
                color = [UIColor colorWithRed:115/255.0f green:206/253.0f blue:245/255.0f alpha:alpha];
            } else {
                color = [UIColor colorWithRed:68/255.0f green:86/255.0f blue:133/255.0f alpha:alpha];
            }
            leftLabelView.borderColor = color;
            rightLabelView.borderColor = color;
            leftLabelView.borderWidth = 0.5;
            rightLabelView.borderWidth = 0.5;

        }
        
        //Headar
        self.separatorHeaderView.backgroundColor = [UIColor whiteColor];
        if (message.orderNumber == nil || [message.orderNumber isEqualToString:@""]) {
            self.rightHeaderLabel.text = @"Оплачено";
            self.leftHeaderLabel.text = message.clientCode;

            self.leftHeaderLabel.hidden = NO;
            self.rightHeaderLabel.hidden = NO;
            self.centerHeaderLabel.hidden = YES;
            
            if (message.isNew.boolValue == NO) {
                self.headerView.backgroundColor = [UIColor colorWithRed:115/255.0f green:206/253.0f blue:245/255.0f alpha:0.3f];
                
                self.leftHeaderLabel.textColor = [UIColor blackColor];
                self.rightHeaderLabel.textColor = [UIColor blackColor];
                self.centerHeaderLabel.textColor = [UIColor blackColor];
            } else {
                self.headerView.backgroundColor = [UIColor colorWithRed:115/255.0f green:206/253.0f blue:245/255.0f alpha:1.0f];
                
                self.leftHeaderLabel.textColor = [UIColor blackColor];
                self.rightHeaderLabel.textColor = [UIColor blackColor];
                self.centerHeaderLabel.textColor = [UIColor blackColor];
            }
            
        } else {
            
            self.leftHeaderLabel.text = message.orderNumber;
            self.rightHeaderLabel.text = @"Начислено";
            
            self.leftHeaderLabel.hidden = NO;
            self.rightHeaderLabel.hidden = NO;
            self.centerHeaderLabel.hidden = YES;
            
            if (message.isNew.boolValue == NO) {
                self.headerView.backgroundColor = [UIColor colorWithRed:68/255.0f green:86/255.0f blue:133/255.0f alpha:0.3f];
                
                self.leftHeaderLabel.textColor = [UIColor blackColor];
                self.rightHeaderLabel.textColor = [UIColor blackColor];
                self.centerHeaderLabel.textColor = [UIColor blackColor];
            } else {
                self.headerView.backgroundColor = [UIColor colorWithRed:68/255.0f green:86/255.0f blue:133/255.0f alpha:1.0f];
                
                self.leftHeaderLabel.textColor = [UIColor whiteColor];
                self.rightHeaderLabel.textColor = [UIColor whiteColor];
                self.centerHeaderLabel.textColor = [UIColor whiteColor];
            }
        }
    }

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (CGSize)settingFlexibleSizeForLabel:(UILabel*)label
                               byFont:(UIFont*)font
                             withText:(NSString*)text {
    label.font = font;
    label.lineBreakMode = NSLineBreakByWordWrapping;
    CGSize maximumSize = CGSizeMake(300, 9999);
    
    CGSize myStringSize = [text sizeWithFont:font
                               constrainedToSize:maximumSize
                                   lineBreakMode:label.lineBreakMode];
    
    CGRect newFrame = label.frame;
    newFrame.size.height = myStringSize.height;
    label.frame = newFrame;
    label.numberOfLines = 0;
    [label sizeToFit];
    
    return CGSizeMake(myStringSize.width, myStringSize.height + 20 /*constants.spacingInConteiner * 2*/);
}

//- (BOOL)canBecomeFirstResponder {
//    return YES;
//}
//- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
//{
//    if (action == @selector(copy:)) {
//        return YES;
//    }
//    return NO;
//}


@end
