//
//  MessageTypeDesign16Cell.m
//  YourLogisticsNew
//
//  Created by Dima on 4/17/19.
//  Copyright © 2019 iCenter. All rights reserved.
//

#import "MessageTypeDesign16Cell.h"
#import "ColorPallet.h"

@implementation MessageTypeDesign16Cell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)configWithMessage:(ChatMessage *)message {
    
    NSInteger designCode = message.typeDesign;
    
    ColorPallet *colorPallet = [[ColorPallet alloc] init];
    colorPallet.isNew = message.isNew.boolValue;
    
    UIColor *headerColor = [colorPallet headerSetColorWithCodeDesign:designCode];
    UIColor *borderViewColor = [colorPallet borderSetColorWithCodeDesign:designCode];
    
    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
    formater.locale = [NSLocale localeWithLocaleIdentifier:@"ru_RU"];
    formater.dateFormat = @"d MMMM yyyy HH:mm";
    formater.timeZone = [NSTimeZone systemTimeZone];
    
    if (![message.clearanceDate isEqualToDate:[NSDate dateWithTimeIntervalSince1970:0]]) {
        self.dateLabel.text = [formater stringFromDate:message.clearanceDate];
    }else{
        self.dateLabel.text = [formater stringFromDate:message.date];
    }
    
    self.headerLabel.backgroundColor = headerColor;
    [self.headerLabel setBorderWidth:1];
    [self.headerLabel setBorderColor:borderViewColor];
    
    for (UILabel * currentLabel in self.borders) {
        currentLabel.backgroundColor = borderViewColor;
    }
    
    
    

    
    self.dateLabel.textColor = [UIColor blackColor];
    
    NSArray *strings = [message.text componentsSeparatedByString:@"ß"];

    self.headerLabel.text = strings[0];
    self.managerLabel.text = strings[1];
    self.commentLabel.text = strings[2];
    
    [self.managerLabel setTransform:CGAffineTransformMakeRotation(-M_PI / 2)];
    
    ///textForCopy date
    self.labelForCopy.text = [NSString stringWithFormat:@"%@\n%@\n%@", strings[0], strings[1], strings[2]];
}



@end
