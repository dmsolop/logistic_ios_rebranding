//
//  MessageTypeDasign13Cell.h
//  YourLogisticsNew
//
//  Created by Dima on 2/6/19.
//  Copyright © 2019 iCenter. All rights reserved.


#import <UIKit/UIKit.h>

@interface MessageTypeDesign13Cell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *textForCopyLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *conteinerNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *cargoTypeNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *cargoTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *conteinerLabel;
@property (weak, nonatomic) IBOutlet UILabel *autoHasArriveAtBootLabel;
@property (weak, nonatomic) IBOutlet UIView *contentTableView;

@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *borderViews;

- (void)configWithMessage:(ChatMessage *)message;

@end
