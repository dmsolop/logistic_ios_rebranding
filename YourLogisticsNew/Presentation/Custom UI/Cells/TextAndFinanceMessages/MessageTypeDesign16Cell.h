//
//  MessageTypeDesign16Cell.h
//  YourLogisticsNew
//
//  Created by Dima on 4/17/19.
//  Copyright © 2019 iCenter. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MessageTypeDesign16Cell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labelForCopy;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property (weak, nonatomic) IBOutlet UILabel *managerLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentLabel;
@property (weak, nonatomic) IBOutlet UIView *conteinerViewForText;

@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *borders;

- (void)configWithMessage:(ChatMessage *)message;

@end

NS_ASSUME_NONNULL_END
