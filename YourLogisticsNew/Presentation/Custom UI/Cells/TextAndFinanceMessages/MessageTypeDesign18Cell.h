//
//  MessageTypeDesign18Cell.h
//  YourLogisticsNew
//
//  Created by Димон on 12.02.2020.
//  Copyright © 2020 iCenter. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MessageTypeDesign18Cell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *valueLabels;

@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *setColorLabels;

@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *setColorBorderViews;

@property (weak, nonatomic) IBOutlet UILabel *textForCopy;

@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *prepareForCopyLabels;


- (void)configWithMessage:(ChatMessage *)message;

@end

NS_ASSUME_NONNULL_END
