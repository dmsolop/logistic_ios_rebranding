//
//  MessageCell.m
//  YourLogistics
//
//  Created by Aleksandr on 03.06.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import "MessageTypeDesign9Cell.h"
#import "ColorPallet.h"

@interface MessageTypeDesign9Cell()

@end

@implementation MessageTypeDesign9Cell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)configWithMessage:(ChatMessage *)message {
    NSInteger designCode = message.typeDesign;
    ColorPallet *colorPallet = [[ColorPallet alloc] init];
    colorPallet.isNew = message.isNew.boolValue;

    UIColor *rowGreenColor = [colorPallet headerSetColorWithCodeDesign:designCode];
    UIColor *rowOrangeColor = [colorPallet decorViewSetColorWithCodeDesign:designCode];
    
    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
    formater.locale = [NSLocale localeWithLocaleIdentifier:@"ru_RU"];
    formater.dateFormat = @"d MMMM yyyy HH:mm";
    formater.timeZone = [NSTimeZone systemTimeZone];
    
    if (![message.clearanceDate isEqualToDate:[NSDate dateWithTimeIntervalSince1970:0]]) {
        self.dateLabel.text = [formater stringFromDate:message.clearanceDate];
    }else{
        self.dateLabel.text = [formater stringFromDate:message.date];
    }
    
    ///textForCopy date
//    self.textForCopy.text = [NSString stringWithFormat:@"%@\n", [formater stringFromDate:message.date]];
    
    self.dateLabel.textColor = [UIColor blackColor];
    
    if ([message.isNew isEqualToNumber:@(0)]) {
        
    } else {
        
    }
    
    NSArray *strings = [message.text componentsSeparatedByString:@"@"];
    
    for (UILabel *currentLabel in self.valueLabels) {
        NSInteger index = currentLabel.tag;
        switch (index) {
            case 1:
                currentLabel.text = [NSString stringWithFormat:@"Фрахт FOB %@ - Одесса (наш склад)", strings[index]];
                break;
            case 2:
                currentLabel.text = [NSString stringWithFormat:@"АВТОДОСТАВКА В ГОРОД %@",
                                     [strings[index] uppercaseString]];
                break;
            case 3:
                currentLabel.text = [NSString stringWithFormat:@"%@ кг",
                                     strings[index]];
                break;
            case 16:
                currentLabel.text = [NSString stringWithFormat:@"%@", strings[index]];
                break;
            case 17:
                currentLabel.text = message.orderNumber;
                break;
            default:
                if (currentLabel.tag > 14 && currentLabel.tag < 17) {
//                    currentLabel.text = strings[index];
                } else {
                    NSString *string = ![strings[index] isEqual: @""] ? [NSString stringWithFormat:@"$%@", strings[index]] : @"$0";
                    string = [string stringByReplacingOccurrencesOfString:@"," withString:@"."];
                    currentLabel.text = string;
                }
                break;
        }
        ///textForCopy body
        self.textForCopy.text = [NSString stringWithFormat:@"%@%@\n",
                                 self.textForCopy.text, currentLabel.text];
    }
    
    for (UILabel *currentLabel in self.setColorLabels) {
        NSInteger index = currentLabel.tag;
        switch (index) {
            case 0:
                currentLabel.backgroundColor = rowGreenColor;
                break;
            case 2:
                currentLabel.backgroundColor = rowGreenColor;
                break;
            case 3:
                currentLabel.backgroundColor = rowGreenColor;
                break;
            case 4:
                currentLabel.backgroundColor = colorPallet.brightTelegray;
                break;
            case 15:
                currentLabel.backgroundColor = colorPallet.brightTelegray;
                break;
            case 16:
                currentLabel.backgroundColor = rowOrangeColor;
                break;
            case 17:
                currentLabel.backgroundColor = rowOrangeColor;
                break;
            default:
                break;
        }
    }
    
    for (UILabel * currentLabel in self.setColorBorderViews) {
        currentLabel.backgroundColor = colorPallet.pearlDarkGray;
    }
    
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
