//
//  MessageTypeDesign20Cell.m
//  YourLogisticsNew
//
//  Created by Димон on 22.05.2021.
//  Copyright © 2021 iCenter. All rights reserved.
//

#import "MessageTypeDesign20Cell.h"
#import "ColorPallet.h"

@implementation MessageTypeDesign20Cell



- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)configWithMessage:(ChatMessage *)message {
    NSInteger designCode = message.typeDesign;
    ColorPallet *colorPallet = [[ColorPallet alloc] init];
    colorPallet.isNew = message.isNew.boolValue;
    
    UIColor *headerColor = [colorPallet headerSetColorWithCodeDesign:designCode];
    UIColor *bodyLineColor = [colorPallet decorViewSetColorWithCodeDesign:designCode];
    UIColor *rowOrangeColor = [colorPallet decorViewSetColorWithCodeDesign:designCode];
    
    NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    
    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
    formater.locale = [NSLocale localeWithLocaleIdentifier:@"ru_RU"];
    formater.dateFormat = @"d MMMM yyyy HH:mm";
    formater.timeZone = [NSTimeZone systemTimeZone];
    
    if (![message.clearanceDate isEqualToDate:[NSDate dateWithTimeIntervalSince1970:0]]) {
        self.dateLabel.text = [formater stringFromDate:message.clearanceDate];
    } else {
        self.dateLabel.text = [formater stringFromDate:message.date];
    }
    
    self.dateLabel.textColor = [UIColor blackColor];
    
    if ([message.isNew isEqualToNumber:@(0)]) {
        
    } else {
        
    }
    
    NSArray *strings = [message.text componentsSeparatedByString:@"@"];
    
    for (UILabel *currentLabel in self.valueLabels) {
        NSInteger index = currentLabel.tag;
        switch (index) {
            case 0:
                currentLabel.text = message.orderNumber;
                break;
            case 1:
                currentLabel.text = [NSString stringWithFormat:@"%@", strings[0]];
                break;
            case 2:
                currentLabel.text = [NSString stringWithFormat:@"%@", strings[1]];
                break;
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
                currentLabel.text = [NSString stringWithFormat:@"%@", strings[index]];
                break;
            case 8:
                currentLabel.text = [NSString stringWithFormat:@"%@ %@", strings[8], strings[9]];
                break;
            case 9:
                currentLabel.text = [NSString stringWithFormat:@"%@ %@", strings[10], strings[11]];
                break;
            case 10:
                currentLabel.text = [NSString stringWithFormat:@"%@ %@", strings[12], strings[13]];
                break;
            case 11:
                currentLabel.text = [NSString stringWithFormat:@"%@ %@", strings[14], strings[15]];
                break;
            default:
                break;
        }
    }
    
    self.myImageView.image = [UIImage imageNamed: strings[2]];
    self.line = strings[2];
    
    [self createTextForCopyFrom:strings withDate:self.dateLabel.text];
    
    for (UILabel *currentLabel in self.setColorLabels) {
        NSInteger index = currentLabel.tag;
        switch (index) {
            case 0:
                currentLabel.backgroundColor = rowOrangeColor;
                break;
            case 12:
                currentLabel.backgroundColor = headerColor;
                break;
            default:
                break;
        }
        
    }
    self.setColorView.backgroundColor = headerColor;

    for (UILabel * currentLabel in self.setColorBordersViews) {
        currentLabel.backgroundColor = colorPallet.pearlDarkGray;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (NSString*)getTextByTag:(int)tag {
    NSString* text = [NSString new];
    for (UILabel *lab in self.prepareForCopyLabels) {
        if (tag == lab.tag) { text = lab.text; }
    }
    return text;
}

- (void)createTextForCopyFrom:(NSArray*)strings withDate:(NSString*) date {
    self.textForCopy.text = [NSString stringWithFormat:@"%@\n%@\n%@\n%@ %@\n",
                             date,
                             [self getTextByTag:12],
                             [self getTextByTag:0],
                             [self getTextByTag:1],
                             [self getTextByTag:2]
                             ];
    
    self.textForCopy.text = [NSString stringWithFormat:@"%@\n%@\n%@ %@\n%@ %@\n%@ %@\n%@ %@\n%@ %@\n%@ %@\n%@ %@\n%@ %@\n%@ %@",
                             self.textForCopy.text,
                             
                             self.line,
                                                          
                             [self getTextByTag:13],
                             [self getTextByTag:3],
                             
                             [self getTextByTag:14],
                             [self getTextByTag:4],
                             
                             [self getTextByTag:15],
                             [self getTextByTag:5],
                             
                             [self getTextByTag:16],
                             [self getTextByTag:6],
                             
                             [self getTextByTag:17],
                             [self getTextByTag:7],
                             
                             [self getTextByTag:18],
                             [self getTextByTag:8],
                             
                             [self getTextByTag:19],
                             [self getTextByTag:9],
                             
                             [self getTextByTag:20],
                             [self getTextByTag:10],
                             
                             [self getTextByTag:21],
                             [self getTextByTag:11]
                             
                             ];
}

@end
