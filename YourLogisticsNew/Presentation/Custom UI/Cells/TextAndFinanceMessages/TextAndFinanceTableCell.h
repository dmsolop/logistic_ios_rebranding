//
//  FinanceTableCell.h
//  YourLogisticsNew
//
//  Created by VitaliyK on 09.09.2018.
//  Copyright © 2018 iCenter. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface TextAndFinanceTableCell : UITableViewCell {
    
}

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *leftHeaderLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightHeaderLabel;
@property (weak, nonatomic) IBOutlet UILabel *centerHeaderLabel;
@property (weak, nonatomic) IBOutlet UIView *contentTableView;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIView *separatorHeaderView;
@property (weak, nonatomic) IBOutlet UILabel *textForCopy;

- (void)configWithMessage:(ChatMessage *)message;
- (CGSize)settingFlexibleSizeForLabel:(UILabel*)label byFont:(UIFont*)font withText:(NSString*)text;




@end
