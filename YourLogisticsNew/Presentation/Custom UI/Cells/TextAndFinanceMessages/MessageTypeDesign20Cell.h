//
//  MessageTypeDesign20Cell.h
//  YourLogisticsNew
//
//  Created by Димон on 22.05.2021.
//  Copyright © 2021 iCenter. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MessageTypeDesign20Cell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *textForCopy;

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *setColorBordersViews;

@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *setColorLabels;
@property (weak, nonatomic) IBOutlet UIView *setColorView;

@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *valueLabels;

@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *prepareForCopyLabels;

@property (weak, nonatomic) IBOutlet UIImageView *myImageView;

@property (strong, nonatomic) NSString *line;

- (void)configWithMessage:(ChatMessage *)message;

@end

NS_ASSUME_NONNULL_END
