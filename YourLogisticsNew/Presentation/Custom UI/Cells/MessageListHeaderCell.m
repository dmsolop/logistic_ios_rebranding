//
//  MessageListHeaderCell.m
//  YourLogistics
//
//  Created by Aleksandr on 20.05.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import "MessageListHeaderCell.h"

@interface MessageListHeaderCell ()

@property (weak, nonatomic) IBOutlet UIView *orderNumberColumnView;

@property (weak, nonatomic) IBOutlet UIButton *orderNumberSortingButton;

@property (weak, nonatomic) IBOutlet UILabel *orderNumberTitleLabel;

@property (weak, nonatomic) IBOutlet UILabel *categoryLabel;

@property (weak, nonatomic) IBOutlet UIView *statusColumnView;

@property (weak, nonatomic) IBOutlet UIButton *statusSortingButton;

@property (weak, nonatomic) IBOutlet UILabel *statusTitleLabel;

@property (nonatomic) BOOL isOrderAscending;

@end

@implementation MessageListHeaderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    UITapGestureRecognizer *sortingOrderNumberTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(switchOrderNumberSortingType:)];
    [self.orderNumberColumnView addGestureRecognizer:sortingOrderNumberTapRecognizer];
    UITapGestureRecognizer *sortingStatusTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(switchStatusSortingType:)];
    [self.statusColumnView addGestureRecognizer:sortingStatusTapRecognizer];    
}

-(void)switchOrderNumberSortingType:(UITapGestureRecognizer *)sender{
    
    self.statusSortingButton.selected = NO;
    if (self.orderNumberSortingButton.selected) {
        self.isOrderAscending = !self.isOrderAscending;
    } else {
        self.orderNumberSortingButton.selected = YES;
        self.isOrderAscending = NO;
    }
    [self.orderNumberSortingButton setImage:[self iconImageNameForOrder:self.isOrderAscending] forState:UIControlStateSelected];
    if ([self.delegate respondsToSelector:@selector(selectedSortingType:isAscending:)]) {
        [self.delegate selectedSortingType:SortingTypeOrderNumber isAscending:self.isOrderAscending];
    }
}

-(void)switchStatusSortingType:(UITapGestureRecognizer *)sender{
    self.orderNumberSortingButton.selected = NO;
    if (self.statusSortingButton.selected) {
        self.isOrderAscending = !self.isOrderAscending;
    } else {
        self.statusSortingButton.selected = YES;
        self.isOrderAscending = YES;
    }
    [self.statusSortingButton setImage:[self iconImageNameForOrder:self.isOrderAscending] forState:UIControlStateSelected];
    if ([self.delegate respondsToSelector:@selector(selectedSortingType:isAscending:)]) {
        [self.delegate selectedSortingType:SortingTypeStatus isAscending:self.isOrderAscending];
    }
}

-(UIImage *)iconImageNameForOrder:(BOOL)isAscending{
    return isAscending ? [UIImage imageNamed:@"SortIconAsc"] : [UIImage imageNamed:@"SortIconDesc"];
}

@end
