//
//  LoadingActivityCell.h
//  YourLogistics
//
//  Created by Aleksandr on 06.05.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadingActivityCell : UITableViewCell

-(void)startAnimating;

@end
