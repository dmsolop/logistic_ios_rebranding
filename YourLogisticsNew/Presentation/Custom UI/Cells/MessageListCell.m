//
//  MessageListCell.m
//  YourLogistics
//
//  Created by Aleksandr on 20.05.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import "MessageListCell.h"

@implementation MessageListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.categoryButton.titleLabel.numberOfLines = 0;
    self.categoryButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.statusButton.titleLabel.numberOfLines = 0;
    self.statusButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
}

- (IBAction)selectedOrder:(UIButton *)sender {
//    NSLog(@"%@", [sender.titleLabel.text substringFromIndex:1]);
    FiltringCondition *condition = [[FiltringCondition alloc] initWithType:FilteringTypeOrderNumber filterBy:sender.titleLabel.text];
    if ([self.delegate respondsToSelector:@selector(selectedSampleWithCondition:)])
        [self.delegate selectedSampleWithCondition:condition];
}

- (IBAction)selectedCategory:(UIButton *)sender {
//    NSLog(@"%@", sender.titleLabel.text);
    FiltringCondition *condition = [[FiltringCondition alloc] initWithType:FilteringTypeCategory filterBy:sender.titleLabel.text];
    if ([self.delegate respondsToSelector:@selector(selectedSampleWithCondition:)])
        [self.delegate selectedSampleWithCondition:condition];
}

- (IBAction)selectedStatus:(UIButton *)sender {
//    NSLog(@"%@", sender.titleLabel.text);
    if ([self.delegate respondsToSelector:@selector(selectedSampleWithCondition:)]){
        FiltringCondition *condition = [[FiltringCondition alloc] initWithType:FilteringTypeStatus filterBy:sender.titleLabel.text];
        [self.delegate selectedSampleWithCondition:condition];
    }
}

@end
