//
//  ListOfServicesVC.m
//  YourLogistics
//
//  Created by Aleksandr on 04.05.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import "ListOfServicesVC.h"
#import "APIService.h"
#import "ArticleVC.h"
#import "Article.h"
#import "ServiceCell.h"
#import "ServiceDetailsVC.h"
#import "Utilities.h"
#import "TriangleToTopView.h"
#import "ColorPallet.h"

static const NSInteger kDocumentationServiceId = 1032;

@interface ListOfServicesVC () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSArray *documentationServices;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIView *baseForTriangleTopView;
@property (weak, nonatomic) IBOutlet UIView *topView;

@property (nonatomic, strong) UIRefreshControl *refreshControll;

@property (nonatomic, strong) ColorPallet *colorPallet;


- (void)addGreenTriangleToTop;

@end

@implementation ListOfServicesVC

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    
    self.colorPallet = [ColorPallet new];
    
    self.tableView.estimatedRowHeight = 80.f;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.contentInset = UIEdgeInsetsMake(22, 0, 22, 0);

    if (!self.isDocumentationServiceContent) {
        [self prepareContent];
    }
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
    [self.refreshControll beginRefreshing];
    self.refreshControll = refreshControl;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];

    UINavigationBar *navigationBar = self.navigationController.navigationBar;
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [navigationBar setBarTintColor:self.colorPallet.mintGreen];
    [navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : self.colorPallet.whiteColor}];
    [navigationBar setShadowImage:[[UIImage alloc] init]];
    [navigationBar setBackgroundImage:[[UIImage alloc]init] forBarMetrics:UIBarMetricsDefault];
    [self addGreenTriangleToTop];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:YES];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}

- (void)addGreenTriangleToTop {
    TriangleToTopView *triangleView = [[TriangleToTopView alloc] initWithFrame:CGRectMake(self.baseForTriangleTopView.frame.size.width/2 - self.baseForTriangleTopView.frame.size.height, 0, self.baseForTriangleTopView.frame.size.height*2, self.baseForTriangleTopView.frame.size.height)];
    [self.baseForTriangleTopView addSubview:triangleView];
    self.topView.backgroundColor = self.colorPallet.mintGreen;
}

- (void)refresh:(UIRefreshControl *)refreshControl {
    [self prepareContent];
}

-(void)prepareContent{
    [[APIService sharedAPIClient] getCompanyServicesWithCompletion:^(NSDictionary *services) {
        self.services = services[@"AllServices"];
        self.documentationServices = services[@"Documentation"];
        [self.tableView reloadData];
        [self.refreshControll endRefreshing];
    } failure:^(NSError *error) {
        NSLog(@"Error while getting list of services");
        [self.refreshControll endRefreshing];
        UIAlertController *errorAlert = [Utilities downloadingErrorAlert];
        [self presentViewController:errorAlert animated:YES completion:nil];
    }];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
//    if (self.services) {
        Article *service = self.services[indexPath.row];
        ServiceCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ServiceCell"];
//        cell.cView.layer.shadowColor = [[UIColor colorWithRed:0.f/255.f green:0.f/255.f blue:0.f/255.f alpha:0.3f] CGColor];
//        cell.cView.layer.shadowOpacity = .4f;
//        cell.cView.layer.shadowRadius = 4.f;
//        cell.cView.layer.shadowOffset = CGSizeMake(0, 1);
//        cell.cView.layer.masksToBounds = NO;
    
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineSpacing = 3;
        paragraphStyle.lineBreakMode = NSLineBreakByTruncatingTail;
        cell.serviceNameLabel.attributedText = [[NSAttributedString alloc] initWithString:service.title
                                                                                attributes:@{NSParagraphStyleAttributeName : paragraphStyle,
                                                                                             NSFontAttributeName : [UIFont fontWithName:@"Museo Sans Cyrl" size:16],
                                                                                             NSForegroundColorAttributeName : [UIColor colorWithRed:74/255.f green:74/255.f blue:74/255.f alpha:1.f]
                                                                                             }];
        cell.serviceNameLabel.textAlignment = NSTextAlignmentLeft;
    
        NSURL *url = [NSURL URLWithString:(service.imageLink)];
        NSData *data = [NSData dataWithContentsOfURL:url];
        UIImage *gettedImage = [[UIImage imageWithData: data] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        cell.image.image = gettedImage;
        cell.image.tintColor = [ColorPallet new].baseLightBlue;
        return cell;
//    } else {
//        return [self.tableView dequeueReusableCellWithIdentifier:@"LoadingActivityCell"];
//    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.services.count;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    Article *service = self.services[indexPath.row];
    if (service.ID == kDocumentationServiceId) {
        ListOfServicesVC *losvc = [self.storyboard instantiateViewControllerWithIdentifier:@"ListOfServicesScene"];
        losvc.isDocumentationServiceContent = YES;
        losvc.services = self.documentationServices;
        [self.navigationController pushViewController:losvc animated:YES];
    } else{
        [self performSegueWithIdentifier:@"ShowServiceDetails" sender:service];
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 1)];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"ShowServiceDetails"]) {
        ServiceDetailsViewController *sdvc = segue.destinationViewController;
        [[APIService sharedAPIClient] getCompanyServiceDetailsForService:sender
                                                              completion:^(ArticleDetails *article) {
                                                                  sdvc.service = article;
                                                              }
                                                                 failure:^(NSError *error) {
                                                                     [sdvc showDownloadingError];
                                                                     NSLog(@"Error while downloading service details");
                                                                 }];
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

@end
