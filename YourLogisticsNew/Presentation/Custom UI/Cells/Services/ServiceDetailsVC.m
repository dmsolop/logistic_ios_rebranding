//
//  ServiceDetailsVC.m
//  YourLogistics
//
//  Created by Aleksandr on 19.05.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import "ServiceDetailsVC.h"
#import "ArticleDetails.h"
#import "Utilities.h"

@interface ServiceDetailsVC ()

@property (weak, nonatomic) IBOutlet UITextView *contentTextView;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingActivityView;
//@property (nonatomic) BOOL downloading
@end

@implementation ServiceDetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    UINavigationBar *navigationBar = self.navigationController.navigationBar;
    [navigationBar setBarTintColor:[UIColor clearColor]];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self updateContent];
}

-(void)showDownloadingError{
    [self.loadingActivityView stopAnimating];
    UIAlertController *alert = [Utilities downloadingErrorAlert];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)updateContent{
    if (self.service)
        [self.loadingActivityView stopAnimating];
    NSString *contentWithTitle = [self.service.title stringByAppendingFormat:@"<br><br>%@", self.service.content];
    NSMutableAttributedString *content = [[NSMutableAttributedString alloc] initWithData:[contentWithTitle dataUsingEncoding:NSUTF8StringEncoding]
                                                                   options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                                             NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
                                                        documentAttributes:nil
                                                                     error:nil];
    UIColor *titleColor = [UIColor colorWithRed:140.f/255.f green:213.f/255.f blue:11.f/255.f alpha:1.f];
    [content addAttributes:@{NSFontAttributeName : [UIFont fontWithName:@"Museo Sans Cyrl" size:17.f],
                             NSForegroundColorAttributeName : titleColor}
                     range:NSMakeRange(0, self.service.title.length)];
    [content addAttributes:@{NSFontAttributeName : [UIFont fontWithName:@"Museo Sans Cyrl" size:14.f]}
                     range:NSMakeRange(self.service.title.length, content.length - self.service.title.length - 1)];
    self.contentTextView.attributedText = content;
    self.contentTextView.textContainerInset = UIEdgeInsetsMake(20, 16, 20, 16);
    
    self.navigationItem.title = self.service.title;
}

-(void)setService:(ArticleDetails *)service{
    _service = service;
    if (self.isViewLoaded && self.view.window)
        [self updateContent];
}

@end
