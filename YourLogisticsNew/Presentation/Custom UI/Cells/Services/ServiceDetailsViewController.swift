//
//  ServiceDetailsViewController.swift
//  YourLogisticsNew
//
//  Created by VitaliyK on 05.09.2018.
//  Copyright © 2018 iCenter. All rights reserved.
//

import UIKit
import WebKit

class ServiceDetailsViewController: BasicVC {
    @IBOutlet weak var webViewNew: WKWebView!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    
    var allowLoad = true

    @objc var service: ArticleDetails? {
        didSet {
            title = service?.title
            
            guard let link = service?.link else { return }
            
            activityIndicatorView.startAnimating()
            
            let request = URLRequest(url: link)
            webViewNew.load(request)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.barTintColor = UIColor.white
        webViewNew.navigationDelegate = self
        guard let link = service?.link else { return }
        let request = URLRequest(url: link)
        webViewNew.load(request)
    }

    @objc func showDownloadingError() {
        let alert = Utilities.downloadingErrorAlert()
        present(alert, animated: true)
    }
}

extension ServiceDetailsViewController: WKNavigationDelegate  {

    //Equivalent of shouldStartLoadWithRequest:
        func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
            var action: WKNavigationActionPolicy?

            defer {
                decisionHandler(action ?? .allow)
            }

            guard let url = navigationAction.request.url else { return }
            print("decidePolicyFor - url: \(url)")

            //Uncomment below code if you want to open URL in safari
            /*
            if navigationAction.navigationType == .linkActivated, url.absoluteString.hasPrefix("https://developer.apple.com/") {
                action = .cancel  // Stop in WebView
                UIApplication.shared.open(url)
            }
            */
        }
    
    //Equivalent of webViewDidFinishLoad:
        func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
            print("didFinish - webView.url: \(String(describing: webView.url?.description))")
        }
}
