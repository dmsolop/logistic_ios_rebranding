//
//  ListOfServicesVC.h
//  YourLogistics
//
//  Created by Aleksandr on 04.05.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import "BasicVC.h"

@interface ListOfServicesVC : BasicVC

@property (nonatomic) BOOL isDocumentationServiceContent;

@property (nonatomic, strong) NSArray *services;

@end
