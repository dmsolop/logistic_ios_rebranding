//
//  ServiceDetailsVC.h
//  YourLogistics
//
//  Created by Aleksandr on 19.05.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import "BasicVC.h"

@class ArticleDetails;

@interface ServiceDetailsVC : BasicVC

@property (nonatomic, strong) ArticleDetails *service;

-(void)showDownloadingError;

@end
