//
//  MessageCategoryCell.h
//  YourLogistics
//
//  Created by Aleksandr on 19.05.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageCategoryCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *categoryImageView;

@property (weak, nonatomic) IBOutlet UILabel *categoryTitleLabel;

@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *lastMessageLabel;

-(void)setNumberOfNewMessages:(NSUInteger)newMessages;

@end
