//
//  MessageListCell.h
//  YourLogistics
//
//  Created by Aleksandr on 20.05.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FiltringCondition.h"


@protocol OrderCellDelegate <NSObject>

-(void)selectedSampleWithCondition:(FiltringCondition *)condition;

@end

@interface MessageListCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *orderNumberButton;

@property (weak, nonatomic) IBOutlet UIButton *categoryButton;

@property (weak, nonatomic) IBOutlet UIButton *statusButton;

@property (nonatomic, weak) id <OrderCellDelegate> delegate;

@end
