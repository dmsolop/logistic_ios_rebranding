//
//  MessageCategoryCell.m
//  YourLogistics
//
//  Created by Aleksandr on 19.05.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import "MessageCategoryCell.h"

@interface MessageCategoryCell ()

@property (weak, nonatomic) IBOutlet UIView *numberOfNewMessagesView;

@property (weak, nonatomic) IBOutlet UILabel *numberOfNewMessagesLabel;


@end

@implementation MessageCategoryCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
}

-(void)setNumberOfNewMessages:(NSUInteger)newMessages{
    if (newMessages > 0) {
        self.numberOfNewMessagesView.hidden = NO;
        self.numberOfNewMessagesLabel.text = [NSString stringWithFormat:@"%d", newMessages];
    } else {
        self.numberOfNewMessagesView.hidden = YES;
    }
}

@end
