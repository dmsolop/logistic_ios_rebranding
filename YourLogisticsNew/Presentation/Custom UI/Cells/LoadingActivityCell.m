//
//  LoadingActivityCell.m
//  YourLogistics
//
//  Created by Aleksandr on 06.05.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import "LoadingActivityCell.h"

@interface LoadingActivityCell ()

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@end

@implementation LoadingActivityCell

-(void)startAnimating{
    [self.activityIndicator startAnimating];
}

@end
