//
//  MessageListHeaderCell.h
//  YourLogistics
//
//  Created by Aleksandr on 20.05.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, SortingType) {
    SortingTypeOrderNumber,
    SortingTypeStatus
};

@protocol SortingOrderDelegate <NSObject>

-(void)selectedSortingType:(SortingType)type isAscending:(BOOL)isAscending;

@end

@interface MessageListHeaderCell : UITableViewCell

@property (nonatomic, weak) id <SortingOrderDelegate> delegate;

@end
