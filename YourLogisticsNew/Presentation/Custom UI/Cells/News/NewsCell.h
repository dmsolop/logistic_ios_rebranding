//
//  NewsCell.h
//  YourLogistics
//
//  Created by Aleksandr on 04.05.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *articleImageView;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *articleTitleLabel;
@end
