//
//  NewsCell.m
//  YourLogistics
//
//  Created by Aleksandr on 04.05.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import "NewsCell.h"

@implementation NewsCell

-(void)prepareForReuse{
    self.articleImageView.image = nil;
    self.dateLabel.text = @"";
    self.articleTitleLabel.text = @"";
}

@end
