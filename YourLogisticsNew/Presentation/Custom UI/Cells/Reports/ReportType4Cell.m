//
//  ReportType4Cell.m
//  YourLogisticsNew
//
//  Created by Dima on 5/8/19.
//  Copyright © 2019 iCenter. All rights reserved.
//

#import "ReportType4Cell.h"
#import <PureLayout/PureLayout.h>
#import "ColorPallet.h"
#import "ReportClass.h"
#import "ManagerClass.h"
#import "DayClass.h"

@interface ReportType4Cell()


@property (nonatomic, strong) UIStackView *horizontalStackView;
@property (nonatomic, copy) ColorPallet *colorSet;

@end

@implementation ReportType4Cell

- (void)configWithReport:(ReportClass *)report {
    [self layoutIfNeeded];
    
    NSArray <ManagerClass *> *managers = report.managers;
    
    ///Settings of colors
    self.colorSet = [ColorPallet new];
    self.colorSet.isNew = report.isNew;
    [self.colorSet prepareColors];
//    UIColor *nameLabelColor = [self.colorSet lime];
    UIColor *commonLabelColor = [self.colorSet brightGreenColor];

    
    ///Settings of fonts
    UIFont *myFont = [UIFont fontWithName:@"MuseoSansCyrl-300" size:11];
    UIFont *myFontBold = [UIFont fontWithName:@"MuseoSansCyrl-500" size:12];
    
    ///Settings of date format
    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
    formater.locale = [NSLocale localeWithLocaleIdentifier:@"ru_RU"];
    formater.dateFormat = @"d MMMM yyyy HH:mm";
    formater.timeZone = [NSTimeZone systemTimeZone];
    
    [self.contentTableView.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj removeFromSuperview];
    }];

    NSMutableArray<NSString *> *days = [NSMutableArray array];
    for (DayClass *date in managers[0].days) {
        NSString *day = [date.date componentsSeparatedByString:@"."][0];
        [days addObject:day];
    }
    
    ///Settings of common dimensions of column of managers
    CGFloat nameLabelHight = 70.f;
    CGFloat otherLabelsHeight = 25.f;
//    CGFloat daysWidth = otherLabelsHeight * 2;
//    CGFloat otherManagersWidth = (self.contentTableView.bounds.size.width - daysWidth) / report.managers.count;
    NSInteger daysAmount = days.count;
    CGFloat columnHeight = otherLabelsHeight * daysAmount + nameLabelHight;

    
    ///Settings of stackView
    self.horizontalStackView = nil;
    self.horizontalStackView = [[UIStackView alloc] init];
    [self.contentTableView addSubview:self.horizontalStackView];
    
    [self.contentTableView autoSetDimension:(ALDimension) NSLayoutAttributeHeight toSize:columnHeight];
    [self.horizontalStackView autoPinEdgesToSuperviewEdges];
    self.horizontalStackView.axis = UILayoutConstraintAxisHorizontal;
    self.horizontalStackView.distribution = UIStackViewDistributionFillEqually;
    self.horizontalStackView.alignment = UIStackViewAlignmentFill;
    self.horizontalStackView.spacing = 0;
    self.horizontalStackView.backgroundColor = UIColor.blueColor;
    
    ///TextTableviewCell
    ///Date
    self.dateLabel.text = [formater stringFromDate:report.date];
    
    ///Header
    self.headerLabel.text = report.reportName;
    
    ///Body rows
    ///Get all date insert in stackView

    UIStackView *dayViews = [[UIStackView alloc] init];
    dayViews.axis = UILayoutConstraintAxisVertical;
    dayViews.distribution = UIStackViewDistributionEqualSpacing;
    dayViews.alignment = UIStackViewAlignmentFill;
    dayViews.spacing = 0;
    [self.horizontalStackView addArrangedSubview:dayViews];
    
    
    self.headerNameView1 = [[UIView alloc] init];
    [self.headerNameView1 autoSetDimension:(ALDimension) NSLayoutAttributeHeight toSize:nameLabelHight];
    self.headerNameView1.borderColor = UIColor.blackColor;
    self.headerNameView1.borderWidth = 0.8;

    [dayViews addArrangedSubview:self.headerNameView1];
    
    NSInteger index = daysAmount / 2;
    
    for (NSString *day in days) {
        
        UIView *dayView = [[UIView alloc] init];
        UILabel *dayLabel = [[UILabel alloc] init];
        dayView.backgroundColor = UIColor.grayColor;
        [dayView autoSetDimension:(ALDimension) NSLayoutAttributeHeight toSize:otherLabelsHeight];
        dayView.borderColor = UIColor.blackColor;
        dayView.borderWidth = 0.8;
        [dayView addSubview:dayLabel];
        [dayLabel autoPinEdgesToSuperviewEdges];
        dayLabel.font = myFontBold;
        dayLabel.textAlignment = NSTextAlignmentCenter;
        [dayViews addArrangedSubview:dayView];
        dayLabel.text = day;
        
        if (index > 0) {
            dayView.backgroundColor = UIColor.blackColor;
            dayLabel.textColor = UIColor.whiteColor;
            dayView.borderColor = UIColor.grayColor;
        }
        index --;
    }
    
    for (ManagerClass *manager in report.managers) {
        
        UIStackView *columnViews = [[UIStackView alloc] init];
        columnViews.axis = UILayoutConstraintAxisVertical;
        columnViews.distribution = UIStackViewDistributionEqualSpacing;
        columnViews.alignment = UIStackViewAlignmentFill;
        columnViews.spacing = 0;
        [self.horizontalStackView addArrangedSubview:columnViews];
        
        self.headerNameView2 = [[UIView alloc] init];
        UILabel *headerLabel = [[UILabel alloc] init];
        [self.headerNameView2 autoSetDimension:(ALDimension) NSLayoutAttributeHeight toSize:nameLabelHight];
        self.headerNameView2.borderColor = UIColor.blackColor;
        self.headerNameView2.backgroundColor = commonLabelColor;
        self.headerNameView2.borderWidth = 0.8;
        [self.headerNameView2 addSubview:headerLabel];
        headerLabel.font = myFontBold;
        headerLabel.textAlignment = NSTextAlignmentLeft;
        [headerLabel setTransform:CGAffineTransformMakeRotation(-M_PI / 2)];
        [headerLabel autoSetDimension:(ALDimension) NSLayoutAttributeWidth toSize:nameLabelHight - 10];
        [headerLabel autoCenterInSuperview];
        [columnViews addArrangedSubview:self.headerNameView2];
        
        headerLabel.text = manager.name;
        

        for (DayClass *day in manager.days) {
            UIView *dayView = [[UIView alloc] init];
            [dayView autoSetDimension:(ALDimension) NSLayoutAttributeHeight toSize:otherLabelsHeight];
            UILabel *dayLabel = [[UILabel alloc] init];
            dayView.borderColor = UIColor.blackColor;
            dayView.borderWidth = 0.8;
            [dayView addSubview:dayLabel];
            [dayLabel autoPinEdgesToSuperviewEdges];
            dayLabel.font = myFont;
            dayLabel.textAlignment = NSTextAlignmentCenter;
            dayLabel.numberOfLines = 1;
            dayLabel.adjustsFontSizeToFitWidth = true;
            dayLabel.minimumScaleFactor = 0.5;
            dayLabel.backgroundColor = day.effectiveness ? UIColor.yellowColor : UIColor.whiteColor;//commonLabelColor;
            dayLabel.text = [day.client_code  isEqual: @""] ? @" " : [day.client_code stringByReplacingOccurrencesOfString:@"U" withString:@""];
            [columnViews addArrangedSubview:dayView];
            
        }
    }
}

@end

