//
//  ReportType5Cell.m
//  YourLogisticsNew
//
//  Created by Dima on 5/13/19.
//  Copyright © 2019 iCenter. All rights reserved.
//

#import "ReportType5Cell.h"
#import <PureLayout/PureLayout.h>
#import "ColorPallet.h"
#import "ReportClass.h"
#import "ClientDebitClass.h"
#import "OrderClass.h"


@interface ReportType5Cell()

@property (nonatomic, strong) UIStackView *mainStackView;
@property (nonatomic, copy) ColorPallet *colorSet;

@end

@implementation ReportType5Cell

- (void)configWithReport:(ReportClass *)report {
//    [self layoutIfNeeded];

    NSArray<ManagerClass *> *clients = report.clients;
    
    ///Settings of colors
    self.colorSet = [ColorPallet new];
    self.colorSet.isNew = report.isNew;
    [self.colorSet prepareColors];
    UIColor *blackColor = self.colorSet.blackColor;
    UIColor *whiteColor = self.colorSet.whiteColor;

    ///Settings of fonts
    UIFont *myFont = [UIFont fontWithName:@"MuseoSansCyrl-300" size:11];
    UIFont *myFontBold = [UIFont fontWithName:@"MuseoSansCyrl-500" size:13];

    ///Settings of date format
    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
    formater.locale = [NSLocale localeWithLocaleIdentifier:@"ru_RU"];
    formater.dateFormat = @"d MMMM yyyy HH:mm";
    formater.timeZone = [NSTimeZone systemTimeZone];

    [self.contentTableView.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj removeFromSuperview];
    }];


    ///Settings of common width of column
    CGFloat selfBoundsWidth = self.contentTableView.bounds.size.width;
    
    CGFloat firstSmallerWidth = (selfBoundsWidth / 3) * 1.3f;
    CGFloat firstBiggerWidth = selfBoundsWidth - firstSmallerWidth;
    
    CGFloat secondBiggerWidth = (firstSmallerWidth / 3.f) * 2;
    CGFloat secondSmallerWidth = firstSmallerWidth - secondBiggerWidth;
    CGFloat secondAverageWidth = firstBiggerWidth / 4;
    
    CGFloat thirdBiggerWidth = selfBoundsWidth - secondAverageWidth;
    
    CGFloat firstSizeHeight = 40.f;
    CGFloat secondSizeHeight = 30.f;
    
    ///Setting of common height ofreport
    CGFloat heightOfOrdersBlock = 0.f;
    CGFloat heightOfClientBlock = 0.f;
    CGFloat heightOfReport = 0.f;

    for (ClientDebitClass *client in report.clients) {
        heightOfOrdersBlock = secondSizeHeight * client.orders.count;
        heightOfClientBlock = heightOfOrdersBlock + secondSizeHeight * 2 + firstSizeHeight * 5;
        heightOfReport += heightOfClientBlock;
    }

    ///Settings of stackView
    self.mainStackView = nil;
    self.mainStackView = [[UIStackView alloc] init];
    self.mainStackView.axis = UILayoutConstraintAxisVertical;
    self.mainStackView.distribution = UIStackViewDistributionFillProportionally;
    self.mainStackView.alignment = UIStackViewAlignmentFill;
    self.mainStackView.spacing = 0;
    
    [self.contentTableView addSubview:self.mainStackView];
    [self.contentTableView autoSetDimension:(ALDimension) NSLayoutAttributeHeight toSize:heightOfReport];
    [self.mainStackView autoPinEdgesToSuperviewEdges];
    
    ///TextTableviewCell
    ///Date
    self.dateLabel.text = [formater stringFromDate:report.date];

    ///Header
    self.headerLabel.text = report.reportName;

    ///Body rows
    
    for (ClientDebitClass *client in clients) {
    ///Add Client code
        UIView *clientCodeView =  [self makeViewWithColor:self.colorSet.reportGrayColor width:selfBoundsWidth height:firstSizeHeight];
        UILabel *clientCodeLabel = [self makeLabelWithFont:myFontBold textColor:whiteColor alignmentCenter:YES];
        [clientCodeView addSubview:clientCodeLabel];
        [clientCodeLabel autoPinEdgesToSuperviewEdges];
        clientCodeLabel.text = client.clientCode;
        
        [self.mainStackView addArrangedSubview:clientCodeView];

    ///Add credit of trust
        UIStackView *firstStackView = [self makeStackViewWithHeight:secondSizeHeight];
        [self.mainStackView addArrangedSubview:firstStackView];
        
        UIView *trustViewName = [self makeViewWithColor:self.colorSet.reportYelowColor
                                              width:firstSmallerWidth
                                             height:secondSizeHeight];
        
        UILabel *trustLabelName = [self makeLabelWithFont:myFont
                                            textColor:blackColor
                                      alignmentCenter:YES];
        
        trustLabelName.text = @"Кредит доверия";
        [trustViewName addSubview:trustLabelName];
        [trustLabelName autoPinEdgesToSuperviewEdges];

        [firstStackView addArrangedSubview:trustViewName];
        
        UIView *trustViewValue = [self makeViewWithColor:self.colorSet.reportYelowColor
                                                  width:firstSmallerWidth
                                                 height:secondSizeHeight];
        
        UILabel *trustLabelValue = [self makeLabelWithFont:myFont
                                                textColor:blackColor
                                          alignmentCenter:YES];
        
        trustLabelValue.text = client.clientCode;
        [trustViewValue addSubview:trustLabelValue];
        [trustLabelValue autoPinEdgesToSuperviewEdges];
        
        [firstStackView addArrangedSubview:trustViewValue];
        
    ///Add date to work
        UIStackView *secondStackView = [self makeStackViewWithHeight:secondSizeHeight];
        [self.mainStackView addArrangedSubview:secondStackView];
        
        UIView *dateToWorkViewName = [self makeViewWithColor:UIColor.whiteColor//self.colorSet.reportGreenColor
                                                  width:firstSmallerWidth
                                                 height:secondSizeHeight];
        
        UILabel *dateToWorkLabelName = [self makeLabelWithFont:myFont
                                                textColor:blackColor
                                          alignmentCenter:YES];
        
        dateToWorkLabelName.text = @"Кредит доверия";
        [dateToWorkViewName addSubview:dateToWorkLabelName];
        [dateToWorkLabelName autoPinEdgesToSuperviewEdges];
        
        [secondStackView addArrangedSubview:dateToWorkViewName];
        
        UIView *dateToWorkViewValue = [self makeViewWithColor:UIColor.whiteColor//self.colorSet.reportGreenColor
                                                   width:firstSmallerWidth
                                                  height:secondSizeHeight];
        
        UILabel *dateToWorkLabelValue = [self makeLabelWithFont:myFont
                                                 textColor:blackColor
                                           alignmentCenter:YES];
        
        dateToWorkLabelValue.text = client.clientCode;
        [dateToWorkViewValue addSubview:dateToWorkLabelValue];
        [dateToWorkLabelValue autoPinEdgesToSuperviewEdges];
        
        [secondStackView addArrangedSubview:dateToWorkViewValue];
        
    ///Add header to order block
        UIStackView *thirdStackView = [self makeStackViewWithHeight:firstSizeHeight];
        [self.mainStackView addArrangedSubview:thirdStackView];
        
        UIView *orderNumberView = [self makeViewWithColor:self.colorSet.whiteColor
                                                       width:secondBiggerWidth
                                                      height:firstSizeHeight];
        
        UILabel *orderNumberLabel = [self makeLabelWithFont:myFont
                                                     textColor:blackColor
                                               alignmentCenter:YES];
        
        orderNumberLabel.text = @"Номер заявки";
        [orderNumberView addSubview:orderNumberLabel];
        [orderNumberLabel autoPinEdgesToSuperviewEdges];
        
        [thirdStackView addArrangedSubview:orderNumberView];
        
        
        UIView *orderDateView = [self makeViewWithColor:self.colorSet.whiteColor
                                                        width:secondSmallerWidth
                                                       height:firstSizeHeight];
        
        UILabel *orderDateLabel = [self makeLabelWithFont:myFont
                                                      textColor:blackColor
                                                alignmentCenter:YES];
        
        orderDateLabel.text = @"Дата\nсчета";
        [orderDateView addSubview:orderDateLabel];
        [orderDateLabel autoPinEdgesToSuperviewEdges];
        
        [thirdStackView addArrangedSubview:orderDateView];
        
        
        UIView *usdView = [self makeViewWithColor:self.colorSet.reportGreeGrayColor
                                                  width:secondAverageWidth
                                                 height:firstSizeHeight];
        
        UILabel *usdLabel = [self makeLabelWithFont:myFont
                                                textColor:blackColor
                                          alignmentCenter:YES];
        
        usdLabel.text = @"USD";
        [usdView addSubview:usdLabel];
        [usdLabel autoPinEdgesToSuperviewEdges];
        
        [thirdStackView addArrangedSubview:usdView];
        
        
        UIView *uahView = [self makeViewWithColor:self.colorSet.whiteColor
                                            width:secondAverageWidth
                                           height:firstSizeHeight];
        
        UILabel *uahLabel = [self makeLabelWithFont:myFont
                                          textColor:blackColor
                                    alignmentCenter:YES];
        
        uahLabel.text = @"UAH";
        [uahView addSubview:uahLabel];
        [uahLabel autoPinEdgesToSuperviewEdges];
        
        [thirdStackView addArrangedSubview:uahView];
        
        
        UIView *eurView = [self makeViewWithColor:self.colorSet.reportGreeGrayColor
                                            width:secondAverageWidth
                                           height:firstSizeHeight];
        
        UILabel *eurLabel = [self makeLabelWithFont:myFont
                                          textColor:blackColor
                                    alignmentCenter:YES];
        
        eurLabel.text = @"EUR";
        [eurView addSubview:eurLabel];
        [eurLabel autoPinEdgesToSuperviewEdges];
        
        [thirdStackView addArrangedSubview:eurView];
        
        
        UIView *rubView = [self makeViewWithColor:self.colorSet.whiteColor
                                            width:secondAverageWidth
                                           height:firstSizeHeight];
        
        UILabel *rubLabel = [self makeLabelWithFont:myFont
                                          textColor:blackColor
                                    alignmentCenter:YES];
        
        rubLabel.text = @"RUB";
        [rubView addSubview:rubLabel];
        [rubLabel autoPinEdgesToSuperviewEdges];
        
        [thirdStackView addArrangedSubview:rubView];
        
    ///Add orders block
        for (OrderClass *order in client.orders) {
            
            UIStackView *fourthStackView = [self makeStackViewWithHeight:secondSizeHeight];
            [self.mainStackView addArrangedSubview:fourthStackView];
            
            UIView *orderNumberView = [self makeViewWithColor:self.colorSet.whiteColor
                                                        width:secondBiggerWidth
                                                       height:secondSizeHeight];
            
            UILabel *orderNumberLabel = [self makeLabelWithFont:myFont
                                                      textColor:blackColor
                                                alignmentCenter:YES];
            
            orderNumberLabel.text = order.orderNumber;
            [orderNumberView addSubview:orderNumberLabel];
            [orderNumberLabel autoPinEdgesToSuperviewEdges];
            
            [fourthStackView addArrangedSubview:orderNumberView];
            
            
            UIView *orderDateView = [self makeViewWithColor:self.colorSet.whiteColor
                                                      width:secondSmallerWidth
                                                     height:secondSizeHeight];
            
            UILabel *orderDateLabel = [self makeLabelWithFont:myFont
                                                    textColor:blackColor
                                              alignmentCenter:YES];
            
            orderDateLabel.text = order.invoiceDate;
            [orderDateView addSubview:orderDateLabel];
            [orderDateLabel autoPinEdgesToSuperviewEdges];
            [orderDateLabel autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:5];
            
            [fourthStackView addArrangedSubview:orderDateView];
            
            
            UIView *usdView = [self makeViewWithColor:self.colorSet.reportGreeGrayColor
                                                width:secondAverageWidth
                                               height:secondSizeHeight];
            
            UILabel *usdLabel = [self makeLabelWithFont:myFont
                                              textColor:blackColor
                                        alignmentCenter:YES];
            
            usdLabel.text = [@(order.usd) stringValue];
            [usdView addSubview:usdLabel];
            [usdLabel autoPinEdgesToSuperviewEdges];
            
            [fourthStackView addArrangedSubview:usdView];
            
            
            UIView *uahView = [self makeViewWithColor:self.colorSet.whiteColor
                                                width:secondAverageWidth
                                               height:secondSizeHeight];
            
            UILabel *uahLabel = [self makeLabelWithFont:myFont
                                              textColor:blackColor
                                        alignmentCenter:YES];
            
            uahLabel.text = [@(order.uah) stringValue];
            [uahView addSubview:uahLabel];
            [uahLabel autoPinEdgesToSuperviewEdges];
            
            [fourthStackView addArrangedSubview:uahView];
            
            
            UIView *eurView = [self makeViewWithColor:self.colorSet.reportGreeGrayColor
                                                width:secondAverageWidth
                                               height:secondSizeHeight];
            
            UILabel *eurLabel = [self makeLabelWithFont:myFont
                                              textColor:blackColor
                                        alignmentCenter:YES];
            
            eurLabel.text = [@(order.eur) stringValue];
            [eurView addSubview:eurLabel];
            [eurLabel autoPinEdgesToSuperviewEdges];
            
            [fourthStackView addArrangedSubview:eurView];
            
            
            UIView *rubView = [self makeViewWithColor:self.colorSet.whiteColor
                                                width:secondAverageWidth
                                               height:secondSizeHeight];
            
            UILabel *rubLabel = [self makeLabelWithFont:myFont
                                              textColor:blackColor
                                        alignmentCenter:YES];
            
            rubLabel.text = [@(order.rub) stringValue];
            [rubView addSubview:rubLabel];
            [rubLabel autoPinEdgesToSuperviewEdges];
            
            [fourthStackView addArrangedSubview:rubView];
        }
        
    ///Add cause
        UIStackView *fifthStackView = [self makeStackViewWithHeight:firstSizeHeight];
        [self.mainStackView addArrangedSubview:fifthStackView];
        
        UIView *causeViewName = [self makeViewWithColor:self.colorSet.reportBlackColor
                                                  width:secondAverageWidth
                                                 height:firstSizeHeight];
        
        UILabel *causeLabelName = [self makeLabelWithFont:myFont
                                                textColor:whiteColor
                                          alignmentCenter:YES];
        
        causeLabelName.text = @"Причина";
        [causeViewName addSubview:causeLabelName];
        [causeLabelName autoPinEdgesToSuperviewEdges];
        
        [fifthStackView addArrangedSubview:causeViewName];
        
        UIView *causeViewValue = [self makeViewWithColor:self.colorSet.reportBlackColor
                                                   width:thirdBiggerWidth
                                                  height:firstSizeHeight];
        
        UILabel *causeLabelValue = [self makeLabelWithFont:myFont
                                                 textColor:whiteColor
                                           alignmentCenter:NO];
        
        causeLabelValue.text = client.cause;
        [causeViewValue addSubview:causeLabelValue];
        [causeLabelValue autoPinEdgesToSuperviewEdges];
        
        [fifthStackView addArrangedSubview:causeViewValue];
        
        
    ///Add action
        UIStackView *sixthStackView = [self makeStackViewWithHeight:firstSizeHeight];
        [self.mainStackView addArrangedSubview:sixthStackView];
        
        UIView *actionViewName = [self makeViewWithColor:self.colorSet.reportLightGrayColor
                                                  width:secondAverageWidth
                                                 height:firstSizeHeight];
        
        UILabel *actionLabelName = [self makeLabelWithFont:myFont
                                                textColor:blackColor
                                          alignmentCenter:YES];
        
        actionLabelName.text = @"Действия";
        [actionViewName addSubview:actionLabelName];
        [actionLabelName autoPinEdgesToSuperviewEdges];
        
        [sixthStackView addArrangedSubview:actionViewName];
        
        UIView *actionViewValue = [self makeViewWithColor:self.colorSet.reportLightGrayColor
                                                   width:thirdBiggerWidth
                                                  height:firstSizeHeight];
        
        UILabel *actionLabelValue = [self makeLabelWithFont:myFont
                                                 textColor:blackColor
                                           alignmentCenter:NO];
        
        actionLabelValue.text = client.action;
        [actionViewValue addSubview:actionLabelValue];
        [actionLabelValue autoPinEdgesToSuperviewEdges];
        
        [sixthStackView addArrangedSubview:actionViewValue];
        
        
    ///Add total and debt status
        UIStackView *seventhStackView = [self makeStackViewWithHeight:firstSizeHeight];
        [self.mainStackView addArrangedSubview:seventhStackView];
        
        UIView *debtStatusView = [self makeViewWithColor:self.colorSet.reportYelowColor
                                                   width:firstSmallerWidth
                                                  height:firstSizeHeight];
        
        UILabel *debtStatusLabel = [self makeLabelWithFont:myFont
                                                 textColor:blackColor
                                           alignmentCenter:YES];
        
        debtStatusLabel.text = client.debtStatus;
        [debtStatusView addSubview:debtStatusLabel];
        [debtStatusLabel autoPinEdgesToSuperviewEdges];
        
        [seventhStackView addArrangedSubview:debtStatusView];
        
        UIView *totalUsdView = [self makeViewWithColor:self.colorSet.reportYelowColor
                                                    width:secondAverageWidth
                                                   height:firstSizeHeight];
        
        UILabel *totalUsdLabel = [self makeLabelWithFont:myFont
                                                  textColor:blackColor
                                            alignmentCenter:YES];
        
        totalUsdLabel.text = [@(client.totalUSD) stringValue];
        [totalUsdView addSubview:totalUsdLabel];
        [totalUsdLabel autoPinEdgesToSuperviewEdges];
        
        [seventhStackView addArrangedSubview:totalUsdView];
        
        
        UIView *totalUahView = [self makeViewWithColor:self.colorSet.reportYelowColor
                                                 width:secondAverageWidth
                                                height:firstSizeHeight];
        
        UILabel *totalUahLabel = [self makeLabelWithFont:myFont
                                               textColor:blackColor
                                         alignmentCenter:YES];
        
        totalUahLabel.text = [@(client.totalUAH) stringValue];
        [totalUahView addSubview:totalUahLabel];
        [totalUahLabel autoPinEdgesToSuperviewEdges];
        
        [seventhStackView addArrangedSubview:totalUahView];
        
        
        UIView *totalEurView = [self makeViewWithColor:self.colorSet.reportYelowColor
                                                 width:secondAverageWidth
                                                height:firstSizeHeight];
        
        UILabel *totalEurLabel = [self makeLabelWithFont:myFont
                                               textColor:blackColor
                                         alignmentCenter:YES];
        
        totalEurLabel.text = [@(client.totalEUR) stringValue];
        [totalEurView addSubview:totalEurLabel];
        [totalEurLabel autoPinEdgesToSuperviewEdges];
        
        [seventhStackView addArrangedSubview:totalEurView];
        
        
        UIView *totalRubView = [self makeViewWithColor:self.colorSet.reportYelowColor
                                                 width:secondAverageWidth
                                                height:firstSizeHeight];
        
        UILabel *totalRubLabel = [self makeLabelWithFont:myFont
                                               textColor:blackColor
                                         alignmentCenter:YES];
        
        totalRubLabel.text = [@(client.totalRUB) stringValue];
        [totalRubView addSubview:totalRubLabel];
        [totalRubLabel autoPinEdgesToSuperviewEdges];
        
        [seventhStackView addArrangedSubview:totalRubView];
    }

//    [self layoutIfNeeded];


}

-(UIStackView*)makeStackViewWithHeight:(CGFloat) height {
    UIStackView *stackView = [[UIStackView alloc] init];
    [stackView autoSetDimension:(ALDimension) NSLayoutAttributeHeight toSize:height];
    stackView.axis = UILayoutConstraintAxisHorizontal;
    stackView.distribution = UIStackViewDistributionFillProportionally;
    stackView.alignment = UIStackViewAlignmentFill;
    stackView.spacing = 0;
    return stackView;
}

-(UIView*)makeViewWithColor:(UIColor*)color width:(CGFloat)width height:(CGFloat)height {
    UIView *view = [[UIView alloc] init];
    [view autoSetDimension:(ALDimension) NSLayoutAttributeHeight toSize:height];
    [view autoSetDimension:(ALDimension) NSLayoutAttributeWidth toSize:width];
    view.backgroundColor = color;
    view.borderColor = UIColor.blackColor;
    view.borderWidth = 0.8;
    return view;
}

-(UILabel*)makeLabelWithFont:(UIFont*)font textColor:(UIColor*)color alignmentCenter:(BOOL)alignment {
    UILabel *label = [[UILabel alloc] init];
    label.font = font;
    label.textAlignment = alignment ? NSTextAlignmentCenter : NSTextAlignmentLeft;
    label.textColor = color;
    return label;
}

@end
