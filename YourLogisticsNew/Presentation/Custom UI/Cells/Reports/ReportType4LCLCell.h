//
//  ReportType4LCLCell.h
//  YourLogisticsNew
//
//  Created by Dima on 5/23/19.
//  Copyright © 2019 iCenter. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReportClass.h"

NS_ASSUME_NONNULL_BEGIN

@interface ReportType4LCLCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property (weak, nonatomic) IBOutlet UIView *contentTableView;

@property (nonatomic, strong) UIView *headerNameView1;
@property (nonatomic, strong) UIView *headerNameView2;

- (void)configWithReport:(ReportClass *)report;

@end

NS_ASSUME_NONNULL_END
