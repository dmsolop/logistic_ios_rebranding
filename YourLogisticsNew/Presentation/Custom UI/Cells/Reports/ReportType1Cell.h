//
//  ReportType1Cell.h
//  YourLogisticsNew
//
//  Created by Dima on 3/7/19.
//  Copyright © 2019 iCenter. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ReportClass;

NS_ASSUME_NONNULL_BEGIN

@interface ReportType1Cell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property (weak, nonatomic) IBOutlet UILabel *firstLabel;
@property (weak, nonatomic) IBOutlet UILabel *lastLabel;
@property (weak, nonatomic) IBOutlet UILabel *stackViewsLabel;
@property (weak, nonatomic) IBOutlet UIView *contentTableView;
@property (weak, nonatomic) IBOutlet UIStackView *headerStackView;

- (void)configWithReport:(ReportClass *)report;

@end

NS_ASSUME_NONNULL_END
