//
//  ReportType5Cell.h
//  YourLogisticsNew
//
//  Created by Dima on 5/13/19.
//  Copyright © 2019 iCenter. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReportClass.h"

NS_ASSUME_NONNULL_BEGIN

@interface ReportType5Cell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property (weak, nonatomic) IBOutlet UIView *contentTableView;

- (void)configWithReport:(ReportClass *)report;

@end

NS_ASSUME_NONNULL_END
