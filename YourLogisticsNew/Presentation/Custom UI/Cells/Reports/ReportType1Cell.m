//
//  ReportType1Cell.m
//  YourLogisticsNew
//
//  Created by Dima on 3/7/19.
//  Copyright © 2019 iCenter. All rights reserved.
//

#import "ReportType1Cell.h"
#import <PureLayout/PureLayout.h>
#import "ColorPallet.h"
#import "Constants.h"
#import "TriangleView.h"
#import "CurveLinePath.h"
#import "DividerView.h"
#import "ReportClass.h"

@interface ReportType1Cell()

@property (nonatomic, strong) UIStackView *stackView;
@property (nonatomic, copy) ColorPallet *colorSet;

@end

@implementation ReportType1Cell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)prepareForReuse {
    [super prepareForReuse];
    [self.stackView removeAllArrangedSubviews];
}


- (void)configWithReport:(ReportClass *)report {
    
    CGFloat reportType = report.type;
    self.colorSet = [ColorPallet new];
    self.colorSet.isNew = report.isNew;
    [self.colorSet prepareColors];
    Constants *constant = [[Constants alloc] init];
    
    UIFont *myFont = [UIFont fontWithName:@"MuseoSansCyrl-300" size:11];
    UIFont *myFontBold = [UIFont fontWithName:@"MuseoSansCyrl-500" size:12];
    
    UIColor *nameLabelColor = [self.colorSet lightLightGreen];
    UIColor *commonLabelColor = [self.colorSet greenTea];
    
    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
    formater.locale = [NSLocale localeWithLocaleIdentifier:@"ru_RU"];
    formater.dateFormat = @"d MMMM yyyy HH:mm";
    formater.timeZone = [NSTimeZone systemTimeZone];
    
    [self.contentTableView.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj removeFromSuperview];
    }];
    
    if (report.managers.count == 0 || report.managers == nil) {
        UILabel *noDataLabel = [[UILabel alloc] init];
        [self.contentTableView addSubview:noDataLabel];
        [noDataLabel autoSetDimension:(ALDimension) NSLayoutAttributeHeight toSize:50];
        [noDataLabel autoPinEdgesToSuperviewEdges];
        noDataLabel.backgroundColor = UIColor.orangeColor;
        noDataLabel.borderColor = UIColor.blackColor;
        noDataLabel.borderWidth = 0.8;
        noDataLabel.text = constant.noDataForDisplay;
        noDataLabel.textAlignment = NSTextAlignmentCenter;
        self.dateLabel.text = [formater stringFromDate:report.date];
        self.headerLabel.text = report.reportName;
        return;
    }
    
    self.stackView = nil;
    
    self.stackView = [[UIStackView alloc] init];
    [self.contentTableView addSubview:self.stackView];
    
    [self.stackView autoPinEdgesToSuperviewEdges];
    
    self.stackView.axis = UILayoutConstraintAxisVertical;
    self.stackView.distribution = UIStackViewDistributionFillProportionally;
    self.stackView.alignment = UIStackViewAlignmentFill;
    self.stackView.spacing = 0;
    
    ///TextTableviewCell
    ///Date
    self.dateLabel.text = [formater stringFromDate:report.date];
    
    ///Header
    self.headerLabel.text = report.reportName;

    ///Body rows
    for (ManagerClass *manager in report.managers) {
        
        DividerView *dividerView1 = [[DividerView alloc] initWithType:reportType];
        DividerView *dividerView2 = [[DividerView alloc] initWithType:reportType];
        DividerView *dividerView3 = [[DividerView alloc] initWithType:reportType];
        
        TriangleView *decorView1 = [[TriangleView alloc] init];
        TriangleView *decorView2 = [[TriangleView alloc] init];
        TriangleView *decorView3 = [[TriangleView alloc] init];
        
        CurveLinePath *curveLine1 = [[CurveLinePath alloc] init];
        CurveLinePath *curveLine2 = [[CurveLinePath alloc] init];
        CurveLinePath *curveLine3 = [[CurveLinePath alloc] init];
        
        NSMutableArray *labels = [NSMutableArray array];
        
        UIStackView *rowView = [[UIStackView alloc] init];
        rowView.axis = UILayoutConstraintAxisHorizontal;
        rowView.distribution = UIStackViewDistributionFillProportionally;
        rowView.alignment = UIStackViewAlignmentFill;
        rowView.spacing = 0;
        
        [rowView autoSetDimension:(ALDimension) NSLayoutAttributeHeight toSize:50];
        [rowView autoSetDimension:(ALDimension) NSLayoutAttributeWidth toSize:self.contentTableView.frame.size.width];
        
        [self.stackView addArrangedSubview:rowView];

        UIView *nameView = [[UIView alloc] init];
        UILabel *nameLabel = [[UILabel alloc] init];
        [nameView addSubview:nameLabel];
        [nameLabel autoPinEdgesToSuperviewEdges];
        [nameLabel autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:4];
        nameLabel.font = myFontBold;
        nameLabel.textAlignment = NSTextAlignmentLeft;
        
        [nameView autoSetDimension:(ALDimension) NSLayoutAttributeWidth toSize:self.firstLabel.frame.size.width];
        
        UIView *toView = [[UIView alloc] init];
        UILabel *topToLabel = [[UILabel alloc] init];
        UILabel *bottomToLabel = [[UILabel alloc] init];
        [labels addObject:topToLabel];
        [labels addObject:bottomToLabel];
        [toView addSubview:dividerView1];
        [dividerView1 autoPinEdgesToSuperviewEdges];
        [toView addSubview:topToLabel];
        [toView addSubview:bottomToLabel];
//        [topToLabel autoAlignAxisToSuperviewAxis:ALAxisVertical];
        topToLabel.tag = 1;
        [topToLabel autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:2];
        [topToLabel autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:2];
        [topToLabel autoAlignAxis:ALAxisHorizontal toSameAxisOfView:dividerView1 withMultiplier:0.6];
//        [bottomToLabel autoAlignAxisToSuperviewAxis:ALAxisVertical];
        bottomToLabel.tag = 1;
        [bottomToLabel autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:2];
        [bottomToLabel autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:2];
        [bottomToLabel autoAlignAxis:ALAxisHorizontal toSameAxisOfView:dividerView1 withMultiplier:1.4];
        
        [toView autoSetDimension:(ALDimension)NSLayoutAttributeWidth toSize:self.stackViewsLabel.frame.size.width + 0.1];

        UIView *niView = [[UIView alloc] init];
        UILabel *topNiLabel = [[UILabel alloc] init];
        UILabel *bottomNiLabel = [[UILabel alloc] init];
        [labels addObject:topNiLabel];
        [labels addObject:bottomNiLabel];
        [niView addSubview:dividerView2];
        [dividerView2 autoPinEdgesToSuperviewEdges];
        [niView addSubview:topNiLabel];
        [niView addSubview:bottomNiLabel];
        topNiLabel.tag = 1;
        [topNiLabel autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:2];
        [topNiLabel autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:2];
        [topNiLabel autoAlignAxis:ALAxisHorizontal toSameAxisOfView:dividerView2 withMultiplier:0.6];
        bottomNiLabel.tag = 1;
        [bottomNiLabel autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:2];
        [bottomNiLabel autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:2];
        [bottomNiLabel autoAlignAxis:ALAxisHorizontal toSameAxisOfView:dividerView2 withMultiplier:1.4];
        
        [niView autoSetDimension:(ALDimension)NSLayoutAttributeWidth toSize:self.stackViewsLabel.frame.size.width + 0.15];
        
        UIView *lclLtlView = [[UIView alloc] init];
        UILabel *lclLabel = [[UILabel alloc] init];
        UILabel *ltlLabel = [[UILabel alloc] init];
        [labels addObject:lclLabel];
        [labels addObject:ltlLabel];
        lclLabel.textAlignment = NSTextAlignmentLeft;
        ltlLabel.textAlignment = NSTextAlignmentRight;
        lclLabel.tag = 2;
        ltlLabel.tag = 2;
        
        [lclLtlView addSubview:decorView1];
        [decorView1 autoPinEdgesToSuperviewEdges];
        [lclLtlView addSubview:curveLine1];
        [curveLine1 autoPinEdgesToSuperviewEdges];
        [lclLtlView addSubview:lclLabel];
        [lclLabel autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:4];
        [lclLabel autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:4];
        [lclLabel autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:15];
        [lclLtlView addSubview:ltlLabel];
        [ltlLabel autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:4];
        [ltlLabel autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:4];
        [ltlLabel autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:15];
        
        [lclLtlView autoSetDimension:(ALDimension)NSLayoutAttributeWidth toSize:(self.stackViewsLabel.frame.size.width * 2) + 0.2];
        
        UIView *ftlFclView = [[UIView alloc] init];
        UILabel *ftlLabel = [[UILabel alloc] init];
        UILabel *fclLabel = [[UILabel alloc] init];
        [labels addObject:ftlLabel];
        [labels addObject:fclLabel];
        ftlLabel.textAlignment = NSTextAlignmentLeft;
        fclLabel.textAlignment = NSTextAlignmentRight;
        fclLabel.tag = 2;
        ftlLabel.tag = 2;

        [ftlFclView addSubview:decorView2];
        [decorView2 autoPinEdgesToSuperviewEdges];
        [ftlFclView addSubview:curveLine2];
        [curveLine2 autoPinEdgesToSuperviewEdges];
        [ftlFclView addSubview:ftlLabel];
        [ftlLabel autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:4];
        [ftlLabel autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:4];
        [ftlLabel autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:15];
        [ftlFclView addSubview:fclLabel];
        [fclLabel autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:4];
        [fclLabel autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:4];
        [fclLabel autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:15];
        
        [ftlFclView autoSetDimension:(ALDimension)NSLayoutAttributeWidth toSize:(self.stackViewsLabel.frame.size.width * 2) + 0.2];
        
        UIView *akView = [[UIView alloc] init];
        UILabel *aLabel = [[UILabel alloc] init];
        UILabel *kLabel = [[UILabel alloc] init];
        [labels addObject:aLabel];
        [labels addObject:kLabel];
        aLabel.textAlignment = NSTextAlignmentLeft;
        kLabel.textAlignment = NSTextAlignmentRight;
        aLabel.tag = 2;
        kLabel.tag = 2;
        
        [akView addSubview:decorView3];
        [decorView3 autoPinEdgesToSuperviewEdges];
        [akView addSubview:curveLine3];
        [curveLine3 autoPinEdgesToSuperviewEdges];
        [akView addSubview:aLabel];
        [aLabel autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:4];
        [aLabel autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:4];
        [aLabel autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:15];
        [akView addSubview:kLabel];
        [kLabel autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:4];
        [kLabel autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:4];
        [kLabel autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:15];

        
        [akView autoSetDimension:(ALDimension)NSLayoutAttributeWidth toSize:(self.stackViewsLabel.frame.size.width * 2) + 0.2];
        
        UIView *frahtView = [[UIView alloc] init];
        UILabel *topLabel = [[UILabel alloc] init];
        UILabel *bottomLabel = [[UILabel alloc] init];
        [labels addObject:topLabel];
        [labels addObject:bottomLabel];
        [frahtView addSubview:dividerView3];
        [dividerView3 autoPinEdgesToSuperviewEdges];
        [frahtView addSubview:topLabel];
        [frahtView addSubview:bottomLabel];
//        [topLabel autoAlignAxisToSuperviewAxis:ALAxisVertical];
        topLabel.tag = 1;
        [topLabel autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:2];
        [topLabel autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:2];
        [topLabel autoAlignAxis:ALAxisHorizontal toSameAxisOfView:dividerView3 withMultiplier:0.6];
//        [bottomLabel autoAlignAxisToSuperviewAxis:ALAxisVertical];
        bottomLabel.tag = 1;
        [bottomLabel autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:2];
        [bottomLabel autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:2];
        [bottomLabel autoAlignAxis:ALAxisHorizontal toSameAxisOfView:dividerView3 withMultiplier:1.4];
        
        [frahtView autoSetDimension:(ALDimension)NSLayoutAttributeWidth toSize:self.lastLabel.frame.size.width];
        
        [rowView addArrangedSubview:nameView];
        [rowView addArrangedSubview:toView];
        [rowView addArrangedSubview:niView];
        [rowView addArrangedSubview:lclLtlView];
        [rowView addArrangedSubview:ftlFclView];
        [rowView addArrangedSubview:akView];
        [rowView addArrangedSubview:frahtView];
        
        nameView.backgroundColor = commonLabelColor;//nameLabelColor;
        toView.backgroundColor = UIColor.whiteColor;//commonLabelColor;
        frahtView.backgroundColor = UIColor.whiteColor;//commonLabelColor;
        self.firstLabel.backgroundColor = commonLabelColor;//nameLabelColor;
        self.lastLabel.backgroundColor = UIColor.whiteColor;//commonLabelColor;
        
        for (UILabel *label in labels) {
            label.textColor = UIColor.blackColor;
            label.font = myFont;
            label.adjustsFontSizeToFitWidth=YES;
            label.minimumScaleFactor=0.5;
            if (label.tag == 1) {
                label.numberOfLines = 2;
                label.textAlignment = NSTextAlignmentCenter;
            }
        }
        
        for (UIView *view in rowView.subviews) {
            view.borderColor = UIColor.blackColor;
            view.borderWidth = 0.8;
        }
        
        for (UILabel *label in self.headerStackView.subviews) {
            label.borderColor = UIColor.blackColor;
            label.borderWidth = 0.8;
        }
        
        ///Fill text fields
        NSArray *strings    = [NSArray array];
        nameLabel.text      = manager.name;
        strings             = [manager.to componentsSeparatedByString:@"/"];
        topToLabel.text     = strings[0];
        bottomToLabel.text  = strings[1];
        strings             = [manager.ni componentsSeparatedByString:@"/"];
        topNiLabel.text     = strings[0];
        bottomNiLabel.text  = strings[1];
        lclLabel.text       = manager.lcl;
        ltlLabel.text       = manager.ltl;
        ftlLabel.text       = manager.ftl;
        fclLabel.text       = manager.fcl;
        aLabel.text         = manager.a;
        kLabel.text         = manager.k;
        strings             = [manager.fraht componentsSeparatedByString:@"/"];
        topLabel.text       = strings[0];
        bottomLabel.text    = strings[1];

        
    }
//    [self layoutIfNeeded];
}


@end
