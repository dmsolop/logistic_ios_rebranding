//
//  ReportType6Cell.h
//  YourLogisticsNew
//
//  Created by Dima on 5/22/19.
//  Copyright © 2019 iCenter. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReportClass.h"

NS_ASSUME_NONNULL_BEGIN

@interface ReportType6Cell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property (weak, nonatomic) IBOutlet UIView *contentTableView;

- (void)configWithReport:(ReportClass *)report;

@end

NS_ASSUME_NONNULL_END
