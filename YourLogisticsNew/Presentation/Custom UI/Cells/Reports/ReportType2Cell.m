//
//  ReportType2Cell.m
//  YourLogisticsNew
//
//  Created by Dima on 3/7/19.
//  Copyright © 2019 iCenter. All rights reserved.
//

#import "ReportType2Cell.h"
#import <PureLayout/PureLayout.h>
#import "ColorPallet.h"
#import "Constants.h"
#import "ReportClass.h"
#import "DividerView.h"


@interface ReportType2Cell()

@property (nonatomic, strong) UIStackView *stackView;
@property (nonatomic, copy) ColorPallet *colorSet;

@end

@implementation ReportType2Cell


- (void)configWithReport:(ReportClass *)report {
    CGFloat reportType = report.type;
    self.colorSet = [ColorPallet new];
    self.colorSet.isNew = report.isNew;
    [self.colorSet prepareColors];
    CGFloat lineHeight = 0.0f;
    Constants *constant = [[Constants alloc] init];
    
    CGFloat widthSize = self.headerStackView.subviews[1].frame.size.width;
    
    UIFont *myFont = [UIFont fontWithName:@"MuseoSansCyrl-300" size:11];
    UIFont *myFontBold = [UIFont fontWithName:@"MuseoSansCyrl-500" size:12];
    
    UIColor *nameLabelColor = [self.colorSet lightLightGreen];
    UIColor *commonLabelColor = [self.colorSet greenTea];
    
    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
    formater.locale = [NSLocale localeWithLocaleIdentifier:@"ru_RU"];
    formater.dateFormat = @"d MMMM yyyy HH:mm";
    formater.timeZone = [NSTimeZone systemTimeZone];
    
    [self.contentTableView.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj removeFromSuperview];
    }];
    
    for (UILabel *label in self.headerStackView.subviews) {
        label.borderColor = UIColor.blackColor;
        label.borderWidth = 0.8;
    }
    
    switch (report.type) {
        case ReportTypeSalesTO: {
            NSArray *headerNames = constant.headerNamesType3;
            for (int i = 0; i < self.headerStackView.subviews.count; i++) {
                UILabel *label = self.headerStackView.subviews[i];
                label.text = headerNames[i];
                lineHeight = 45;
            }
        }
            break;
        case ReportTypeSalesOPT_NI: {
            NSArray *headerNames = constant.headerNamesType4;
            for (int i = 0; i < self.headerStackView.subviews.count; i++) {
                UILabel *label = self.headerStackView.subviews[i];
                label.text = headerNames[i];
                lineHeight = 45;
            }
        }
            break;
        case ReportTypeDaylyTransit: {
            NSArray *headerNames = constant.headerNamesType6;
            for (int i = 0; i < self.headerStackView.subviews.count; i++) {
                UILabel *label = self.headerStackView.subviews[i];
                label.text = headerNames[i];
                lineHeight = 30;
            }
        }
            break;
        default:
            break;
    }
    
    [self.headerStackView.subviews[1] setHidden:report.type == ReportTypeDaylyTransit ? NO : YES ];
    
    if (report.managers.count == 0 || report.managers == nil) {
        UILabel *noDataLabel = [[UILabel alloc] init];
        [self.contentTableView addSubview:noDataLabel];
        [noDataLabel autoSetDimension:(ALDimension) NSLayoutAttributeHeight toSize:50];
        [noDataLabel autoPinEdgesToSuperviewEdges];
        noDataLabel.backgroundColor = UIColor.orangeColor;
        noDataLabel.borderColor = UIColor.blackColor;
        noDataLabel.borderWidth = 0.8;
        noDataLabel.text = constant.noDataForDisplay;
        noDataLabel.textAlignment = NSTextAlignmentCenter;
        self.dateLabel.text = [formater stringFromDate:report.date];
        self.headerLabel.text = report.reportName;
        return;
    }
    
    self.stackView = nil;
    
    self.stackView = [[UIStackView alloc] init];
    [self.contentTableView addSubview:self.stackView];
    
    [self.stackView autoPinEdgesToSuperviewEdges];
    
    self.stackView.axis = UILayoutConstraintAxisVertical;
    self.stackView.distribution = UIStackViewDistributionFillProportionally;
    self.stackView.alignment = UIStackViewAlignmentFill;
    self.stackView.spacing = 0;
    
    ///TextTableviewCell
    ///Date
    self.dateLabel.text = [formater stringFromDate:report.date];
    
    ///Header
    self.headerLabel.text = report.reportName;
    
    ///Body rows
    for (ManagerClass *manager in report.managers) {
        
        DividerView *dividerView2 = [[DividerView alloc] initWithType:reportType];
        DividerView *dividerView3 = [[DividerView alloc] initWithType:reportType];
        DividerView *dividerView4 = [[DividerView alloc] initWithType:reportType];
        
        NSMutableArray *labels = [NSMutableArray array];
        
        UIStackView *rowView = [[UIStackView alloc] init];
        rowView.axis = UILayoutConstraintAxisHorizontal;
        rowView.distribution = UIStackViewDistributionFill;
        rowView.alignment = UIStackViewAlignmentFill;
        rowView.spacing = 0;
        
        [rowView autoSetDimension:(ALDimension) NSLayoutAttributeHeight toSize:lineHeight];
        [rowView autoSetDimension:(ALDimension) NSLayoutAttributeWidth toSize:self.contentTableView.frame.size.width];
        
        [self.stackView addArrangedSubview:rowView];
        
        UIView *nameView = [[UIView alloc] init];
        UILabel *nameLabel = [[UILabel alloc] init];
        [nameView addSubview:nameLabel];
        [nameLabel autoPinEdgesToSuperviewEdges];
        [nameLabel autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:4];
        nameLabel.font = myFontBold;
        nameLabel.textAlignment = NSTextAlignmentLeft;
        [rowView addArrangedSubview:nameView];
        
        UIView *pogranView = [[UIView alloc] init];
        UILabel *pogranLabel = [[UILabel alloc] init];
        if (report.type == ReportTypeDaylyTransit) {
            [pogranView addSubview:pogranLabel];
            [pogranLabel autoPinEdgesToSuperviewEdges];
            [labels addObject:pogranLabel];
            [rowView addArrangedSubview:pogranView];
            [pogranView autoSetDimension:(ALDimension)NSLayoutAttributeWidth toSize:widthSize];
        }
        
        UIView *firstView = [[UIView alloc] init];
        UILabel *toLabel = [[UILabel alloc] init];
        [firstView addSubview:toLabel];
        [toLabel autoPinEdgesToSuperviewEdges];
        [labels addObject:toLabel];
        [rowView addArrangedSubview:firstView];
        
        [firstView autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:nameView];
        
        UIView *secondView = [[UIView alloc] init];
        UILabel *topSecondLabel = [[UILabel alloc] init];
        UILabel *bottomSecondLabel = [[UILabel alloc] init];
        UILabel *middleSecondLabel = [[UILabel alloc] init];
        [labels addObject:topSecondLabel];
        [labels addObject:bottomSecondLabel];
        [labels addObject:middleSecondLabel];
        [secondView addSubview:dividerView2];
        [dividerView2 autoPinEdgesToSuperviewEdges];
        [secondView addSubview:middleSecondLabel];
        [middleSecondLabel autoPinEdgesToSuperviewEdges];
        [secondView addSubview:topSecondLabel];
        [topSecondLabel autoAlignAxisToSuperviewAxis:ALAxisVertical];
        [topSecondLabel autoAlignAxis:ALAxisHorizontal toSameAxisOfView:dividerView2 withMultiplier:0.6];
        [secondView addSubview:bottomSecondLabel];
        [bottomSecondLabel autoAlignAxisToSuperviewAxis:ALAxisVertical];
        [bottomSecondLabel autoAlignAxis:ALAxisHorizontal toSameAxisOfView:dividerView2 withMultiplier:1.4];
        [rowView addArrangedSubview:secondView];
        
        [secondView autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:nameView];
        
        UIView *thirdView = [[UIView alloc] init];
        UILabel *topThirdLabel = [[UILabel alloc] init];
        UILabel *bottomThirdLabel = [[UILabel alloc] init];
        UILabel *middleThirdLabel = [[UILabel alloc] init];
        [labels addObject:topThirdLabel];
        [labels addObject:bottomThirdLabel];
        [labels addObject:middleThirdLabel];
        [thirdView addSubview:dividerView3];
        [dividerView3 autoPinEdgesToSuperviewEdges];
        [thirdView addSubview:middleThirdLabel];
        [middleThirdLabel autoPinEdgesToSuperviewEdges];
        [thirdView addSubview:topThirdLabel];
        [topThirdLabel autoAlignAxisToSuperviewAxis:ALAxisVertical];
        [topThirdLabel autoAlignAxis:ALAxisHorizontal toSameAxisOfView:dividerView3 withMultiplier:0.6];
        [thirdView addSubview:bottomThirdLabel];
        [bottomThirdLabel autoAlignAxisToSuperviewAxis:ALAxisVertical];
        [bottomThirdLabel autoAlignAxis:ALAxisHorizontal toSameAxisOfView:dividerView3 withMultiplier:1.4];
        [rowView addArrangedSubview:thirdView];
        
        [thirdView autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:nameView];
        
        UIView *fourthView = [[UIView alloc] init];
        UILabel *topFourthLabel = [[UILabel alloc] init];
        UILabel *bottomFourthLabel = [[UILabel alloc] init];
        UILabel *middleFourthLabel = [[UILabel alloc] init];
        [labels addObject:topFourthLabel];
        [labels addObject:bottomFourthLabel];
        [labels addObject:middleFourthLabel];
        [fourthView addSubview:dividerView4];
        [dividerView4 autoPinEdgesToSuperviewEdges];
        [fourthView addSubview:middleFourthLabel];
        [middleFourthLabel autoPinEdgesToSuperviewEdges];
        [fourthView addSubview:topFourthLabel];
        [topFourthLabel autoAlignAxisToSuperviewAxis:ALAxisVertical];
        [topFourthLabel autoAlignAxis:ALAxisHorizontal toSameAxisOfView:dividerView4 withMultiplier:0.6];
        [fourthView addSubview:bottomFourthLabel];
        [bottomFourthLabel autoAlignAxisToSuperviewAxis:ALAxisVertical];
        [bottomFourthLabel autoAlignAxis:ALAxisHorizontal toSameAxisOfView:dividerView4 withMultiplier:1.4];
        [rowView addArrangedSubview:fourthView];
        
        [fourthView autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:nameView];
        
        nameView.backgroundColor = commonLabelColor;//nameLabelColor;
        pogranView.backgroundColor = UIColor.whiteColor;//commonLabelColor;
        secondView.backgroundColor = UIColor.whiteColor;//commonLabelColor;
        fourthView.backgroundColor = UIColor.whiteColor;//commonLabelColor;
        
        for (UILabel *label in labels) {
            label.textAlignment = NSTextAlignmentCenter;
            label.textColor = UIColor.blackColor;
            label.font = myFont;
        }
        
        for (UIView *view in rowView.subviews) {
            view.borderColor = UIColor.blackColor;
            view.borderWidth = 0.8;
        }
    
        
        ///Fill text fields
        NSArray *strings = [NSArray array];
        nameLabel.text      = manager.name;
        
        switch (report.type) {
            case ReportTypeSalesTO:{
                
                strings = [manager.marginLogistic componentsSeparatedByString:@"/"];
                toLabel.text            = manager.toFormalize;
                middleSecondLabel.text  = manager.logisticFormalize;
                topThirdLabel.text      = strings[0];
                bottomThirdLabel.text   = strings[1];
                middleFourthLabel.text  = manager.amountToClear;
                
                [dividerView2 setHidden:YES];
                [dividerView3 setHidden:NO];
                [dividerView4 setHidden:YES];
                break;
            }
            case ReportTypeSalesOPT_NI:{
                toLabel.text            = manager.amountNiFormalize;
                
                strings                 = [manager.marginNi componentsSeparatedByString:@"/"];
                topSecondLabel.text     = strings[0];
                bottomSecondLabel.text  = strings[1];
                
                middleThirdLabel.text   = manager.amountLclFormalize;
                
                strings                 = [manager.mzLclFormalize componentsSeparatedByString:@"/"];
                topFourthLabel.text     = strings[0];
                bottomFourthLabel.text  = strings[1];
                
                [dividerView2 setHidden:NO];
                [dividerView3 setHidden:YES];
                [dividerView4 setHidden:NO];
                break;
            }
            case ReportTypeDaylyTransit:{
                pogranLabel.text        = manager.pogran;
                toLabel.text            = manager.weekNumber;
                middleSecondLabel.text  = manager.dayWeekNumber;
                middleThirdLabel.text   = manager.ttAmount;
                middleFourthLabel.text  = manager.guarantyAmount;
                
                [dividerView2 setHidden:YES];
                [dividerView3 setHidden:YES];
                [dividerView4 setHidden:YES];
                break;
            }   
            default:
                break;
        }
        

    }
}




@end
