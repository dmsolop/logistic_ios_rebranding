//
//  ReportType6Cell.m
//  YourLogisticsNew
//
//  Created by Dima on 5/22/19.
//  Copyright © 2019 iCenter. All rights reserved.
//

#import "ReportType6Cell.h"
#import <PureLayout/PureLayout.h>
#import "ColorPallet.h"
#import "Constants.h"
#import "ReportClass.h"


@interface ReportType6Cell()

@property (nonatomic, strong) UIStackView *stackView;
@property (nonatomic, copy) ColorPallet *colorSet;

@end

@implementation ReportType6Cell


- (void)configWithReport:(ReportClass *)report {
    self.colorSet = [ColorPallet new];
    self.colorSet.isNew = report.isNew;
    [self.colorSet prepareColors];
    Constants *constant = [[Constants alloc] init];
    
    CGFloat widthSize = self.contentTableView.frame.size.width / 3;
    NSUInteger nameWidth = widthSize - 15;
    CGFloat priceWidth = widthSize - 20;
    CGFloat productWidth = self.contentTableView.frame.size.width - (nameWidth + priceWidth);
    
    UIFont *myFont = [UIFont fontWithName:@"MuseoSansCyrl-300" size:11];
    UIFont *myFontBold = [UIFont fontWithName:@"MuseoSansCyrl-500" size:12];
    UIFont *headerFontBold = [UIFont fontWithName:@"MuseoSansCyrl-500" size:15];
    
    UIColor *nameLabelColor = [self.colorSet lightLightGreen];
    UIColor *commonLabelColor = [self.colorSet greenTea];
    
    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
    formater.locale = [NSLocale localeWithLocaleIdentifier:@"ru_RU"];
    formater.dateFormat = @"d MMMM yyyy HH:mm";
    formater.timeZone = [NSTimeZone systemTimeZone];
    
    [self.contentTableView.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj removeFromSuperview];
    }];
    
    
    self.stackView = nil;
    self.stackView = [[UIStackView alloc] init];
    
    self.stackView.axis = UILayoutConstraintAxisVertical;
    self.stackView.distribution = UIStackViewDistributionFillProportionally;
    self.stackView.alignment = UIStackViewAlignmentFill;
    self.stackView.spacing = 0;
    
    [self.contentTableView addSubview:self.stackView];
    [self.stackView autoPinEdgesToSuperviewEdges];
    
    ///TextTableviewCell
    ///Date
    self.dateLabel.text = [formater stringFromDate:report.date];
    
    ///Header
    self.headerLabel.text = report.reportName;
    
    NSMutableArray *headerlabels = [NSMutableArray array];
    
    UIStackView *rowHeaderView = [[UIStackView alloc] init];
    rowHeaderView.axis = UILayoutConstraintAxisHorizontal;
    rowHeaderView.distribution = UIStackViewDistributionFillProportionally;
    rowHeaderView.alignment = UIStackViewAlignmentFill;
    rowHeaderView.spacing = 0;
    
    [rowHeaderView autoSetDimension:(ALDimension) NSLayoutAttributeHeight toSize:40];
    [rowHeaderView autoSetDimension:(ALDimension) NSLayoutAttributeWidth toSize:self.contentTableView.frame.size.width];
    
    [self.stackView addArrangedSubview:rowHeaderView];
    
    UIView *nameHeaderView = [[UIView alloc] init];
    UILabel *nameHeaderLabel = [[UILabel alloc] init];
    [nameHeaderView addSubview:nameHeaderLabel];
    [nameHeaderLabel autoPinEdgesToSuperviewEdges];
    [nameHeaderLabel autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:4];
    nameHeaderLabel.font = myFontBold;
    nameHeaderLabel.textAlignment = NSTextAlignmentLeft;
    [rowHeaderView addArrangedSubview:nameHeaderView];
    [headerlabels addObject:nameHeaderLabel];
    
    [nameHeaderView autoSetDimension:(ALDimension)NSLayoutAttributeWidth toSize:nameWidth];
    
    UIView *firstHeaderView = [[UIView alloc] init];
    UILabel *firstHeaderLabel = [[UILabel alloc] init];
    [firstHeaderView addSubview:firstHeaderLabel];
    [firstHeaderLabel autoPinEdgesToSuperviewEdges];
    [rowHeaderView addArrangedSubview:firstHeaderView];
    [headerlabels addObject:firstHeaderLabel];
    
    [firstHeaderView autoSetDimension:(ALDimension)NSLayoutAttributeWidth toSize:productWidth];
    
    UIView *secondHeaderView = [[UIView alloc] init];
    UILabel *secondHeaderLabel = [[UILabel alloc] init];
    [secondHeaderView addSubview:secondHeaderLabel];
    [secondHeaderLabel autoPinEdgesToSuperviewEdges];
    [rowHeaderView addArrangedSubview:secondHeaderView];
    [headerlabels addObject:secondHeaderLabel];
    
    [secondHeaderView autoSetDimension:(ALDimension)NSLayoutAttributeWidth toSize:priceWidth];

    nameHeaderLabel.text = constant.headerNamesType9[0];
    firstHeaderLabel.text = constant.headerNamesType9[1];
    secondHeaderLabel.text = constant.headerNamesType9[2];
    
    nameHeaderView.backgroundColor = commonLabelColor;//nameLabelColor;
    secondHeaderView.backgroundColor = UIColor.whiteColor;//commonLabelColor;
    
    for (UILabel *label in headerlabels) {
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = UIColor.blackColor;
        label.textAlignment = NSTextAlignmentCenter;
        label.font = headerFontBold;
    }
    
    for (UIView *view in rowHeaderView.subviews) {
        view.borderColor = UIColor.blackColor;
        view.borderWidth = 0.8;
    }
    
    ///Body rows
    for (OrderClass *order in report.orders) {
        
        NSMutableArray *labels = [NSMutableArray array];
        
        UIStackView *rowView = [[UIStackView alloc] init];
        rowView.axis = UILayoutConstraintAxisHorizontal;
        rowView.distribution = UIStackViewDistributionFillProportionally;
        rowView.alignment = UIStackViewAlignmentFill;
        rowView.spacing = 0;
        
        [rowView autoSetDimension:(ALDimension) NSLayoutAttributeHeight toSize:35];
        [rowView autoSetDimension:(ALDimension) NSLayoutAttributeWidth toSize:self.contentTableView.frame.size.width];
        
        [self.stackView addArrangedSubview:rowView];
        
        UIView *nameView = [[UIView alloc] init];
        UILabel *nameLabel = [[UILabel alloc] init];
        [nameView addSubview:nameLabel];
        [nameLabel autoPinEdgesToSuperviewEdges];
        [nameLabel autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:4];
        nameLabel.font = myFontBold;
        nameLabel.textAlignment = NSTextAlignmentCenter;
        [rowView addArrangedSubview:nameView];
        [nameView autoSetDimension:(ALDimension)NSLayoutAttributeWidth toSize:nameWidth];
        
        UIView *firstView = [[UIView alloc] init];
        UILabel *firstLabel = [[UILabel alloc] init];
        [firstView addSubview:firstLabel];
        [firstLabel autoPinEdgesToSuperviewEdges];
        firstLabel.lineBreakMode = NSLineBreakByWordWrapping;
        firstLabel.numberOfLines = 0;
        [labels addObject:firstLabel];
        [rowView addArrangedSubview:firstView];
        [firstView autoSetDimension:(ALDimension)NSLayoutAttributeWidth toSize:productWidth];
        
        UIView *secondView = [[UIView alloc] init];
        UILabel *secondLabel = [[UILabel alloc] init];
        [secondView addSubview:secondLabel];
        [secondLabel autoPinEdgesToSuperviewEdges];
        [labels addObject:secondLabel];
        [rowView addArrangedSubview:secondView];
        [secondView autoSetDimension:(ALDimension)NSLayoutAttributeWidth toSize:priceWidth];
        
        nameView.backgroundColor = commonLabelColor;//nameLabelColor;
        secondView.backgroundColor = UIColor.whiteColor;//commonLabelColor;
        
        for (UILabel *label in labels) {
            label.textAlignment = NSTextAlignmentCenter;
            label.textColor = UIColor.blackColor;
            label.font = myFont;
        }
        
        for (UIView *view in rowView.subviews) {
            view.borderColor = UIColor.blackColor;
            view.borderWidth = 0.8;
        }
        
        ///Fill text fields
        nameLabel.text    = order.name;
        firstLabel.text   = order.product;
        secondLabel.text  = order.price;
        
    }
    
}

@end
