//
//  ReportType3Cell.m
//  YourLogisticsNew
//
//  Created by Dima on 3/7/19.
//  Copyright © 2019 iCenter. All rights reserved.
//

#import "ReportType3Cell.h"
#import <PureLayout/PureLayout.h>
#import "ColorPallet.h"
#import "Constants.h"
#import "ReportClass.h"


@interface ReportType3Cell()

@property (nonatomic, strong) UIStackView *stackView;
@property (nonatomic, copy) ColorPallet *colorSet;

@end

@implementation ReportType3Cell


- (void)configWithReport:(ReportClass *)report {
    self.colorSet = [ColorPallet new];
    self.colorSet.isNew = report.isNew;
    [self.colorSet prepareColors];
    Constants *constant = [[Constants alloc] init];
    
    CGFloat widthSize = self.headerStackView.subviews[1].frame.size.width;
    
    UIFont *myFont = [UIFont fontWithName:@"MuseoSansCyrl-300" size:11];
    UIFont *myFontBold = [UIFont fontWithName:@"MuseoSansCyrl-500" size:12];
    
    UIColor *nameLabelColor = [self.colorSet lightLightGreen];
    UIColor *commonLabelColor = [self.colorSet greenTea];
    
    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
    formater.locale = [NSLocale localeWithLocaleIdentifier:@"ru_RU"];
    formater.dateFormat = @"d MMMM yyyy HH:mm";
    formater.timeZone = [NSTimeZone systemTimeZone];
    
    [self.contentTableView.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj removeFromSuperview];
    }];
    
    for (UILabel *label in self.headerStackView.subviews) {
        label.borderColor = UIColor.blackColor;
        label.borderWidth = 0.8;
    }
    
    switch (report.type) {
        case ReportTypeDepartment: {
            [self.headerStackView.subviews[1] setHidden:YES];
            NSArray *headerNames = constant.headerNamesType5;
            for (int i = 0; i < self.headerStackView.subviews.count; i++) {
                UILabel *label = self.headerStackView.subviews[i];
                label.text = headerNames[i];
            }
        }
            break;
        case ReportTypeAccumTransit: {
            [self.headerStackView.subviews[1] setHidden:NO];
            NSArray *headerNames = constant.headerNamesType7;
            for (int i = 0; i < self.headerStackView.subviews.count; i++) {
                UILabel *label = self.headerStackView.subviews[i];
                label.text = headerNames[i];
            }
        }
            break;
        default:
            break;
    }
    
    if ((report.managers.count == 0 || report.managers == nil)) {
        UILabel *noDataLabel = [[UILabel alloc] init];
        [self.contentTableView addSubview:noDataLabel];
        [noDataLabel autoSetDimension:(ALDimension) NSLayoutAttributeHeight toSize:50];
        [noDataLabel autoPinEdgesToSuperviewEdges];
        noDataLabel.backgroundColor = UIColor.orangeColor;
        noDataLabel.borderColor = UIColor.blackColor;
        noDataLabel.borderWidth = 0.8;
        noDataLabel.text = constant.noDataForDisplay;
        noDataLabel.textAlignment = NSTextAlignmentCenter;
        self.dateLabel.text = [formater stringFromDate:report.date];
        self.headerLabel.text = report.reportName;
        return;
    }
    
    self.stackView = nil;
    self.stackView = [[UIStackView alloc] init];
    [self.contentTableView addSubview:self.stackView];
    
    [self.stackView autoPinEdgesToSuperviewEdges];
    
    self.stackView.axis = UILayoutConstraintAxisVertical;
    self.stackView.distribution = UIStackViewDistributionFillProportionally;
    self.stackView.alignment = UIStackViewAlignmentFill;
    self.stackView.spacing = 0;
    
    ///TextTableviewCell
    ///Date
    self.dateLabel.text = [formater stringFromDate:report.date];
    
    ///Header
    self.headerLabel.text = report.reportName;
    
    ///Body rows
        
        for (ManagerClass *manager in report.managers) {
            
            NSMutableArray *labels = [NSMutableArray array];
            
            UIStackView *rowView = [[UIStackView alloc] init];
            rowView.axis = UILayoutConstraintAxisHorizontal;
            rowView.distribution = UIStackViewDistributionFillProportionally;
            rowView.alignment = UIStackViewAlignmentFill;
            rowView.spacing = 0;
            
            [rowView autoSetDimension:(ALDimension) NSLayoutAttributeHeight toSize:30];
            [rowView autoSetDimension:(ALDimension) NSLayoutAttributeWidth toSize:self.contentTableView.frame.size.width];
            
            [self.stackView addArrangedSubview:rowView];
            
            UIView *nameView = [[UIView alloc] init];
            UILabel *nameLabel = [[UILabel alloc] init];
            [nameView addSubview:nameLabel];
            [nameLabel autoPinEdgesToSuperviewEdges];
            [nameLabel autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:4];
            nameLabel.font = myFontBold;
            nameLabel.textAlignment = NSTextAlignmentLeft;
            [rowView addArrangedSubview:nameView];
                        
            UIView *pogranView = [[UIView alloc] init];
            UILabel *pogranLabel = [[UILabel alloc] init];
            if (report.type == ReportTypeAccumTransit) {
                [pogranView addSubview:pogranLabel];
                [pogranLabel autoPinEdgesToSuperviewEdges];
                [labels addObject:pogranLabel];
                [rowView addArrangedSubview:pogranView];
                [pogranView autoSetDimension:(ALDimension)NSLayoutAttributeWidth toSize:widthSize];
            }
            
            UIView *firstView = [[UIView alloc] init];
            UILabel *firstLabel = [[UILabel alloc] init];
            [firstView addSubview:firstLabel];
            [firstLabel autoPinEdgesToSuperviewEdges];
            [labels addObject:firstLabel];
            [rowView addArrangedSubview:firstView];
            
            [firstView autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:nameView];
            
            UIView *secondView = [[UIView alloc] init];
            UILabel *secondLabel = [[UILabel alloc] init];
            [secondView addSubview:secondLabel];
            [secondLabel autoPinEdgesToSuperviewEdges];
            [labels addObject:secondLabel];
            [rowView addArrangedSubview:secondView];
            
            [secondView autoMatchDimension:ALDimensionWidth toDimension:ALDimensionWidth ofView:nameView];
            
            nameView.backgroundColor = commonLabelColor;//nameLabelColor;
            pogranView.backgroundColor = UIColor.whiteColor;//commonLabelColor;
            secondView.backgroundColor = UIColor.whiteColor;//commonLabelColor;
            
            for (UILabel *label in labels) {
                label.textAlignment = NSTextAlignmentCenter;
                label.textColor = UIColor.blackColor;
                label.font = myFont;
            }
            
            for (UIView *view in rowView.subviews) {
                view.borderColor = UIColor.blackColor;
                view.borderWidth = 0.8;
            }
            
            
            ///Fill text fields
            nameLabel.text      = manager.name;
            
            switch (report.type) {
                case ReportTypeDepartment:{
                    firstLabel.text   = manager.amountLclFormalize;
                    secondLabel.text  = manager.mzLclFormalize;
                    break;
                }
                case ReportTypeAccumTransit:{
                    pogranLabel.text  = manager.pogran;
                    firstLabel.text   = manager.ttAmount;
                    secondLabel.text  = manager.guarantyAmount;
                    break;
                }
                default:
                    break;
            }
        }
}

@end
