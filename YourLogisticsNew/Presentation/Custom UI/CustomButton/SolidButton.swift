//
//  SolidColorButton.swift
//  OdlarYurdu
//
//  Created by Vitaliy on 27.12.2017.
//  Copyright © 2017 prosper. All rights reserved.
//

import UIKit

@objc class SolidButton: ShadowButton
{
    override func awakeFromNib()
    {
        super.awakeFromNib()
        backgroundColor = UIColor(red: 53/255, green: 149/255, blue: 253/255, alpha: 1.0)

//        textColor = .white
        shadowColor1 = UIColor.clear
        shadowColor2 = UIColor.clear
        startGradientColor = UIColor(red: 53/255, green: 149/255, blue: 253/255, alpha: 1.0)
    }

    override func layoutSubviews()
    {
        super.layoutSubviews()
//        xInsCornerRadius = min(bounds.height, bounds.width) / 2.0
    }
}
