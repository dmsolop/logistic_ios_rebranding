//
//  ShadowButton.swift
//  Myxoon
//
//  Created by Vitaliy Kislyy on 07.05.17.
//  Copyright © 2017 WaveOfHealth All rights reserved.
//

import Foundation
import UIKit
import PureLayout
import BonMot


@objc class ShadowButton: UIButton
{
    override class var layerClass: AnyClass { return CAGradientLayer.self }
    var gradientLayer: CAGradientLayer { return layer as! CAGradientLayer }

    var startGradientColor: UIColor = UIColor.clear { didSet { updateColors() } }
    var endGradientColor: UIColor? { didSet { updateColors() } }

    var shadowColor1: UIColor = UIColor.clear { didSet { updateColors() } }
    var shadowColor2: UIColor = UIColor.clear { didSet { updateColors() } }

    var textColor: UIColor? = nil { didSet { updateContent() } }

    override init(frame: CGRect)
    {
        super.init(frame: frame)
        setup()
    }

    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        setup()
    }

    fileprivate var pressedCompletion: (() -> Void)?
    @objc func didPress(completion: @escaping () -> Void)
    {
        pressedCompletion = completion
    }

    func setup()
    {
        addTarget(self, action: #selector(touchUpInside), for: .touchUpInside)
        addTarget(self, action: #selector(touchDown), for: .touchDown)
        addTarget(self, action: #selector(touchUpOutside), for: .touchUpOutside)
        addTarget(self, action: #selector(touchCancel), for: .touchCancel)

        layer.masksToBounds = true
//        layer.cornerRadius = 8.0

        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 1, y: 1)

        updateColors()

        adjustsImageWhenHighlighted = false
    }

    fileprivate func updateColors()
    {
        gradientLayer.colors = [startGradientColor.cgColor, (endGradientColor ?? startGradientColor).cgColor]

        setShadowColor(for: shadowView1, color: shadowColor1)
        setShadowColor(for: shadowView2, color: shadowColor2)
    }

    @objc var title = UILabel()
    @objc var image = UIImageView()

    override func awakeFromNib()
    {
        super.awakeFromNib()

        configShadow(for: shadowView1)
        configShadow(for: shadowView2)
        configContent()

        updateContent()
    }

    var titleCache: String?
    var imageCache: UIImage?


    func setTitle(_ title: String?)
    {
        titleLabel?.text = title
        configContent()
        updateContent()
    }

    fileprivate func configContent()
    {
        imageCache = imageView?.image
        setImage(nil, for: .normal)
        titleCache = titleLabel?.text
        setTitle(nil, for: .normal)

        addSubview(title)
        addSubview(image)

        //        image.autoPinEdge(toSuperviewEdge: .right, withInset: 16)
        //        image.autoAlignAxis(toSuperviewAxis: .horizontal)

        image.autoCenterInSuperview()
        title.autoCenterInSuperview()
        //        title.autoPinEdge(toSuperviewEdge: .left, withInset: 16)
        //        title.autoAlignAxis(toSuperviewAxis: .horizontal)
    }

    fileprivate func configShadow(for view: UIView)
    {
        superview?.addSubview(view)
        superview?.insertSubview(view, belowSubview: self)


        view.autoPinEdge(.left, to: .left, of: self, withOffset: 26)
        view.autoPinEdge(.right, to: .right, of: self, withOffset: -26)
        view.autoPinEdge(.bottom, to: .bottom, of: self, withOffset: -2)
        view.autoSetDimension(.height, toSize: 20)

        view.layer.cornerRadius = 8
        view.layer.shadowRadius = 50 / UIScreen.main.scale
        view.layer.shadowOpacity = 1
    }

    fileprivate func updateContent()
    {
        let colorOfText = self.textColor ?? self.titleColor(for: .normal) ?? .white
        
        let baseStyle = StringStyle(
            .font(.systemFont(ofSize: 17, weight: UIFont.Weight.light)),
            .color(colorOfText)
            //            .tracking(Tracking.point(1.96))
        )

        if let text = titleCache
        {
            title.attributedText = text.styled(with: baseStyle)
        }

        image.image = imageCache
    }

    fileprivate func setShadowColor(for view: UIView, color: UIColor)
    {
        view.backgroundColor = .clear//UIColor.ColorScheme.Buttons.shadowViewBackground.withAlphaComponent(1)
        view.layer.shadowColor = color.cgColor
    }

    override func layoutSubviews()
    {
        super.layoutSubviews()

        shadowView1.layer.shadowOffset = CGSize(width: 0, height: 0.0)
        shadowView2.layer.shadowOffset = CGSize(width: 0, height: 0.0)
    }

    var shadowView1 = UIView()
    var shadowView2 = UIView()


    @objc fileprivate func touchUpInside(_ sender: Any)
    {
        animateReleased()
    }

    @objc fileprivate func touchDown(_ sender: Any)
    {
        animatePressedDown()
    }

    @objc fileprivate func touchUpOutside(_ sender: Any)
    {
        animateReleased(isCancel: true)
    }

    @objc fileprivate func touchCancel(_ sender: Any)
    {
        animateReleased(isCancel: true)
    }

    fileprivate func animatePressedDown()
    {
        UIView.animate(withDuration: 0.1) {
            self.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
            self.shadowView1.transform = CGAffineTransform(scaleX: 0.9, y: 1).translatedBy(x: 0, y: -5)
            self.shadowView2.transform = CGAffineTransform(scaleX: 0.9, y: 1).translatedBy(x: 0, y: -5)
        }

        animateBlurShadow(for: shadowView1, from: 50, to: 25, duration: 0.1)
        animateBlurShadow(for: shadowView2, from: 50, to: 25, duration: 0.1)
    }

    fileprivate func animateReleased(isCancel cancel: Bool = false)
    {
        UIView.animate(withDuration: 0.2, animations: {
            self.transform = CGAffineTransform.identity
            self.shadowView1.transform = CGAffineTransform.identity
            self.shadowView2.transform = CGAffineTransform.identity
        }) { _ in
            if !cancel
            {
                self.pressedCompletion?()
            }
        }

        animateBlurShadow(for: shadowView1, from: 25, to: 50, duration: 0.2)
        animateBlurShadow(for: shadowView2, from: 25, to: 50, duration: 0.2)
    }

    fileprivate func animateBlurShadow(for view: UIView, from fromValue: CGFloat, to toValue: CGFloat, duration: CFTimeInterval)
    {
        let anim = CABasicAnimation(keyPath: "shadowRadius")
        anim.fromValue = fromValue / UIScreen.main.scale
        anim.toValue = toValue / UIScreen.main.scale
        anim.duration = duration
        view.layer.add(anim, forKey: "shadowRadius")
        view.layer.shadowRadius = toValue / UIScreen.main.scale
    }

    override var isHidden: Bool {
        didSet {
            shadowView1.isHidden = isHidden
            shadowView2.isHidden = isHidden
        }
    }
}

