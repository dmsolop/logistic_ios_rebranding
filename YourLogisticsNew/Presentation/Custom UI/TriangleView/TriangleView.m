//
//  TriangleView.m
//  YourLogisticsNew
//
//  Created by Dima on 3/12/19.
//  Copyright © 2019 iCenter. All rights reserved.
//

#import "TriangleView.h"
#import "ColorPallet.h"


@implementation TriangleView

- (void)drawRect:(CGRect)rect {
    
    UIBezierPath* trianglePath = [UIBezierPath bezierPath];
    [trianglePath moveToPoint:CGPointMake(0, 0)];
    [trianglePath addLineToPoint:CGPointMake(self.frame.size.width, 0)];
    [trianglePath addLineToPoint:CGPointMake(0, self.frame.size.height)];
    [trianglePath closePath];
    
    UIBezierPath* curvLinePath = [UIBezierPath bezierPath];
    [curvLinePath moveToPoint:CGPointMake(self.frame.size.width, 0)];
    [curvLinePath addLineToPoint:CGPointMake(self.frame.size.width, 1)];
    [curvLinePath addLineToPoint:CGPointMake(1, self.frame.size.height)];
    [curvLinePath addLineToPoint:CGPointMake(0, self.frame.size.height)];
    [curvLinePath closePath];
    
    CAShapeLayer *triangleMaskLayer = [CAShapeLayer layer];
    [triangleMaskLayer setPath:trianglePath.CGPath];
    self.layer.mask = triangleMaskLayer;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
//    self.backgroundColor = [ColorPallet new].lightLightGreen;
}

@end
