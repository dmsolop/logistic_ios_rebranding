//
//  CurveLinePath.m
//  YourLogisticsNew
//
//  Created by Dima on 4/1/19.
//  Copyright © 2019 iCenter. All rights reserved.
//

#import "CurveLinePath.h"

@implementation CurveLinePath

- (void)drawRect:(CGRect)rect {
    
    UIBezierPath* curvLinePath = [UIBezierPath bezierPath];
    [curvLinePath moveToPoint:CGPointMake(self.frame.size.width, 0)];
    [curvLinePath addLineToPoint:CGPointMake(self.frame.size.width, 1)];
    [curvLinePath addLineToPoint:CGPointMake(1, self.frame.size.height)];
    [curvLinePath addLineToPoint:CGPointMake(0, self.frame.size.height)];
    [curvLinePath closePath];
    
    CAShapeLayer *curvLineMaskLayer = [CAShapeLayer layer];
    [curvLineMaskLayer setPath:curvLinePath.CGPath];
    self.layer.mask = curvLineMaskLayer;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.backgroundColor = UIColor.blackColor;
}

@end
