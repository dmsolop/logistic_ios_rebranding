//
//  TopTabBarView.m
//  Pioneer UA
//
//  Created by Aleksandr Ponomarenko on 08.02.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import "TopTabBarView.h"
#import "ColorPallet.h"

#define underlineHeight 4.f
#define labelPadding 4.f
#define dividerWidth 1.f

#define kDeselectedSectionColor [UIColor whiteColor]
#define kSelectedSectionColor [ColorPallet new].baseLightBlue//[UIColor colorWithRed:140.f/255.f green:213.f/255.f blue:11.f/255.f alpha:1.0]

#define kSectionTitleColor [UIColor blackColor]
#define kSelectedSectionTitleColor [UIColor blackColor]

#define kSectionTitleFont [UIFont fontWithName:@"Museo Sans Cyrl" size:15.f]
#define kSelectedSectionTitleFont [UIFont fontWithName:@"Museo Sans Cyrl" size:15.f]

#define kSectionUnderlineColor [UIColor colorWithRed:168.f/255.f green:168.f/255.f blue:168.f/255.f alpha:1.0]
#define kSelectedSectionUnderlineColor [UIColor blackColor]

#define kDividerColor [UIColor colorWithRed:214.f/255.f green:214.f/255.f blue:214.f/255.f alpha:1.0]

static const NSInteger kSectionViewTagStartNumber = 100;
static const NSInteger kSectionTitleLabelTagStartNumber = 200;
static const NSInteger kSectionUnderlineTagStartNumber = 300;

@interface TopTabBarView ()

@property (nonatomic) NSInteger numberOfSections;

@property (nonatomic, strong) NSArray *underlineViews;

@property (nonatomic, strong) NSArray *sectionsViews;

@property (nonatomic, strong) NSArray *dividers;

@property (nonatomic, strong) NSArray *titleLabels;

@end


@implementation TopTabBarView

@synthesize sectionColor = _sectionColor;
@synthesize selectedSectionColor = _selectedSectionColor;
@synthesize sectionTitleFont = _sectionTitleFont;
@synthesize selectedSectionTitleFont = _selectedSectionTitleFont;
@synthesize sectionTitleColor = _sectionTitleColor;
@synthesize selectedSectionTitleColor = _selectedSectionTitleColor;
@synthesize sectionUnderlineColor = _sectionUnderlineColor;
@synthesize selectedSectionUnderlineColor = _selectedSectionUnderlineColor;

- (instancetype)initWithFrame:(CGRect)aRect
                sectionTitles:(NSArray *)sectionsTitles{
    
    self = [super initWithFrame:aRect];
    if (self) {
        _selectedSection = 0;
        NSMutableArray *underlineViews = [NSMutableArray array];
        NSMutableArray *titleLabels = [NSMutableArray array];
        NSMutableArray *sectionsViews = [NSMutableArray array];
        self.backgroundColor = [UIColor magentaColor];
        CGFloat sectionViewHeight = aRect.size.height - underlineHeight;
        CGFloat underlineYPosition = aRect.size.height - underlineHeight;
        CGFloat sectionWidth = aRect.size.width / sectionsTitles.count;
        self.numberOfSections = sectionsTitles.count;
        for (NSInteger count = 0; count < self.numberOfSections; count++) {
            CGRect underlineViewFrame = CGRectMake(sectionWidth * count, underlineYPosition, sectionWidth, underlineHeight);
            
            UIView *underlineView = [[UIView alloc] initWithFrame:underlineViewFrame];
            underlineView.backgroundColor = count == 0 ? kSelectedSectionUnderlineColor : kSectionUnderlineColor;
            underlineView.tag = count + kSectionUnderlineTagStartNumber;
            [self addSubview:underlineView];
            [underlineViews addObject:underlineView];
            
            CGRect sectionViewFrame = CGRectMake(sectionWidth * count, 0, sectionWidth, sectionViewHeight);
            UIView *sectionView = [[UIView alloc] initWithFrame:sectionViewFrame];
            sectionView.backgroundColor = count == 0 ? kSelectedSectionColor : kDeselectedSectionColor;
            UITapGestureRecognizer *selectionRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sectionSelected:)];
            [sectionView addGestureRecognizer:selectionRecognizer];
            sectionView.tag = count + kSectionViewTagStartNumber;
            [self addSubview:sectionView];
            [sectionsViews addObject:sectionView];
            
            CGRect labelFrame = CGRectMake(labelPadding, labelPadding + 4, sectionViewFrame.size.width - labelPadding * 2, sectionViewFrame.size.height - labelPadding * 2);
            UILabel *sectionTitleLabel = [[UILabel alloc] initWithFrame:labelFrame];
            sectionTitleLabel.text = sectionsTitles[count];
            sectionTitleLabel.font = count == 0 ? kSelectedSectionTitleFont : kSectionTitleFont;
            sectionTitleLabel.textAlignment = NSTextAlignmentCenter;
            sectionTitleLabel.textColor = count == 0 ? kSelectedSectionTitleColor : kSectionTitleColor;
            sectionTitleLabel.tag = count + kSectionTitleLabelTagStartNumber;
            
            [sectionView addSubview:sectionTitleLabel];
            [titleLabels addObject:sectionTitleLabel];
            
            self.titleLabels = [titleLabels copy];
            self.sectionsViews = [sectionsViews copy];
            self.underlineViews = [underlineViews copy];
        }
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    }
    return self;
}

-(void)setSelectedSection:(NSInteger)selectedSection{
    UIView *sectionView = [self viewWithTag:self.selectedSection + kSectionViewTagStartNumber];
    sectionView.backgroundColor = self.sectionColor;
    UILabel *selectedLabel = [self viewWithTag:self.selectedSection + kSectionTitleLabelTagStartNumber];
    selectedLabel.textColor = self.sectionTitleColor;
    selectedLabel.font = self.sectionTitleFont;
    UIView *underlineView = [self viewWithTag:self.selectedSection + kSectionUnderlineTagStartNumber];
    underlineView.backgroundColor = self.sectionUnderlineColor;
    
    _selectedSection = selectedSection;
    UILabel *newSelectedLabel = [self viewWithTag:selectedSection + kSectionTitleLabelTagStartNumber];
    newSelectedLabel.textColor = self.selectedSectionTitleColor;
    newSelectedLabel.font = self.selectedSectionTitleFont;
    UIView *selectedUnderlineView = [self viewWithTag:selectedSection + kSectionUnderlineTagStartNumber];
    selectedUnderlineView.backgroundColor = self.selectedSectionUnderlineColor;
    UIView *selectedSectionView = [self viewWithTag:selectedSection + kSectionViewTagStartNumber];
    selectedSectionView.backgroundColor = self.selectedSectionColor;
}

- (instancetype)initWithFrame:(CGRect)aRect
                sectionTitles:(NSArray *)sectionsTitles
               sectionsColors:(NSArray *)sectionsColors {
    
    self = [super initWithFrame:aRect];
    if (self) {
        [self setDefaultValues];
        NSMutableArray *underlineViews = [NSMutableArray array];
        NSMutableArray *dividers = [NSMutableArray array];
        NSMutableArray *sectionsViews = [NSMutableArray array];
        self.backgroundColor = [UIColor magentaColor];
        CGFloat sectionViewHeight = aRect.size.height - underlineHeight;
        CGFloat underlineYPosition = aRect.size.height - underlineHeight;
        CGFloat sectionWidth = aRect.size.width / sectionsTitles.count;
        CGFloat dividerHeight = aRect.size.height - underlineHeight - 4;
        self.numberOfSections = sectionsTitles.count;
        for (NSInteger count = 0; count < self.numberOfSections; count++) {
            CGRect underlineViewFrame = CGRectMake(sectionWidth * count, underlineYPosition, sectionWidth, underlineHeight);
            
            UIView *underlineView = [[UIView alloc] initWithFrame:underlineViewFrame];
            underlineView.backgroundColor = sectionsColors[count];
            underlineView.tag = count + kSectionUnderlineTagStartNumber;
            [self addSubview:underlineView];
            [underlineViews addObject:underlineView];
            
            CGRect sectionViewFrame = CGRectMake(sectionWidth * count, 0, sectionWidth, sectionViewHeight);
            UIView *sectionView = [[UIView alloc] initWithFrame:sectionViewFrame];
            sectionView.backgroundColor = [UIColor whiteColor];
            UITapGestureRecognizer *selectionRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sectionSelected:)];
            [sectionView addGestureRecognizer:selectionRecognizer];
            sectionView.tag = count + kSectionViewTagStartNumber;
            [self addSubview:sectionView];
            [sectionsViews addObject:sectionView];
            
            CGRect labelFrame = CGRectMake(labelPadding, labelPadding, sectionViewFrame.size.width - labelPadding * 2, sectionViewFrame.size.height - labelPadding * 2);
            UILabel *sectionTitleLabel = [[UILabel alloc] initWithFrame:labelFrame];
            sectionTitleLabel.text = sectionsTitles[count];
            sectionTitleLabel.font = kSectionTitleFont;
            sectionTitleLabel.textAlignment = NSTextAlignmentCenter;
            sectionTitleLabel.textColor = count == self.selectedSection ? self.selectedSectionColor : self.sectionColor;
            sectionTitleLabel.tag = count + kSectionTitleLabelTagStartNumber;
            [sectionView addSubview:sectionTitleLabel];
            
            if (count != 0 && count != sectionsTitles.count) {
                CGRect dividerFrame = CGRectMake(sectionWidth * count - 1, 2, dividerWidth, dividerHeight);
                UIView *dividerView = [[UIView alloc] initWithFrame:dividerFrame];
                dividerView.backgroundColor = self.dividerColor;
                dividerView.tag = count + 9;
                [self addSubview:dividerView];
                [dividers addObject:dividerView];
            }
            self.dividers = [dividers copy];
            self.sectionsViews = [sectionsViews copy];
            self.underlineViews = [underlineViews copy];
        }
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    }
    return self;
}

-(void)setDefaultValues{
    self.labelsFont = [UIFont systemFontOfSize:13.f];
    self.sectionColor = kDeselectedSectionColor;
    self.selectedSectionColor = kSelectedSectionColor;
    self.dividerColor = kDividerColor;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    CGFloat sectionViewHeight = self.frame.size.height - underlineHeight;
    CGFloat underlineYPosition = self.frame.size.height - underlineHeight;
    CGFloat sectionWidth = self.frame.size.width / self.numberOfSections;
    CGFloat dividerHeight = self.frame.size.height - underlineHeight - 4;
    
    for (NSInteger i = 0; i < self.underlineViews.count; i++) {
        UIView *underlineView = self.underlineViews[i];
        CGRect underlineViewFrame = CGRectMake(sectionWidth * i, underlineYPosition, sectionWidth, underlineHeight);
        [underlineView setFrame:underlineViewFrame];
    }
    
    for (NSInteger i = 0; i < self.sectionsViews.count; i++) {
        UIView *sectionView = self.sectionsViews[i];
        CGRect sectionViewFrame = CGRectMake(sectionWidth * i, 0, sectionWidth, sectionViewHeight);
        [sectionView setFrame:sectionViewFrame];
        UILabel *titleLabel = [sectionView.subviews firstObject];
        CGRect labelFrame = CGRectMake(labelPadding, labelPadding + 4, sectionViewFrame.size.width - labelPadding * 2, sectionViewFrame.size.height - labelPadding * 2);
        [titleLabel setFrame:labelFrame];
    }
    
    for (NSInteger i = 0; i < self.dividers.count; i++) {
        CGRect dividerFrame = CGRectMake(sectionWidth * (i + 1) - 1, 2, dividerWidth, dividerHeight);
        [self.dividers[i] setFrame:dividerFrame];
    }
}

- (IBAction)sectionSelected:(UITapGestureRecognizer *)sender {
    if (sender.view.tag - kSectionViewTagStartNumber != self.selectedSection) {
        self.selectedSection = sender.view.tag - kSectionViewTagStartNumber;
        if([self.delegate respondsToSelector:@selector(selectedSectionChanged:)])
           [self.delegate selectedSectionChanged:self.selectedSection];
    }
}

-(void)setSectionColor:(UIColor *)sectionColor{
    if (![self.sectionColor isEqual:sectionColor]){
        _sectionColor = sectionColor;
        for (UIView *section in self.sectionsViews){
            if (section.tag != self.selectedSection + kSectionViewTagStartNumber)
                section.backgroundColor = sectionColor;
//            else
//                section.backgroundColor = self.selectedSectionColor ? self.selectedSectionColor : kSelectedSectionColor;
        }
    }
}

-(void)setSelectedSectionColor:(UIColor *)selectedSectionColor{
    if (![_selectedSectionColor isEqual:selectedSectionColor]){
        _selectedSectionColor = selectedSectionColor;
        for (UIView *section in self.sectionsViews){
            if (section.tag == self.selectedSection + kSectionViewTagStartNumber)
                section.backgroundColor = selectedSectionColor;
        }
    }
}

-(void)setSectionTitleColor:(UIColor *)sectionTitleColor{
    if (![_sectionTitleColor isEqual:sectionTitleColor]){
        _sectionTitleColor = sectionTitleColor;
        for (UILabel *section in self.sectionsTitles){
            if (section.tag != self.selectedSection + kSectionTitleLabelTagStartNumber)
                section.textColor = sectionTitleColor;
        }
    }
}

-(void)setSelectedSectionTitleColor:(UIColor *)selectedSectionTitleColor{
    if (![_selectedSectionTitleColor isEqual:selectedSectionTitleColor]){
        _selectedSectionTitleColor = selectedSectionTitleColor;
        for (UILabel *section in self.sectionsTitles){
            if (section.tag == self.selectedSection + kSectionTitleLabelTagStartNumber)
                section.textColor = selectedSectionTitleColor;
        }
    }
}

-(void)setSectionTitleFont:(UIFont *)sectionTitleFont{
    if (![_sectionTitleFont isEqual:sectionTitleFont]){
        _sectionTitleFont = sectionTitleFont;
        for (UILabel *section in self.sectionsTitles){
            if (section.tag != self.selectedSection + kSectionTitleLabelTagStartNumber)
                section.font = sectionTitleFont;
        }
    }
}

-(void)setSelectedSectionTitleFont:(UIFont *)selectedSectionTitleFont{
    if (![_selectedSectionTitleFont isEqual:selectedSectionTitleFont]){
        _selectedSectionTitleFont = selectedSectionTitleFont;
        for (UILabel *section in self.sectionsTitles){
            if (section.tag == self.selectedSection + kSectionTitleLabelTagStartNumber)
                section.font = selectedSectionTitleFont;
        }
    }
}

-(void)setSectionUnderlineColor:(UIColor *)sectionUnderlineColor{
    if (![_sectionUnderlineColor isEqual:sectionUnderlineColor]){
        _sectionUnderlineColor = sectionUnderlineColor;
        for (UIView *section in self.underlineViews){
            if (section.tag != self.selectedSection + kSectionUnderlineTagStartNumber)
                section.backgroundColor = sectionUnderlineColor;
        }
    }
}

-(void)setSelectedSectionUnderlineColor:(UIColor *)selectedSectionUnderlineColor{
    if (![_selectedSectionUnderlineColor isEqual:selectedSectionUnderlineColor]){
        _selectedSectionUnderlineColor = selectedSectionUnderlineColor;
        for (UIView *section in self.underlineViews){
            if (section.tag != self.selectedSection + kSectionUnderlineTagStartNumber)
                section.backgroundColor = selectedSectionUnderlineColor;
        }
    }
}

-(UIColor *)sectionColor{
    if (!_sectionColor) {
        _sectionColor = kDeselectedSectionColor;
    }
    return _sectionColor;
}

-(UIColor *)selectedSectionColor{
    if (!_selectedSectionColor) {
        _selectedSectionColor = kSelectedSectionColor;
    }
    return _selectedSectionColor;
}

-(UIColor *)sectionTitleColor{
    if (!_sectionTitleColor) {
        _sectionTitleColor = kSectionTitleColor;
    }
    return _sectionTitleColor;
}

-(UIColor *)selectedSectionTitleColor{
    if (!_selectedSectionTitleColor) {
        _selectedSectionTitleColor = kSelectedSectionTitleColor;
    }
    return _selectedSectionTitleColor;
}

-(UIColor *)sectionUnderlineColor{
    if (!_sectionUnderlineColor){
        _sectionUnderlineColor = kSectionUnderlineColor;
    }
    return _sectionUnderlineColor;
}

-(UIColor *)selectedSectionUnderlineColor{
    if (!_selectedSectionUnderlineColor) {
        _selectedSectionUnderlineColor = kSelectedSectionUnderlineColor;
    }
    return _selectedSectionUnderlineColor;
}

-(UIFont *)sectionTitleFont{
    if (!_sectionTitleFont) {
        _sectionTitleFont = kSectionTitleFont;
    }
    return _sectionTitleFont;
}

-(UIFont *)selectedSectionTitleFont{
    if (!_selectedSectionTitleFont) {
        _selectedSectionTitleFont = kSelectedSectionTitleFont;
    }
    return _selectedSectionTitleFont;
}

@end
