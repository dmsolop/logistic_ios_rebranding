//
//  TriangleToTopView.m
//  YourLogisticsNew
//
//  Created by Mac on 20.09.2018.
//  Copyright © 2018 iCenter. All rights reserved.
//

#import "TriangleToTopView.h"
#import "ColorPallet.h"

@implementation TriangleToTopView

- (void)drawRect:(CGRect)rect {
    
    UIBezierPath* trianglePath = [UIBezierPath bezierPath];
    [trianglePath moveToPoint:CGPointMake(0, 0)];
    [trianglePath addLineToPoint:CGPointMake(self.frame.size.width, 0)];
    [trianglePath addLineToPoint:CGPointMake(self.frame.size.width/2, self.frame.size.height)];
    [trianglePath closePath];
    
    CAShapeLayer *triangleMaskLayer = [CAShapeLayer layer];
    [triangleMaskLayer setPath:trianglePath.CGPath];
    self.layer.mask = triangleMaskLayer;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.backgroundColor = [ColorPallet new].mintGreen;
}


@end
