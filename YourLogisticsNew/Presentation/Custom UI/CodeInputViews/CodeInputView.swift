//
//  PhoneNumberInputView.swift
//  OdlarYurdu
//
//  Created by Roma Dudka on 12.01.2018.
//  Copyright © 2018 prosper. All rights reserved.
//

import Foundation
import UIKit

@objc class CodeInputView: NibDesignable {
    @IBOutlet private var numbersTextFields: [UITextField]!
    @IBOutlet private var containerView: UIView!

    private var currentIndex = 0

    @objc var numberCount: Int {
        return numbersTextFields.count
    }

    fileprivate var didFinishBlock: ((_ numberString: String) -> Void)?
    fileprivate var didChangeBlock: ((_ numberString: String) -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        configureInitialState()
    }

    @objc func inputDidFinish(block: @escaping (_ numberString: String) -> Void) {
        didFinishBlock = block
    }

    @objc func inputDidChange(block: @escaping (_ numberString: String) -> Void) {
        didChangeBlock = block
    }

    private func configureInitialState() {
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapPhoneNumberInput(_:))))
    }

    @objc func tapPhoneNumberInput(_: UITapGestureRecognizer) {
        becomeFirstResponder()
    }

    // MARK: Animation

    @objc func shakeAnimation(withDuration duration: TimeInterval, animations: @escaping () -> Void, completion: @escaping () -> Void) {
        UIView.animate(withDuration: duration, delay: 0, usingSpringWithDamping: 0.01, initialSpringVelocity: 0.35, options: UIView.AnimationOptions(), animations: {
            animations()
        }) { _ in
            completion()
        }
    }

    fileprivate var shakeCount = 0
    fileprivate var direction = false
    open func shakeAnimationWithCompletion(_ completion: @escaping () -> Void) {
        let maxShakeCount = 5
        let centerX = bounds.midX
        let centerY = containerView.bounds.midY
        var duration = 0.10
        var moveX: CGFloat = 5

        if shakeCount == 0 || shakeCount == maxShakeCount {
            duration *= 0.5
        } else {
            moveX *= 2
        }
        shakeAnimation(withDuration: duration, animations: {
            if !self.direction {
                self.containerView.center = CGPoint(x: centerX + moveX, y: centerY)
            } else {
                self.containerView.center = CGPoint(x: centerX - moveX, y: centerY)
            }
        }) {
            if self.shakeCount >= maxShakeCount {
                self.shakeAnimation(withDuration: duration, animations: {
                    let realCenterX = self.superview!.bounds.midX
                    self.containerView.center = CGPoint(x: realCenterX, y: centerY)
                }) {
                    self.direction = false
                    self.shakeCount = 0
                    completion()
                }
            } else {
                self.shakeCount += 1
                self.direction = !self.direction
                self.shakeAnimationWithCompletion(completion)
            }
        }
    }
}

// MARK: - Helpers method

@objc extension CodeInputView {
    @objc var code: String {
        get {
            return numbersTextFields.compactMap({ $0.text }).joined()
        }
        set {
            clear()
            newValue.forEach { self.insertText(String($0)) }
        }
    }

    // MARK: - UIResponder

    open override var canBecomeFirstResponder: Bool {
        return true
    }
}

// MARK: - UIKeyInput

@objc extension CodeInputView: UIKeyInput {
    @objc public var hasText: Bool {
        return currentIndex > 0 ? true : false
    }

    fileprivate func updateCurrentNumbers(text: String = "", isDelete: Bool = false) {
        let currentTextField = numbersTextFields[currentIndex]

        UIView.transition(with: currentTextField, duration: 0.05, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
            currentTextField.text = text
        }, completion: nil)
    }

    @objc open func insertText(_ text: String) {
        if currentIndex < numberCount {
            updateCurrentNumbers(text: text)

            currentIndex += 1

            didChangeBlock?(code)
            if currentIndex == numberCount {
                didFinishBlock?(code)
            }
        }
    }

    @objc open func deleteBackward() {
        if currentIndex > 0 {
            currentIndex -= 1
            updateCurrentNumbers(isDelete: true)
            didChangeBlock?(code)
        }
    }

    @objc open func clear() {
        while currentIndex > 0 {
            currentIndex -= 1
            updateCurrentNumbers(isDelete: true)
        }
    }

    // MARK: - UITextInputTraits

    @objc open var keyboardType: UIKeyboardType { get { return .numberPad } set {} }
}
