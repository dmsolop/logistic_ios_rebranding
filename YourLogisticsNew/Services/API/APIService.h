//
//  NetworkService.h
//  AdamaPhoto
//
//  Created by Aleksandr Ponomarenko on 01.09.15.
//  Copyright (c) 2015 Aleksandr Ponomarenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>
#import "Article.h"
#import "Transit.h"

@class ArticleDetails;
@class NewsArticle;
@class NewsArticleDetails;

static NSString * const kPushTokenKey = @"YourLogisticsGCMPushToken";
static NSString * const kClientCodeKey = @"YourLogisticsClientCode";
static NSString * const kClientTypeKey = @"YourLogisticsClientType";
static NSString * const kClientPhoneNumberKey = @"YourLogisticsClientPhoneNumber";

static NSString * const kLastClientPhoneNumberKey = @"YourLogisticsLastClientPhoneNumber";
static NSString * const kLastClientCodeKey = @"YourLogisticsLastClientCode";

@interface APIService : AFHTTPSessionManager

@property (nonatomic, assign) BOOL isUserAuthorized;

+ (APIService *)sharedAPIClient;

//-(void)sendPushToken:(NSString *)pushToken
//          completion:(void(^)())completion
//             failure:(void (^)(NSError *))failure;

-(void)loginWithClientCode:(NSString *)code
               phoneNumber:(NSString *)phoneNumber
               pinCode:(NSString *)pinCode
                completion:(void(^)(NSString *))completion
                   failure:(void (^)(NSError *))failure;

-(void)getCompanyServicesWithCompletion:(void(^)(NSDictionary *))completion
                                failure:(void (^)(NSError *))failure;

-(void)getCompanyServiceDetailsForService:(Article *)article
                                completion:(void(^)(ArticleDetails *))completion
                                   failure:(void (^)(NSError *))failure;

-(void)getCompanyContactsWithCompletion:(void(^)(NSDictionary *))completion
                                failure:(void (^)(NSError *))failure;

-(void)getLogisticsNewsWithCompletion:(void (^)(NSArray *news))completion
                              failure:(void (^)(NSError *error))failure;

-(void)getCompanyNewsWithCompletion:(void (^)(NSArray *))completion
                            failure:(void (^)(NSError *))failure;

-(void)getNewsArticleDetailsForArticle:(NewsArticle *)article
                            completion:(void(^)(NewsArticleDetails *article))completion
                               failure:(void (^)(NSError *error))failure;

-(void)getServiceDetailsForArticle:(Article *)article
                        completion:(void(^)(ArticleDetails *article))completion
                           failure:(void (^)(NSError *error))failure;

+(void)cabinetLogout;

-(void)verifyRecievedPushWithLastMessageId:(NSInteger )messageId;

-(void)getMessageWithType:(NSInteger )type
                messageId:(NSString* )messageId
               completion:(void (^)(NSArray *response))completion
                  failure:(void (^)(NSError *))failure;

-(void)getTransitListWithCompletion:(void(^)(NSArray *))completion
                            failure:(void (^)(NSError *))failure;

- (void)postImage:(UIImage *)image
    enterTranstit:(Transit *)enterTransit
      exitTransit:(Transit *)exitTransit
       completion:(void (^)(NSString *))completion
          failure:(void (^)(NSError *))failure;

- (void)postShareImage:(UIImage *)image
         enterTranstit:(Transit *)enterTransit
           exitTransit:(Transit *)exitTransit
            completion:(void (^)(NSString *))completion
               failure:(void (^)(NSError *))failure;

+ (NSString *)getJSON:(id)object;

@end

NSString *getJSON(id object);
