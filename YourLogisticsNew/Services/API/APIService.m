//
//  NetworkService.m
//  AdamaPhoto
//
//  Created by Aleksandr Ponomarenko on 01.09.15.
//  Copyright (c) 2015 Aleksandr Ponomarenko. All rights reserved.
//

#import "APIService.h"
#import "Utilities.h"
#import "Article.h"
#import "ArticleDetails.h"
#import "NewsArticle.h"
#import "NewsArticleDetails.h"
#import "AppDelegate.h"
#import "Transit.h"
#import <QuartzCore/QuartzCore.h>

static NSString * const kBaseURL = @"https://mobile-api.yourlogistics.com.ua";

static NSString * const kURL = @"http://mob.info.cv.ua/get"; //@"http://dev.development.pp.ua/get";

static NSString * const kAuthorizationURL = @"/login/sign";

static NSString * const kGetAllServicesURL = @"/articles/getAllservices";

static NSString * const kGetContactsURL = @"/contacts/getContacts";

static NSString * const kGetLogisticsNewsURL = @"/articles/getAllUkranianNews";

static NSString * const kGetCompanyNewsURL = @"/articles/getCompanyNews";

static NSString * const kGetTransitListURL = @"http://mob.info.cv.ua/get-transit";

static NSString * const kSendPhotoURL = @"http://mob.info.cv.ua/add-image";

static NSString * const kGetLogisticsNewsByIdURL = @"/articles/getUkranianNewsById";

static NSString * const kGetCompanyNewsByIdURL = @"/articles/getCompanyNewsById";

static NSString * const kGetCompanyServiceByIdURL = @"/articles/getServiceById";

static NSString * const kSolKey = @"JkWllEYxDCSQU$xt}jB3@@suYGN$sJ@$GDLR?Zmm@GDLqyn8MoVw8VMLm7Ysk@86g@xW";

static NSString * const kVerifyPushURL = @"/verifypush/verify";

@implementation APIService

+(APIService *)sharedAPIClient{
    static APIService *sharedAPIClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedAPIClient = [[self alloc] init];
    });
    return sharedAPIClient;
}

-(instancetype)init{
    self = [super initWithBaseURL:[NSURL URLWithString:kBaseURL]];
    if (self) {
        self.securityPolicy.allowInvalidCertificates = YES;
        self.securityPolicy.validatesDomainName = NO;
        self.responseSerializer = [AFJSONResponseSerializer serializer];
        self.requestSerializer = [AFHTTPRequestSerializer serializer];
        self.responseSerializer.acceptableContentTypes = [self.responseSerializer.acceptableContentTypes setByAddingObjectsFromArray:@[@"image/jpeg", @"image/png"]];
        self.requestSerializer.timeoutInterval = 60;
    }
    return self;
}

//-(void)sendPushToken:(NSString *)pushToken
//          completion:(void(^)())completion
//             failure:(void (^)(NSError *))failure{
//    
//    NSUInteger clientCode = 98427;
//    NSString *phoneNumber = @"0969668136";
//    NSString *clientHash = [Utilities getMD5FromString:[phoneNumber stringByAppendingString:kSolKey]];
//    NSDictionary *parameters = @{
//                                 @"client_code" : @(clientCode),
//                                 @"client_hash" : clientHash,
//                                 @"platform" : @"iOS",
//                                 @"token" : pushToken
//                                 };
//    [self POST:kAuthorizationURL parameters:parameters progress:nil success:^(NSURLSessionDataTask *task, id  responseObject) {
//        if (completion)
//            completion();
//    } failure:^(NSURLSessionDataTask *task, NSError *error) {
//        if (failure)
//            failure(error);
//    }];
//}

-(void)loginWithClientCode:(NSString *)code
               phoneNumber:(NSString *)phoneNumber
                   pinCode:(NSString *)pinCode
                completion:(void (^)(NSString *))completion
                   failure:(void (^)(NSError *))failure {
    NSString *registrationToken = [[NSUserDefaults standardUserDefaults] objectForKey:kPushTokenKey];
    
    NSString *clientHash = [Utilities getMD5FromString:[phoneNumber stringByAppendingString:kSolKey]];
    NSLog(@"******Client hash = %@ ******", clientHash);
    NSString *pinCodeHash = [Utilities getMD5FromString:[pinCode stringByAppendingString:kSolKey]];
    
    NSMutableDictionary *parameters = @{
                                        @"client_code" : code,
                                        @"client_hash" : clientHash,
                                        @"pin" : pinCodeHash,
                                        @"platform" : @"iOS",
                                        }.mutableCopy;
    if (registrationToken) {
        parameters[@"token"] = registrationToken;
    }
    
    [self POST:kAuthorizationURL parameters:parameters headers:nil progress:nil success:^(NSURLSessionDataTask *task, id  responseObject) {
        NSString *clientType = responseObject[@"data"][@"client_type"];
        if (!([responseObject[@"success"] boolValue] == YES)) {
            failure([responseObject objectForKey:@"errormsg"]);
        }
        if (!clientType) {
            failure([NSError errorWithDomain:NSItemProviderErrorDomain
                                        code:112
                                    userInfo: @{ NSLocalizedDescriptionKey : @"clientType Empty!!" }]);
        }
        else {
            completion(clientType);
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        if (failure)
            failure(error);
    }];
}

-(void)getCompanyServicesWithCompletion:(void(^)(NSDictionary *))completion
                                failure:(void (^)(NSError *))failure{
    
    [self POST:kGetAllServicesURL parameters:nil headers:nil progress:nil success:^(NSURLSessionDataTask *task, id  responseObject) {
        if ([responseObject[@"success"] boolValue] == YES) {
            NSDictionary *response = responseObject[@"data"];
            NSDictionary *services = @{
                                       @"AllServices" : [self parseServices:response[@"all_services"]],
                                       @"Documentation" : [self parseServices:response[@"docs_approval"]]
                                       };
            if (completion)
                completion(services);
        } else {
            failure(nil);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        if (failure)
            failure(error);
    }];
}

-(NSArray *)parseServices:(NSArray *)servicesResponce{
    NSMutableArray *services = [NSMutableArray array];
    for (NSDictionary *singleService in servicesResponce) {
        Article *service = [[Article alloc] init];
        service.ID = [singleService[@"ID"] integerValue];
        service.title = singleService[@"post_title"];
        service.imageLink = singleService[@"post_image"];
        service.type = ArticleTypeCompanyService;
        [services addObject:service];
    }
    return [services copy];
}

-(void)getCompanyServiceDetailsForService:(Article *)article
                               completion:(void(^)(ArticleDetails *))completion
                                  failure:(void (^)(NSError *))failure{
    
    [self POST:kGetCompanyServiceByIdURL
    parameters:@{ @"service_id" : @(article.ID) }
    headers:nil
    progress:nil
    success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
           if ([responseObject[@"success"] boolValue] == YES) {
               ArticleDetails *articleDetails = [[ArticleDetails alloc] initWithArticle:article];
               NSDictionary *data = responseObject[@"data"];
               articleDetails.content = data[@"post_content"];
               articleDetails.link = [[NSURL alloc] initWithString:data[@"post_link"]];
               articleDetails.title = data[@"post_title"];
               if (completion)
                   completion(articleDetails);
           } else {
               failure(nil);
           }
       }
       failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//           NSLog(@"%@", error);
           if (failure)
               failure(error);
       }];
}

-(void)getCompanyContactsWithCompletion:(void (^)(NSDictionary *))completion
                                failure:(void (^)(NSError *))failure{
    [self GET:kGetContactsURL parameters:nil headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (completion)
            completion(responseObject[@"data"]);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure)
            failure(error);
    }];
}

-(void)getCompanyNewsWithCompletion:(void (^)(NSArray *))completion
                              failure:(void (^)(NSError *))failure{
    [self GET:kGetCompanyNewsURL parameters:nil headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSArray *articles = [self parseNewsArray:responseObject[@"data"] withType:ArticleTypeCompany];
        if (completion)
            completion(articles);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure)
            failure(error);
    }];
}

- (void)getTransitListWithCompletion:(void (^)(NSArray *))completion
                             failure:(void (^)(NSError *))failure {
    [self GET:kGetTransitListURL parameters:nil headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSArray *transits = [self parseTransitListArray:responseObject[@"data"]];
        if (completion)
            completion(transits);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure)
            failure(error);
    }];
}

-(void)getLogisticsNewsWithCompletion:(void (^)(NSArray *))completion
                              failure:(void (^)(NSError *))failure{
    [self GET:kGetLogisticsNewsURL parameters:nil headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSArray *articles = [self parseNewsArray:responseObject[@"data"] withType:ArticleTypeLogistics];
        if (completion)
            completion([articles copy]);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure)
            failure(error);
    }];
    
}

-(NSArray *)parseTransitListArray:(NSArray *)transitArray {
    NSMutableArray *transits = [NSMutableArray arrayWithCapacity:transitArray.count];
    for (NSDictionary *transit in transitArray) {
        Transit *transitItem = [[Transit alloc] init];
        transitItem.name = transit[@"name"];
        transitItem.date = transit[@"date"];
        transitItem.guid = transit[@"guid"];
        
        [transits addObject:transitItem];
    }
    return [transits copy];
}

-(NSArray *)parseNewsArray:(NSArray *)newsArray withType:(ArticleType)type{
    NSMutableArray *articles = [NSMutableArray arrayWithCapacity:newsArray.count];
    for (NSDictionary *singleNews in newsArray) {
        id newsItem;
        if (type != ArticleTypeCompanyService) {
            newsItem = [[NewsArticle alloc] initWithType:type];
            [newsItem setImageLink:singleNews[@"image"]];
            NSString *dateString = singleNews[@"post_date"];
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            formatter.dateFormat = @"YYYY-MM-dd HH:mm:ss";
            [newsItem setDate:[formatter dateFromString:dateString]];
        } else {
            newsItem = [[Article alloc] initWithType:type];
        }
        [newsItem setID:[singleNews[@"ID"] integerValue]];
        [newsItem setTitle:singleNews[@"post_title"]];
        [articles addObject:newsItem];
    }
    return [articles copy];
}

-(void)getNewsArticleDetailsForArticle:(NewsArticle *)article
                            completion:(void(^)(NewsArticleDetails *article))completion
                               failure:(void (^)(NSError *error))failure{
    [self POST:article.type == ArticleTypeCompany ? kGetCompanyNewsByIdURL : kGetLogisticsNewsByIdURL
    parameters:@{ @"news_id" : @(article.ID) }
    headers:nil
      progress:nil
       success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
           if ([responseObject[@"success"] boolValue] == YES) {
               NewsArticleDetails *articleDetails = [[NewsArticleDetails alloc] initWithArticle:article];
               NSDictionary *data = responseObject[@"data"];
               [articleDetails setContent:data[@"post_content"]];
               [articleDetails setLargeImageLink:data[@"image"]];
               if (completion)
                   completion(articleDetails);
           } else {
               failure(nil);
           }
       }
       failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//           NSLog(@"%@", error);
           if (failure)
               failure(error);
    }];
}

-(void)getServiceDetailsForArticle:(Article *)article
                        completion:(void(^)(ArticleDetails *article))completion
                           failure:(void (^)(NSError *error))failure{
    [self POST:kGetCompanyServiceByIdURL
    parameters:@{ @"service_id" : @(article.ID) }
       headers:nil
      progress:nil
       success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
           if ([responseObject[@"success"] boolValue] == YES) {
               ArticleDetails *articleDetails = [[ArticleDetails alloc] initWithArticle:article];
               NSDictionary *data = responseObject[@"data"];
               [articleDetails setContent:data[@"post_content"]];
               if (completion)
                   completion(articleDetails);
           } else {
               failure(nil);
           }
       }
       failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//           NSLog(@"%@", error);
           if (failure)
               failure(error);
       }];
}

- (BOOL)isUserAuthorized {
    NSUserDefaults *UD = [NSUserDefaults standardUserDefaults];
    return [UD objectForKey:kClientCodeKey] && [UD objectForKey:kClientPhoneNumberKey];
}

+ (void)cabinetLogout {
    NSUserDefaults *UD = [NSUserDefaults standardUserDefaults];
    [UD removeObjectForKey:kClientCodeKey];
    [UD removeObjectForKey:kClientPhoneNumberKey];
    [UD synchronize];
    
    UD = [[NSUserDefaults alloc] initWithSuiteName:@"group.photoshare.extension"];
    [UD removeObjectForKey:kClientCodeKey];
    [UD synchronize];
}

-(void)verifyRecievedPushWithLastMessageId:(NSInteger )messageId{
    
    [self POST:kVerifyPushURL parameters:@{ @"service_id" : @(2) }
       headers:nil progress:nil
       success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
           
       }
       failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//           NSLog(@"%@", error);
       }];
}

-(void)getMessageWithType:(NSInteger )type
                messageId:(NSString* )messageId
               completion:(void (^)(NSArray *response))completion
                  failure:(void (^)(NSError *))failure {
    
    NSUserDefaults *UD = [NSUserDefaults standardUserDefaults];
    NSString *clientHash = [Utilities getMD5FromString:[[UD objectForKey:kClientPhoneNumberKey] stringByAppendingString:kSolKey]];
//    NSString *clientHash = [Utilities getMD5FromString:[@"0665818067" stringByAppendingString:kSolKey]];
//    0996006363
    NSDictionary* parameters = @{
                                 @"type" : @(type),
                                 @"userhash" : clientHash,
                                 @"from" : messageId
                                 };
        
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    dispatch_async(queue, ^{
        
        [self GET:kURL parameters:parameters headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            getJSON(responseObject);
            if (completion)
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    completion(responseObject);
                });
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            if (failure)
                failure(error);
        }];

    });
    
}

- (void)postImage:(UIImage *)image
    enterTranstit:(Transit *)enterTransit
      exitTransit:(Transit *)exitTransit
       completion:(void (^)(NSString *))completion
          failure:(void (^)(NSError *))failure
{
    NSUserDefaults *UD = [NSUserDefaults standardUserDefaults];
    NSString *clientCode = [UD objectForKey:kClientCodeKey];
    NSString *base64img = [UIImageJPEGRepresentation(image, 0.1) base64EncodedStringWithOptions:0];
    
    CFTimeInterval startTime = CACurrentMediaTime();
    
    [self POST:kSendPhotoURL parameters:nil headers:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        [formData appendPartWithFormData:[base64img dataUsingEncoding:NSUTF8StringEncoding] name:@"b64_img"];
        [formData appendPartWithFormData:[clientCode dataUsingEncoding:NSUTF8StringEncoding] name:@"client_code"];
        [formData appendPartWithFormData:[enterTransit.guid dataUsingEncoding:NSUTF8StringEncoding] name:@"enter"];
        [formData appendPartWithFormData:[exitTransit.guid dataUsingEncoding:NSUTF8StringEncoding] name:@"exit"];
        [formData appendPartWithFormData:[@"JPEG" dataUsingEncoding:NSUTF8StringEncoding] name:@"img_ext"];
    }
      progress:nil
       success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
           CFTimeInterval elapsedTime = CACurrentMediaTime() - startTime;
           NSLog(@"Send Image, elapsedTime: %f", elapsedTime);
           
           if ([responseObject[@"success"] boolValue] == YES) {
               if (completion) {
                   completion(responseObject[@"data"]);
               }
           } else {
               failure(nil);
           }
       }
       failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
           if (failure) {
               failure(error);
           }
       }];
}

- (void)postShareImage:(UIImage *)image
         enterTranstit:(Transit *)enterTransit
           exitTransit:(Transit *)exitTransit
            completion:(void (^)(NSString *))completion
               failure:(void (^)(NSError *))failure
{
    NSUserDefaults *UD = [[NSUserDefaults alloc] initWithSuiteName:@"group.photoshare.extension"];
    NSString *clientCode = [UD objectForKey:kClientCodeKey];
    NSString *base64img = [UIImageJPEGRepresentation(image, 0.1) base64EncodedStringWithOptions:0];
    
    [self POST:kSendPhotoURL parameters:nil headers:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        [formData appendPartWithFormData:[base64img dataUsingEncoding:NSUTF8StringEncoding] name:@"b64_img"];
        [formData appendPartWithFormData:[clientCode dataUsingEncoding:NSUTF8StringEncoding] name:@"client_code"];
        [formData appendPartWithFormData:[enterTransit.guid dataUsingEncoding:NSUTF8StringEncoding] name:@"enter"];
        [formData appendPartWithFormData:[exitTransit.guid dataUsingEncoding:NSUTF8StringEncoding] name:@"exit"];
        [formData appendPartWithFormData:[@"JPEG" dataUsingEncoding:NSUTF8StringEncoding] name:@"img_ext"];
    }
      progress:nil
       success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
           if ([responseObject[@"success"] boolValue] == YES) {
               if (completion) {
                   completion(responseObject[@"data"]);
               }
           } else {
               failure(nil);
           }
       }
       failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
           if (failure) {
               failure(error);
           }
       }];
}

+ (NSString *)getJSON:(id)object
{
    NSError *writeError = nil;
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:object options:NSJSONWritingPrettyPrinted error:&writeError];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    return jsonString;
}

@end

NSString *getJSON(id object)
{
    NSError *writeError = nil;
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:object options:NSJSONWritingPrettyPrinted error:&writeError];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    return jsonString;
}
