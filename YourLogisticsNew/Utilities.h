//
//  Utilities.h
//  YourLogistics
//
//  Created by Aleksandr on 29.04.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UIView;
@class UIAlertController;

static NSString * _Nonnull const kNotificationMessageRecieved = @"NotificationMessageRecieved";

@interface Utilities : NSObject

+(NSString *_Nullable)getMD5FromString:(NSString *_Nonnull)key;

+(NSAttributedString *_Nonnull)greenMarkDateFromDate:(NSDate *_Nonnull)date;

+(void)addShadowToView:(UIView *_Nonnull)view;

+(UIAlertController * _Nonnull)downloadingErrorAlert;

@end
