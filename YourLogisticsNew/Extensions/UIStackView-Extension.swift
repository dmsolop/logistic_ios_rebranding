//
//  UIStackView-Extension.swift
//  YourLogisticsNew
//
//  Created by Mac on 16.01.2019.
//  Copyright © 2019 iCenter. All rights reserved.
//

import Foundation

@objc extension UIStackView {
    
    @objc func removeAllArrangedSubviews() {
        
        let removedSubviews = arrangedSubviews.reduce([]) { (allSubviews, subview) -> [UIView] in
            self.removeArrangedSubview(subview)
            return allSubviews + [subview]
        }
        
        // Deactivate all constraints
        NSLayoutConstraint.deactivate(removedSubviews.flatMap({ $0.constraints }))
        
        // Remove the views from self
        removedSubviews.forEach({ $0.removeFromSuperview() })
    }
}
