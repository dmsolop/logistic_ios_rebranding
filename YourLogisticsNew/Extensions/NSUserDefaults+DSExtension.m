//
//  NSUserDefaults+DSExtension.m
//  YourLogisticsNew
//
//  Created by Dima on 3/20/19.
//  Copyright © 2019 iCenter. All rights reserved.
//

#import "NSUserDefaults+DSExtension.h"


@implementation NSUserDefaults (DSExtension)

-(void)saveEntryPhoneNumber:(NSString*)phoneNumber {
    
    [self setObject:phoneNumber forKey:@"phoneNumber"];

    [self synchronize];
    
    NSLog(@"Data saved");
}

-(NSString*)takeEntryPhoneNumber {
    
    NSString *phoneNumber = [self objectForKey:@"phoneNumber"];
    NSLog(@"Data got");
    return phoneNumber;
}

@end
