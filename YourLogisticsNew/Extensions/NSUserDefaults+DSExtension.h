//
//  NSUserDefaults+DSExtension.h
//  YourLogisticsNew
//
//  Created by Dima on 3/20/19.
//  Copyright © 2019 iCenter. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSUserDefaults (DSExtension)

-(void)saveEntryPhoneNumber:(NSString*)phoneNumber;

-(NSString*)takeEntryPhoneNumber;

@end

NS_ASSUME_NONNULL_END
