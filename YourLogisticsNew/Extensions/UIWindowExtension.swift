//
//  UIWindow+Extension.swift
//  MarketingPlan
//
//  Created by VitaliyK on 10.03.17.
//  Copyright © 2017 VitaliyK. All rights reserved.
//

import Foundation
import UIKit

@objc extension UIWindow {
    @objc func setRoot(viewController: UIViewController, animated: Bool = true, completion: ((Bool) -> Void)? = nil) {
        rootViewController = viewController

        let closure = { [weak self] in
            guard let `self` = self else { return }
            self.makeKeyAndVisible()
        }

        if animated {
            UIView.transition(with: self, duration: 0.5, options: .transitionCrossDissolve, animations: closure, completion: completion)
        } else {
            closure()
            completion?(true)
        }
    }
}
