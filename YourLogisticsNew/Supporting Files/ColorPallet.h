//
//  ColorPallet.h
//  YourLogisticsNew
//
//  Created by Mac on 28.09.2018.
//  Copyright © 2018 iCenter. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ColorPallet : UIImage

///Base colors
@property (nonatomic, strong) UIColor *baseBlue;
@property (nonatomic, strong) UIColor *baseLightBlue;

///Message Colors
@property (nonatomic, strong) UIColor *lightLightGreen;
@property (nonatomic, strong) UIColor *lime;
@property (nonatomic, strong) UIColor *malachite;
@property (nonatomic, strong) UIColor *mintGreen;
@property (nonatomic, strong) UIColor *irishGreenColor;
@property (nonatomic, strong) UIColor *blackColor;
@property (nonatomic, strong) UIColor *pastelGreenColor;
@property (nonatomic, strong) UIColor *greenGrayColor;
@property (nonatomic, strong) UIColor *greenLimeColor;
@property (nonatomic, strong) UIColor *honeyDewColor;
@property (nonatomic, strong) UIColor *yelowGreenColor;
@property (nonatomic, strong) UIColor *muslimGreenColor;
@property (nonatomic, strong) UIColor *brightGreenColor;
@property (nonatomic, strong) UIColor *whiteColor;
@property (nonatomic, strong) UIColor *chateauGreenColor;
@property (nonatomic, strong) UIColor *camouflageGreenColor;
@property (nonatomic, strong) UIColor *darkGrayColor;
@property (nonatomic, strong) UIColor *lightGrayColor;
@property (nonatomic, strong) UIColor *greenColor;
@property (nonatomic, strong) UIColor *groassyGreenColor;
@property (nonatomic, strong) UIColor *harlequinColor;
@property (nonatomic, strong) UIColor *screamingGreenColor;
@property (nonatomic, strong) UIColor *signalGrayColor;
@property (nonatomic, strong) UIColor *brightTelegray;
@property (nonatomic, strong) UIColor *pearlDarkGray;
@property (nonatomic, strong) UIColor *dahliaYellow;
@property (nonatomic, strong) UIColor *redColor;
@property (nonatomic, strong) UIColor *reachOliveGreen;
@property (nonatomic, strong) UIColor *greenTea;
@property (nonatomic, strong) UIColor *caribianGreen;
@property (nonatomic, strong) UIColor *iconReportColor;

///Report Colors
@property (nonatomic, strong) UIColor *reportGreenColor;
@property (nonatomic, strong) UIColor *reportGreeGrayColor;
@property (nonatomic, strong) UIColor *reportYelowColor;
@property (nonatomic, strong) UIColor *reportGrayColor;
@property (nonatomic, strong) UIColor *reportLightGrayColor;
@property (nonatomic, strong) UIColor *reportBlackColor;


@property (nonatomic, assign) BOOL isNew;
@property (nonatomic, assign) double alpha;

-(void)prepareColors;
-(UIColor*)headerSetColorWithCodeDesign:(NSUInteger)code;
-(UIColor*)borderSetColorWithCodeDesign:(NSUInteger)code;
-(UIColor*)separatorSetColorWithCodeDesign:(NSUInteger)code;
-(UIColor*)decorViewSetColorWithCodeDesign:(NSUInteger)code;
-(UIColor*)textSetColorWithCodeDesign:(NSUInteger)code;

@end
