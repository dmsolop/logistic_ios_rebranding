//
//  SharedManager.m
//  YourLogisticsNew
//
//  Created by Dima on 3/24/19.
//  Copyright © 2019 iCenter. All rights reserved.
//

#import "SharedManager.h"

@implementation SharedManager

@synthesize isBossPhoneUsing;

- (instancetype)init
{
    self = [super init];
    if (self) {
        isBossPhoneUsing = NO;
    }
    return self;
}

+ (instancetype)sharedManager
{
    static SharedManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[SharedManager alloc] init];
        
    });
    return sharedManager;
}

@end
