//
//  Constants.m
//  YourLogisticsNew
//
//  Created by Mac on 01.10.2018.
//  Copyright © 2018 iCenter. All rights reserved.
//

#import "Constants.h"

@implementation Constants

- (instancetype)init
{
    self = [super init];
    if (self) {
        _spacingInConteiner = 10;
        _bossNumber1 = @"0971000000";
        _bossNumber2 = @"0665818067";
        _bossNumber3 = @"0934329225";
        _bossNumber4 = @"0930944570";
        _headerNamesType3 = @[@"Менеджер", @"", @"ТО\n\"Оформ-\nляем\"", @"Логистика \n\"Оформ-\nляем\"", @"Маржа\nлогис\nтика", @"Кол-воТО\n\"Раста-\nможено\""];
        _headerNamesType4 = @[@"Менеджер", @"", @"Кол-во НИ\n\"Оформ-\nляем\"", @"Маржа\nНИ", @"Кол-воLCL\n\"Оформ-\nляем\"", @"М3 LCL\n\"Оформ-\nляем\""];
        _headerNamesType5 = @[@"Менеджер", @"", @"Кол-во LCL\n\"Оформляем\"", @"М3 LCL\n\"Оформляем\""];
        _headerNamesType6 = @[@"Менеджер", @"П/П", @"Номер\nнедели", @"Номер\nдня\nнедели", @"Кол-во\nТТ", @"Кол-во\nгарантий"];
        _headerNamesType7 = @[@"Менеджер", @"П/П", @"Количество\nТТ", @"Количество\nгарантий"];
        _headerNamesType9 = @[@"Заявка", @"Товар", @"$ кг брутто"];
        _noDataForDisplay = @"Нет данных для отображения";
        _titleNews = @"Новости";
        _titleReports = @"Отчеты";
        _initialTime = [[[NSDate date] dateByAddingTimeInterval:-1*24*60*60] timeIntervalSince1970]*1000;
    }
    return self;
}

@end
