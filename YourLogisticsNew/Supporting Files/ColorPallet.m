//
//  ColorPallet.m
//  YourLogisticsNew
//
//  Created by Mac on 28.09.2018.
//  Copyright © 2018 iCenter. All rights reserved.
//

#import "ColorPallet.h"

@implementation ColorPallet

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.isNew = YES;
        [self prepareColors];
    }
    return self;
}

-(void)prepareColors {
    
    self.alpha = self.isNew ? 1.0f : 0.5f;
    
    self.lightLightGreen = [UIColor colorWithRed:63/255.0f green:207/255.0f blue:233/255.0f alpha:self.alpha];
    self.lime = [UIColor colorWithRed:115/255.0f green:206/253.0f blue:245/255.0f alpha:self.alpha];
    self.malachite = [UIColor colorWithRed:88/255.0f green:206/255.0f blue:213/255.0f alpha:self.alpha];
    self.mintGreen = [UIColor colorWithRed:18/255.0f green:0/255.0f blue:199/255.0f alpha:self.alpha];
    self.caribianGreen = [UIColor colorWithRed:58/255.0f green:191/255.0f blue:223/255.0f alpha:self.alpha];
    self.reachOliveGreen = [UIColor colorWithRed:15/255.0f green:23/255.0f blue:60/255.0f alpha:self.alpha];
    self.greenTea = [UIColor colorWithRed:214/255.0f green:242/255.0f blue:255/255.0f alpha:self.alpha];
    self.irishGreenColor = [UIColor colorWithRed:43/255.0f green:102/255.0f blue:238/255.0f alpha:self.alpha];
    self.pastelGreenColor = [UIColor colorWithRed:135/255.0f green:202/255.0f blue:243/255.0f alpha:self.alpha];
    self.greenGrayColor = [UIColor colorWithRed:68/255.0f green:86/255.0f blue:133/255.0f alpha:self.alpha];
    self.greenLimeColor = [UIColor colorWithRed:125/255.0f green:230/255.0f blue:255/255.0f alpha:self.alpha];
    self.yelowGreenColor = [UIColor colorWithRed:151/255.0f green:173/255.0f blue:223/255.0f alpha:self.alpha];
    self.honeyDewColor = [UIColor colorWithRed:201/255.0f green:255/255.0f blue:249/255.0f alpha:self.alpha];
    self.muslimGreenColor = [UIColor colorWithRed:33/255.0f green:67/255.0f blue:212/255.0f alpha:self.alpha];
    self.brightGreenColor = [UIColor colorWithRed:102/255.0f green:222/255.0f blue:251/255.0f alpha:self.alpha];
    self.camouflageGreenColor = [UIColor colorWithRed:78/255.0f green:80/255.0f blue:199/255.0f alpha:self.alpha];
    self.darkGrayColor = [UIColor colorWithRed:155/255.0f green:163/255.0f blue:148/255.0f alpha:self.alpha];
    self.lightGrayColor = [UIColor colorWithRed:183/255.0f green:183/255.0f blue:183/255.0f alpha:self.alpha];
    self.greenColor = [UIColor colorWithRed:17/255.0f green:37/255.0f blue:111/255.0f alpha:self.alpha];
    self.groassyGreenColor = [UIColor colorWithRed:33/255.0f green:79/255.0f blue:239/255.0f alpha:self.alpha];
    self.harlequinColor = [UIColor colorWithRed:28/255.0f green:55/255.0f blue:192/255.0f alpha:self.alpha];
    self.screamingGreenColor = [UIColor colorWithRed:136/255.0f green:191/255.0f blue:255/255.0f alpha:self.alpha];
    self.signalGrayColor = [UIColor colorWithRed:150/255.0f green:153/255.0f blue:146/255.0f alpha:self.alpha];
    self.brightTelegray = [UIColor colorWithRed:208/255.0f green:208/255.0f blue:208/255.0f alpha:self.alpha];
    self.pearlDarkGray = [UIColor colorWithRed:130/255.0f green:130/255.0f blue:130/255.0f alpha:self.alpha];
    self.dahliaYellow = [UIColor colorWithRed:243/255.0f green:175/255.0f blue:6/255.0f alpha:self.alpha];
    self.redColor = [UIColor colorWithRed:255/255.0f green:0/255.0f blue:0/255.0f alpha:self.alpha];
    self.iconReportColor = [UIColor colorWithRed:89/255.0f green:152/255.0f blue:253/255.0f alpha:self.alpha];
    self.blackColor = UIColor.blackColor;
    self.whiteColor = UIColor.whiteColor;
    
    ///Report colors
    self.reportGreenColor = [UIColor colorWithRed:115/255.0f green:206/253.0f blue:245/255.0f alpha:self.alpha];
    self.reportGreeGrayColor = [UIColor colorWithRed:214/255.0f green:242/255.0f blue:255/255.0f alpha:self.alpha];
    self.reportYelowColor = [UIColor colorWithRed:245/255.0f green:239/255.0f blue:53/255.0f alpha:self.alpha];
    self.reportGrayColor = [UIColor colorWithRed:95/255.0f green:95/255.0f blue:95/255.0f alpha:self.alpha];
    self.reportLightGrayColor = [UIColor colorWithRed:204/255.0f green:204/255.0f blue:204/255.0f alpha:self.alpha];
    self.reportBlackColor = [UIColor colorWithRed:0/255.0f green:0/255.0f blue:0/255.0f alpha:self.alpha];
    
    ///Base colors
    self.baseBlue = [UIColor colorWithRed:15/255.0f green:0/253.0f blue:199/255.0f alpha:self.alpha];
    self.baseLightBlue = [UIColor colorWithRed:53/255.0f green:149/255.0f blue:253/255.0f alpha:self.alpha];
}

-(UIColor*)headerSetColorWithCodeDesign:(NSUInteger)code {
    [self prepareColors];

    switch (code) {
        case 1:
            return self.irishGreenColor;
            break;
        case 2:
            return self.greenLimeColor;
            break;
        case 3:
            return self.muslimGreenColor;
            break;
        case 4:
            return self.brightGreenColor;
            break;
        case 5:
            return self.greenTea;
            break;
        case 6:
            return self.greenColor;
            break;
        case 7:
            return self.screamingGreenColor;
            break;
        case 8:
            return self.redColor;
            break;
        case 10:
            return self.malachite;
            break;
        case 11:
            return self.lightGrayColor;
            break;
        case 9:
        case 12:
        case 15:
        case 18:
        case 19:
        case 20:
            return self.groassyGreenColor;
            break;
        case 13:
        case 17:
            return self.reachOliveGreen;
            break;
        case 14:
            return self.caribianGreen;
            break;
        case 16:
            return self.lime;
            break;
        default:
            break;
    }
    return nil;
}

-(UIColor*)borderSetColorWithCodeDesign:(NSUInteger)code {
    [self prepareColors];
    
    switch (code) {
        case 1:
            return self.irishGreenColor;
            break;
        case 2:
            return self.greenLimeColor;
            break;
        case 3:
            return self.muslimGreenColor;
            break;
        case 4:
            return self.brightGreenColor;
            break;
        case 5:
            return self.greenTea;
            break;
        case 6:
            return self.greenColor;
            break;
        case 7:
            return self.screamingGreenColor;
            break;
        case 8:
            return self.redColor;
            break;
        case 10:
            return self.malachite;
            break;
        case 11:
            return self.redColor;
            break;
        case 14:
            return self.caribianGreen;
            break;
        case 16:
            return self.greenColor;
            break;
        default:
            break;
    }
    return nil;
}

-(UIColor*)separatorSetColorWithCodeDesign:(NSUInteger)code {
    [self prepareColors];

    switch (code) {
        case 1:
            return self.pastelGreenColor;
            break;
        case 2:
            return self.honeyDewColor;
            break;
        case 3:
            return self.irishGreenColor;
            break;
        case 4:
            return self.whiteColor;
            break;
        case 5:
            return self.whiteColor;
            break;
        case 6:
            return self.harlequinColor;
            break;
        case 7:
            return self.yelowGreenColor;
            break;
        case 8:
            return self.whiteColor;
            break;
        case 10:
            return UIColor.clearColor;
            break;
        case 11:
            return UIColor.clearColor;
            break;
        case 14:
            return UIColor.clearColor;
            break;
        default:
            break;
    }
    return nil;
}

-(UIColor*)decorViewSetColorWithCodeDesign:(NSUInteger)code {
    [self prepareColors];

    switch (code) {
        case 1:
            return self.greenGrayColor;
            break;
        case 2:
            return self.greenGrayColor;
            break;
        case 3:
            return self.greenGrayColor;
            break;
        case 4:
            return self.greenGrayColor;
            break;
        case 5:
            return self.irishGreenColor;
            break;
        case 6:
            return self.irishGreenColor;
            break;
        case 7:
            return self.signalGrayColor;
            break;
        case 8:
            return self.blackColor;
            break;
        case 9:
        case 12:
        case 15:
        case 18:
        case 19:
        case 20:
            return self.dahliaYellow;
            break;
        default:
            break;
    }
    return nil;
}

-(UIColor*)textSetColorWithCodeDesign:(NSUInteger)code {
    [self prepareColors];

    switch (code) {
        case 1:
            return self.blackColor;
            break;
        case 2:
            return self.blackColor;
            break;
        case 3:
            return self.whiteColor;
            break;
        case 4:
            return self.blackColor;
            break;
        case 5:
            return self.blackColor;
            break;
        case 6:
            return self.whiteColor;
            break;
        case 7:
            return self.blackColor;
            break;
        case 8:
            return self.whiteColor;
            break;
        case 10:
            return self.blackColor;
            break;
        case 11:
            return self.blackColor;
            break;
        default:
            break;
    }
    return nil;
}



@end
