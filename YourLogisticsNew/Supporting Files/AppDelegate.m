//
//  AppDelegate.m
//  YourLogistics
//
//  Created by Aleksandr on 29.04.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import "AppDelegate.h"
#import "APIService.h"
#import <MagicalRecord/MagicalRecord.h>
#import "MessageSaver.h"
#import <UserNotifications/UserNotifications.h>

#import "OrderMessage.h"
#import "FinanceMessage.h"
#import "TextMessage.h"
#import "ChatMessage.h"
#import "ColorPallet.h"
//#import <AFNetworkActivityLogger.h>

@import Firebase;

NSString *const SubscriptionTopic = @"/topics/global";

@interface AppDelegate () <UIApplicationDelegate, UNUserNotificationCenterDelegate , FIRMessagingDelegate>

@property(nonatomic, strong) void (^registrationHandler)
(NSString *registrationToken, NSError *error);
@property(nonatomic, assign) BOOL connectedToGCM;
//@property(nonatomic, strong) NSString* registrationToken;
@property(nonatomic, assign) BOOL subscribedToTopic;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
//#ifdef DEBUG
//    [[AFNetworkActivityLogger sharedLogger] startLogging];
//    [[AFNetworkActivityLogger sharedLogger] setLogLevel: AFLoggerLevelDebug];
//#endif
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"db_is_clean"]) {
        [self cleanAndResetupDB];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"db_is_clean"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    } else {
        [self configureCoreData];
    }
    
    
    [self configureNavBar];
    
    [self configureGCM];
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:[MessageSaver allUnreadMessagesCount]];
    
//    [MessageSaver deleteAllMessages];
    
    NSDictionary *userInfo = [launchOptions valueForKey:@"UIApplicationLaunchOptionsRemoteNotificationKey"];
    NSLog(@"%@", userInfo);
    if (userInfo) {
        UIAlertController *con = [UIAlertController alertControllerWithTitle:@"finish" message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ac = [UIAlertAction actionWithTitle:@"aaaa" style:UIAlertActionStyleDefault handler:nil];
        [con addAction:ac];
        [self.window.rootViewController presentViewController:con animated:YES completion:nil];
    }
    
    self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
    
    if ([APIService sharedAPIClient].isUserAuthorized) {
        NSUserDefaults *UD = [NSUserDefaults standardUserDefaults];
        NSString *clientType = [UD objectForKey:kClientTypeKey];
        NSString *clientCode = [UD objectForKey:kClientCodeKey];
        UIStoryboard *storyboard;
        
        NSUserDefaults *UDGroupe = [[NSUserDefaults alloc] initWithSuiteName:@"group.photoshare.extension"];
        [UDGroupe removeObjectForKey:kClientCodeKey];
        [UDGroupe synchronize];
        
        if ([clientType isEqualToString: @"client"]) {
            storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        } else if ([clientType isEqualToString: @"driver"]) {
            [UDGroupe setObject:clientCode forKey:kClientCodeKey];
            [UDGroupe synchronize];
            
            storyboard = [UIStoryboard storyboardWithName:@"MainDriver" bundle:nil];
        } else {
            storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        }
        
        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"MainNav"];
        self.window.rootViewController = viewController;
        [self.window makeKeyAndVisible];
    } else {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Authorization" bundle:nil];
        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"SplashNav"];
        
        self.window.rootViewController = viewController;
        [self.window makeKeyAndVisible];
    }
    
    return YES;
}



-(void)configureGCM{
    
    [FIRApp configure];
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeNone | UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    } else {
        // iOS 10 or later
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
        // For iOS 10 display notification (sent via APNS)
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
        UNAuthorizationOptions authOptions =
        UNAuthorizationOptionAlert
        | UNAuthorizationOptionSound
        | UNAuthorizationOptionBadge
        | UNAuthorizationOptionNone;
        [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:authOptions completionHandler:^(BOOL granted, NSError * _Nullable error) {
        }];
        
        // For iOS 10 data message (sent via FCM)
        [FIRMessaging messaging].delegate = self;
#endif
    }
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    
    NSString *fcmToken = [FIRMessaging messaging].FCMToken;
    NSLog(@"FCM registration token: %@", fcmToken);
    [[NSUserDefaults standardUserDefaults] setObject:fcmToken forKey:kPushTokenKey];
    [[NSUserDefaults standardUserDefaults] synchronize];

}
 
- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    [FIRMessaging messaging].APNSToken = deviceToken;
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    //register to receive notifications
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"Did Fail to Register for Remote Notifications");
    NSLog(@"%@, %@", error, error.localizedDescription);
    
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    NSLog(@"Message ID: %@", userInfo);
    
}

#pragma mark - Firebase Messaging
// [START refresh_token]
- (void)messaging:(nonnull FIRMessaging *)messaging didRefreshRegistrationToken:(nonnull NSString *)fcmToken {
    // Note that this callback will be fired everytime a new token is generated, including the first
    // time. So if you need to retrieve the token as soon as it is available this is where that
    // should be done.
    
    [[NSUserDefaults standardUserDefaults] setObject:fcmToken forKey:kPushTokenKey]; //@"DeviceToken"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSLog(@"FCM registration token: %@", fcmToken);
    
    // TODO: If necessary send token to application server.
}

- (void)messaging:(FIRMessaging *)messaging didReceiveRegistrationToken:(NSString *)fcmToken {
    [[NSUserDefaults standardUserDefaults] setObject:fcmToken forKey:kPushTokenKey]; //@"DeviceToken"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSLog(@"FCM registration token: %@", fcmToken);
}

-(void)configureCoreData{
    [MagicalRecord setupCoreDataStack];
    [MagicalRecord setupAutoMigratingCoreDataStack];
    
//    [MessageSaver preloadFakeMessages];
//    [MessageSaver deleteAllMessages];
}

- (void)cleanAndResetupDB
{
    [MagicalRecord cleanUp];
    
    NSString *dbStore = [MagicalRecord defaultStoreName];
    
    NSURL *storeURL = [NSPersistentStore MR_urlForStoreName:dbStore];
    NSURL *walURL = [[storeURL URLByDeletingPathExtension] URLByAppendingPathExtension:@"sqlite-wal"];
    NSURL *shmURL = [[storeURL URLByDeletingPathExtension] URLByAppendingPathExtension:@"sqlite-shm"];
    
    NSError *error = nil;
    BOOL result = YES;
    
    for (NSURL *url in @[storeURL, walURL, shmURL]) {
        if ([[NSFileManager defaultManager] fileExistsAtPath:url.path]) {
            result = [[NSFileManager defaultManager] removeItemAtURL:url error:&error];
        }
    }
    
    if (result) {
        [self configureCoreData];
    } else {
        NSLog(@"An error has occurred while deleting %@ error %@", dbStore, error);
    }
}

-(void)configureNavBar{
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:74/255.f green:74/255.f blue:74/255.f alpha:1.f],
                                                           NSFontAttributeName:[UIFont fontWithName:@"Museo Sans Cyrl" size:16]}];
//    [[UINavigationBar appearance] setBarTintColor:[UIColor greenColor]];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    [[UITabBar appearance] setTintColor:[ColorPallet new].baseLightBlue]; //[UIColor colorWithRed:140/255.f green:213/255.f blue:11/255.f alpha:1.f]];
}
    
- (void)subscribeToTopic {
    // If the app has a registration token and is connected to GCM, proceed to subscribe to the
    // topic
    if (_registrationToken && _connectedToGCM) {
//        [[GCMPubSub sharedInstance] subscribeWithToken:_registrationToken
//                                                 topic:SubscriptionTopic
//                                               options:nil
//                                               handler:^(NSError *error) {
//                                                   if (error) {
//                                                       // Treat the "already subscribed" error more gently
//                                                       if (error.code == 3001) {
//                                                           NSLog(@"Already subscribed to %@",
//                                                                 SubscriptionTopic);
//                                                       } else {
//                                                           NSLog(@"Subscription failed: %@",
//                                                                 error.localizedDescription);
//                                                       }
//                                                   } else {
//                                                       self.subscribedToTopic = true;
//                                                       NSLog(@"Subscribed to %@", SubscriptionTopic);
//                                                   }
//                                               }];
//    }
    }
}
    
#pragma mark - Firebase Messaging
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    // TODO: Handle data of notification
    
    [[NSNotificationCenter defaultCenter] postNotificationName:_messageKey
                                                         object:nil
                                                       userInfo:userInfo];
    NSData *data = [userInfo[@"message"] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSDictionary *messageDic = [NSDictionary dictionary];
    
    @try {
        messageDic = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    } @catch (NSException *exception) {
        NSLog(@"%@", exception.reason);
    } @finally {
        NSLog(@"Finally condition");
    }
    
     NSLog(@"default message %@", messageDic);
//     [MessageSaver saveMessage:messageDic];
//     [[UIApplication sharedApplication] setApplicationIconBadgeNumber:[MessageSaver allUnreadMessagesCount]];
    
    
    // Print full message.
    NSLog(@"%@", userInfo);
    
    if (application.applicationState == UIApplicationStateActive) {
//        UIAlertController *con = [UIAlertController alertControllerWithTitle:@"Новое сообщение" message:nil preferredStyle:UIAlertControllerStyleAlert];
//
//        UIAlertAction *ac = [UIAlertAction actionWithTitle:@"Ок" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//
//        }];
//
//        [con addAction:ac];
//        [self.window.rootViewController presentViewController:con animated:YES completion:nil];
        
//        MessageType type = [[messageDic objectForKey:@"type"] integerValue];
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"IncrementBadgeNotification" object:@(type)];
//
    }else{
        NSInteger index = 0;
        MessageType type = [[messageDic objectForKey:@"type"] integerValue];
        switch (type) {
            case MessageTypeText:
                index = 1;
                break;
            case MessageTypeFinance:
                index = 2;
                break;
            case MessageTypeOrder:
                index = 2;
                break;
            default:
                NSLog(@"No message category with such id: %ld", type);
                break;
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"EnterScreenNotification" object:@(type)];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"SelectTabIndexNotification" object:@(type)];
    }

    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshDataNotification" object:nil];
    
    completionHandler(UIBackgroundFetchResultNewData);
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler
{
    NSLog( @"Handle push from foreground" );
    // custom code to handle push while app is in the foreground
    
    [[NSNotificationCenter defaultCenter] postNotificationName:_messageKey
                                                        object:nil
                                                      userInfo:notification.request.content.userInfo];
    
    NSDictionary *messageDic = [NSDictionary dictionary];
    NSError* err;
    @try {
        if ([notification.request.identifier isEqualToString:@"UYLLocalNotification"]) {
            messageDic = notification.request.content.userInfo;
        }else{
            NSData *data = [notification.request.content.userInfo[@"message"] dataUsingEncoding:NSUTF8StringEncoding];
            messageDic = [NSJSONSerialization JSONObjectWithData:data options:0 error:&err];
        }
    } @catch (NSException *exception) {
        NSLog(@"%@", exception.reason);
    } @finally {
        NSLog(@"Finally condition");
    }
    
    
    NSLog(@"default message %@, err = %@", messageDic, err);
//    [MessageSaver saveMessage:messageDic];
//    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:[MessageSaver allUnreadMessagesCount]];
//    UIAlertController *con = [UIAlertController alertControllerWithTitle:@"Новое сообщение" message:nil preferredStyle:UIAlertControllerStyleAlert];
//    UIAlertAction *ac = [UIAlertAction actionWithTitle:@"Ок" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//
//    }];
//    [con addAction:ac];
//    [self.window.rootViewController presentViewController:con animated:YES completion:nil];
    
    MessageType type = [[messageDic objectForKey:@"type"] integerValue];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"IncrementBadgeNotification" object:@(type)];
    
    NSLog(@"%@", notification.request.content.userInfo);
    
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshDataNotification" object:nil];
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void (^)(void))completionHandler {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:_messageKey
                                                        object:nil
                                                      userInfo:response.notification.request.content.userInfo];
    
    NSDictionary *messageDic = [NSDictionary dictionary];
     NSError* err;
     @try {
         if ([response.notification.request.identifier isEqualToString:@"UYLLocalNotification"]) {
             messageDic = response.notification.request.content.userInfo;
         }else{
             NSData *data = [response.notification.request.content.userInfo[@"message"] dataUsingEncoding:NSUTF8StringEncoding];
             messageDic = [NSJSONSerialization JSONObjectWithData:data options:0 error:&err];
         }
     } @catch (NSException *exception) {
         NSLog(@"%@", exception.reason);
     } @finally {
         NSLog(@"Finally condition");
     }


    NSLog(@"default message %@", messageDic);
//    [MessageSaver saveMessage:messageDic];
//    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:[MessageSaver allUnreadMessagesCount]];

    NSLog( @"Handle push from background or closed" );
    // if you set a member variable in didReceiveRemoteNotification, you  will know if this is from closed or background
    NSLog(@"%@", response.notification.request.content.userInfo);
    
    UIApplication* application = [UIApplication sharedApplication];

    if (application.applicationState == UIApplicationStateActive) {
//        UIAlertController *con = [UIAlertController alertControllerWithTitle:@"Новое сообщение" message:nil preferredStyle:UIAlertControllerStyleAlert];
//        UIAlertAction *ac = [UIAlertAction actionWithTitle:@"Ок" style:UIAlertActionStyleDefault handler:nil];
//        [con addAction:ac];
//        [self.window.rootViewController presentViewController:con animated:YES completion:nil];
//
        MessageType type = [[messageDic objectForKey:@"type"] integerValue];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"IncrementBadgeNotification" object:@(type)];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshDataNotification" object:nil];
    }else{
        NSInteger index = 0;
        MessageType type = [[messageDic objectForKey:@"type"] integerValue];
        switch (type) {
            case MessageTypeText:
                index = 1;
                break;
            case MessageTypeFinance:
                index = 2;
                break;
            case MessageTypeOrder:
                index = 2;
                break;
            default:
                NSLog(@"No message category with such id: %ld", type);
                break;
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"EnterScreenNotification" object:@(type)];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"SelectTabIndexNotification" object:@(type)];
        
    }
    
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    // Connect to the GCM server to receive non-APNS notifications
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    [self removeMessageFor3Days];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"InvalidateTimerNotification" object:nil];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
//    NSArray *orders = [OrderMessage MR_findAllSortedBy:@"messageId"
//                                             ascending:NO
//                                         withPredicate:nil];
//    
//    NSArray *finance = [FinanceMessage MR_findAllSortedBy:@"messageId"
//                                             ascending:NO
//                                         withPredicate:nil];
//    NSArray *textMess = [TextMessage MR_findAllSortedBy:@"messageId"
//                                                ascending:NO
//                                            withPredicate:nil];
//    
//    NSNumber* maxMess1 = MAX([[orders firstObject] messageId], [[finance firstObject] messageId]);
//    NSNumber* maxMess = MAX(maxMess1, [[textMess firstObject] messageId]);
//
//    
//    [[APIService sharedAPIClient] verifyRecievedPushWithLastMessageId:[maxMess integerValue]];
    
    [self removeMessageFor3Days];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshDataNotification" object:nil];

    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:_messageKey
                                                        object:nil
                                                      userInfo:notification.userInfo];
    NSDictionary *messageDic;
    NSError* err;
    
    messageDic = notification.userInfo;
    
    NSLog(@"default message %@, err = %@", getJSON(messageDic), err);
//    [MessageSaver saveMessage:messageDic];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:[MessageSaver allUnreadMessagesCount]];
    UIAlertController *con = [UIAlertController alertControllerWithTitle:@"Новое сообщение" message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ac = [UIAlertAction actionWithTitle:@"Ок" style:UIAlertActionStyleDefault handler:nil];
    [con addAction:ac];
    [self.window.rootViewController presentViewController:con animated:YES completion:nil];
    
    NSLog(@"%@", getJSON(notification.userInfo));
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshDataNotification" object:nil];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
    [self removeMessageFor3Days];
    
}

-(void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler{
    //DO SOMETHING

    completionHandler(UIBackgroundFetchResultNewData);
}

-(void)removeMessageFor3Days{
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"messageId <= %f", [[[NSDate date] dateByAddingTimeInterval:-3*24*60*60] timeIntervalSince1970]*1000];
    
    [TextMessage MR_deleteAllMatchingPredicate:predicate];
    [FinanceMessage MR_deleteAllMatchingPredicate:predicate];
    [OrderMessage MR_deleteAllMatchingPredicate:predicate];
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
    
}

@end
