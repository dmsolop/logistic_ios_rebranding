//
//  SharedManager.h
//  YourLogisticsNew
//
//  Created by Dima on 3/24/19.
//  Copyright © 2019 iCenter. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SharedManager : NSObject

@property (nonatomic, assign) BOOL isBossPhoneUsing;

+(SharedManager *)sharedManager;

@end

NS_ASSUME_NONNULL_END
