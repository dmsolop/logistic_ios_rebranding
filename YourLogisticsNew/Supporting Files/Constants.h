//
//  Constants.h
//  YourLogisticsNew
//
//  Created by Mac on 01.10.2018.
//  Copyright © 2018 iCenter. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Constants : NSObject

@property (nonatomic, assign) double spacingInConteiner;
@property (nonatomic, strong) NSString *bossNumber1;
@property (nonatomic, strong) NSString *bossNumber2;
@property (nonatomic, strong) NSString *bossNumber3;
@property (nonatomic, strong) NSString *bossNumber4;
@property (nonatomic, strong) NSArray *headerNamesType3;
@property (nonatomic, strong) NSArray *headerNamesType4;
@property (nonatomic, strong) NSArray *headerNamesType5;
@property (nonatomic, strong) NSArray *headerNamesType6;
@property (nonatomic, strong) NSArray *headerNamesType7;
@property (nonatomic, strong) NSArray *headerNamesType9;
@property (nonatomic, strong) NSString *noDataForDisplay;
@property (nonatomic, strong) NSString *titleReports;
@property (nonatomic, strong) NSString *titleNews;
@property (nonatomic) NSInteger initialTime;


@end
