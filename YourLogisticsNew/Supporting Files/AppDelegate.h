//
//  AppDelegate.h
//  YourLogistics
//
//  Created by Aleksandr on 29.04.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property(nonatomic, readonly, strong) NSString *registrationKey;
@property(nonatomic, strong) NSString* registrationToken;

@property(nonatomic, readonly, strong) NSString *messageKey;
@property(nonatomic, readonly, strong) NSString *gcmSenderID;
@property(nonatomic, readonly, strong) NSDictionary *registrationOptions;

- (void)subscribeToTopic;

@end

