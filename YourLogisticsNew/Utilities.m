//
//  Utilities.m
//  YourLogistics
//
//  Created by Aleksandr on 29.04.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import "Utilities.h"
#import <CommonCrypto/CommonDigest.h>
#import <UIKit/UIKit.h>

@implementation Utilities

+(NSString *)getMD5FromString:(NSString *)key{
    const char * pointer = [key UTF8String];
    unsigned char md5Buffer[CC_MD5_DIGEST_LENGTH];
    
    CC_MD5(pointer, (CC_LONG)strlen(pointer), md5Buffer);
    
    NSMutableString *string = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for (int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [string appendFormat:@"%02x",md5Buffer[i]];
    
    return [string copy];
}

+(NSAttributedString *)greenMarkDateFromDate:(NSDate *)date{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"dd.MM.YYYY  |  HH:mm";
    NSMutableAttributedString *dateString = [[NSMutableAttributedString alloc] initWithString:[formatter stringFromDate:date]];
    NSRange endOfGreenTextColorRange = [dateString.string rangeOfString:@"|"];
    UIColor *dateColor = [UIColor colorWithRed:99.f/255.f green:149.f/255.f blue:33.f/255.f alpha:1.f];
    [dateString addAttribute:NSForegroundColorAttributeName
                       value:dateColor
                       range:NSMakeRange(0, endOfGreenTextColorRange.location + 1)];
    return dateString;
}

+(void)addShadowToView:(UIView *)view{
    view.layer.shadowColor = [UIColor blackColor].CGColor;
    view.layer.shadowOpacity = 0.3;
    view.layer.shadowRadius = 3;
    view.layer.shadowOffset = CGSizeMake(1.5f, 1.5f);
}

+(UIAlertController *)downloadingErrorAlert{
    UIAlertController *errorAlert = [UIAlertController alertControllerWithTitle:@"Ошибка при загрузке данных!" message:@"Попробуйте познее" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK"
                                                       style:UIAlertActionStyleDefault
                                                     handler:nil];
    [errorAlert addAction:okAction];
    return errorAlert;
}

@end
