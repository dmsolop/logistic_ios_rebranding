//
//  ClientDebitClass.h
//  YourLogisticsNew
//
//  Created by Dima on 5/13/19.
//  Copyright © 2019 iCenter. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OrderClass.h"

NS_ASSUME_NONNULL_BEGIN

@interface ClientDebitClass : NSObject

@property (nonatomic) NSInteger totalUSD;
@property (nonatomic) NSInteger totalUAH;
@property (nonatomic) NSInteger totalEUR;
@property (nonatomic) NSInteger totalRUB;
@property (nonatomic) NSInteger creditOfTrust;
@property (nullable, nonatomic, copy) NSString *clientCode;
@property (nullable, nonatomic, copy) NSString *dateToWork;
@property (nullable, nonatomic, copy) NSString *cause;
@property (nullable, nonatomic, copy) NSString *action;
@property (nullable, nonatomic, copy) NSString *debtStatus;
@property (nullable, nonatomic, copy) NSArray<OrderClass *> *orders;

@end

NS_ASSUME_NONNULL_END
