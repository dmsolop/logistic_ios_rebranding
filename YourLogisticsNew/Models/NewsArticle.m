//
//  NewsArticle.m
//  YourLogistics
//
//  Created by Aleksandr on 19.05.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import "NewsArticle.h"

@implementation NewsArticle

- (instancetype)initWithType:(ArticleType)type
{
    self = [super init];
    if (self) {
        self.type = type;
    }
    return self;
}

@end
