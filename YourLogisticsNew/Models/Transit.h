//
//  Transit.h
//  YourLogisticsNew
//
//  Created by Roma Dudka on 20.05.2018.
//  Copyright © 2018 iCenter. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Transit : NSObject

@property (strong, nonatomic) NSString *date;
@property (strong, nonatomic) NSString *guid;
@property (strong, nonatomic) NSString *name;

@end
