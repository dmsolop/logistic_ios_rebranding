//
//  Report.m
//  YourLogisticsNew
//
//  Created by Dima on 3/15/19.
//  Copyright © 2019 iCenter. All rights reserved.
//

#import "ReportClass.h"

@implementation ReportClass

- (instancetype)initWithType:(ReportType)type
{
    self = [super init];
    if (self) {
        _type = type;
    }
    return self;
}

@end
