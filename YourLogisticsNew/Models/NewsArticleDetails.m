//
//  NewsArticleDetails.m
//  YourLogistics
//
//  Created by Aleksandr on 19.05.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import "NewsArticleDetails.h"

@implementation NewsArticleDetails

- (instancetype)initWithArticle:(NewsArticle *)article
{
    self = [super init];
    if (self) {
        self.ID = article.ID;
        self.title = article.title;
        self.imageLink = article.imageLink;
        self.date = article.date;
        self.type = article.type;
    }
    return self;
}

@end
