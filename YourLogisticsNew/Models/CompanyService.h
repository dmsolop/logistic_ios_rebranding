//
//  CompanyService.h
//  YourLogistics
//
//  Created by Aleksandr on 04.05.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CompanyService : NSObject

@property (nonatomic) NSUInteger ID;

@property (nonatomic, strong) NSString *title;

@property (nonatomic, strong) NSString *imageLink;

@property (nonatomic, strong) NSString *content;

@end
