//
//  Article.m
//  YourLogistics
//
//  Created by Aleksandr on 04.05.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import "Article.h"

@implementation Article

- (instancetype)initWithType:(ArticleType)type
{
    self = [super init];
    if (self) {
        _type = type;
    }
    return self;
}

@end
