//
//  ChatMessage.h
//  YourLogistics
//
//  Created by Aleksandr Ponomarenko on 26.05.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FinanceTableItem.h"

typedef NS_ENUM(NSInteger, MessageType) {
    MessageTypeText = 1,
    MessageTypeFinance = 2,
    MessageTypeOrder = 3,
    MessageTypeNews = 4,
    MessageTypeReport = 5
};



@interface ChatMessage : NSObject

@property (nullable, nonatomic, retain) NSNumber *isNew;

@property (nonatomic, strong) NSDate * _Nullable date;

@property (nonatomic, strong) NSDate * _Nullable clearanceDate;

@property (nonatomic) NSUInteger messageId;

@property (nonatomic, strong) NSString * _Nullable text;

@property (nonatomic, strong) NSString * _Nullable port;

@property (nonatomic, strong) NSString * _Nullable orderNumber;

@property (nonatomic, strong) NSString * _Nullable cargoType;

@property (nonatomic, strong) NSString * _Nullable currency;

@property (nullable, nonatomic, retain) NSNumber * price;

@property (nonatomic) NSUInteger typeDesign;

@property (nonatomic, strong) NSString * _Nullable weight;

@property (nonatomic, assign) NSInteger operation;

@property (nullable, nonatomic, retain) NSNumber *balance;

@property (nullable, nonatomic, retain) NSNumber *summ;

@property (nullable, nonatomic, retain) NSString *clientCode;

@property (nonatomic) MessageType type;

@property (nonatomic, strong) NSArray<FinanceTableItem *> *rowsOfFinanceTable;


@end

