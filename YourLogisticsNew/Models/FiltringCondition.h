//
//  FiltringCondition.h
//  YourLogistics
//
//  Created by Aleksandr on 01.06.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, FilteringType) {
    FilteringTypeOrderNumber,
    FilteringTypeCategory,
    FilteringTypeStatus
};

@interface FiltringCondition : NSObject

@property (nonatomic, readonly) FilteringType type;

@property (nonatomic, strong, readonly) id filterBy;

- (instancetype)initWithType:(FilteringType)type filterBy:(id)filter;

- (NSString *)conditionValue;

@end
