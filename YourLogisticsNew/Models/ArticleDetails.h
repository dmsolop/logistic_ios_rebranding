//
//  ArticleDetails.h
//  YourLogistics
//
//  Created by Aleksandr on 06.05.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import "Article.h"

@interface ArticleDetails : Article

@property (nonatomic, strong) NSString *content;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSURL *link;

-(instancetype)initWithArticle:(Article *)article;

@end
