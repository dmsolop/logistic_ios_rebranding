//
//  FiltringCondition.m
//  YourLogistics
//
//  Created by Aleksandr on 01.06.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import "FiltringCondition.h"

@interface FiltringCondition ()

@property (nonatomic) FilteringType type;

@property (nonatomic, strong) id filterBy;

@end

@implementation FiltringCondition

- (instancetype)initWithType:(FilteringType)type filterBy:(id)filter
{
    self = [super init];
    if (self) {
        _type = type;
        _filterBy = filter;
    }
    return self;
}

-(NSString *)conditionValue{
    NSString *stringCondition = @"";
    if (self.type == FilteringTypeOrderNumber) {
        stringCondition = [NSString stringWithFormat:@"orderNumber =[c] '%@'", self.filterBy];
    } else if (self.type == FilteringTypeStatus) {
        stringCondition = [NSString stringWithFormat:@"status =[c] '%@'", self.filterBy];
    } else if (self.type == FilteringTypeCategory){
        stringCondition = [NSString stringWithFormat:@"category =[c] '%@'", self.filterBy];
    }
    NSLog(@"%@", stringCondition);
    return stringCondition;
}

@end
