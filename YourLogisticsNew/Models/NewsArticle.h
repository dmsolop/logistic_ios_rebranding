//
//  NewsArticle.h
//  YourLogistics
//
//  Created by Aleksandr on 19.05.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import "Article.h"

@interface NewsArticle : Article

@property (nonatomic, strong) NSDate *date;

@property (nonatomic, strong) NSString *imageLink;

-(instancetype)initWithType:(ArticleType)type;

@end
