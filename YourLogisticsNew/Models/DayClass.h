//
//  DayClass.h
//  YourLogisticsNew
//
//  Created by Dima on 5/8/19.
//  Copyright © 2019 iCenter. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface DayClass : NSObject

@property (nullable, nonatomic, copy) NSString *date;
@property (nullable, nonatomic, copy) NSString *client_code;
@property (nonatomic) BOOL effectiveness;

@end

NS_ASSUME_NONNULL_END
