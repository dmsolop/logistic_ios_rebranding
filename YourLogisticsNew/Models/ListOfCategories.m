//
//  ListOfCategories.m
//  YourLogistics
//
//  Created by Aleksandr on 25.05.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import "ListOfCategories.h"

@implementation ListOfCategories

- (instancetype)initWithCategories:(NSArray *)categories numberOfNewMessages:(NSUInteger)newMessages
{
    self = [super init];
    if (self) {
        _categories = categories;
        _numberOfAllNewMessages = newMessages;
    }
    return self;
}

@end
