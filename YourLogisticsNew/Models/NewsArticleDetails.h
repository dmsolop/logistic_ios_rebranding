//
//  NewsArticleDetails.h
//  YourLogistics
//
//  Created by Aleksandr on 19.05.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import "NewsArticle.h"

@interface NewsArticleDetails : NewsArticle

@property (nonatomic, strong) NSString *content;

@property (nonatomic, strong) NSString *largeImageLink;

-(instancetype)initWithArticle:(NewsArticle *)article;

@end
