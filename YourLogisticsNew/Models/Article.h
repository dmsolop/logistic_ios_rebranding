//
//  Article.h
//  YourLogistics
//
//  Created by Aleksandr on 04.05.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, ArticleType) {
    ArticleTypeLogistics,
    ArticleTypeCompany,
    ArticleTypeCompanyService
};

@interface Article : NSObject

@property (nonatomic) NSUInteger ID;

@property (nonatomic) ArticleType type;

@property (nonatomic, strong) NSString *title;

@property (nonatomic, strong) NSString *imageLink;

-(instancetype)initWithType:(ArticleType)type;

@end
