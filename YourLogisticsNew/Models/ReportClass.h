//
//  Report.h
//  YourLogisticsNew
//
//  Created by Dima on 3/15/19.
//  Copyright © 2019 iCenter. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ManagerClass.h"
#import "OrderClass.h"
#import "ClientDebitClass.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, ReportType) {
    ReportTypeSalesDayLogistic = 1,
    ReportTypeSalesMonthLogistic,
    ReportTypeSalesTO,
    ReportTypeSalesOPT_NI,
    ReportTypeDepartment,
    ReportTypeDaylyTransit,
    ReportTypeAccumTransit,
    ReportTypeMeeting,
    ReportTypeCalculated,
    ReportTypeDebit
};

@interface ReportClass : NSObject

@property (nonatomic) bool isNew;
@property (nonatomic) NSInteger reportId;
@property (nonatomic) ReportType type;
@property (nullable, nonatomic, copy) NSDate *date;
@property (nullable, nonatomic, copy) NSString *reportName;
@property (nullable, nonatomic, copy) NSArray<ManagerClass *> *managers;
@property (nullable, nonatomic, copy) NSArray<OrderClass *> *orders;
@property (nullable, nonatomic, copy) NSArray<ClientDebitClass *> *clients;


- (instancetype)initWithType:(ReportType)type;

@end

NS_ASSUME_NONNULL_END
