//
//  ListOfCategories.h
//  YourLogistics
//
//  Created by Aleksandr on 25.05.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ListOfCategories : NSObject

@property (nonatomic, strong) NSArray *categories;

@property (nonatomic) NSUInteger numberOfAllNewMessages;

- (instancetype)initWithCategories:(NSArray *)categories numberOfNewMessages:(NSUInteger)newMessages;

@end
