//
//  ArticleDetails.m
//  YourLogistics
//
//  Created by Aleksandr on 06.05.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import "ArticleDetails.h"

@implementation ArticleDetails

- (instancetype)initWithArticle:(Article *)article
{
    self = [super init];
    if (self) {
        self.ID = article.ID;
        self.title = article.title;
        self.type = article.type;
        self.imageLink = article.imageLink;

    }
    return self;
}

@end
