//
//  MessageSaver.m
//  YourLogistics
//
//  Created by Aleksandr on 24.05.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import "MessageSaver.h"
#import <MagicalRecord/MagicalRecord.h>
#import <Realm/Realm.h>
#import "TextMessage.h"
#import "FinanceMessage.h"
#import "OrderMessage.h"
#import "MessageCategory.h"
#import "ListOfCategories.h"
#import "ChatMessage.h"
#import "FiltringCondition.h"
#import "Utilities.h"
#import "NewsRead.h"
#import "Report+CoreDataClass.h"
#import "Manager+CoreDataClass.h"
#import "Order+CoreDataClass.h"
#import "Day+CoreDataClass.h"
#import "FinanceTableItem.h"
#import "SharedManager.h"
#import "ReportRLM.h"
#import "ManagerRLM.h"
#import "DayRLM.h"
#import "OrderRLM.h"
#import "ClientDebitRLM.h"
#import "Constants.h"

@implementation MessageSaver

+(NSArray *)allChatMessages{
    
    NSMutableArray *messages = [NSMutableArray array];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"messageId == 1544289043"];
    
    NSArray *textMessages = [TextMessage MR_findAllWithPredicate:predicate];
    [messages addObjectsFromArray:[self chatMessagesFromTextMessages:textMessages isNew:YES]];
    
    NSArray *financeMessages = [FinanceMessage MR_findAll];
//    [messages addObjectsFromArray:[self chatMessagesFromFinanceMessages:financeMessages]];
    [messages addObjectsFromArray:[self chatMessagesFromFinanceMessagesWithoutRead:financeMessages]];
    
    NSArray *orderMessages = [OrderMessage MR_findAll];
    [messages addObjectsFromArray:[self chatMessagesFromOrderMessagesWithoutRead:orderMessages]];
//    [messages addObjectsFromArray:[self chatMessagesFromOrderMessages:orderMessages]];

    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"messageId"
                                                                   ascending:NO];
    NSArray *sortedAllMessages = [messages sortedArrayUsingDescriptors:@[sortDescriptor]];
    return sortedAllMessages;
}

+ (NSArray *)financeChatMessagesWithType:(FinanceMessageType)type isNew:(BOOL)isNew {
    NSArray *messages;
    if (type != FinanceMessageTypeUndefined) {
        NSPredicate *typePredicate = [NSPredicate predicateWithFormat:@"operation = %@, messageId >= %f", @(type), [[[NSDate date] dateByAddingTimeInterval:-3*24*60*60] timeIntervalSince1970]*1000];
        messages = [FinanceMessage MR_findAllSortedBy:@"messageId"
                                            ascending:NO
                                        withPredicate:typePredicate];
    } else {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"messageId >= %f", [[[NSDate date] dateByAddingTimeInterval:-3*24*60*60] timeIntervalSince1970]*1000];

        messages = [FinanceMessage MR_findAllSortedBy:@"messageId" ascending:NO withPredicate:predicate];
    }
    return [self chatMessagesFromFinanceMessages:messages isNew:isNew];
}

+(NSArray *)orderChatMessages{
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"messageId >= %f", [[[NSDate date] dateByAddingTimeInterval:-3*24*60*60] timeIntervalSince1970]*1000];
    NSArray *messages = [OrderMessage MR_findAllSortedBy:@"messageId" ascending:NO withPredicate:nil];
    return [self chatMessagesFromOrderMessages:messages];
}

+(NSArray *)textChatMessagesIsNew:(BOOL)isNew {
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"messageId >= %f", [[[NSDate date] dateByAddingTimeInterval:-3*24*60*60] timeIntervalSince1970]*1000];
    NSArray *messages = [TextMessage MR_findAllSortedBy:@"messageId" ascending:NO withPredicate:nil];
    return [self chatMessagesFromTextMessages:messages isNew:isNew];
}

+(NSArray *)chatMessagesFromTextMessages:(NSArray *)messages isNew:(BOOL)isNew {
    NSMutableArray *convertedMessages = [NSMutableArray array];
    for (TextMessage *textMessage in messages) {
        ChatMessage *chatMessage = [[ChatMessage alloc] init];

        chatMessage.clearanceDate = [NSDate dateWithTimeIntervalSince1970:[textMessage.clearanceDate integerValue]/1000.0];
        chatMessage.date = [NSDate dateWithTimeIntervalSince1970:[textMessage.messageId integerValue]/1000.0];
        chatMessage.text = textMessage.messageDesign;
        chatMessage.messageId = [textMessage.messageId integerValue];
        chatMessage.cargoType = textMessage.cargoType;
        chatMessage.currency = textMessage.currency;
        chatMessage.orderNumber = textMessage.orderNumber;
        chatMessage.price = textMessage.price;
        chatMessage.typeDesign = [textMessage.typeDesign integerValue];
        chatMessage.weight = textMessage.weight;
        chatMessage.type = MessageTypeText;
        chatMessage.isNew = textMessage.isNew;
//        chatMessage.clientCode = textMessage.clientCode;
        [convertedMessages addObject:chatMessage];
        [MagicalRecord saveWithBlock:^(NSManagedObjectContext * _Nonnull localContext) {
            TextMessage *local = [textMessage MR_inContext:localContext];
            if (isNew == NO){
                local.isNew = @(NO);
            }
        } completion:^(BOOL contextDidSave, NSError * _Nullable error) {
            [[UIApplication sharedApplication] setApplicationIconBadgeNumber:[MessageSaver allUnreadMessagesCount]];
        }];
    }
    return [convertedMessages copy];
}

+ (NSArray *)chatMessagesFromFinanceMessages:(NSArray *)messages isNew:(BOOL)isNew {
    NSMutableArray *convertedMessages = [NSMutableArray array];
    
    for (FinanceMessage *financeMessage in messages) {
        ChatMessage *chatMessage = [[ChatMessage alloc] init];
        
        chatMessage.clearanceDate = [NSDate dateWithTimeIntervalSince1970:[financeMessage.clearanceDate integerValue]/1000.0];
        chatMessage.date          = [NSDate dateWithTimeIntervalSince1970:[financeMessage.messageId integerValue]/1000];
        chatMessage.text          = financeMessage.messageContent;
        chatMessage.messageId     = [financeMessage.messageId integerValue];
        chatMessage.type          = MessageTypeFinance;
        chatMessage.balance       = financeMessage.currentBalance;
        chatMessage.summ           = financeMessage.sum;
        chatMessage.operation     = [financeMessage.operation integerValue];
        chatMessage.isNew         = financeMessage.isNew;
        chatMessage.orderNumber   = financeMessage.orderNumber;
        chatMessage.clientCode    = financeMessage.clientCode;
        
        chatMessage.rowsOfFinanceTable = financeMessage.rowsOfTable;
        
        [convertedMessages addObject:chatMessage];
    
        [MagicalRecord saveWithBlock:^(NSManagedObjectContext * _Nonnull localContext) {
            FinanceMessage *local = [financeMessage MR_inContext:localContext];
            if (isNew == NO){
                local.isNew = @(NO);
            }
        } completion:^(BOOL contextDidSave, NSError * _Nullable error) {
            [[UIApplication sharedApplication] setApplicationIconBadgeNumber:[MessageSaver allUnreadMessagesCount]];
        }];
    }
    return [convertedMessages copy];
}

+(NSArray *)chatMessagesFromOrderMessages:(NSArray *)messages{
    NSMutableArray *convertedMessages = [NSMutableArray array];
    for (OrderMessage *orderMessage in messages) {
        ChatMessage *chatMessage = [[ChatMessage alloc] init];
        chatMessage.clearanceDate = [NSDate dateWithTimeIntervalSince1970:[orderMessage.clearanceDate integerValue]/1000.0];
        chatMessage.date = [NSDate dateWithTimeIntervalSince1970:[orderMessage.messageId integerValue]/1000];
        chatMessage.text = orderMessage.messageContent;
        chatMessage.messageId = [orderMessage.messageId integerValue];
        chatMessage.type = MessageTypeOrder;
        chatMessage.isNew = orderMessage.isNew;
        chatMessage.port = orderMessage.port;
        [convertedMessages addObject:chatMessage];
        [MagicalRecord saveWithBlock:^(NSManagedObjectContext * _Nonnull localContext) {
            OrderMessage *local = [orderMessage MR_inContext:localContext];
            local.isNew = @(NO);
        } completion:^(BOOL contextDidSave, NSError * _Nullable error) {
            [[UIApplication sharedApplication] setApplicationIconBadgeNumber:[MessageSaver allUnreadMessagesCount]];
        }];
    }
    return [convertedMessages copy];
}

+(NSArray *)chatMessagesFromFinanceMessagesWithoutRead:(NSArray *)messages{
    NSMutableArray *convertedMessages = [NSMutableArray array];
    for (FinanceMessage *financeMessage in messages) {
        ChatMessage *chatMessage = [[ChatMessage alloc] init];
        chatMessage.clearanceDate = [NSDate dateWithTimeIntervalSince1970:[financeMessage.clearanceDate integerValue]/1000.0];
        chatMessage.date = [NSDate dateWithTimeIntervalSince1970:[financeMessage.messageId integerValue]/1000];
        chatMessage.text = financeMessage.messageContent;
        chatMessage.messageId = [financeMessage.messageId integerValue];
        chatMessage.type = MessageTypeFinance;
        chatMessage.balance = financeMessage.currentBalance;
        chatMessage.summ = financeMessage.sum;
        chatMessage.operation = [financeMessage.operation integerValue];
        chatMessage.isNew = financeMessage.isNew;
        chatMessage.clientCode = financeMessage.clientCode;
        [convertedMessages addObject:chatMessage];
//        [MagicalRecord saveWithBlock:^(NSManagedObjectContext * _Nonnull localContext) {
//            FinanceMessage *local = [financeMessage MR_inContext:localContext];
//            local.isNew = @(YES);
//        } completion:^(BOOL contextDidSave, NSError * _Nullable error) {
//            [[UIApplication sharedApplication] setApplicationIconBadgeNumber:[MessageSaver allUnreadMessagesCount]];
//        }];
    }
    return [convertedMessages copy];
}

+(NSArray *)chatMessagesFromOrderMessagesWithoutRead:(NSArray *)messages{
    NSMutableArray *convertedMessages = [NSMutableArray array];
    for (OrderMessage *orderMessage in messages) {
        ChatMessage *chatMessage = [[ChatMessage alloc] init];
        chatMessage.clearanceDate = [NSDate dateWithTimeIntervalSince1970:[orderMessage.clearanceDate integerValue]/1000.0];
        chatMessage.date = [NSDate dateWithTimeIntervalSince1970:[orderMessage.messageId integerValue]/1000];
        chatMessage.text = orderMessage.messageContent;
        chatMessage.messageId = [orderMessage.messageId integerValue];
        chatMessage.type = MessageTypeOrder;
        chatMessage.isNew = orderMessage.isNew;
        chatMessage.port = orderMessage.port;
        [convertedMessages addObject:chatMessage];
//        [MagicalRecord saveWithBlock:^(NSManagedObjectContext * _Nonnull localContext) {
//            OrderMessage *local = [orderMessage MR_inContext:localContext];
//            local.isNew = @(YES);
//        } completion:^(BOOL contextDidSave, NSError * _Nullable error) {
//            [[UIApplication sharedApplication] setApplicationIconBadgeNumber:[MessageSaver allUnreadMessagesCount]];
//        }];
    }
    return [convertedMessages copy];
}

+(NSUInteger)allUnreadMessagesCount{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"messageId >= %f", [[[NSDate date] dateByAddingTimeInterval:-3*24*60*60] timeIntervalSince1970]*1000];
    NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"isNew = YES"];
    NSPredicate* condition = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:predicate, predicate2,nil]];
    
    NSUInteger messageCount = [TextMessage MR_countOfEntitiesWithPredicate:condition];
    messageCount += [FinanceMessage MR_countOfEntitiesWithPredicate:condition];
//        messageCount += [OrderMessage MR_countOfEntitiesWithPredicate:predicate2];
    if (![SharedManager sharedManager].isBossPhoneUsing) {
        messageCount += [NewsRead MR_countOfEntitiesWithPredicate:predicate2];
    } else {
        messageCount += [ReportRLM objectsWithPredicate:predicate2].count;
    }
    return messageCount;
}

+(NSUInteger)UnreadMessageCountWithType:(NSInteger )type {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"messageId >= %f", [[[NSDate date] dateByAddingTimeInterval:-3*24*60*60] timeIntervalSince1970]*1000];
    NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"reportId >= %ld", [[[NSDate date] dateByAddingTimeInterval:-3*24*60*60] timeIntervalSince1970]*1000];
    NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"isNew = YES"];
    NSPredicate* condition = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:predicate, predicate2,nil]];
    NSPredicate* condition2 = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:predicate1, predicate2,nil]];
//    NSPredicate *condition = [NSPredicate predicateWithFormat:@"isNew = YES"];
    NSUInteger messageCount = 0;
    if (type == MessageTypeText) {
        messageCount = [[TextMessage MR_numberOfEntitiesWithPredicate:condition] integerValue];
    }else if (type == MessageTypeOrder){
        messageCount = [[OrderMessage MR_numberOfEntitiesWithPredicate:predicate2] integerValue];
    }else if (type == MessageTypeFinance){
        messageCount = [[FinanceMessage MR_numberOfEntitiesWithPredicate:condition] integerValue];
    }else if (type == MessageTypeNews){
        if (![SharedManager sharedManager].isBossPhoneUsing) {
            messageCount = [NewsRead MR_countOfEntitiesWithPredicate:predicate2];
        } else {
            messageCount = [ReportRLM objectsWithPredicate:predicate2].count;
        }
    }
    return messageCount;
}



+(ListOfCategories *)messageOrderWithType:(NSInteger )typeMessage andOrderNumber:(NSString* )orderNumber {
    
    NSMutableArray *categories = [NSMutableArray array];
    NSUInteger numberOfAllNewMessages = 0;
    NSInteger textMessagesCount = 0;
    
    if (MessageTypeOrder == typeMessage) {
        if ([[OrderMessage MR_numberOfEntities] integerValue] > 0) {
            NSPredicate *newOrderMessageCondition = [NSPredicate predicateWithFormat:@"orderNumber = %@ && isNew = YES", orderNumber];
            NSUInteger newMessages = [[OrderMessage MR_numberOfEntitiesWithPredicate:newOrderMessageCondition] integerValue];
            numberOfAllNewMessages += newMessages;
            MessageCategory *orderCategory = [[MessageCategory alloc] initWithType:MessageCategoryTypeAll
                                                                 numberOfNewMessages:newMessages];
            [categories addObject:orderCategory];
        }
    }
    
    if (categories.count > 0 || textMessagesCount > 0)
        return [[ListOfCategories alloc] initWithCategories:categories.count > 0 ? categories : nil
                                        numberOfNewMessages:numberOfAllNewMessages];
    else
        return nil;
}


+(void)saveTextMessage:(NSDictionary *)infoToParse{

    if ((void)([infoToParse[@"message"] isKindOfClass:[NSString class]]),
        [infoToParse[@"blockdesign"] isKindOfClass:[NSDictionary class]]) {
        NSArray* messageEntity = [TextMessage MR_findByAttribute:@"messageId" withValue:@([infoToParse[@"id"] integerValue])];
        if (messageEntity.count == 0) {
//            NSString *text = [NSString stringWithFormat:@"%@", infoToParse[@"blockdesign"][@"client_code"]];
            [MagicalRecord saveWithBlock:^(NSManagedObjectContext * _Nonnull localContext) {
                TextMessage *messageEntity  = [TextMessage MR_createEntityInContext:localContext];
                messageEntity.messageId     = @([infoToParse[@"id"] integerValue]);
                messageEntity.content       = infoToParse[@"message"];
                messageEntity.isNew         = @YES;
                messageEntity.orderNumber   = infoToParse[@"blockdesign"][@"order_number"];
//                messageEntity.clientCode  = text;
                messageEntity.cargoType     = infoToParse[@"blockdesign"][@"cargo_type"];
                messageEntity.weight        = infoToParse[@"blockdesign"][@"weight"];
                messageEntity.price         = @([infoToParse[@"blockdesign"][@"price"] integerValue]);
                messageEntity.currency      = infoToParse[@"blockdesign"][@"currency"];
                messageEntity.clearanceDate = @([infoToParse[@"blockdesign"][@"clearance_date"] integerValue]);
                messageEntity.typeDesign    = @([infoToParse[@"blockdesign"][@"type_design"] integerValue]);
                messageEntity.messageDesign = infoToParse[@"blockdesign"][@"message_design"];
                
            } completion:^(BOOL contextDidSave, NSError * _Nullable error) {
                [self postReceivedMessageNotification];
            }];
        }
    }
}

+(void)saveReportRLM:(NSDictionary *)infoToParse {
    
    RLMResults<ReportRLM *> *reportsEntity = [ReportRLM objectsWhere:@"reportId = %ld", @([infoToParse[@"id"] integerValue])];
        
    if (reportsEntity.count > 0) { return; }
//    if ((NSInteger)infoToParse[@"id"] < cons.initialTime) { return; }
    
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm transactionWithBlock:^{
        
        ReportRLM *reportEntity = [[ReportRLM alloc] init];
        reportEntity.reportId   = [infoToParse[@"id"] integerValue];
        
        NSArray *data = infoToParse[@"table"];
        if (data.count == 0) { return; }
        NSDictionary *report = data[0];
        NSArray *subsections = report[@"report"];
        
        if (report) {
            @try {
                reportEntity.name       = report[@"report_name"];
                reportEntity.typeReport = [report[@"type_report"] integerValue];
                reportEntity.isNew      = @YES;
            } @catch (NSException *exception) {
                NSLog(@"@%", exception.reason);
            }
            
            if ([report[@"type_report"] integerValue] == 9) {
                NSMutableArray<OrderRLM *> *orderEntities = [[NSMutableArray alloc] init];
                
                if (subsections) {
                    for (NSDictionary *order in subsections) {
                        OrderRLM *orderEntity = [[OrderRLM alloc] init];
                        NSMutableDictionary *changedRows = [NSMutableDictionary dictionary];
                        NSArray *rows = order[@"rows"];
                        @try {
                            orderEntity.name    = order[@"order"];
                            orderEntity.uid     = order[@"id_order"];
                        } @catch (NSException *exception) {
                            NSLog(@"@%", exception.reason);
                            continue;
                        }
                        
                        for (NSDictionary * row in rows) {
                            @try {
                                NSString *criterion = row[@"criterion"];
                                NSString *value = row[@"value"];
                                [changedRows setObject:value forKey:criterion];
                            } @catch (NSException *exception) {
                                NSLog(@"@%", exception.reason);
                                continue;
                            }
                        }
                        
                        @try {
                            orderEntity.product = changedRows[@"product"];
                            orderEntity.price   = changedRows[@"price"];
                        } @catch (NSException *exception) {
                            NSLog(@"@%", exception.reason);
                            continue;
                        }
                        [realm addObject:orderEntity];
                        [orderEntities addObject:orderEntity];
                    }
                    reportEntity.orders = (RLMArray<OrderRLM *><OrderRLM> * _Nonnull)orderEntities;
                }
            } else if ([report[@"type_report"] integerValue] == 10) {
                NSMutableArray<ClientDebitRLM *> *clientEntities = [[NSMutableArray alloc] init];
                
                if (subsections) {
                    for (NSDictionary *client in subsections) {
                        NSMutableArray<OrderRLM *> *orderEntities = [[NSMutableArray alloc] init];
                        ClientDebitRLM *clientEntity = [[ClientDebitRLM alloc] init];
                        NSArray *rows = client[@"rows"];
                        
                        for (NSDictionary * ord in rows) {
                            OrderRLM *orderEntity = [[OrderRLM alloc] init];
                            @try {
                                orderEntity.orderNumber = ord[@"order_number"];
                                orderEntity.invoiceDate = ord[@"invoice_date"];
                                orderEntity.usd         = [ord[@"USD"] integerValue];
                                orderEntity.uah         = [ord[@"UAH"] integerValue];
                                orderEntity.eur         = [ord[@"EUR"] integerValue];
                                orderEntity.rub         = [ord[@"RUB"] integerValue];
                                
                                [realm addObject:orderEntity];
                                [orderEntities addObject:orderEntity];
                                
                            } @catch (NSException *exception) {
                                NSLog(@"@%", exception.reason);
                                continue;
                            }
                        }
                        
                        @try {
                            clientEntity.clientCode     = client[@"client_code"];
                            clientEntity.creditOfTrust  = [client[@"credit_of_trust"] integerValue];
                            clientEntity.dateToWork     = client[@"date_to_work"];
                            clientEntity.cause          = client[@"cause"];
                            clientEntity.action         = client[@"action"];
                            clientEntity.debtStatus     = client[@"debt_status"];
                            clientEntity.totalUSD       = [client[@"total"][@"USD"] integerValue];
                            clientEntity.totalUAH       = [client[@"total"][@"UAH"] integerValue];
                            clientEntity.totalEUR       = [client[@"total"][@"EUR"] integerValue];
                            clientEntity.totalRUB       = [client[@"total"][@"RUB"] integerValue];
                            clientEntity.orders = (RLMArray<OrderRLM *><OrderRLM> * _Nonnull)orderEntities;
                            
                            [realm addObject:clientEntity];
                            [clientEntities addObject:clientEntity];
                            
                        } @catch (NSException *exception) {
                            NSLog(@"@%", exception.reason);
                            continue;
                        }
                        
                    }
                    reportEntity.clients = (RLMArray<ClientDebitRLM *><ClientDebitRLM> * _Nonnull)clientEntities;
                }
                
            } else {
                NSMutableArray<ManagerRLM *> *managerEntities = [[NSMutableArray alloc] init];
                if (subsections) {
                    for (NSDictionary *manager in subsections) {
                        ManagerRLM *managerEntity = [[ManagerRLM alloc] init];
                        NSMutableDictionary *changedRows = [NSMutableDictionary dictionary];
                        NSArray *rows = manager[@"rows"];
                        
                        @try {
                            managerEntity.name  = manager[@"manager"];
                            managerEntity.uid   = manager[@"id_manager"];
                        } @catch (NSException *exception) {
                            NSLog(@"@%", exception.reason);
                            continue;
                        }
                        
                        if ([report[@"type_report"] integerValue] == 8){
                            NSMutableArray *days = [[NSMutableArray alloc] init];
                            for (NSDictionary * row in rows) {
                                DayRLM *dayEntity = [[DayRLM alloc] init];
                                @try {
                                    dayEntity.date          = row[@"day"];
                                    dayEntity.client_code   = row[@"client_code"];
                                    dayEntity.effectiveness = [row[@"effectiveness"] boolValue];
                                } @catch (NSException *exception) {
                                    NSLog(@"@%", exception.reason);
                                    continue;
                                }
                                [days addObject:dayEntity];
                                [realm addObject:dayEntity];
                            }
                            managerEntity.days = (RLMArray<DayRLM *><DayRLM> * _Nonnull)days;
                        } else {
                            for (NSDictionary * row in rows) {
                                @try {
                                    NSString *criterion = row[@"criterion"];
                                    NSString *value     = row[@"value"];
                                    [changedRows setObject:value forKey:criterion];
                                } @catch (NSException *exception) {
                                    NSLog(@"@%", exception.reason);
                                    continue;
                                }
                            }
                        }
                        
                        switch ([report[@"type_report"] integerValue]) {
                            case 1:
                            case 2: {
                                @try {
                                    managerEntity.to    = changedRows[@"to"];
                                    managerEntity.lcl   = changedRows[@"lcl"];
                                    managerEntity.ltl   = changedRows[@"ltl"];
                                    managerEntity.ftl   = changedRows[@"ftl"];
                                    managerEntity.a     = changedRows[@"a"];
                                    managerEntity.ni    = changedRows[@"ni"];
                                    managerEntity.fcl   = changedRows[@"fcl"];
                                    managerEntity.k     = changedRows[@"k"];
                                    managerEntity.fraht = changedRows[@"fraht"];
                                } @catch (NSException *exception) {
                                    NSLog(@"@%", exception.reason);
                                    continue;
                                }
                                break;
                            }
                            case 3: {
                                @try {
                                    managerEntity.toFormalize       = changedRows[@"to"];
                                    managerEntity.logisticFormalize = changedRows[@"logistics"];
                                    managerEntity.marginLogistic    = changedRows[@"margin"];
                                    managerEntity.amountToClear     = changedRows[@"amountto"];
                                } @catch (NSException *exception) {
                                    NSLog(@"@%", exception.reason);
                                    continue;
                                }
                                break;
                            }
                            case 4: {
                                @try {
                                    managerEntity.amountNiFormalize = changedRows[@"ni"];
                                    managerEntity.marginNi          = changedRows[@"marginni"];
                                    managerEntity.amountLclFormalize= changedRows[@"lcl"];
                                    managerEntity.mzLclFormalize    = changedRows[@"m3lcl"];
                                } @catch (NSException *exception) {
                                    NSLog(@"@%", exception.reason);
                                    continue;
                                }
                                break;
                            }
                            case 5: {
                                @try {
                                    managerEntity.amountLclFormalize    = changedRows[@"lcl"];
                                    managerEntity.mzLclFormalize        = changedRows[@"m3lcl"];
                                } @catch (NSException *exception) {
                                    NSLog(@"@%", exception.reason);
                                    continue;
                                }
                                break;
                            }
                            case 6: {
                                @try {
                                    managerEntity.pogran        = changedRows[@"pogran"];
                                    managerEntity.weekNumber    = changedRows[@"week_number"];
                                    managerEntity.dayWeekNumber = changedRows[@"week_day"];
                                    managerEntity.ttAmount      = changedRows[@"amount_tt"];
                                    managerEntity.guarantyAmount= changedRows[@"amount_guarantee"];
                                } @catch (NSException *exception) {
                                    NSLog(@"@%", exception.reason);
                                    continue;
                                }
                                break;
                            }
                            case 7: {
                                @try {
                                    managerEntity.pogran        = changedRows[@"pogran"];
                                    managerEntity.ttAmount      = changedRows[@"amount_tt"];
                                    managerEntity.guarantyAmount= changedRows[@"amount_guarantee"];
                                } @catch (NSException *exception) {
                                    NSLog(@"@%", exception.reason);
                                    continue;
                                }
                                break;
                            }
                            default:
                                break;
                        }
                        [managerEntities addObject:managerEntity];
                        [realm addObject:managerEntity];
                    }
                    reportEntity.managers = (RLMArray<ManagerRLM *><ManagerRLM> * _Nonnull)managerEntities;
                }
            }
            [realm addObject:reportEntity];
        }
        [self postReceivedMessageNotification];
    }];
}

+ (void)saveFinanceMessage:(NSDictionary *)infoToParse {
    NSArray *messageEntity = [FinanceMessage MR_findByAttribute:@"messageId" withValue:@([infoToParse[@"id"] integerValue])];
    
    if (messageEntity.count == 0) {
        [MagicalRecord saveWithBlock:^(NSManagedObjectContext * _Nonnull localContext) {
            FinanceMessage *messageEntity = [FinanceMessage MR_createEntityInContext:localContext];
            
            NSString* text = [NSString stringWithFormat:@"%@", infoToParse[@"table"][@"client_code"]];
            
            if (text != nil) {
                messageEntity.clientCode = text;
            }
            
            messageEntity.messageId = @([infoToParse[@"id"] integerValue]);
       
            NSLog(@"%@", infoToParse[@"table"][@"client_code"]);
            
            NSDictionary *table = infoToParse[@"table"];
            
            if (table) {
                NSMutableArray<FinanceTableItem *> *items = [[NSMutableArray alloc] init];
                [table[@"Rows"] enumerateObjectsUsingBlock:^(NSDictionary * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    [items addObject:[[FinanceTableItem alloc] initWithService:obj[@"Service"] sum:[obj[@"Sum"] doubleValue] currency:obj[@"Currency"] isPaid:[obj[@"Paid"] boolValue]]];
                }];
                
                messageEntity.rowsOfTable = items;
                messageEntity.orderNumber = table[@"order_number"];
                messageEntity.isNew = @YES;
                messageEntity.operation = @([FinanceMessage financeTypeFromCode:table[@"operation"]]);
                messageEntity.sum = @([table[@"sum"] integerValue]);
            } else {
                NSDictionary *data = infoToParse[@"message"];
                messageEntity.operation = @([FinanceMessage financeTypeFromCode:data[@"operation"]]);
                messageEntity.sum = @([data[@"sum"] integerValue]);
                messageEntity.currentBalance = @([data[@"balance"] integerValue]);
                messageEntity.messageContent = data[@"text"];
                messageEntity.isNew = @YES;
                messageEntity.clearanceDate = @([infoToParse[@"clearance_date"] integerValue]);

            }
            
            
        } completion:^(BOOL contextDidSave, NSError * _Nullable error) {
            [self postReceivedMessageNotification];
        }];
    }
}

+(void)saveOrderMessage:(NSDictionary *)infoToParse{
    NSDictionary *messageDic = infoToParse[@"message"];

        NSArray* messageEntity = [OrderMessage MR_findByAttribute:@"messageId" withValue:@([infoToParse[@"id"] integerValue])];
        if (messageEntity.count == 0) {
            [MagicalRecord saveWithBlock:^(NSManagedObjectContext * _Nonnull localContext) {
                OrderMessage *messageEntity = [OrderMessage MR_createEntityInContext:localContext];
                messageEntity.messageId = @([infoToParse[@"id"] integerValue]);
                messageEntity.serviceId = @([OrderMessage serviceTypeFromCode:messageDic[@"service"]]);
                messageEntity.status = messageDic[@"status"];
                if ([messageEntity.serviceId isEqual:@(ServiceTypeCourier)]) {
                    messageEntity.operatorId = messageDic[@"operatorID"];
                }
                messageEntity.messageContent = messageDic[@"text"];
                messageEntity.category = messageDic[@"cargo"];
                messageEntity.orderNumber = messageDic[@"order_number"];
                messageEntity.isNew = @(YES);
                messageEntity.clearanceDate = @([infoToParse[@"clearance_date"] integerValue]);
//                messageEntity.port = infoToParse[@"port"];
            } completion:^(BOOL contextDidSave, NSError * _Nullable error) {
                [self postReceivedMessageNotification];
            }];
        }
}



+(void)postReceivedMessageNotification{
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:[MessageSaver allUnreadMessagesCount]];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationMessageRecieved object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateBadges" object:nil];
}

@end
