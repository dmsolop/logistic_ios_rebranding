//
//  MessageSaver.h
//  YourLogistics
//
//  Created by Aleksandr on 24.05.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FinanceMessage.h"

@class FiltringCondition;
@class ListOfCategories;

@interface MessageSaver : NSObject

+(void)deleteAllReports;

+(ListOfCategories *)messageOrderWithType:(NSInteger )typeMessage andOrderNumber:(NSString* )orderNumber;

+(NSUInteger)allUnreadMessagesCount;

+(NSUInteger)UnreadMessageCountWithType:(NSInteger )type;

+(NSArray *)allChatMessages;

+(NSArray *)financeChatMessagesWithType:(FinanceMessageType)type isNew:(BOOL)isNew;

+(NSArray *)orderChatMessages;

+(NSArray *)textChatMessagesIsNew:(BOOL)isNew;


+(void)saveTextMessage:(NSDictionary *)infoToParse;

+(void)saveFinanceMessage:(NSDictionary *)infoToParse;

+(void)saveOrderMessage:(NSDictionary *)infoToParse;

+(void)saveReportRLM:(NSDictionary *)infoToParse;

@end
