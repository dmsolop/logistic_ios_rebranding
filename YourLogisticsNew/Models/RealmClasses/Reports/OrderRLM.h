//
//  OrderRLM.h
//  YourLogisticsNew
//
//  Created by Dima on 5/11/19.
//  Copyright © 2019 iCenter. All rights reserved.
//

#import <Realm/Realm.h>
#import "RLMObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface OrderRLM : RLMObject

@property NSString *name;
@property NSString *price;
@property NSString *product;
@property NSString *uid;
@property NSString *orderNumber;
@property NSString *invoiceDate;
@property NSInteger uah;
@property NSInteger usd;
@property NSInteger eur;
@property NSInteger rub;

@end

RLM_ARRAY_TYPE(OrderRLM)

NS_ASSUME_NONNULL_END

