//
//  DayRLM.h
//  YourLogisticsNew
//
//  Created by Dima on 5/11/19.
//  Copyright © 2019 iCenter. All rights reserved.
//

#import <Realm/Realm.h>
#import "RLMObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface DayRLM : RLMObject

@property NSString *client_code;
@property NSString *date;
@property BOOL effectiveness;

@end

RLM_ARRAY_TYPE(DayRLM)

NS_ASSUME_NONNULL_END

