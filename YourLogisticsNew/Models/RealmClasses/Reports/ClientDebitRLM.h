//
//  ClientDebitRLM.h
//  YourLogisticsNew
//
//  Created by Dima on 5/13/19.
//  Copyright © 2019 iCenter. All rights reserved.
//

#import <Realm/Realm.h>
#import "RLMObject.h"

@class OrderRLM;
@protocol OrderRLM;

NS_ASSUME_NONNULL_BEGIN

@interface ClientDebitRLM : RLMObject

@property NSString *clientCode;
@property NSString *dateToWork;
@property NSString *cause;
@property NSString *action;
@property NSString *debtStatus;
@property NSInteger creditOfTrust;
@property NSInteger totalUSD;
@property NSInteger totalUAH;
@property NSInteger totalEUR;
@property NSInteger totalRUB;
@property RLMArray<OrderRLM *><OrderRLM> *orders;

@end

NS_ASSUME_NONNULL_END
