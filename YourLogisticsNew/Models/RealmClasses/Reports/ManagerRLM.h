//
//  ManagerRLM.h
//  YourLogisticsNew
//
//  Created by Dima on 5/11/19.
//  Copyright © 2019 iCenter. All rights reserved.
//

#import <Realm/Realm.h>
#import "RLMObject.h"

@class DayRLM;
@protocol DayRLM;

NS_ASSUME_NONNULL_BEGIN

@interface ManagerRLM : RLMObject

@property NSString *a;
@property NSString *amountLclFormalize;
@property NSString *amountNiFormalize;
@property NSString *amountToClear;
@property NSString *dayWeekNumber;
@property NSString *fcl;
@property NSString *fraht;
@property NSString *ftl;
@property NSString *guarantyAmount;
@property NSString *k;
@property NSString *lcl;
@property NSString *logisticFormalize;
@property NSString *ltl;
@property NSString *marginLogistic;
@property NSString *marginNi;
@property NSString *mzLclFormalize;
@property NSString *name;
@property NSString *ni;
@property NSString *pogran;
@property NSString *to;
@property NSString *toFormalize;
@property NSString *ttAmount;
@property NSString *uid;
@property NSString *weekNumber;
@property RLMArray<DayRLM *><DayRLM> *days;

@end

RLM_ARRAY_TYPE(ManagerRLM)

NS_ASSUME_NONNULL_END
