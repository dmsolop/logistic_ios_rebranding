//
//  ReportRLM.h
//  YourLogisticsNew
//
//  Created by Dima on 5/11/19.
//  Copyright © 2019 iCenter. All rights reserved.
//

#import <Realm/Realm.h>
#import "RLMObject.h"

@class ManagerRLM;
@class OrderRLM;
@class ClientDebitRLM;
@protocol ManagerRLM;
@protocol OrderRLM;
@protocol ClientDebitRLM;

NS_ASSUME_NONNULL_BEGIN

@interface ReportRLM : RLMObject

@property  BOOL isNew;
@property NSString *name;
@property NSInteger reportId;
@property NSInteger typeReport;
@property RLMArray<ManagerRLM *><ManagerRLM> *managers;
@property RLMArray<OrderRLM *><OrderRLM> *orders;
@property RLMArray<ClientDebitRLM *><ClientDebitRLM> *clients;

@end

NS_ASSUME_NONNULL_END
