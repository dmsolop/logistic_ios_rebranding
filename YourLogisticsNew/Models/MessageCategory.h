//
//  MessageCategory.h
//  YourLogistics
//
//  Created by Aleksandr on 25.05.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, MessageCategoryType) {
    MessageCategoryTypeSea,
    MessageCategoryTypeAvia,
    MessageCategoryTypeAuto,
    MessageCategoryTypeTO,
    MessageCategoryTypeKuski,
    MessageCategoryTypeCourier,
    MessageCategoryTypeDistribucia,
    MessageCategoryTypeFinance,
    MessageCategoryTypeCustoms,
    MessageCategoryTypeText,
    MessageCategoryTypeAll
};

@interface MessageCategory : NSObject

@property (nonatomic, strong) NSString *name;

@property (nonatomic) MessageCategoryType type;

@property (nonatomic) NSUInteger numberOfNewMessages;

- (instancetype)initWithType:(MessageCategoryType)type numberOfNewMessages:(NSUInteger)newMessages;

-(NSString *)categoryImageName;

@end
