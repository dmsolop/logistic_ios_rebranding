//
//  OrderMessage+CoreDataClass.h
//  
//
//  Created by Bohdan on 12.12.2017.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface OrderMessage : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "OrderMessage+CoreDataProperties.h"
