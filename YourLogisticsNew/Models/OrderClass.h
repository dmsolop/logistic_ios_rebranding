//
//  ManagerClass.h
//  YourLogisticsNew
//
//  Created by Dima on 3/28/19.
//  Copyright © 2019 iCenter. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface OrderClass : NSObject

@property (nullable, nonatomic, copy) NSString *product;
@property (nullable, nonatomic, copy) NSString *price;
@property (nullable, nonatomic, copy) NSString *name;
@property (nullable, nonatomic, copy) NSString *uid;
@property (nullable, nonatomic, copy) NSString *orderNumber;
@property (nullable, nonatomic, copy) NSString *invoiceDate;
@property (nonatomic) NSInteger uah;
@property (nonatomic) NSInteger usd;
@property (nonatomic) NSInteger eur;
@property (nonatomic) NSInteger rub;

@end

NS_ASSUME_NONNULL_END
