//
//  MessageCategory.m
//  YourLogistics
//
//  Created by Aleksandr on 25.05.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import "MessageCategory.h"

@implementation MessageCategory

- (instancetype)initWithType:(MessageCategoryType)type numberOfNewMessages:(NSUInteger)newMessages
{
    self = [super init];
    if (self) {
        _type = type;
        _numberOfNewMessages = newMessages;
        _name = [self nameOfCategoryForType:type];
    }
    return self;
}

-(NSString *)categoryImageName{
    NSString *imageName;
    switch (self.type) {
        case MessageCategoryTypeAuto:
            imageName = @"img_type_auto";
            break;
        case MessageCategoryTypeAvia:
            imageName = @"img_type_avia";
            break;
        case MessageCategoryTypeCustoms:
            imageName = @"img_type_auto";
            break;
        case MessageCategoryTypeFinance:
            imageName = @"img_type_auto";
            break;
        case MessageCategoryTypeSea:
            imageName = @"img_type_sea";
            break;
        case MessageCategoryTypeTO:
            imageName = @"img_type_to";
            break;
        case MessageCategoryTypeKuski:
            imageName = @"img_type_kuski";
            break;
        case MessageCategoryTypeDistribucia:
            imageName = @"img_type_courier";
            break;
        case MessageCategoryTypeCourier:
            imageName = @"img_type_courier";
            break;
        default:
            break;
    }
    return imageName;
}

-(NSString *)nameOfCategoryForType:(MessageCategoryType)type{
    NSString *name = @"";
    switch (type) {
        case MessageCategoryTypeAuto:
            name = @"АВТОПЕРЕВОЗКИ";
            break;
        case MessageCategoryTypeAvia:
            name = @"АВИАПЕРЕВОЗКИ";
            break;
        case MessageCategoryTypeCustoms:
            name = @"ТАМОЖЕННОЕ ОФОРМЛЕНИЕ";
            break;
        case MessageCategoryTypeFinance:
            name = @"ФИНАНСЫ";
            break;
        case MessageCategoryTypeSea:
            name = @"МОРСКИЕ ПЕРЕВОЗКИ";
            break;
        case MessageCategoryTypeTO:
            name = @"Таможенное оформление";
            break;
        case MessageCategoryTypeKuski:
            name = @"Куски";
            break;
        case MessageCategoryTypeDistribucia:
            name = @"Дистрибуция";
            break;
        case MessageCategoryTypeCourier:
            name = @"Перевозка курьером";
            break;
        default:
            break;
    }
    return name;
    
}

-(void)setType:(MessageCategoryType)type{
    _type = type;
    _name = [self nameOfCategoryForType:type];
}

@end
