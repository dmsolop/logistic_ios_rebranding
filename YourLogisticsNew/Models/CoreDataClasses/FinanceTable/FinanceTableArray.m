//
//  FinanceTableArray.m
//  YourLogisticsNew
//
//  Created by VitaliyK on 09.09.2018.
//  Copyright © 2018 iCenter. All rights reserved.
//

#import "FinanceTableArray.h"

@implementation FinanceTableArray

+ (Class)transformedValueClass
{
    return [NSArray class];
}

+ (BOOL)allowsReverseTransformation
{
    return YES;
}

- (id)transformedValue:(id)value
{
    return [NSKeyedArchiver archivedDataWithRootObject:value];
}

- (id)reverseTransformedValue:(id)value
{
    return [NSKeyedUnarchiver unarchiveObjectWithData:value];
}

@end
