//
//  FinanceTableItem.m
//  YourLogisticsNew
//
//  Created by VitaliyK on 09.09.2018.
//  Copyright © 2018 iCenter. All rights reserved.
//

#import "FinanceTableItem.h"

@implementation FinanceTableItem
@synthesize service, sum, currency, isPaid;

- (instancetype)initWithService:(NSString *)service sum:(double)sum currency:(NSString *)currency isPaid:(BOOL)isPaid
{
    self = [super init];
    if (!self)
    {
        return nil;
    }
    
    self.service = service;
    self.sum = sum;
    self.currency = currency;
    self.isPaid = isPaid;
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super init]) {
        service      = [aDecoder decodeObjectForKey:@"service"];
        sum          = [aDecoder decodeDoubleForKey:@"sum"];
        currency     = [aDecoder decodeObjectForKey:@"currency"];
        isPaid       = [aDecoder decodeBoolForKey:@"isPaid"];
    }
    return self;
}

- (void)encodeWithCoder:(nonnull NSCoder *)aCoder { 
    [aCoder encodeObject:service forKey:@"service"];
    [aCoder encodeDouble:sum forKey:@"sum"];
    [aCoder encodeObject:currency forKey:@"currency"];
    [aCoder encodeBool:isPaid forKey:@"isPaid"];
}

@end
