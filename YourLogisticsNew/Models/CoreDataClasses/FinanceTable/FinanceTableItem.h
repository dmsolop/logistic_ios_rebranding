//
//  FinanceTableItem.h
//  YourLogisticsNew
//
//  Created by VitaliyK on 09.09.2018.
//  Copyright © 2018 iCenter. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FinanceTableItem : NSObject <NSCoding>

@property (nonatomic, strong) NSString *service;
@property (nonatomic, assign) double sum;
@property (nonatomic, strong) NSString *currency;
@property (nonatomic, assign) BOOL isPaid;

- (instancetype)initWithService:(NSString *)service sum:(double)sum currency:(NSString *)currency isPaid:(BOOL)isPaid;

@end
