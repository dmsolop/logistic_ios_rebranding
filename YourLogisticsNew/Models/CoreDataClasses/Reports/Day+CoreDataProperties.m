//
//  Day+CoreDataProperties.m
//  YourLogisticsNew
//
//  Created by Dima on 5/9/19.
//  Copyright © 2019 iCenter. All rights reserved.
//
//

#import "Day+CoreDataProperties.h"

@implementation Day (CoreDataProperties)

+ (NSFetchRequest<Day *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"Day"];
}

@dynamic client_code;
@dynamic date;
@dynamic effectiveness;
@dynamic manager;

@end
