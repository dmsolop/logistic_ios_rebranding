//
//  Order+CoreDataProperties.m
//  YourLogisticsNew
//
//  Created by Dima on 5/9/19.
//  Copyright © 2019 iCenter. All rights reserved.
//
//

#import "Order+CoreDataProperties.h"

@implementation Order (CoreDataProperties)

+ (NSFetchRequest<Order *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"Order"];
}

@dynamic name;
@dynamic price;
@dynamic product;
@dynamic uid;
@dynamic report;

@end
