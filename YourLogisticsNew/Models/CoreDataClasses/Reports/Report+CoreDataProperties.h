//
//  Report+CoreDataProperties.h
//  YourLogisticsNew
//
//  Created by Dima on 5/9/19.
//  Copyright © 2019 iCenter. All rights reserved.
//
//

#import "Report+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Report (CoreDataProperties)

+ (NSFetchRequest<Report *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *isNew;
@property (nullable, nonatomic, copy) NSString *name;
@property (nullable, nonatomic, copy) NSNumber *reportId;
@property (nullable, nonatomic, copy) NSNumber *typeReport;
@property (nullable, nonatomic, retain) NSSet<Manager *> *manager;
@property (nullable, nonatomic, retain) NSSet<Order *> *order;

@end

@interface Report (CoreDataGeneratedAccessors)

- (void)addManagerObject:(Manager *)value;
- (void)removeManagerObject:(Manager *)value;
- (void)addManager:(NSSet<Manager *> *)values;
- (void)removeManager:(NSSet<Manager *> *)values;

- (void)addOrderObject:(Order *)value;
- (void)removeOrderObject:(Order *)value;
- (void)addOrder:(NSSet<Order *> *)values;
- (void)removeOrder:(NSSet<Order *> *)values;

@end

NS_ASSUME_NONNULL_END
