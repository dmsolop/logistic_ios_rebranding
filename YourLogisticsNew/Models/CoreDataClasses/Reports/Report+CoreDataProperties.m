//
//  Report+CoreDataProperties.m
//  YourLogisticsNew
//
//  Created by Dima on 5/9/19.
//  Copyright © 2019 iCenter. All rights reserved.
//
//

#import "Report+CoreDataProperties.h"

@implementation Report (CoreDataProperties)

+ (NSFetchRequest<Report *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"Report"];
}

@dynamic isNew;
@dynamic name;
@dynamic reportId;
@dynamic typeReport;
@dynamic manager;
@dynamic order;

@end
