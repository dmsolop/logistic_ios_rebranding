//
//  Report+CoreDataClass.h
//  YourLogisticsNew
//
//  Created by Dima on 5/9/19.
//  Copyright © 2019 iCenter. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Manager, Order;

NS_ASSUME_NONNULL_BEGIN

@interface Report : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Report+CoreDataProperties.h"
