//
//  Manager+CoreDataProperties.h
//  YourLogisticsNew
//
//  Created by Dima on 5/9/19.
//  Copyright © 2019 iCenter. All rights reserved.
//
//

#import "Manager+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Manager (CoreDataProperties)

+ (NSFetchRequest<Manager *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *a;
@property (nullable, nonatomic, copy) NSString *amountLclFormalize;
@property (nullable, nonatomic, copy) NSString *amountNiFormalize;
@property (nullable, nonatomic, copy) NSString *amountToClear;
@property (nullable, nonatomic, copy) NSString *dayWeekNumber;
@property (nullable, nonatomic, copy) NSString *fcl;
@property (nullable, nonatomic, copy) NSString *fraht;
@property (nullable, nonatomic, copy) NSString *ftl;
@property (nullable, nonatomic, copy) NSString *guarantyAmount;
@property (nullable, nonatomic, copy) NSString *k;
@property (nullable, nonatomic, copy) NSString *lcl;
@property (nullable, nonatomic, copy) NSString *logisticFormalize;
@property (nullable, nonatomic, copy) NSString *ltl;
@property (nullable, nonatomic, copy) NSString *marginLogistic;
@property (nullable, nonatomic, copy) NSString *marginNi;
@property (nullable, nonatomic, copy) NSString *mzLclFormalize;
@property (nullable, nonatomic, copy) NSString *name;
@property (nullable, nonatomic, copy) NSString *ni;
@property (nullable, nonatomic, copy) NSString *pogran;
@property (nullable, nonatomic, copy) NSString *to;
@property (nullable, nonatomic, copy) NSString *toFormalize;
@property (nullable, nonatomic, copy) NSString *ttAmount;
@property (nullable, nonatomic, copy) NSString *uid;
@property (nullable, nonatomic, copy) NSString *weekNumber;
@property (nullable, nonatomic, retain) NSSet<Day *> *day;
@property (nullable, nonatomic, retain) Report *report;

@end

@interface Manager (CoreDataGeneratedAccessors)

- (void)addDayObject:(Day *)value;
- (void)removeDayObject:(Day *)value;
- (void)addDay:(NSSet<Day *> *)values;
- (void)removeDay:(NSSet<Day *> *)values;

@end

NS_ASSUME_NONNULL_END
