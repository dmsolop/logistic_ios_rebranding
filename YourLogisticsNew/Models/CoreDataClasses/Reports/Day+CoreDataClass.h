//
//  Day+CoreDataClass.h
//  YourLogisticsNew
//
//  Created by Dima on 5/9/19.
//  Copyright © 2019 iCenter. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Manager;

NS_ASSUME_NONNULL_BEGIN

@interface Day : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Day+CoreDataProperties.h"
