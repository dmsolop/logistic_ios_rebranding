//
//  Manager+CoreDataProperties.m
//  YourLogisticsNew
//
//  Created by Dima on 5/9/19.
//  Copyright © 2019 iCenter. All rights reserved.
//
//

#import "Manager+CoreDataProperties.h"

@implementation Manager (CoreDataProperties)

+ (NSFetchRequest<Manager *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"Manager"];
}

@dynamic a;
@dynamic amountLclFormalize;
@dynamic amountNiFormalize;
@dynamic amountToClear;
@dynamic dayWeekNumber;
@dynamic fcl;
@dynamic fraht;
@dynamic ftl;
@dynamic guarantyAmount;
@dynamic k;
@dynamic lcl;
@dynamic logisticFormalize;
@dynamic ltl;
@dynamic marginLogistic;
@dynamic marginNi;
@dynamic mzLclFormalize;
@dynamic name;
@dynamic ni;
@dynamic pogran;
@dynamic to;
@dynamic toFormalize;
@dynamic ttAmount;
@dynamic uid;
@dynamic weekNumber;
@dynamic day;
@dynamic report;

@end
