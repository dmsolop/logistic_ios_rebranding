//
//  Day+CoreDataProperties.h
//  YourLogisticsNew
//
//  Created by Dima on 5/9/19.
//  Copyright © 2019 iCenter. All rights reserved.
//
//

#import "Day+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Day (CoreDataProperties)

+ (NSFetchRequest<Day *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *client_code;
@property (nullable, nonatomic, copy) NSString *date;
@property (nullable, nonatomic, copy) NSNumber *effectiveness;
@property (nullable, nonatomic, retain) Manager *manager;

@end

NS_ASSUME_NONNULL_END
