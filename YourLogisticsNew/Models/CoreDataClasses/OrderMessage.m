//
//  OrderMessage.m
//  YourLogistics
//
//  Created by Aleksandr on 24.05.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import "OrderMessage.h"

static NSString * const kSeaTransportCode = @"00219";
static NSString * const kAutoTransportCode = @"00216";
static NSString * const kAviaTransportCode = @"00218";
static NSString * const kContainerCode = @"00217";

static NSString * const kTOCode = @"00027";
static NSString * const kKuskiCode = @"00243";
static NSString * const kCourierCode = @"00229";
static NSString * const kDistribuciaCode = @"00272";
static NSString * const kKonsKonteiner = @"00230";
static NSString * const kKonsAvto = @"00231";


@implementation OrderMessage

+(ServiceType)serviceTypeFromCode:(NSString *)code{
    if ([code isEqualToString:kSeaTransportCode]) {
        return ServiceTypeSea;
    } else if ([code isEqualToString:kAutoTransportCode] || [code isEqualToString:kContainerCode]){
        return ServiceTypeAuto;
    } else if ([code isEqualToString:kAviaTransportCode]){
        return ServiceTypeAvia;
    }else if ([code isEqualToString:kTOCode]){
            return ServiceTypeTO;
    }else if ([code isEqualToString:kKuskiCode]){
        return ServiceTypeKuski;
    }else if ([code isEqualToString:kCourierCode]){
        return ServiceTypeCourier;
    }else if ([code isEqualToString:kDistribuciaCode]){
        return ServiceTypeDistribucia;
    }else if ([code isEqualToString:kKonsKonteiner]){
        return ServiceTypeKonsKonteiner;
    }else if ([code isEqualToString:kKonsAvto]){
        return ServiceTypeKonsAvto;
    }  else {
        return ServiceTypeUndefined;
    }
}


+(NSString *)serviceTypeCodeForType:(ServiceType)type{
    NSString *code;
    switch (type) {
        case ServiceTypeSea:
            code = kSeaTransportCode;
            break;
        
        case ServiceTypeAuto:
            code =  kAutoTransportCode;
            break;
        
        case ServiceTypeAvia:
            code = kAviaTransportCode;
            break;
            
        case ServiceTypeTO:
            code = kTOCode;
            break;
            
        case ServiceTypeKuski:
            code = kKuskiCode;
            break;
            
        case ServiceTypeCourier:
            code = kCourierCode;
            break;
            
        case ServiceTypeDistribucia:
            code = kDistribuciaCode;
            break;
            
        case ServiceTypeKonsKonteiner:
            code = kKonsKonteiner;
            break;
            
        case ServiceTypeKonsAvto:
            code = kKonsAvto;
            break;
        
        default:
            break;
    }
    return code;
}
// Insert code here to add functionality to your managed object subclass

@end
