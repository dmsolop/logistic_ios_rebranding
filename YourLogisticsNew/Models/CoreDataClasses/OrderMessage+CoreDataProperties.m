//
//  OrderMessage+CoreDataProperties.m
//  
//
//  Created by Bohdan on 12.12.2017.
//
//

#import "OrderMessage+CoreDataProperties.h"

@implementation OrderMessage (CoreDataProperties)

+ (NSFetchRequest<OrderMessage *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"OrderMessage"];
}

@dynamic category;
@dynamic isNew;
@dynamic messageContent;
@dynamic messageId;
@dynamic operatorId;
@dynamic orderNumber;
@dynamic serviceId;
@dynamic status;
@dynamic clearanceDate;
@dynamic port;

@end
