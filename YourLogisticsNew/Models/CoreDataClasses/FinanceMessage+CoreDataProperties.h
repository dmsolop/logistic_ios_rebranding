//
//  FinanceMessage+CoreDataProperties.h
//  
//
//  Created by VitaliyK on 09.09.2018.
//
//

#import "FinanceMessage.h"


NS_ASSUME_NONNULL_BEGIN

@interface FinanceMessage (CoreDataProperties)

+ (NSFetchRequest<FinanceMessage *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *clearanceDate;
@property (nullable, nonatomic, copy) NSNumber *currentBalance;
@property (nullable, nonatomic, copy) NSNumber *isNew;
@property (nullable, nonatomic, copy) NSString *messageContent;
@property (nullable, nonatomic, copy) NSNumber *messageId;
@property (nullable, nonatomic, copy) NSNumber *operation;
@property (nullable, nonatomic, copy) NSNumber *sum;
@property (nullable, nonatomic, retain) id rowsOfTable;
@property (nullable, nonatomic, copy) NSString *orderNumber;
@property (nullable, nonatomic, copy) NSString *clientCode;

@end

NS_ASSUME_NONNULL_END
