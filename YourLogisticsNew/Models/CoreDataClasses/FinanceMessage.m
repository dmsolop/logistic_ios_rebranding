//
//  FinanceMessage.m
//  YourLogistics
//
//  Created by Aleksandr on 24.05.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import "FinanceMessage.h"

@implementation FinanceMessage

+(FinanceMessageType)financeTypeFromCode:(NSString *)code{
    if ([code isEqualToString:kDebitCode]) {
        return FinanceMessageTypeDebit;
    } else if ([code isEqualToString:kCreditCode]){
        return FinanceMessageTypeCredit;
    } else {
        return FinanceMessageTypeUndefined;
    }
}

// Insert code here to add functionality to your managed object subclass

@end
