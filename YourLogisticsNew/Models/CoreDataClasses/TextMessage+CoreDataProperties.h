//
//  TextMessage+CoreDataProperties.h
//  YourLogistics
//
//  Created by Aleksandr on 24.05.16.
//  Copyright © 2016 iCenter. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "TextMessage.h"

NS_ASSUME_NONNULL_BEGIN

@interface TextMessage (CoreDataProperties)

+ (NSFetchRequest<TextMessage *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *messageId;
@property (nullable, nonatomic, copy) NSString *content;
@property (nullable, nonatomic, copy) NSNumber *isNew;
@property (nullable, nonatomic, copy) NSNumber *clearanceDate;
@property (nullable, nonatomic, copy) NSString *cargoType;
@property (nullable, nonatomic, copy) NSString *currency;
@property (nullable, nonatomic, copy) NSString *messageDesign;
@property (nullable, nonatomic, copy) NSString *orderNumber;
@property (nullable, nonatomic, copy) NSNumber *price;
@property (nullable, nonatomic, copy) NSNumber *typeDesign;
@property (nullable, nonatomic, copy) NSString *weight;
@property (nullable, nonatomic, copy) NSString *clientCode;

@property (nullable, nonatomic, retain) id rowsOfTable;

@end

NS_ASSUME_NONNULL_END
