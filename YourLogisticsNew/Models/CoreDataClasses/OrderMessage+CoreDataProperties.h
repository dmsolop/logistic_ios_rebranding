//
//  OrderMessage+CoreDataProperties.h
//  
//
//  Created by Bohdan on 12.12.2017.
//
//

#import "OrderMessage.h"


NS_ASSUME_NONNULL_BEGIN

@interface OrderMessage (CoreDataProperties)

+ (NSFetchRequest<OrderMessage *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *category;
@property (nullable, nonatomic, copy) NSNumber *isNew;
@property (nullable, nonatomic, copy) NSString *messageContent;
@property (nullable, nonatomic, copy) NSNumber *messageId;
@property (nullable, nonatomic, copy) NSString *operatorId;
@property (nullable, nonatomic, copy) NSString *orderNumber;
@property (nullable, nonatomic, copy) NSNumber *serviceId;
@property (nullable, nonatomic, copy) NSString *status;
@property (nullable, nonatomic, retain) NSNumber *clearanceDate;
@property (nullable, nonatomic, copy) NSString *port;


@end

NS_ASSUME_NONNULL_END
