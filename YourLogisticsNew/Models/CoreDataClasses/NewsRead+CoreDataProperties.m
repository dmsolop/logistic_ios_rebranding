//
//  NewsRead+CoreDataProperties.m
//  
//
//  Created by VitaliyK on 03.04.2018.
//
//

#import "NewsRead.h"

@implementation NewsRead (CoreDataProperties)

+ (NSFetchRequest<NewsRead *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"NewsRead"];
}

@dynamic isRead;
@dynamic newsId;
@dynamic isNew;

@end
