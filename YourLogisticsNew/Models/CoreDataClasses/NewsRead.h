//
//  NewsRead+CoreDataClass.h
//  
//
//  Created by VitaliyK on 27.03.2018.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface NewsRead : NSManagedObject



@end

NS_ASSUME_NONNULL_END

#import "NewsRead+CoreDataProperties.h"
