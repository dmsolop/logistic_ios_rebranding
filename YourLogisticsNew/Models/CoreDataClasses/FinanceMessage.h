//
//  FinanceMessage.h
//  YourLogistics
//
//  Created by Aleksandr on 24.05.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, FinanceMessageType) {
    FinanceMessageTypeDebit,
    FinanceMessageTypeCredit,
    FinanceMessageTypeUndefined
};

static NSString * const kDebitCode = @"dt";
static NSString * const kCreditCode = @"kt";

@interface FinanceMessage : NSManagedObject

+(FinanceMessageType)financeTypeFromCode:(NSString *)code;

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "FinanceMessage+CoreDataProperties.h"
