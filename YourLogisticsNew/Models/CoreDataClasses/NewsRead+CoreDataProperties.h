//
//  NewsRead+CoreDataProperties.h
//  
//
//  Created by VitaliyK on 03.04.2018.
//
//

#import "NewsRead.h"


NS_ASSUME_NONNULL_BEGIN

@interface NewsRead (CoreDataProperties)

+ (NSFetchRequest<NewsRead *> *)fetchRequest;

@property (nonatomic) BOOL isRead;
@property (nullable, nonatomic, copy) NSNumber *newsId;
@property (nonatomic) BOOL isNew;

@end

NS_ASSUME_NONNULL_END
