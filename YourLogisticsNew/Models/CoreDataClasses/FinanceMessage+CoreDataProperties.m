//
//  FinanceMessage+CoreDataProperties.m
//  
//
//  Created by VitaliyK on 09.09.2018.
//
//

#import "FinanceMessage.h"

@implementation FinanceMessage (CoreDataProperties)

+ (NSFetchRequest<FinanceMessage *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"FinanceMessage"];
}

@dynamic clearanceDate;
@dynamic currentBalance;
@dynamic isNew;
@dynamic messageContent;
@dynamic messageId;
@dynamic operation;
@dynamic sum;
@dynamic rowsOfTable;
@dynamic orderNumber;
@dynamic clientCode;

@end
