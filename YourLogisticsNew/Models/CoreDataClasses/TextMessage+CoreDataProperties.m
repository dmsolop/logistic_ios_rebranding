//
//  TextMessage+CoreDataProperties.m
//  YourLogistics
//
//  Created by Aleksandr on 24.05.16.
//  Copyright © 2016 iCenter. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "TextMessage+CoreDataProperties.h"

@implementation TextMessage (CoreDataProperties)

+ (NSFetchRequest<TextMessage *> *)fetchRequest {
    return [NSFetchRequest fetchRequestWithEntityName:@"TextMessage"];
}

@dynamic messageId;
@dynamic content;
@dynamic isNew;
@dynamic clearanceDate;
@dynamic cargoType;
@dynamic currency;
@dynamic messageDesign;
@dynamic orderNumber;
@dynamic price;
@dynamic typeDesign;
@dynamic weight;
@dynamic rowsOfTable;
@dynamic clientCode;

@end
