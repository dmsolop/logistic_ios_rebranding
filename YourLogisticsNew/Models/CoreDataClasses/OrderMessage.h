//
//  OrderMessage.h
//  YourLogistics
//
//  Created by Aleksandr on 24.05.16.
//  Copyright © 2016 iCenter. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

typedef NS_ENUM(NSInteger, ServiceType) {
    ServiceTypeSea,
    ServiceTypeAuto,
    ServiceTypeAvia,
    ServiceTypeTO,
    ServiceTypeCourier,
    ServiceTypeDistribucia,
    ServiceTypeKuski,
    ServiceTypeUndefined,
    ServiceTypeKonsKonteiner,
    ServiceTypeKonsAvto
};

NS_ASSUME_NONNULL_BEGIN

@interface OrderMessage : NSManagedObject

+(NSString *)serviceTypeCodeForType:(ServiceType)type;

+(ServiceType)serviceTypeFromCode:(NSString *)code;

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "OrderMessage+CoreDataProperties.h"
