//
//  ShareViewController.m
//  YourLogisticsImageShareExtension
//
//  Created by VitaliyK on 01.06.2018.
//  Copyright © 2018 iCenter. All rights reserved.
//

#import "ShareViewController.h"
#import "Transit.h"
#import "APIService.h"
#import "UIImage+vImageScaling.h"


@interface ShareViewController () <UIPickerViewDelegate, UIPickerViewDataSource, UIGestureRecognizerDelegate, UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UIPickerView *inputPicker;
@property (weak, nonatomic) IBOutlet UIPickerView *outputPicker;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;

@property (weak, nonatomic) IBOutlet UISearchBar *inputSearchBar;
@property (weak, nonatomic) IBOutlet UISearchBar *outputSearchBar;

@property (strong,nonatomic) UITapGestureRecognizer* panRecognizer;

@property (strong, nonatomic) NSMutableArray<Transit *> *transitList;
@property (strong, nonatomic) UIImage *image;
@property (strong, nonatomic) Transit *inputSelectedTransit;
@property (strong, nonatomic) Transit *outputSelectedTransit;

@end


@implementation ShareViewController 

- (void)configureImage:(NSData *)imgData {
    dispatch_async(dispatch_get_main_queue(), ^(){
        self.image = [UIImage imageWithData:imgData];
        if (!self.image) {
            [self dismiss];
        }
        
        UIImage *scaledImage = [self.image vImageScaledImageWithSize:CGSizeMake(1920, 1920) contentMode:UIViewContentModeScaleAspectFit];
        self.image = scaledImage;
        self.imageView.image = self.image;
    });
}

- (void)configureTransitList:(NSArray<Transit *> *)transitList {
    self.transitList = transitList.mutableCopy;
    [self.transitList insertObject:[[Transit alloc] init] atIndex:0];
    
    self.inputPicker.dataSource = self;
    self.outputPicker.dataSource = self;
    
    self.inputPicker.delegate = self;
    self.outputPicker.delegate = self;
    
    [self.inputPicker reloadAllComponents];
    [self.outputPicker reloadAllComponents];
    
    self.inputSelectedTransit = self.transitList.firstObject;
    self.outputSelectedTransit = self.transitList.firstObject;
    
    [self validateButton];
}

- (void)prepareChooser:(UIView *)dimingView {
    [[APIService sharedAPIClient] getTransitListWithCompletion:^(NSArray *transitList) {
        [self dismiss:dimingView];
        [self configureTransitList:transitList];
    } failure:^(NSError *error) {
        [self dismiss:dimingView];
        UIAlertController *errorAlert = [UIAlertController alertControllerWithTitle:@"Ошибка!" message:@"Невозможно загрузить список." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * _Nonnull action) {
                                                             [self dismiss];
                                                         }];
        [errorAlert addAction:okAction];
        
        [self presentViewController:errorAlert animated:YES completion:nil];
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSUserDefaults *UD = [[NSUserDefaults alloc] initWithSuiteName:@"group.photoshare.extension"];
    
    if ([UD objectForKey:kClientCodeKey] == nil) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Ошибка авторизации!"
                                                                                 message:@"Войдите в приложение для отправки фото."
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {
                                                                  [self dismiss];
                                                              }];
        [alertController addAction:defaultAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
        
        return;
    }
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(hideKeyboard:)];
    self.panRecognizer = tap;
    self.panRecognizer.delegate = self;
    
    self.inputSearchBar.delegate = self;
    self.outputSearchBar.delegate = self;

    [self.view addGestureRecognizer:tap];
    
    UIView *dimingView = [self addLoadingActivityView];
    
    // TODO: Find replacing for depricated type kUTTypeImage
    
//    for (NSItemProvider *itemProvider in ((NSExtensionItem *)self.extensionContext.inputItems[0]).attachments ) {
//        if ([itemProvider hasItemConformingToTypeIdentifier: (NSString *)kUTTypeImage]) {
//            NSLog(@"itemprovider = %@", itemProvider);
//
//            [itemProvider loadItemForTypeIdentifier:(NSString *)kUTTypeImage options:nil completionHandler: ^(id<NSSecureCoding> item, NSError *error) {
//                NSData *imgData;
//
//                if ([(NSObject *)item isKindOfClass:[NSURL class]]) {
//                    imgData = [NSData dataWithContentsOfURL:(NSURL *)item];
//                }
//
//                if ([(NSObject *)item isKindOfClass:[UIImage class]]) {
//                    imgData = UIImagePNGRepresentation((UIImage *)item);
//                }
//
//                [self configureImage:imgData];
//                [self prepareChooser:dimingView];
//            }];
//        } else {
//            [self dismiss:dimingView];
//            [self dismiss];
//        }
//    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.view.transform = CGAffineTransformMakeTranslation(0, self.view.frame.size.height);
    [UIView animateWithDuration:0.25 animations:^
     {
         self.view.transform = CGAffineTransformIdentity;
     }];
}

- (void)dismiss
{
    [UIView animateWithDuration:0.20 animations:^
     {
         self.view.transform = CGAffineTransformMakeTranslation(0, self.view.frame.size.height);
     }
                     completion:^(BOOL finished)
     {
         [self.extensionContext completeRequestReturningItems:nil completionHandler:nil];
     }];
}

- (void)dismiss:(UIView *)dimingView
{
    [UIView animateWithDuration:0.20 animations:^
     {
         dimingView.alpha = 0.0;
     }
                     completion:^(BOOL finished)
     {
         [dimingView removeFromSuperview];
     }];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

-(void)hideKeyboard:(UIPanGestureRecognizer *)sender{
    [self.inputSearchBar resignFirstResponder];
    [self.outputSearchBar resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)numberOfComponentsInPickerView:(nonnull UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(nonnull UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return self.transitList != nil ? self.transitList.count : 0;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    Transit *transit = self.transitList[row];
    return transit != nil ? transit.name : @"";
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if (pickerView.tag == 1) {
        self.inputSelectedTransit = self.transitList[row];
    } else {
        self.outputSelectedTransit = self.transitList[row];
    }
    [self validateButton];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"SELF.name contains[cd] %@", searchText];
    NSArray<Transit *> *filteredArray = [self.transitList filteredArrayUsingPredicate:bPredicate];
    
    if (filteredArray.count) {
        NSUInteger index = [self.transitList indexOfObject:filteredArray.firstObject];
        if (searchBar.tag == 1) {
            [self.inputPicker selectRow:index inComponent:0 animated:YES];
            [self pickerView:self.inputPicker didSelectRow:index inComponent:0];
        } else if (searchBar.tag == 2) {
            [self.outputPicker selectRow:index inComponent:0 animated:YES];
            [self pickerView:self.outputPicker didSelectRow:index inComponent:0];
        }
    }
}

- (IBAction)cancelButtonAction:(UIButton *)sender {
    [self dismiss];
}

- (IBAction)sendButtonTapped:(UIButton *)sender {
    if (self.inputSelectedTransit.guid == nil || self.outputSelectedTransit.guid == nil)
    {
        UIAlertController *errorAlert = [UIAlertController alertControllerWithTitle:@"Уведомление"
                                                                            message:@"Внесите данные Вход/Выход"
                                                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        }];
        [errorAlert addAction:okAction];
        [self presentViewController:errorAlert animated:YES completion:nil];
        
        return;
    }
    
    UIView *dimingView = [self addLoadingActivityView];
    
    [[APIService sharedAPIClient] postShareImage:self.image
                                   enterTranstit:self.inputSelectedTransit
                                     exitTransit:self.outputSelectedTransit
                                      completion:^(NSString *message) {
                                          [self dismiss:dimingView];
                                          UIAlertController *errorAlert = [UIAlertController alertControllerWithTitle:@"Уведомление"
                                                                                                              message:@"Изображение загружено успешно"
                                                                                                       preferredStyle:UIAlertControllerStyleAlert];
                                          UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
                                              [self dismiss];
                                          }];
                                          [errorAlert addAction:okAction];
                                          [self presentViewController:errorAlert animated:YES completion:nil];
                                      } failure:^(NSError *error) {
                                          [self dismiss:dimingView];
                                          UIAlertController *errorAlert = [UIAlertController alertControllerWithTitle:@"Ошибка" message:@"Не удается загрузить изображение!" preferredStyle:UIAlertControllerStyleAlert];
                                          UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
                                              [self dismiss];
                                          }];
                                          [errorAlert addAction:okAction];
                                          
                                          [self presentViewController:errorAlert animated:YES completion:nil];
                                      }];
}

- (void)validateButton {
    [self.sendButton setEnabled: self.inputSelectedTransit != nil && self.outputSelectedTransit != nil];
}

- (UIView *)addLoadingActivityView {
    
    UIView *dimingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    dimingView.backgroundColor = [UIColor blackColor];
    dimingView.alpha = 0.0;
    [self.view addSubview:dimingView];
    CGRect indicatorFrame = CGRectMake(self.view.center.x - 10.f, self.view.center.y - 10 - 64, 20, 20);
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithFrame:indicatorFrame];
    [indicator startAnimating];

    [dimingView addSubview:indicator];
    
    [UIView animateWithDuration:0.20 animations:^
     {
         dimingView.alpha = 0.5;
     }];
    
    return dimingView;
}

@end
