//
//  ShareViewController.h
//  YourLogisticsImageShareExtension
//
//  Created by VitaliyK on 01.06.2018.
//  Copyright © 2018 iCenter. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>

@interface ShareViewController : SLComposeViewController

@end
